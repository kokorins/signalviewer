QT += core gui widgets
TARGET = locomotorplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/stats

include(../../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../algos

DEPENDPATH += $$PWD/../../algos
LIBS += -L$$OUT_PWD/../.. -lalgos

win32:PRE_TARGETDEPS += $$OUT_PWD/../../algos.lib
else:PRE_TARGETDEPS += $$OUT_PWD/../../libalgos.a


win32 {
  DEFINES += _SCL_SECURE_NO_WARNINGS
}

HEADERS += \
    locomotor/locomotorcalculator.h \
    locomotor/locomotorstatsprovider.h \
    locomotor/locomotorwidget.h

SOURCES += \
    locomotor/locomotorcalculator.cpp \
    locomotor/locomotorstatsprovider.cpp \
    locomotor/locomotorwidget.cpp

FORMS += \
    locomotor/locomotorwidget.ui
