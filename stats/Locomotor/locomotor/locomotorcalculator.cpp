#include "locomotorcalculator.h"
#include <stats/seriesstat.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace stats {
const std::string LocomotorCalculator::kTypeId = "signalviewer.stats.LocomotorCalculator.1";
double LocomotorCalculator::calc(const std::vector<double> &signal, Cache &) const
{
  return SeriesStats::Variance(signal);
}

const std::string & LocomotorCalculator::type() const
{
  return kTypeId;
}

std::string LocomotorCalculator::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put(StatProperties::kType(), kTypeId);
  pt.put("name", _name);
  pt.put("num_sec", (int)_num_sec);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool LocomotorCalculator::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(StatProperties::ReadType(pt)!=kTypeId)
    return false;
  _name = pt.get<std::string>("name");
  _num_sec = (size_t) pt.get<int>("num_sec");
  return true;
}

void LocomotorCalculator::setNumSec(size_t num_sec)
{
  _num_sec = num_sec;
}

size_t LocomotorCalculator::numSec() const
{
  return _num_sec;
}

const std::string &LocomotorCalculator::name() const
{
  return _name;
}

void LocomotorCalculator::setName(const std::string &name)
{
  _name = name;
}

} //namespace stats
