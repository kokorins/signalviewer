#include "locomotorwidget.h"
#include "ui_locomotorwidget.h"

LocomotorWidget::LocomotorWidget(QWidget *parent) : QWidget(parent), ui(new Ui::LocomotorWidget)
{
  ui->setupUi(this);
}

LocomotorWidget::~LocomotorWidget()
{
  delete ui;
}

QString LocomotorWidget::name() const
{
  return ui->name_edit->text();
}

void LocomotorWidget::setName(const QString &name)
{
  ui->name_edit->setText(name);
}

size_t LocomotorWidget::epochSize() const
{
  return (size_t)ui->epoch_spin_locomotor->value();
}

void LocomotorWidget::setEpochSize(size_t epoch_size)
{
  ui->epoch_spin_locomotor->setValue(epoch_size);
}
