#ifndef LOCOMOTORWIDGET_H
#define LOCOMOTORWIDGET_H

#include <cstddef>
#include <QWidget>
namespace Ui {
class LocomotorWidget;
}

class LocomotorWidget : public QWidget {
  Q_OBJECT
public:
  explicit LocomotorWidget(QWidget *parent = 0);
  ~LocomotorWidget();
  QString name()const;
  void setName(const QString& name);
  size_t epochSize()const;
  void setEpochSize(size_t epoch_size);
private:
  Ui::LocomotorWidget *ui;
};

#endif // LOCOMOTORWIDGET_H
