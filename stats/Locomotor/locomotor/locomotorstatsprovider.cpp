#include "locomotorstatsprovider.h"
#include "locomotorcalculator.h"
#include "locomotorwidget.h"
#include <QStackedWidget>

namespace stats {
LocomotorStatsProvider::LocomotorStatsProvider(QObject *parent) : QObject(parent), _lw(0) {}

std::vector<std::string> LocomotorStatsProvider::defaultParameters(size_t) const
{
  std::vector<std::string> res;
  return res;
}

std::string LocomotorStatsProvider::updateParameters(const std::string &new_json, void *parent_)
{
  LocomotorCalculator lc;
  QStackedWidget*parent = (QStackedWidget*)parent_;
  if(!_lw || !parent->isAncestorOf(_lw)) {
    _lw = new LocomotorWidget(parent);
    parent->addWidget(_lw);
  }
  if(!new_json.empty()) {
    if(!lc.fromString(new_json))
      return new_json;
    _lw->setName(QString::fromStdString(lc.name()));
    _lw->setEpochSize(lc.numSec());
  }
  lc.setName(_lw->name().toStdString());
  lc.setNumSec(_lw->epochSize());
  return lc.toString();
}

Calculator LocomotorStatsProvider::createCalculator() const
{
  return Calculator(new LocomotorCalculator());
}

std::string LocomotorStatsProvider::type() const
{
  return LocomotorCalculator::kTypeId;
}

std::wstring LocomotorStatsProvider::name() const
{
  return L"Locomotor";
}
std::wstring LocomotorStatsProvider::author() const
{
  return L"Sergey Kokorin";
}
std::wstring LocomotorStatsProvider::version() const
{
  return L"0.5";
}
} //namespace stats
//Q_EXPORT_PLUGIN2(locomotorplugin, stats::LocomotorStatsProvider)
