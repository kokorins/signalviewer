#ifndef LOCOMOTORSTATSPROVIDER_H
#define LOCOMOTORSTATSPROVIDER_H

#include <interfaces/stats/statisticsprovider_qt.h>
#include <QObject>
class LocomotorWidget;
namespace stats {
class LocomotorStatsProvider : public QObject, public stats::IStatisticsProvider {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "signalviewer.LocomotorStatsProvider")
  Q_INTERFACES(stats::IStatisticsProvider)
public:
  explicit LocomotorStatsProvider(QObject *parent = 0);
  virtual std::wstring name()const;
  virtual std::wstring author()const;
  virtual std::wstring version()const;
  virtual Calculator createCalculator()const;
  virtual std::string type()const;
  virtual std::vector<std::string> defaultParameters(size_t epoch_len)const;
  virtual std::string updateParameters(const std::string& new_json, void* parent);
private:
  LocomotorWidget* _lw;
};
} //namespace stats
#endif // LOCOMOTORSTATSPROVIDER_H
