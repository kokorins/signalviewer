QT += core gui widgets
TARGET = bandplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/stats
include(../../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../algos

DEPENDPATH += $$PWD/../../algos
LIBS += -L$$OUT_PWD/../.. -lalgos

win32:PRE_TARGETDEPS += $$OUT_PWD/../../algos.lib
else:PRE_TARGETDEPS += $$OUT_PWD/../../libalgos.a


win32 {
  DEFINES += _SCL_SECURE_NO_WARNINGS
}

HEADERS += \
    band/bandstatsprovider.h \
    band/bandcalculator.h \
    band/bandwidget.h

SOURCES += \
    band/bandstatsprovider.cpp \
    band/bandcalculator.cpp \
    band/bandwidget.cpp

FORMS += \
    band/bandwidget.ui
