#ifndef BANDSTATSPROVIDER_H
#define BANDSTATSPROVIDER_H
#include <interfaces/stats/statisticsprovider_qt.h>
#include <QObject>

class BandWidget;

namespace stats {
class BandStatsProvider : public QObject, public stats::IStatisticsProvider {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "signalviewer.BandStatsProvider")
  Q_INTERFACES(stats::IStatisticsProvider)
public:
  explicit BandStatsProvider(QObject *parent = 0);
  virtual std::wstring name()const;
  virtual std::wstring author()const;
  virtual std::wstring version()const;
  virtual Calculator createCalculator()const;
  virtual std::string type()const;
  virtual std::vector<std::string> defaultParameters(size_t epoch_len)const;
  virtual std::string updateParameters(const std::string& new_json, void* parent);
private:
  BandWidget* _bw;
};
} //namespace stats
#endif // BANDSTATSPROVIDER_H
