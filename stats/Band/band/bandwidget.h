#ifndef BANDWIDGET_H
#define BANDWIDGET_H

#include <QtWidgets/QWidget>

namespace Ui {
class BandWidget;
}

class BandWidget : public QWidget {
  Q_OBJECT
public:
  explicit BandWidget(QWidget *parent = 0);
  ~BandWidget();
  QString name()const;
  void setName(const QString& name);
  size_t epochSize()const;
  void setEpochSize(size_t epoch_size);
  double lowBand()const;
  void setLowBand(double);
  double highBand()const;
  void setHighBand(double);
private:
  Ui::BandWidget *ui;
};

#endif // BANDWIDGET_H
