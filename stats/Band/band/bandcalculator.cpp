#include "bandcalculator.h"
#include <stats/seriesstat.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace stats {
const std::string BandCalculator::kTypeId = "signalviewer.stats.BandCalculator.1";
double BandCalculator::calc(const std::vector<double> &signal, Cache &cache) const
{
  if(!cache.isFft()) {
    const std::vector<TPoint>& ppt = CUtils::Periodogram(signal, _num_sec);
    cache.putFft(ppt);
  }
  const std::vector<TPoint>& fft = cache.fft();
  return SeriesStats::getFrequenceWeight(fft, _low_band, _high_band);
}

const std::string& BandCalculator::type() const
{
  return kTypeId;
}

std::string BandCalculator::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put(StatProperties::kType(), kTypeId);
  pt.put("name", _name);
  pt.put("num_sec", (int)_num_sec);
  pt.put("low_band", (double)_low_band);
  pt.put("high_band", (double)_high_band);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool BandCalculator::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(StatProperties::ReadType(pt)!=kTypeId)
    return false;
  _name = pt.get<std::string>("name");
  _num_sec = (size_t) pt.get<int>("num_sec");
  _low_band = pt.get<double>("low_band");
  _high_band = pt.get<double>("high_band");
  return true;
}

void BandCalculator::setNumSec(size_t num_sec)
{
  _num_sec = num_sec;
}

size_t BandCalculator::numSec() const
{
  return _num_sec;
}

void BandCalculator::setLowBand(double low_band)
{
  _low_band = low_band;
}

double BandCalculator::lowBand() const
{
  return _low_band;
}

void BandCalculator::setHighBand(double high_band)
{
  _high_band = high_band;
}

double BandCalculator::highBand() const
{
  return _high_band;
}

const std::string &BandCalculator::name() const
{
  return _name;
}

void BandCalculator::setName(const std::string &name)
{
  _name = name;
}

} //namespace stats
