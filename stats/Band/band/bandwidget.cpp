#include "bandwidget.h"
#include "ui_bandwidget.h"

BandWidget::BandWidget(QWidget *parent) : QWidget(parent), ui(new Ui::BandWidget)
{
  ui->setupUi(this);
}

BandWidget::~BandWidget()
{
  delete ui;
}

QString BandWidget::name() const
{
  return ui->name_edit->text();
}

void BandWidget::setName(const QString &name)
{
  ui->name_edit->setText(name);
}

size_t BandWidget::epochSize() const
{
  return ui->epoch_spin_band->value();
}

void BandWidget::setEpochSize(size_t epoch_size)
{
  ui->epoch_spin_band->setValue(epoch_size);
}

double BandWidget::lowBand() const
{
  return ui->low_spin->value();
}

void BandWidget::setLowBand(double val)
{
  ui->low_spin->setValue(val);
}

double BandWidget::highBand() const
{
  return ui->high_spin->value();
}

void BandWidget::setHighBand(double val)
{
  ui->high_spin->setValue(val);
}
