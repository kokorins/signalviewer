#include "bandstatsprovider.h"
#include "bandcalculator.h"
#include "bandwidget.h"
#include <QtWidgets/QStackedWidget>

namespace stats {
BandStatsProvider::BandStatsProvider(QObject *parent) : QObject(parent),_bw(0) {}

std::vector<std::string> BandStatsProvider::defaultParameters(size_t epoch_len) const
{
  std::vector<std::string> res;
  BandCalculator bc;
  bc.setNumSec(epoch_len);
  bc.setLowBand(1);
  bc.setHighBand(5);
  bc.setName("Delta");
  res.push_back(bc.toString());

  bc.setLowBand(5);
  bc.setHighBand(8);
  bc.setName("Theta");
  res.push_back(bc.toString());

  bc.setLowBand(8);
  bc.setHighBand(12);
  bc.setName("Alpha");
  res.push_back(bc.toString());

  bc.setLowBand(12);
  bc.setHighBand(30);
  bc.setName("Beta");
  res.push_back(bc.toString());

  bc.setLowBand(30);
  bc.setHighBand(55);
  bc.setName("Gamma");
  res.push_back(bc.toString());

  bc.setLowBand(65);
  bc.setHighBand(115);
  bc.setName("HGamma");
  res.push_back(bc.toString());

  bc.setLowBand(25);
  bc.setHighBand(55);
  bc.setName("II");
  res.push_back(bc.toString());

  bc.setLowBand(15);
  bc.setHighBand(30);
  bc.setName("HF");
  res.push_back(bc.toString());
  return res;
}

std::string BandStatsProvider::updateParameters(const std::string &new_json, void *parent_)
{
  BandCalculator bc;
  QStackedWidget*parent = (QStackedWidget*)parent_;
  if(!_bw || !parent->isAncestorOf(_bw)) {
    _bw = new BandWidget(parent);
    parent->addWidget(_bw);
  }
  if(!new_json.empty()) {
    if(!bc.fromString(new_json))
      return new_json;
    _bw->setName(QString::fromStdString(bc.name()));
    _bw->setEpochSize(bc.numSec());
    _bw->setLowBand(bc.lowBand());
    _bw->setHighBand(bc.highBand());
  }
  bc.setName(_bw->name().toStdString());
  bc.setNumSec(_bw->epochSize());
  bc.setLowBand(_bw->lowBand());
  bc.setHighBand(_bw->highBand());
  return bc.toString();
}

Calculator BandStatsProvider::createCalculator() const
{
  return Calculator(new BandCalculator());
}

std::string BandStatsProvider::type() const
{
  return BandCalculator::kTypeId;
}

std::wstring BandStatsProvider::name() const
{
  return L"Band";
}
std::wstring BandStatsProvider::author() const
{
  return L"Sergey Kokorin";
}
std::wstring BandStatsProvider::version() const
{
  return L"0.5";
}
} //namespace stats

//Q_EXPORT_PLUGIN2(bandplugin, stats::BandStatsProvider)
