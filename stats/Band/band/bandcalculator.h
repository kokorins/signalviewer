#ifndef BANDCALCULATOR_H
#define BANDCALCULATOR_H
#include <stats/statcalculator.h>

namespace stats {
class BandCalculator : public ITypedStatCalculator {
public:
  const static std::string kTypeId;
public:
  virtual double calc(const std::vector<double>& signal, Cache& cache)const;
  virtual const std::string &type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
  virtual const std::string& name()const;
  virtual void setName(const std::string& name);
public:
  void setNumSec(size_t num_sec);
  size_t numSec()const;
  void setLowBand(double low_band);
  double lowBand()const;
  void setHighBand(double high_band);
  double highBand()const;
private:
  size_t _num_sec;
  double _low_band;
  double _high_band;
  std::string _name;
};
} //namespace stats
#endif // BANDCALCULATOR_H
