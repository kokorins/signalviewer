TEMPLATE = subdirs
SUBDIRS += \
    algos \
    epoch \
    fileformats \
    stats \
    signalviewer \
    interfaces \
    loglite
