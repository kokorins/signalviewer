QT -= qt core gui
TARGET = algos
TEMPLATE = lib
CONFIG += staticlib
DESTDIR = $$OUT_PWD/../
include(../signalviewer.pri)
LIBS += -lboost_thread -lboost_system -lboost_filesystem -lboost_date_time

INCLUDEPATH += \
    $$PWD/../ \
    $$PWD/../algos/ \
    $$PWD/../algos/treap

win32: DEFINES += _USE_MATH_DEFINES _CRT_SECURE_NO_WARNINGS _SCL_SECURE_NO_WARNINGS

!win32: LIBS += -lpthread

HEADERS += \
    filter/typedfilter.h \
    filter/robustfilter.h \
    filter/filter_helpers.h \
    filter/filter.h \
    filter/butterfilter.h \
    filter/adaptivefilter.h \
    stats/statproperties.h \
    stats/statcollector.h \
    stats/statcalculator.h \
    stats/seriesstat.h \
    strconvert/strconvert.h \
    transformation/utils.h \
    transformation/typedtrans.h \
    transformation/transformproperties.hpp \
    transformation/transformproperties.h \
    transformation/transform.h \
    transformation/logtrans.h \
    transformation/filtertrans.h \
    transformation/decimatetrans.h \
    transformation/chaintrans.hpp \
    transformation/chaintrans.h \
    fft/realfft.h \
    csvfile/csvrow.h \
    csvfile/csvheader.h \
    csvfile/csvchunk.hpp \
    csvfile/csvchunk.h \
    treap/treap/treap.hpp \
    treap/treap/treap.h \
    treap/event/event.hpp \
    treap/event/event.h

SOURCES += \
    filter/typedfilter.cpp \
    filter/robustfilter.cpp \
    filter/filter_helpers.cpp \
    filter/filter.cpp \
    filter/butterfilter.cpp \
    filter/adaptivefilter.cpp \
    stats/statproperties.cpp \
    stats/statcollector.cpp \
    stats/statcalculator.cpp \
    stats/seriesstat.cpp \
    strconvert/strconvert.cpp \
    transformation/utils.cpp \
    transformation/typedtrans.cpp \
    transformation/transformproperties.cpp \
    transformation/logtrans.cpp \
    transformation/filtertrans.cpp \
    transformation/decimatetrans.cpp \
    fft/realfft.cpp \
    csvfile/csvrow.cpp \
    csvfile/csvheader.cpp \
    csvfile/csvchunk.cpp
