#include "filter_helpers.h"
#include <filter/butterfilter.h>
#include <filter/robustfilter.h>
#include <filter/typedfilter.h>
#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace filter {
const std::string FilterProperties::kType = "type_id";
void FilterProperties::Name(size_t idx, std::string &name)
{
  switch(idx) {
  break; case StepRobust:
    name = StepRobustFilterAlgo::kTypeId;
  break; case QuanRobust:
    name = QuantileWinsorizedFilterAlgo::kTypeId;
  break; case Butter:
    name = ButterFilter::kTypeId;
  break; default:
    name = "";
  }
}

FilterProperties::EFilterType FilterProperties::Idx(std::string &name)
{
  std::string str;
  for(size_t i=0; i<EFilterTypeNum; ++i) {
    Name(i, str);
    if(name==str)
      return (EFilterType)i;
  }
  return EFilterTypeNum;
}

boost::shared_ptr<ITypedFilter> FilterProperties::Create(const std::string &str)
{
  switch(ReadType(str)) {
  break; case StepRobust: {
    StepRobustFilterAlgo* srfa = new StepRobustFilterAlgo();
    if(srfa->fromString(str))
      return boost::shared_ptr<ITypedFilter>(srfa);
  }
  break; case QuanRobust: {
    QuantileWinsorizedFilterAlgo* qwfa = new QuantileWinsorizedFilterAlgo();
    if(qwfa->fromString(str))
      return boost::shared_ptr<ITypedFilter>(qwfa);
  }
  break; case Butter: {
    ButterFilter* bf = new ButterFilter();
    if(bf->fromString(str))
      return boost::shared_ptr<ITypedFilter>(bf);
  }
  break; default:
    return boost::shared_ptr<ITypedFilter>();
  }
  return boost::shared_ptr<ITypedFilter>();
}

FilterProperties::EFilterType FilterProperties::ReadType(const std::string &str)
{
  using boost::property_tree::ptree;
  std::stringstream ss(str);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find(kType);
  if(it==pt.not_found())
    return EFilterTypeNum;
  std::string type_str = pt.get<std::string>(kType);
  return Idx(type_str);
}

FilterProperties::EFilterType FilterProperties::ReadType(const boost::property_tree::ptree &pt)
{
  using boost::property_tree::ptree;
  ptree::const_assoc_iterator it = pt.find(kType);
  if(it==pt.not_found())
    return EFilterTypeNum;
  std::string type_str = pt.get<std::string>(kType);
  return Idx(type_str);
}
}//namespace filter
