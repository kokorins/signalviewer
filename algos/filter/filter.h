#ifndef FILTER_H_
#define FILTER_H_
#include <vector>
#include <algorithm>
/**
 * \file filter.h
 * \mainpage Filter library
 * Library implements a various number of filtering algorithms
 *    - Robusts:
 *      - Step robust
 *      - Quantile robust
 *    - Adaptive frequency filter
 *    - Butterworth filter
 */
namespace filter {
///Abstract signal filter
class IFilter {
public:
  /**
   * Main interface for filtering functions, note inplace data change
   * \param signal input is a signal to be filtered, result is filtered signal itself
   */
  virtual void filter(std::vector<double>& signal)const=0;
  virtual ~IFilter() {}
};

} //namespace filter
#endif //FILTER_H_
