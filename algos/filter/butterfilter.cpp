#include "butterfilter.h"
#include <cmath>
#include <algorithm>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace filter {
const std::string ButterFilter::kTypeId = "signalviewer.filter.ButterFilter";
struct ButterFilter::Inner {
  //inner parameters
  std::vector<double> a;
  std::vector<double> b;
  double _B;
  double _k;
  double _w;
  ///It uses for gluing between epoches. _memory=0 if it start and _memory>4 for other
  int _memory;
  //Initial signal. only 5 last points. 0 is the nearest to current time index.
  ///x_n x_{n-1} x_{n-2} x_{n-3} x_{n-4}
  std::vector<double> _init;
  ///Filtered signal only 5 last points
  std::vector<double> _filtered;
public:
  void init();
  //a0, a1, a2, a3, a4, a2', b0=1, b1, b2, b3, b4, b2'
  void calcAB(const ButterParams& params);
  double filtered() const;
public:
  static double W(const ButterParams& params);
};

void ButterFilter::Inner::init()
{
  _memory = 0;
  _init = std::vector<double>(5);
  _filtered = std::vector<double>(5);
}

void ButterFilter::Inner::calcAB(const ButterParams &params)
{
  _w = W(params);
  _B = params.f2 - params.f1;
  a = std::vector<double>(6);
  b = std::vector<double>(6);
  if(_B == floor(_B)) {
    a[0] = a[4] = 0;
    a[1] = a[3] = 0;
    a[2] = 0;
    a[5] = 0;

    b[0] = 1;
    b[1] = -4;
    b[2] = 4;
    b[3] = -4;
    b[4] = 1;
    b[5] = 2;
  }
  else {
    _k = 1./tan(M_PI*_B);

    a[0] = a[4] = 1/(_k*_k+sqrt(2.)*_k+1);
    a[1] = a[3] = -4 * a[0];
    a[2] = -2 * a[0];
    a[5] = 4 * a[0];

    b[0] = 1;
    b[1] = -2*_k*(2*_k+sqrt(2.))*a[0];
    b[2] = 4*_k*_k*a[0];
    b[3] = 2*_k*(-2*_k+sqrt(2.))*a[0];
    b[4] = (_k*_k-sqrt(2.)*_k+1)*a[0];
    b[5] = 2*(_k*_k-1)*a[0];
  }
}


double ButterFilter::Inner::filtered()const
{
	double rez = 0;
	if(_memory>=0)
		rez += a[0]*_init[0];
	if(_memory>=1)		
		rez += (-b[1]*_w*_filtered[1]);
	if(_memory>=2)
		rez += (a[2]*_init[2] - (b[2]*_w*_w+b[5])*_filtered[2]);
	if(_memory>=3)
		rez += (-b[3]*_w*_filtered[3]);
	if(_memory>=4)
		rez += (a[4]*_init[4]-b[4]*_filtered[4]);
	return rez;
}

double ButterFilter::Inner::W(const ButterParams &params)
{
  double sum = params.f2 + params.f1;
  double sub = params.f2 - params.f1;
  if(sub+0.5==floor(sub+0.5))	{
    if(sum+0.5==floor(sum+0.5))
      return 1;
    return 1e10;
  }
  return cos(M_PI*(sum))/cos(M_PI*(sub));
}

ButterFilter::ButterFilter() : _inner(new ButterFilter::Inner) {}

void ButterFilter::filter(std::vector<double>& signal) const
{
  std::vector<double> average_freq(signal.size());
  for (size_t i=0; i<signal.size(); ++i) {
    for (size_t j = _inner->_init.size()-1; j>0; --j)
      _inner->_init[j] = _inner->_init[j-1];
    for (size_t j = _inner->_filtered.size()-1; j>0; --j)
      _inner->_filtered[j] = _inner->_filtered[j-1];
    _inner->_init[0] = signal[i];
    _inner->_filtered[0] = _inner->filtered();
    average_freq[i] = _inner->_filtered[0];
    ++_inner->_memory;
	}
  std::swap(signal, average_freq);
}

const std::string& ButterFilter::type() const
{
  return kTypeId;
}

void ButterFilter::toString(std::string &json) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("f1", _params.f1);
  pt.put("f2", _params.f2);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

bool ButterFilter::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str==kTypeId) {
    _params.f1 = pt.get<double>("f1");
    _params.f2 = pt.get<double>("f2");
    init(_params);
    return true;
  }
  return false;
}

const ButterParams &ButterFilter::params() const
{
  return _params;
}

void ButterFilter::init( const ButterParams &params)
{
  _params = params;
  _inner->init();
  _inner->calcAB(_params);
}

void ButterFilter::chankFilter(std::vector<double> &signal_chunk)
{
	//Use history of previous series
  if(_inner->_memory > 5)
    _inner->_memory = 5;
  filter(signal_chunk);
}

void ButterFilter::filter(std::vector<double>& signal, const ButterParams& params)
{
  init(params);
  filter(signal);
}
}//namespace filter

