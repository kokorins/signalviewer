#ifndef BUTTERFILTER_H_
#define BUTTERFILTER_H_
#include "filter.h"
#include "typedfilter.h"
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
/// \file butterfilter.h

namespace filter {

struct ButterParams {
  double f1,f2;
};

/**
 * \brief Butterworth pass filter of second order
 */
class ButterFilter : public ITypedFilter {
public:
  const static std::string kTypeId;
public:
  ButterFilter();
  void init(const ButterParams& params);
	//calc resulted values of type "type" for signal
  void filter(std::vector<double> &signal, const ButterParams &params);
	//initialization for calcChunkFilter
  void chankFilter(std::vector<double>& signal_chunk);
public:
  ///basic calculation algorithm
  void filter(std::vector<double>& signal)const;
  virtual const std::string& type()const;
  virtual void toString(std::string& json)const;
  virtual bool fromString(const std::string& json);
public:
  const ButterParams& params()const;
private:
  ///Initial parameters
  ButterParams _params;

  struct Inner;
  boost::shared_ptr<Inner> _inner;
};
} // namespace butterfilter
#endif//BUTTERFILTER_H_
