#ifndef TYPEDFILTER_H
#define TYPEDFILTER_H
#include <string>
#include "filter.h"
/// \file typedfilter.h
namespace filter {
/**
 * \brief Interface for filter parameters save and load
 * Each filter implementing this interface allows to save
 * and load data as a json string by means of
 * the tree properties library from boost.
 */
class ITypedFilter : public IFilter {
public:
  /**
   * type id of a class, this property should be implemented
   * in each class and should identify each class.
   * \return signalviewer.filter.[classname]
   */
  virtual const std::string& type()const = 0;
  /**
   * Translates class to string
   */
  virtual void toString(std::string& json)const=0;
  /**
   * Set parameters from string if correct format
   * \return false if any troubles or mistakes
   */
  virtual bool fromString(const std::string& json)=0;
  virtual ~ITypedFilter() {}
};
} //namespace filter
#endif // TYPEDFILTER_H
