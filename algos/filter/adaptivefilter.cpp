#include "adaptivefilter.h"
#include <cmath>
#include <algorithm>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace filter {
const std::string AdaptiveFilter::kTypeId = "signalviewer.filter.AdaptiveFilter";
struct AdaptiveFilter::Inner {
  //It uses for gluing between epochs. _memory=0 if it start and _memory>4 for other
  int _memory;
  //Initial signal. only 5 last points. 0 is the nearest to current time index.
  //x_n x_{n-1} x_{n-2} x_{n-3} x_{n-4}
  std::vector<double> _init;
  //Filtered signal only 5 last points
  std::vector<double> _filtered;
  //5 last point of estimations of derivative
  std::vector<double> _derivative;
  //inner used parameters
  std::vector<double> _r;
  std::vector<double> _w;
  double _B;
  double _k;
  //inner parameters
  std::vector<double> a;
  std::vector<double> b;
public:
  //initialization for calcChunkFilter
  void init(const AdaptiveParams& params);
  //a0, a1, a2, a3, a4, a2', b0=1, b1, b2, b3, b4, b2'
  void calcAB(const AdaptiveParams& params);
  double filtered()const;
  double derivative()const;
  double r(const AdaptiveParams& params)const;
  double w(const AdaptiveParams& params) const;
};

void AdaptiveFilter::Inner::init(const AdaptiveParams &params)
{
  _memory = 0;
  calcAB(params);
  _init = std::vector<double>(5);
  _w = std::vector<double>(2);
  _r = std::vector<double>(2);
  _derivative = std::vector<double>(5);
  _filtered = std::vector<double>(5);
}

double AdaptiveFilter::Inner::filtered() const
{
	double rez = 0;
	if(_memory>=0)
		rez += a[0]*_init[0];
	if(_memory>=1)		
		rez += (-b[1]*_w[0]*_filtered[1]);
	if(_memory>=2)
		rez += (a[2]*_init[2] - (b[2]*_w[0]*_w[0]+b[5])*_filtered[2]);
	if(_memory>=3)
		rez += (-b[3]*_w[0]*_filtered[3]);
	if(_memory>=4)
		rez += (a[4]*_init[4]-b[4]*_filtered[4]);
	return rez;
}

double AdaptiveFilter::Inner::derivative() const
{
	double rez = 0;
	if(_memory>=1)
		rez += (-(b[1]*_filtered[1]+b[1]*_w[0]*_derivative[1]));
	if(_memory>=2)
		rez += (-(2*b[2]*_w[0]*_filtered[2]+(b[5]+b[2]*_w[0]*_w[0])*_derivative[2]));
	if(_memory>=3)
		rez += (-(b[3]*_filtered[3]+b[3]*_w[0]*_derivative[3]));
	if(_memory>=4)
		rez += (-(b[4]*_derivative[4]));
	return rez;
}

double AdaptiveFilter::Inner::r(const AdaptiveParams& params)const
{
	if(!_memory)
    return params._initR;
  return _r[1]*params._nu+_derivative[0]*_derivative[0];
}

double AdaptiveFilter::Inner::w(const AdaptiveParams& params) const
{
  if(!_memory) {
    if(params._initF2-params._initF1+0.5==floor(params._initF2-params._initF1+0.5)) {
      if(params._initF2+params._initF1+0.5==floor(params._initF2+params._initF1+0.5))
				return 1;
			return 1e10;
		}
    return cos(M_PI*(params._initF2+params._initF1))/cos(M_PI*(params._initF2-params._initF1));
	}
  double rez = _w[1]+params._mu*_derivative[1]*_filtered[1]/_r[1];
  return (rez>0.99?0.99:(rez<-0.99?-0.99:rez));
}

void AdaptiveFilter::Inner::calcAB(const AdaptiveParams& params)
{
  _B = params._initF2 - params._initF1;
	a = std::vector<double>(6);
	b = std::vector<double>(6);
	if(_B == floor(_B))	{
		a[0] = a[4] = 0;
		a[1] = a[3] = 0;
		a[2] = 0;
		a[5] = 0;

		b[0] = 1;
		b[1] = -4;
		b[2] = 4;
		b[3] = -4;
		b[4] = 1;
		b[5] = 2;
	}
  else {
		_k = 1./tan(M_PI*_B);
		
		a[0] = a[4] = 1/(_k*_k+sqrt(2.)*_k+1);
		a[1] = a[3] = -4 * a[0];
		a[2] = -2 * a[0];
		a[5] = 4 * a[0];
		
		b[0] = 1;
		b[1] = -2*_k*(2*_k+sqrt(2.))*a[0];
		b[2] = 4*_k*_k*a[0];
		b[3] = 2*_k*(-2*_k+sqrt(2.))*a[0];
		b[4] = (_k*_k-sqrt(2.)*_k+1)*a[0];
		b[5] = 2*(_k*_k-1)*a[0];
	}
}

double AdaptiveFilter::AverageFrequence(double B, double W)
{
  return acos(W*cos(M_PI*B))/M_PI/2;
}

void AdaptiveFilter::calcChunkFilter(std::vector<double>& signal)
{
	//Use history of previous series
  if(_impl->_memory > 5)
    _impl->_memory = 5;
  filter(signal);
}

void AdaptiveFilter::filter(std::vector<double>& signal) const
{
  std::vector<double> average_freq(signal.size());
  for (size_t i=0; i<signal.size(); ++i) {
    for (size_t j = _impl->_init.size()-1; j>0; --j)
      _impl->_init[j] = _impl->_init[j-1];
    for(size_t j=_impl->_w.size()-1;j>0;--j)
      _impl->_w[j] = _impl->_w[j-1];
    for(size_t j = _impl->_derivative.size()-1; j>0; --j)
      _impl->_derivative[j] = _impl->_derivative[j-1];
    for (size_t j=_impl->_r.size()-1; j>0; --j)
      _impl->_r[j]=_impl->_r[j-1];
    for (size_t j =_impl->_filtered.size()-1; j>0; --j)
      _impl->_filtered[j] = _impl->_filtered[j-1];
    _impl->_init[0] = signal[i];
    _impl->_w[0] = _impl->w(_params);
    _impl->_derivative[0] = _impl->derivative();
    _impl->_r[0] = _impl->r(_params);
    _impl->_filtered[0] = _impl->filtered();
    switch (_params._data_type)	{
    break; case AdaptiveParams::AverageFreq:
			//Use Hz scale
      average_freq[i] = AverageFrequence(_impl->_B, _impl->_w[0]);
    break; case AdaptiveParams::AverageFreqInit:
      average_freq[i] = _impl->_w[0];
    break; case AdaptiveParams::FilteredSignal:
      average_freq[i] = _impl->_filtered[0];
    break; default:
      average_freq[i] = 0;
    }
    ++_impl->_memory;
	}
  std::swap(average_freq, signal);
}

AdaptiveFilter::AdaptiveFilter(): _impl(new Inner()) {}

const std::string &AdaptiveFilter::type() const
{
  return kTypeId;
}

void AdaptiveFilter::toString(std::string &json) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("f1", _params._initF1);
  pt.put("f2", _params._initF2);
  pt.put("mu", _params._mu);
  pt.put("nu", _params._nu);
  pt.put("r", _params._initR);
  pt.put("data_type", (int)_params._data_type);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

bool AdaptiveFilter::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str==kTypeId) {
    _params._initF1 = pt.get<double>("f1");
    _params._initF2 = pt.get<double>("f2");
    _params._mu = pt.get<double>("mu");
    _params._nu = pt.get<double>("nu");
    _params._initR = pt.get<double>("r");
    _params._data_type = (AdaptiveParams::EDataType)pt.get<int>("data_type");
    _impl->init(_params);
    return true;
  }
  return false;
}
} //namespace filter
