#include "robustfilter.h"
#include <numeric>
#include <algorithm>
#include <cstddef>
#include <cmath>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/control_structures.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace filter {
const std::string StepRobustFilterAlgo::kTypeId="signalviewer.filter.StepRobustFilterAlgo";
const std::string QuantileWinsorizedFilterAlgo::kTypeId="signalviewer.filter.QuantileWinsorizedFilterAlgo";
void StepRobustFilterAlgo::filter(std::vector<double>& signal) const
{
  namespace bl = boost::lambda;
  std::vector<double> sample;
  sample.reserve(window_);
  size_t sz = signal.size();
  for(size_t  i=0; i<sz; i+=window_) {
    sample.clear();
    Doubles::iterator begin = signal.begin()+i;
    size_t add_end= sz;
    if(add_end>i+window_)
      add_end = i+window_;
    Doubles::iterator end = signal.begin()+add_end;
    sample.insert(sample.begin(), begin, end);
    if(sample.size()<2)
      break;
    std::sort(sample.begin(), sample.end());
    double step =0;
    for(size_t i=1; i<sample.size(); ++i)
      step += sample[i]-sample[i-1];
    step /= sample.size()-1;
    for(size_t j=0; j<sample.size()-1; ++j) {
      if(sample[j+1]-sample[j]<step_factor_*step) {
        if(j>0)
          std::for_each(begin, end, if_then(bl::_1 < bl::var(sample[j]),
                                            bl::_1 = bl::var(sample[j])));
        break;
      }
    }
    for(size_t j=sample.size()-1; j>0; --j) {
      if(sample[j]-sample[j-1]<step_factor_*step) {
        if(j!=sample.size()-1)
          std::for_each(begin, end, if_then(bl::_1 > bl::var(sample[j]),
                                            bl::_1 = bl::var(sample[j])));
        break;
      }
    }
  }
}

const std::string& StepRobustFilterAlgo::type() const
{
  return kTypeId;
}

void StepRobustFilterAlgo::toString(std::string &json) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("window", window_);
  pt.put("step", step_factor_);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

bool StepRobustFilterAlgo::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str==kTypeId) {
    window_ = pt.get<int>("window");
    step_factor_ = pt.get<double>("step");
    return true;
  }
  return false;
}

int StepRobustFilterAlgo::window() const
{
  return window_;
}

double StepRobustFilterAlgo::stepFactor() const
{
  return step_factor_;
}

void QuantileWinsorizedFilterAlgo::filter(std::vector<double>& signal) const
{
  std::vector<double> sample(window_);
  for(size_t i = 0; i< signal.size(); i += window_) {
    Doubles::iterator begin = signal.begin()+i;
    size_t add_end=signal.size();
    if(add_end>i+window_)
      add_end = i+window_;
    size_t size = add_end-i;
    if(size!=sample.size()) {
      if(size<2)
        break;
      sample.resize(size);
    }
    Doubles::iterator end = signal.begin()+ i + size;
    std::copy(begin, end, sample.begin());
    size_t min_i = (size_t)((1-quantile_)*(sample.size()-1)+0.5);
    size_t max_i = (size_t)(quantile_*(sample.size()-1)+0.5);
    if(min_i>max_i)
      std::swap(min_i, max_i);
    Doubles::iterator max_idx = sample.begin() + max_i;
    std::nth_element(sample.begin(), max_idx, sample.end());
    double max_val = *(max_idx);
    Doubles::iterator min_idx = sample.begin() + min_i;
    double min_val = *(min_idx);
    double dist = coef_*(max_val-min_val);
    min_val -= dist;
    max_val += dist;
    for (Doubles::iterator it = begin; it!=end; ++it) {
      if(*it < min_val)
        *it = min_val;
      if(*it > max_val)
        *it = max_val;
    }
  }
}

const std::string &QuantileWinsorizedFilterAlgo::type() const
{
  return kTypeId;
}

void QuantileWinsorizedFilterAlgo::toString(std::string &json) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("window", window());
  pt.put("quantile", quantile());
  pt.put("coef", coef());
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

int QuantileWinsorizedFilterAlgo::window() const
{
  return window_;
}

double QuantileWinsorizedFilterAlgo::quantile() const
{
  return quantile_;
}

double QuantileWinsorizedFilterAlgo::coef() const
{
  return coef_;
}

bool QuantileWinsorizedFilterAlgo::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str==kTypeId) {
    window_ = pt.get<int>("window");
    quantile_ = pt.get<double>("quantile");
    coef_ = pt.get<double>("coef");
    return true;
  }
  return false;
}
} //namespace fitler
