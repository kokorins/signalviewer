#ifndef ROBUSTFILTER_H_
#define ROBUSTFILTER_H_
#include "typedfilter.h"
#include <boost/shared_ptr.hpp>
#include <vector>
/// \file robustfilter.h
namespace filter {
typedef std::vector<double> Doubles;
/**
 * Class implements the algorithm of robust filtering,
 * the sample divided into parts according to window parameter
 * then there is a sort procedure inside each part and
 * calculate the mean distance between the values,
 * search for the first which has small distance from bottom and
 * first with small distance at top, trancate all values which is higher and
 * lower this two.
 */
class StepRobustFilterAlgo : public ITypedFilter {
public:
  const static std::string kTypeId;
public:
  StepRobustFilterAlgo() : window_(100), step_factor_(0.1) {}
  /**
   * Set the parameters
   * \param window length of the window, which cuts the signal into pieces
   * \param step_factor, the average step betwin sorted values in cut pieces should
   * be smaller than step_factor * mean_step
   */
  StepRobustFilterAlgo(int window, double step_factor) :
                       window_(window), step_factor_(step_factor) {}
  virtual void filter(std::vector<double>& signal)const;
  virtual const std::string& type()const;
  virtual void toString(std::string& json)const;
  virtual bool fromString(const std::string& json);
  virtual ~StepRobustFilterAlgo() {}
  int window()const;
  double stepFactor()const;
private:
  int window_;
  double step_factor_;
};

/**
 * Signal is separate in pieces of window length.
 * After that the low and high quantiles of value (quantile) are calculated,
 * and winsorized the values out the quantiles.
 */
class QuantileWinsorizedFilterAlgo : public ITypedFilter {
public:
  const static std::string kTypeId;
public:
  QuantileWinsorizedFilterAlgo() : window_(100), quantile_(0.1), coef_(0.5) {}
  QuantileWinsorizedFilterAlgo(int window, double quantile, double coef=0.5) :
                               window_(window), quantile_(quantile), coef_(coef) {}
  virtual void filter(std::vector<double>& signal)const;
  virtual const std::string& type()const;
  virtual void toString(std::string& json)const;
  virtual bool fromString(const std::string& json);
  int window()const;
  double quantile()const;
  double coef()const;
private:
  int window_;
  double quantile_;
  double coef_;
};
} // namespace filter

#endif /* ROBUSTFILTER_H_ */
