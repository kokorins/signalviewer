#ifndef ADAPTIVEFILTER_H_
#define ADAPTIVEFILTER_H_
#include "typedfilter.h"
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>

namespace filter {
struct AdaptiveParams {
  /**
   * Type of output data
   * See the :
   * �On-line eeg classification and sleep spindles
   * detection using an adaptive recursive bandpass filter�
   * (R.R. Gharieb and A.Cichocki)
   */
  enum EDataType {
    AverageFreq,
    AverageFreqInit,//W parameter
    FilteredSignal,
    EDataTypeNum
  };

  //Initial parameters, see the article
  double _initF1;
  double _initF2;
  double _mu;
  double _nu;
  double _initR;
  EDataType _data_type;
};

//Algorithm for calculation of average frequency
class AdaptiveFilter : public ITypedFilter {
  const static std::string kTypeId;
public: // interface
  AdaptiveFilter();
  virtual void filter(std::vector<double>& signal)const;
  virtual const std::string& type()const;
  virtual void toString(std::string& json)const;
  virtual bool fromString(const std::string& json);
private:
  static double AverageFrequence(double B, double W);
public:
  void calcChunkFilter(std::vector<double> &signal);
private:
  AdaptiveParams _params;
  struct Inner;
  boost::shared_ptr<Inner> _impl;
};
} //namespace filter
#endif //ADAPTIVEFILTER_H_
