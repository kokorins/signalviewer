#ifndef FILTER_HELPERS_H_
#define FILTER_HELPERS_H_
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree.hpp>
#include <filter/filter.h>

namespace filter {
class ButterFilter;
class StepRobustFilterAlgo;
class QuantileWinsorizedFilterAlgo;
class ITypedFilter;

struct FilterProperties {
  enum EFilterType {
    StepRobust,
    QuanRobust,
    Butter,
    EFilterTypeNum
  };
 const static std::string kType;
 static void Name(size_t idx, std::string& name);
 static EFilterType Idx(std::string& name);
public:
  static boost::shared_ptr<ITypedFilter> Create(const std::string& str);
public:
  static EFilterType ReadType(const std::string& str);
  static EFilterType ReadType(const boost::property_tree::ptree& pt);
};
} // namespace filter
#endif // FILTER_HELPERS_H_
