#include "csvrow.h"
#include <iterator>
#include <istream>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
namespace csvfile {
size_t CsvDoubleRow::read(std::string& in_str, const std::string& splitter, size_t hint_size)
{  
  if (in_str.empty())
    return 0;

  _stats.clear();
  if (hint_size > 0)
    _stats.reserve(hint_size);
  std::vector<std::string> vals;
  boost::algorithm::split(vals, in_str, boost::algorithm::is_any_of(splitter));
  for(size_t i=0; i<vals.size(); ++i) {
    if(vals[i].empty())
      continue;
    try {
      _stats.push_back(boost::lexical_cast<double>(vals[i]));
    }
    catch(boost::bad_lexical_cast& ex) {
      std::cout<<vals[i]<<" "<<ex.what();
    }
  }
  return _stats.size();
}

void CsvDoubleRow::write(std::ostream & out, const std::string& splitter) const
{
  std::vector<std::string> vals(_stats.size());
  for(size_t i=0; i<_stats.size(); ++i) {
    try {
      vals[i] = boost::lexical_cast<std::string>(_stats[i]);
    }
    catch(boost::bad_lexical_cast& ex) {
      std::cout<<_stats[i]<<" "<<ex.what();
    }
  }
  out<<boost::algorithm::join(vals, splitter)<<std::endl;
}

std::ostream & operator <<(std::ostream & out, const CsvDoubleRow & rhs)
{
  rhs.write(out, ", ");
  return out;
}

} //namespace csvfile
