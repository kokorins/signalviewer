#ifndef CSVCHUNK_H_
#define CSVCHUNK_H_
#include <vector>
#include <string>
#include <fstream>
#include <functional>
#include <map>
#include <boost/thread.hpp>

/**
 * \file csvchunk.h "csvfile/csvchunk.h"
 * The enhansed CSV file format reader. File supposed to have a special structure: Header, Row, Row,...
 * The THeader and TRow types are defined as a parameters, which should satisfy the properties defined below:
 * The THeader should have the
 * @code bool read(std::istream& in_str, const std::string& splitter);
 *
 * The funciton should return true if everything is ok, and false else.
 * The input parameter is a stream to be read, and splitter is a splitter of a current file
 * \sa CsvNoHeader, CsvOneLineHeader
 * The TRow type is quite quite the same
 * @code size_t read(std::string& in_str, const std::string& splitter, size_t hint_size = 0);
 *
 * The row is always a one string and it should use the defined splitter
 * \sa CsvDoubleRow
 *
 * The CsvChunkReader operates the chunks: the list of text rows, by default it uses 100 rows as a chunk,
 * each 100 rows the special marker is saved for fast jump to that row. The markers could be set up during
 * the file open or during the continious forward row reading. When the files are of a very big size,
 * the cache_step could be increased. If only the header is needed, then preprocess_cache should be set to false
 */
namespace csvfile {
template <typename TRow, typename THeader>
class CsvChunkReader {
public:
  /**
   * @brief CsvChunkReader The default constructor, the splitter is any of white spaces
   * @param cache_step the markers step, the higher step the less markes are saved, the longer row acquisition (less memory consumes)
   * @param preprocess_cache should the markers be set during the file opening.
   */
  CsvChunkReader(size_t cache_step = 100, bool preprocess_cache = true);
  /**
   * @brief CsvChunkReader Main constructor if the user are going to set his own splitter
   * \sa CsvChunkReader()
   */
  CsvChunkReader(const std::string& splitter, size_t cache_step = 100, bool preprocess_cache = true);
  ~CsvChunkReader();
private:
  CsvChunkReader(const CsvChunkReader& rhs) {}
public:
  /**
   * @brief open file according to filename path
   * @param filename path to the file
   * @return if file is opened for reading
   */
  bool open(const std::string& filename);
  bool isOpen()const;
  /**
   * @brief readChunk read a given number of rows from the file, the number of rows could be less then size if
   * the file is shorter then expected
   * @param from the index of row to start with
   * @param size number of rows expected to be read
   * @return read rows
   */
  std::vector<TRow> readChunk(size_t from, size_t size);
  void close();
  /**
   * @brief knownSize number of marked rows, the 0 line and last line is always marked if file is preprocessed
   * if no the knownSize return the index of a last read +1 id if you call readChunk(0, 10) with preprocess_cache set to false
   * the knownSize will return the 10 even if the file is longer
   * @return known size of file in rows
   */
  size_t knownSize()const;
private:
  bool readHeaders();
public:
  /**
   * @brief headers
   * @return the header object
   */
  THeader headers() const;
private:
  typedef std::map<size_t, std::streampos> TCacheMap;
private:
  std::string _splitter;
  THeader _headers;
  std::ifstream _in;
  TCacheMap _cache;
  size_t _cache_step;
  bool _preprocess_cache;
  size_t _sz;
  mutable boost::shared_mutex _mutex;
};

/**
 * @brief The CsvForwardWriter class naive file writer. The header should be written first, then the linkes should be writter one after another
 */
class CsvForwardWriter {
public:
  /**
   * Default constructor gets splitter from Common class and writes the headers too
   */
  CsvForwardWriter() : _splitter(",") {}
  /**
   * Constructor which specifies arbitrary splitter and if the headers are needed.
   */
  CsvForwardWriter(const std::string& spliter) : _splitter(spliter) {}
public:
  bool open(const std::string& filename);
  template <typename THeader>
  void writeHeader(const THeader& header);
  template <typename TRow>
  void write(const TRow& row);
  /**
   * Writes the vector of rows simultaniously
   */
  template <typename TRow>
  void writeChunk(const std::vector<TRow>& rows);
  void close();
private:
  std::ofstream _out;
  std::string _splitter;
};
} // namespace csvfile
#endif /* CSVCHUNK_H_ */
