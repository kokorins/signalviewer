#ifndef CSVCHUNK_HPP_
#define CSVCHUNK_HPP_
#include "csvchunk.h"
#include <sstream>
#include <iterator>
#include <string>
#include <algorithm>
#include <boost/bind.hpp>

namespace csvfile {
template <typename TRow, typename THeader>
CsvChunkReader<TRow,THeader>::CsvChunkReader(size_t cache_step, bool preprocess_cache):
  _splitter(",\t; "), _cache_step(cache_step), _preprocess_cache(preprocess_cache) {}

template <typename TRow, typename THeader>
CsvChunkReader<TRow,THeader>::CsvChunkReader(const std::string &splitter, size_t cache_step, bool preprocess_cache):
  _splitter(splitter), _cache_step(cache_step), _preprocess_cache(preprocess_cache) {}

template <typename TRow, typename THeader>
CsvChunkReader<TRow,THeader>::~CsvChunkReader()
{
  close();
}

template <typename TRow, typename THeader>
bool CsvChunkReader<TRow,THeader>::open(const std::string &filename)
{
  boost::unique_lock<boost::shared_mutex> lock(_mutex);
  _in.open(filename.c_str());
  if (!_in.is_open())
    return false;
  if(!readHeaders())
    return false;
  _cache.clear();
  std::pair<typename TCacheMap::iterator, bool> res = _cache.insert(std::make_pair(0u, _in.tellg()));
  _sz = 0;
  if(_preprocess_cache) {
    std::string in_str;
    std::getline(_in, in_str); // the zero line is already in cache
    size_t idx = 0;
    while(!_in.eof()) {
      std::streampos row_pos = _in.tellg();
      std::getline(_in, in_str);
      if(!in_str.empty()) {
        ++idx;
        if((idx%_cache_step)==0) {
          _cache.insert(std::make_pair(idx, row_pos));
        }
        if(idx>=_sz)
          _sz = idx+1;
      }
    }
    _in.clear();
    _in.seekg(res.first->second);
    if(_in.fail())
      return false;
  }
  return true;
}

template <typename TRow, typename THeader>
bool CsvChunkReader<TRow,THeader>::isOpen() const
{
  boost::shared_lock<boost::shared_mutex> lock(_mutex);
  return _in.is_open();
}

template <typename TRow, typename THeader>
std::vector<TRow> CsvChunkReader<TRow,THeader>::readChunk(size_t from, size_t size)
{
  boost::upgrade_lock<boost::shared_mutex> lock(_mutex);
  typename TCacheMap::const_iterator lb = _cache.lower_bound(from);
  std::vector<TRow> res;
  if(_cache.empty())
    return res;
  bool need_cache = (lb==_cache.end());
  if(lb==_cache.end() || lb->first != from)
    --lb;
  std::string in_str;
  boost::upgrade_to_unique_lock<boost::shared_mutex> unique_lock(lock);
  _in.clear();
  _in.seekg(lb->second);
  if(_in.fail())
    return res;
  for(size_t i=lb->first; i<from; ++i) {
    std::streampos row_pos = _in.tellg();
    std::getline(_in, in_str);
    if(_in.eof() || in_str.empty())
      return res;
    if(need_cache && (i%_cache_step)==0 && i!=lb->first)
      _cache.insert(std::make_pair(i, row_pos));
  }
  res.reserve(size);
  TRow row;
  while (!_in.eof() && res.size()<size) {
    size_t len = 0;
    std::streampos row_pos = _in.tellg();
    std::getline(_in, in_str);
    if (!in_str.empty()) {
      if(need_cache && ((from+res.size())%_cache_step)==0 && (from+res.size())!=lb->first) {
        _cache.insert(std::make_pair(from+res.size(), row_pos));
      }
      len = row.read(in_str, _splitter, len);
    }
    if (len > 0)
      res.push_back(row);
  }
  if(_sz<from+res.size())
    _sz = from+res.size();
  return res;
}

template <typename TRow, typename THeader>
void CsvChunkReader<TRow, THeader>::close()
{
  boost::unique_lock<boost::shared_mutex> lock(_mutex);
  if (_in.is_open())
    _in.close();
}

template <typename TRow, typename THeader>
size_t CsvChunkReader<TRow, THeader>::knownSize() const
{
  boost::shared_lock<boost::shared_mutex> lock(_mutex);
  return _sz;
}

template <typename TRow, typename THeader>
bool CsvChunkReader<TRow, THeader>::readHeaders()
{
  //NOTE should be used only inside the locked functions
  return _headers.read(_in, _splitter);
}

template <typename TRow, typename THeader>
THeader CsvChunkReader<TRow, THeader>::headers() const
{
  boost::shared_lock<boost::shared_mutex> lock(_mutex);
  return _headers;
}

template <typename THeader>
void CsvForwardWriter::writeHeader(const THeader &header)
{
  header.write(_out, _splitter);
}

template <typename TRow>
void CsvForwardWriter::write(const TRow &row)
{
  row.write(_out, _splitter);
}

template <typename TRow>
void CsvForwardWriter::writeChunk(const std::vector<TRow> &rows)
{
  std::for_each(rows.begin(), rows.end(), boost::bind(&TRow::write, _1, boost::ref(_out), boost::ref(_splitter)));
}

} //namespace csvfile
#endif //CSVCHUNK_HPP_

