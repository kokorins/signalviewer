#include "csvheader.h"
#include <iterator>
#include <istream>
#include <boost/algorithm/string.hpp>
namespace csvfile {
bool CsvNoHeader::read(std::istream &/*in_str*/, const std::string &/*splitter*/)
{
  return true;
}
void CsvNoHeader::write(std::ostream &/*out*/, const std::string &/*splitter*/) const {}

bool CsvOneLineHeader::read(std::istream& in, const std::string& splitter)
{
  std::string in_str;
  std::getline(in, in_str);
  if (in_str.empty())
    return false;
  _stats.clear();
  boost::algorithm::split(_stats, in_str, boost::algorithm::is_any_of(splitter));
  return true;
}

void CsvOneLineHeader::write(std::ostream & out, const std::string& splitter) const
{
  out<<boost::algorithm::join(_stats, splitter)<<std::endl;
}

std::ostream & operator <<(std::ostream & out, const CsvOneLineHeader & rhs)
{
  rhs.write(out, ", ");
  return out;
}
} //namespace csvfile
