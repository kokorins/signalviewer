#ifndef CSVHEADER_H_
#define CSVHEADER_H_
#include <vector>
#include <functional>
#include <fstream>
namespace csvfile {
/**
 * @brief The CsvNoHeader struct the empty header struct, writes nothing to header and read nothing from header
 */
struct CsvNoHeader {
public:
  bool read(std::istream& in_str, const std::string& splitter);
  void write(std::ostream& out, const std::string& splitter) const;
};
/**
 * @brief The CsvOneLineHeader struct read a one line header, which uses the default splitter and
 * contains the std::string names of columns, each column has a one cell header
 */
struct CsvOneLineHeader {
public:
  bool read(std::istream& in_str, const std::string& splitter);
  void write(std::ostream& out, const std::string& splitter) const;
  const std::vector<std::string>& values() const {
    return _stats;
  }
  void setValues(const std::vector<std::string>& stats) {
    _stats = stats;
  }
private:
  std::vector<std::string> _stats;
};
} // namespace csvfile
#endif /* CSVHEADER_H_ */
