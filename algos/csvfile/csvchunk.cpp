#include "csvchunk.h"
#include <sstream>
#include <iterator>
#include <string>
#include <algorithm>
#include <boost/bind.hpp>

namespace csvfile {

bool CsvForwardWriter::open(const std::string &filename)
{
  _out.open(filename.c_str());
  return _out.is_open();
}

void CsvForwardWriter::close()
{
  if(_out.is_open()) {
    _out.flush();
    _out.close();
  }
}
} //namespace csvfile
