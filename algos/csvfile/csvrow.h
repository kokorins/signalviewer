#ifndef CSVROW_H_
#define CSVROW_H_
#include <vector>
#include <functional>
#include <fstream>
namespace csvfile {
/**
 * \brief Default implementation
 * without a mandatory part all the values are double
 */
struct CsvDoubleRow {
public:
  size_t read(std::string& in_str, const std::string& splitter, size_t hint_size = 0);
  void write(std::ostream& out, const std::string& splitter) const;
public:
  const std::vector<double>& values() const {
    return _stats;
  }
  void setValues(const std::vector<double>& stats) {
    _stats = stats;
  }
public:
  friend std::ostream& operator<<(std::ostream&, const CsvDoubleRow&);
private:
  std::vector<double> _stats;
};

std::ostream& operator<<(std::ostream&, const CsvDoubleRow&);
} // namespace csvfile
#endif /* CSVROW_H_ */
