#include "seriesstat.h"
#include <transformation/utils.h>
#include <algorithm>
#include <numeric>
#include <cmath>

std::vector<double> SeriesStats::movingAverage( const std::vector<double>& serie, int window)
{
	std::vector<double> rez(serie.size());
	rez[window]=serie[window];
	for (int i=1; i<=window; ++i) {
		rez[window] += serie[window-i];
		rez[window] += serie[window+i];
	}
	for (int i = window + 1; i<(int)serie.size()-window; ++i)
		rez[i]=rez[i-1]-serie[i-1-window]+serie[i+window];
	for (int i = window; i<(int)serie.size()-window; ++i)
		rez[i]/=(2*window+1);
	for(int i=0; i< window; ++i)
		rez[i]=rez[window];
	for(size_t i=serie.size()-window; i<serie.size(); ++i)
		rez[i]=rez[serie.size()-window-1];
	return rez;
}

std::vector<TPoint> SeriesStats::getNPeaks( const std::vector<TPoint>& serie, int number )
{
	std::vector<TPoint> rez(number);
	std::partial_sort_copy(serie.begin(),serie.end(), rez.begin(), rez.end(), CTPointComparer());
	return rez;
}

double SeriesStats::average( const std::vector<TPoint>& series )
{
	double sum = 0;
	for (size_t i=0; i<series.size(); ++i)
		sum+=series[i].second;
	if(sum > 0)
		return average(series, sum);
	return 0;
}

double SeriesStats::average( const std::vector<TPoint>& series, double totalWeight )
{
	double mean = 0;
	for (size_t i=0; i<series.size(); ++i)
		mean +=(series[i].second*series[i].first);
	mean /= totalWeight;
	return mean;
}

std::vector<double> SeriesStats::getLnEegSignal(const std::vector<double>& serie)
{
	std::vector<double> lnEegSignal(serie.size());
	
	double maxelement = 0;
	for (size_t i = 0; i < serie.size(); i++){
		if(abs(serie[i]) > maxelement) maxelement += abs(serie[i]);
	}

	for (size_t i = 0; i < serie.size(); i++)
	{
		if(abs(serie[i]) < 1e-5) lnEegSignal[i] = 100;
		else lnEegSignal[i] = - log(abs(serie[i])/maxelement);
	}

	return lnEegSignal;

}

std::vector<TPoint> SeriesStats::getPeriodogram(const std::vector<double>& series, double epoch_len)
{
	return CUtils::Periodogram(series, epoch_len);
}

double SeriesStats::variance( const std::vector<TPoint>& serie )
{
	double sum = 0;
	for (size_t i=0; i<serie.size(); ++i)
		sum +=serie[i].second;
	double mean = average(serie, sum);
	double sumsq = 0;
	for (size_t i = 0; i<serie.size(); ++i)
		sumsq += (serie[i].second*serie[i].first*serie[i].first);
	sumsq /= sum;
	sumsq -= (mean*mean);
	return sumsq;
}

double SeriesStats::variance( const std::vector<TPoint>& serie, double mean, double sum)
{
  double sumsq = 0;
  for (size_t i = 0; i<serie.size(); ++i)
    sumsq += (serie[i].second*serie[i].first*serie[i].first);
  sumsq /= sum;
  sumsq -= (mean*mean);
  return sumsq;
}
double SeriesStats::centralMoment( const std::vector<TPoint>& serie, int moment)
{
	double sum = 0;
	for (size_t i=0; i<serie.size(); ++i)
		sum +=serie[i].second;
	double mean = average(serie, sum);
	if(moment == 1)
		return mean;
	double sumsq = 0;
	for (size_t i = 0; i<serie.size(); ++i) {
		double elem = serie[i].second;
		for (int j = 0; j<moment; ++j)
			elem*=(serie[i].first-mean);
		sumsq += elem;
	}
	sumsq /= sum;
	return sumsq;
}

double SeriesStats::centralMoment( const std::vector<double>&serie, int moment )
{
	if(serie.empty())
		return 0;
	double m = mean(serie);
	if(moment == 1)
		return m;
	double sumsq = 0;
	for (size_t i=0; i<serie.size(); ++i) {
		double elem = 1;
		for (int j=0; j<moment; ++j)
			elem*=(serie[i]-m);
		sumsq += elem;
	}
	return sumsq/=serie.size();
}

double SeriesStats::correlation(const std::vector<double>&serie, int lag)
{
	if(serie.empty())
		return 0;
	double m = mean(serie);
	double covariation = 0, variance = centralMoment(serie, 2);
	for (size_t i = 0; i < serie.size() - lag; ++i)
	{
		covariation += (serie[i] - m) * (serie[i + lag] - m);
	}
	return covariation/(variance * (serie.size() - lag));
}

double SeriesStats::correlationLag(const std::vector<double>&serie, int lag)
{
	if(serie.empty())
		return 0;
  //TODO: check that step is ok?
	int step = 100;
  double m;
	std::vector<double> serieStep(step);

	double minCorr = 0;
	double covariation = 0, variance;// = centralMoment(serie, 2);
  for (size_t i = 0; i < serie.size() - step; ++i) {
    for(size_t j = 0; j < (size_t) step; ++j)
			serieStep[j] = serie[i + j];
		m = mean(serieStep);
		variance = centralMoment(serieStep, 2);
		covariation = 0;
    for(int j = 0; j < (int)step - lag; ++j)
			covariation += (serieStep[j] - m) * (serieStep[j + lag] - m);

		if(covariation/(variance * (step - lag)) < minCorr) 
			minCorr = covariation/(variance * (step - lag));
	}
	return minCorr;
}

double SeriesStats::getFrequenceWeightVariance( const std::vector<TPoint>& serie, double lowFreq, double highFreq)
{

	if(serie.empty()) return 0;
	double total = 0;
	double variance = 0;
	double meanWeight = 0;

	int countWeight = 0;

	for(size_t i = 0; i < serie.size(); i++){
		total += serie[i].second;
		if (serie[i].first>lowFreq && serie[i].first<highFreq)
		{
			meanWeight += serie[i].second;
			countWeight++;
		}
	}
	if(countWeight == 0) return 0;
	meanWeight = total/serie.size();
	//meanWeight /= countWeight;

	for(size_t i = 0; i<serie.size(); ++i)
	{
		if (serie[i].first>lowFreq && serie[i].first<highFreq)
		{
			variance += (serie[i].second - meanWeight) * (serie[i].second - meanWeight);
		}
	}

	if(total==0)
		return 0;
	return sqrt(variance)/total;
}

double SeriesStats::getAlphaCorr( const std::vector<double>& serie )
{
	if(serie.empty())
		return 0;
	return ( 2 * correlation(serie, 5) + 4 * correlation(serie, 6) + 5 * correlation(serie, 7) + 
		4 * correlation(serie, 8) + 2 * correlation(serie, 9))/17;
		//( correlation(serie, 6) + correlation(serie, 7) + correlation(serie, 8) )/3;
}

double SeriesStats::getAutoregressionMean1_4( const std::vector<double>& serie ) //<--Nikolay
{
	if(serie.empty())
		return 0;

	return ( 2 * correlationLag(serie, 5) + 4 * correlationLag(serie, 6) + 5 * correlationLag(serie, 7) + 
		4 * correlationLag(serie, 8) + 2 * correlationLag(serie, 9))/17;
}

double SeriesStats::getEntropy( const std::vector<TPoint>& period )
{
	double weight = 0;
	for (size_t i=0; i<period.size(); ++i)
		weight += period[i].second;
	double result = 0;
	for (size_t i=0; i<period.size(); ++i)
	{
		if(period[i].second<1e-5)
			continue;
		double elem = period[i].second/weight;
		result += log(elem);
	}
	return -result/log((double)period.size());
}

double SeriesStats::getSkewness( const std::vector<TPoint>& serie )
{
	double var = centralMoment(serie,2);
	var = sqrt(var)*var;
	return centralMoment(serie,3)/var;
}

double SeriesStats::getKurtosis( const std::vector<TPoint>& serie )
{
	double var = centralMoment(serie,2);
	var = var*var;
	return centralMoment(serie, 4)/var-3;
}

double SeriesStats::getFrequenceWeight( const std::vector<TPoint>& fft, double low_freq, double high_freq)
{
	double total = 0;
	double stripe = 0;
  for(size_t i = 0; i<fft.size(); ++i) {
    total += fft[i].second;
    if (fft[i].first>low_freq && fft[i].first<high_freq)
      stripe += fft[i].second;
	}
	if(total==0)
		return 0;
	return stripe/total;
}

double SeriesStats::getFrequenceWeight( const std::vector<double>& serie, double lowFreq, double higtFreq, double epoch_len)
{
	std::vector<TPoint> period = SeriesStats::getPeriodogram(serie, epoch_len);
	return getFrequenceWeight(period,lowFreq,higtFreq);
}

double SeriesStats::mean( const std::vector<double>& serie )
{
	if(serie.empty())
		return 0;
	return std::accumulate(serie.begin(), serie.end(),0.0)/serie.size();
}

double SeriesStats::ampMean( const std::vector<TPoint>& serie )
{
	if(serie.empty())
		return 0;
	double sum = 0;
	for (size_t i =0; i<serie.size(); ++i)
		sum+=serie[i].second;
	return sum/serie.size();
}

double SeriesStats::ampCentralMoment( const std::vector<TPoint>&serie, int moment )
{
	if(serie.empty())
		return 0;
	double m = ampMean(serie);
	if(moment == 1)
		return m;
	double sumsq = 0;
	for (size_t i=0; i<serie.size(); ++i) {
		double elem = 1;
		for (int j=0; j<moment; ++j)
			elem*=(serie[i].second-m);
		sumsq += elem;
	}
	sumsq/=serie.size();
	return sumsq;
}

double SeriesStats::ampMeanBounded( const std::vector<TPoint>& serie, double lowFreq, double highFreq )
{
	if(serie.empty())
		return 0;
	double sum = 0;
	int count = 0;
	for (size_t i =0; i<serie.size(); ++i) {
		if(serie[i].first>=lowFreq && serie[i].first<= highFreq) {
			sum+=serie[i].second;
			++count;
		}
	}
	if(count>0)
		return sum/count;
	return 0;
}

std::vector<double> SeriesStats::AmpCentralMoments( const std::vector<TPoint>& series, int moment )
{
  std::vector<double> result(moment, 0);
  if(series.empty())
    return result;
  double m = ampMean(series);
  result[0] = m;
  if(moment == 1)
    return result;
  for (size_t i=0; i<series.size(); ++i)
  {
    double term = series[i].second-m;
    double elem = term;
    for (int j=1; j<moment; ++j) {
      elem *= term;
      result[j] += elem;
    }
  }
  for(size_t i=0; i<result.size(); ++i)
    result[i]/= series.size();
  return result;
}

std::vector<double> SeriesStats::CentralMoments( const std::vector<TPoint>& series, int moment )
{
  std::vector<double> result(moment, 0);
  double sum = 0;
  for (size_t i=0; i<series.size(); ++i)
    sum += series[i].second;
  double mean = average(series, sum);
  result[0] = mean;
  if(moment == 1)
    return result;
  for (size_t i = 0; i<series.size(); ++i)
  {
    double term = series[i].first - mean;
    double elem = series[i].second * term;
    for (int j=1; j<moment; ++j)
    {
      elem *= term;
      result[j] += elem;
    }
  }

  // for (size_t i=0; i<result.size(); ++i)
  //-->Nikolay
  for (size_t i=1; i<result.size(); ++i)
    result[i] /= sum;
  return result;  
}

std::vector<double> SeriesStats::diff( const std::vector<double>&serie, int order)
{
	std::vector<double> result(serie.begin(),serie.end());
	for (int i=0; i<order; ++i)
		std::adjacent_difference(result.begin(),result.end(),result.begin());
	return result;
}

double SeriesStats::getSpectralEntropy( const std::vector<TPoint>& period )
{
	double weight = 0;
	for (size_t i=0; i<period.size(); ++i)
		weight += period[i].second;
	double result = 0;
	for (size_t i=0; i<period.size(); ++i) {
		if(period[i].second<1e-5)
			continue;
		double elem = period[i].second/weight;
		result += elem* log(elem);
	}
	return -result/log((double)period.size());
}

double SeriesStats::Quantile( const std::vector<double>& signal, double level )
{
  std::vector<double> s(signal.begin(), signal.end());
  size_t l = (size_t)std::floor((s.size())*level);
  if(l==s.size())
    l=s.size()-1;
  std::nth_element(s.begin(), s.begin()+l, s.end());
  return *(s.begin()+l);
}

double SeriesStats::Variance(const std::vector<double> &signal)
{
  if(signal.size()<2)
    return 0;
  double sum = 0;
  for(size_t i = 1; i<signal.size(); ++i)
    sum += std::fabs(signal[i] - signal[i-1]);
  return sum/(signal.size()-1);
}
