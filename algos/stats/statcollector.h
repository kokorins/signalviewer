#ifndef STATCOLLECTOR_H
#define STATCOLLECTOR_H
#include <vector>
#include <boost/shared_ptr.hpp>
namespace stats {
class ITypedStatCalculator;
class StatProperties;
class StatCollector {
public:
  typedef boost::shared_ptr<ITypedStatCalculator> Calculator;
public:
  void setStats(const StatProperties& stat_props, const std::vector<std::string>& jsons);
  const std::vector<Calculator>& calculator()const;
  void calc(const std::vector<double>& signal, std::vector<double>& stats);
private:
  std::vector<Calculator> _calculators;
};
} //namespace stats
#endif // STATCOLLECTOR_H
