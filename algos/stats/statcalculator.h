#ifndef STATCALCULATOR_H
#define STATCALCULATOR_H
#include "statproperties.h"
#include <transformation/utils.h>
#include <vector>
#include <boost/shared_ptr.hpp>

namespace stats {
class Cache;
class ITypedStatCalculator;
class IStatCalculator {
public:
  virtual double calc(const std::vector<double>& signal, Cache& cache)const=0;
  virtual ~IStatCalculator() {}
};

class ITypedStatCalculator : public IStatCalculator {
public:
  virtual ~ITypedStatCalculator() {}
  virtual const std::string & type()const=0;
  virtual std::string toString()const=0;
  virtual bool fromString(const std::string& json)=0;
  virtual const std::string& name()const=0;
  virtual void setName(const std::string& name)=0;
};

class Cache {
public:
  Cache();
  bool isFft()const;
  const std::vector<TPoint>& fft()const;
  void putFft(const std::vector<TPoint>& fft);
private:
  bool _is_fft;
  std::vector<TPoint> _fft;
};

} //namespace stats
#endif // STATCALCULATOR_H
