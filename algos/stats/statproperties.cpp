#include "statproperties.h"
#include "statcollector.h"
#include "statcalculator.h"
#include <interfaces/stats/statisticsprovider.h>
#include <algorithm>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

namespace stats {
struct istype : public std::unary_function<boost::shared_ptr<IStatisticsProvider>, bool> {
public:
  explicit istype(const std::string& type) : _type(type) {}
  bool operator() (boost::shared_ptr<IStatisticsProvider> v) const {
    return v->type()==_type;
  }
private:
  const std::string& _type;
};

const std::string &StatProperties::kType()
{
  const static std::string kType="type_id";
  return kType;
}

std::string StatProperties::ReadType(const std::string &json)
{
  boost::property_tree::ptree pt;
  std::stringstream ss(json);
  boost::property_tree::json_parser::read_json(ss, pt);
  return ReadType(pt);
}

std::string StatProperties::ReadType(const boost::property_tree::ptree &pt)
{
  using boost::property_tree::ptree;
  ptree::const_assoc_iterator it = pt.find(kType());
  if(it==pt.not_found())
    return std::string();
  return pt.get<std::string>(kType());
}

boost::shared_ptr<ITypedStatCalculator> StatProperties::create(const std::string &json)const
{
  boost::property_tree::ptree pt;
  std::stringstream ss(json);
  boost::property_tree::json_parser::read_json(ss, pt);
  const std::string& type = ReadType(pt);
  std::vector<boost::shared_ptr<IStatisticsProvider> >::const_iterator it =
      std::find_if(_providers.begin(), _providers.end(), istype(type));
  boost::shared_ptr<ITypedStatCalculator> res;
  if(it == _providers.end())
    return res;
  else
    res = (*it)->createCalculator();
  if(res.get())
    res->fromString(json);
  return res;
}

std::vector<boost::shared_ptr<ITypedStatCalculator> > StatProperties::defaultStats(size_t epoch_len)
{
  std::vector<boost::shared_ptr<ITypedStatCalculator> > res;
  BOOST_FOREACH(boost::shared_ptr<IStatisticsProvider> provider, _providers) {
    const std::vector<std::string>& props = provider->defaultParameters(epoch_len);
    BOOST_FOREACH(const std::string& prop, props) {
      res.push_back(create(prop));
    }
  }
  return res;
}

void StatProperties::addProvider(boost::shared_ptr<IStatisticsProvider> provider)
{
  _providers.push_back(provider);
}

std::vector<std::wstring> StatProperties::initProviders(void *parent) const
{
  std::vector<std::wstring> res;
  BOOST_FOREACH(boost::shared_ptr<IStatisticsProvider> provider, _providers) {
    provider->updateParameters("", parent);
    res.push_back(provider->name());
  }
  return res;
}

std::string StatProperties::getParameter(const std::wstring &name, const std::string &new_json,void *parent)
{
  BOOST_FOREACH(boost::shared_ptr<IStatisticsProvider> provider, _providers) {
    if(provider->name()==name)
      return provider->updateParameters(new_json, parent);
  }
  return new_json;
}

std::wstring StatProperties::nameByType(const std::string &type) const
{
  BOOST_FOREACH(boost::shared_ptr<IStatisticsProvider> provider, _providers) {
    if(provider->type()==type)
      return provider->name();
  }
  return L"";
}

void StatProperties::updateCollector(const std::vector<std::string> &jsons, StatCollector& sc)const
{
  sc.setStats(*this, jsons);
}
} //namespace stats
