#include "statcalculator.h"
#include <transformation/utils.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace stats {
Cache::Cache() : _is_fft(false) {}

bool Cache::isFft() const
{
  return _is_fft;
}

const std::vector<TPoint> &Cache::fft() const
{
  return _fft;
}

void Cache::putFft(const std::vector<TPoint> &fft)
{
  _fft = fft;
  _is_fft = true;
}
} //namespace stats
