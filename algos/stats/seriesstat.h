#ifndef SERIESSTAT_H_
#define SERIESSTAT_H_
#include <transformation/utils.h>
#include <vector>
#include <functional>

class SeriesStats {
public:
	static std::vector<double> diff(const std::vector<double>&serie, int order);
	static std::vector<double> movingAverage(const std::vector<double>& serie, int window);
	static std::vector<TPoint> getNPeaks(const std::vector<TPoint>& serie, int number);
	static double mean(const std::vector<double>& serie);
	static double ampMean(const std::vector<TPoint>& serie);
	static double ampMeanBounded(const std::vector<TPoint>& serie, double lowFreq, double highFreq);
	static double ampCentralMoment(const std::vector<TPoint>&serie, int moment);
  static std::vector<double> AmpCentralMoments(const std::vector<TPoint>& series, int moment);
	static double centralMoment(const std::vector<double>&serie, int moment);
	static double correlation(const std::vector<double>&serie, int lag); 
	static double correlationLag(const std::vector<double>&serie, int lag); 
	static double getAlphaCorr(const std::vector<double>&serie); 
	static double getAutoregressionMean1_4(const std::vector<double>&serie); 
	static std::vector<double> getLnEegSignal(const std::vector<double>& serie);
	static double getEntropy(const std::vector<TPoint>& period);
	static double getFrequenceWeightVariance(const std::vector<TPoint>& serie, double lowFreq, double higtFreq);
	static double average(const std::vector<TPoint>& serie);
	static double average(const std::vector<TPoint>& serie, double totalWeight);
	static double variance(const std::vector<TPoint>& serie);
	static double variance(const std::vector<TPoint>& serie, double mean, double sum);
	static double centralMoment(const std::vector<TPoint>& serie, int moment);
	static std::vector<double> CentralMoments(const std::vector<TPoint>& series, int moment);
	static std::vector<TPoint> getPeriodogram(const std::vector<double>& serie, double epoch_len);
	static double getSkewness(const std::vector<TPoint>& serie);
	static double getKurtosis(const std::vector<TPoint>& serie);
  static double getFrequenceWeight(const std::vector<TPoint>& fft, double lowFreq, double higtFreq);
	static double getFrequenceWeight(const std::vector<double>& serie, double lowFreq, double higtFreq, double epoch_len);
	static double getSpectralEntropy(const std::vector<TPoint>& period);
  static double Quantile(const std::vector<double>& signal, double level);
  static double Variance(const std::vector<double>& signal);
};

class CTPointComparer:public std::binary_function<TPoint, TPoint, bool> {
public:
	bool operator()(const TPoint& lhs, const TPoint& rhs)
	{
		return lhs.second>rhs.second;
	}
};
#endif //SERIESSTAT_H_
