#include "statcollector.h"
#include "statcalculator.h"
#include "statproperties.h"
namespace stats {

void StatCollector::calc(const std::vector<double> &signal, std::vector<double> &stats)
{
  stats.clear();
  stats.reserve(_calculators.size());
  Cache c;
  for(size_t i=0; i<_calculators.size(); ++i)
    stats.push_back(_calculators[i]->calc(signal, c));
}

void StatCollector::setStats(const StatProperties &stat_props, const std::vector<std::string> &jsons)
{
  _calculators.clear();
  _calculators.reserve(jsons.size());
  for(size_t i=0; i<jsons.size(); ++i)
    _calculators.push_back(Calculator(stat_props.create(jsons[i])));
}

const std::vector<StatCollector::Calculator> &StatCollector::calculator() const
{
  return _calculators;
}
} //namespace stats
