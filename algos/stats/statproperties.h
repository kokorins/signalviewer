#ifndef STATPROPERTIES_H
#define STATPROPERTIES_H
#include <string>
#include <vector>
#include <cstddef>
#include <boost/shared_ptr.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
namespace stats {
class ITypedStatCalculator;
class StatCollector;
class IStatisticsProvider;
class StatProperties {
public:
  static const std::string& kType();
  static std::string ReadType(const std::string& json);
  static std::string ReadType(const boost::property_tree::ptree& pt);
  boost::shared_ptr<ITypedStatCalculator> create(const std::string& json)const;
  void updateCollector(const std::vector<std::string>& jsons, StatCollector& sc)const;
  std::vector<boost::shared_ptr<ITypedStatCalculator> > defaultStats(size_t epoch_len);
  void addProvider(boost::shared_ptr<IStatisticsProvider>);
  std::vector<std::wstring> initProviders(void*parent)const;
  std::string getParameter(const std::wstring& name, const std::string& new_json, void *parent);
  std::wstring nameByType(const std::string& type)const;
private:
  std::vector<boost::shared_ptr<IStatisticsProvider> > _providers;
};
} // namespace stats
#endif // STATPROPERTIES_H
