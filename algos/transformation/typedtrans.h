#ifndef TYPEDTRANS_H
#define TYPEDTRANS_H
#include "transformproperties.h"
#include "transform.h"
namespace trans {
class ITypedTrans {
public:
  virtual ~ITypedTrans() {}
  virtual TransformProperties::ETransType type()const=0;
  virtual std::string toString()const=0;
  virtual bool fromString(const std::string& json)=0;
};

template <typename T>
class IStrictTypedTrans : public ITrans<T>, public ITypedTrans {
public:
  ~IStrictTypedTrans() {}
};
} //namespace trans
#endif // TYPEDTRANS_H
