#include "logtrans.h"
#include <limits>
#include <cmath>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace trans {
void LogTrans::setBase(LogTrans::ELogBase base)
{
  _base = base;
}

LogTrans::ELogBase LogTrans::base() const
{
  return _base;
}

void LogTrans::setFactor(double factor)
{
  _factor = factor;
}

double LogTrans::factor() const
{
  return _factor;
}

void LogTrans::Name(size_t idx, std::string &name)
{
  switch(idx) {
  break; case Exp:
    name = "Exponent base";
  break; case Dec:
    name = "Decimal base";
  break; case Bin:
    name = "Binary base";
  break; default:
    name = "";
  }
}

LogTrans::ELogBase LogTrans::Idx(std::string &name)
{
  std::string str;
  for(size_t i=0; i<ELogBaseNum; ++i) {
    Name(i, str);
    if(name==str)
      return (ELogBase)i;
  }
  return ELogBaseNum;
}

TransformProperties::ETransType LogTrans::type() const
{
  return TransformProperties::Log;
}

std::string LogTrans::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  std::string name;
  TransformProperties::Name(TransformProperties::Log, name);
  pt.put(TransformProperties::kType, name);
  pt.put("factor", (double)_factor);
  pt.put("base", (int)_base);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool LogTrans::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(TransformProperties::ReadType(pt)==TransformProperties::Log) {
    _factor =  pt.get<double>("factor");
    _base = (ELogBase) pt.get<int>("base");
    return true;
  }
  return false;
}

void LogTrans::calc(std::vector<double> & signal) const
{
  for(size_t i=0; i<signal.size(); ++i) {
    double x = std::fabs(signal[i]);
    if(x<1e-15)
      signal[i] = -1e10;
    else {
      switch(_base) {
      break; case Exp:
        signal[i] = std::log(x);
      break; case Dec:
        signal[i] = std::log10(x);
      break; case Bin:
        signal[i] = std::log(x)/std::log(2.0);
      break; default:
        continue;
      }
      signal[i] *= _factor;
    }
  }
}

void LogTrans::calc(std::vector<TPoint> & fft) const
{
  for(size_t i=0; i<fft.size(); ++i) {
    double x = std::fabs(fft[i].second);
    if(x<1e-15)
      fft[i].second = -1e10;
    else {
      switch(_base) {
      break; case Exp:
        fft[i].second = std::log(x);
      break; case Dec:
        fft[i].second = std::log10(x);
      break; case Bin:
        fft[i].second = std::log(x)/std::log(2.0);
      break; default:
        continue;
      }
      fft[i].second *= _factor;
    }
  }
}
} //namespace trans
