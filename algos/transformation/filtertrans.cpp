#include "filtertrans.h"
#include <filter/filter.h>
#include <filter/typedfilter.h>
#include <filter/filter_helpers.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace trans {
void FilterTrans::setFilter(boost::shared_ptr<filter::ITypedFilter> filter)
{
  _filter = filter;
}

const filter::ITypedFilter &FilterTrans::filter() const
{
  return *_filter;
}

void FilterTrans::calc(std::vector<double> & signal) const
{
  _filter->filter(signal);
}

TransformProperties::ETransType FilterTrans::type() const
{
  return TransformProperties::Filter;
}

std::string FilterTrans::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  std::string name;
  TransformProperties::Name(TransformProperties::Filter, name);
  pt.put(TransformProperties::kType, name);
  ptree inner_pt;
  std::string inner_json;
  _filter->toString(inner_json);
  std::stringstream inner_ss(inner_json);
  boost::property_tree::json_parser::read_json(inner_ss, inner_pt);
  const static std::string filter_str("filter");
  ptree::assoc_iterator it = pt.find(filter_str);
  pt.insert(pt.to_iterator(it), std::make_pair(filter_str, inner_pt));
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool FilterTrans::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(TransformProperties::ReadType(pt)!=TransformProperties::Filter)
    return false;
  ptree::assoc_iterator it = pt.find("filter");
  if(it==pt.not_found())
    return false;
  ptree& inner_pt = it->second;
  std::stringstream inner_ss;
  boost::property_tree::json_parser::write_json(inner_ss, inner_pt);
  std::string inner_json = inner_ss.str();
  _filter = filter::FilterProperties::Create(inner_json);
  return _filter.get()!=0;
}
} //namespace trans
