#include "decimatetrans.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace trans {

void DecimateTrans::calc(std::vector<double> &signal) const
{
  std::vector<double> res;
  res.reserve((signal.size()/_dec));
  for(size_t i=0; i<signal.size(); i+=_dec)
    res.push_back(signal[i]);
  std::swap(res, signal);
}

void DecimateTrans::calc(std::vector<TPoint> & signal) const
{
  std::vector<TPoint> res;
  res.reserve((signal.size()/_dec));
  for(size_t i=0; i<signal.size(); i+=_dec)
    res.push_back(signal[i]);
  std::swap(res, signal);
}

TransformProperties::ETransType DecimateTrans::type() const
{
  return TransformProperties::Decimate;
}

std::string DecimateTrans::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  std::string name;
  TransformProperties::Name(TransformProperties::Decimate, name);
  pt.put(TransformProperties::kType, name);
  pt.put("dec", (int)_dec);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool DecimateTrans::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(TransformProperties::ReadType(pt)==TransformProperties::Decimate) {
    _dec = (size_t) pt.get<int>("dec");
    return true;
  }
  return false;
}

size_t DecimateTrans::dec() const
{
  return _dec;
}

void DecimateTrans::setDec(size_t dec)
{
  _dec = dec;
}
} //namespace trans
