#ifndef TRANSFORM_H_
#define TRANSFORM_H_
#include <boost/shared_ptr.hpp>
#include <vector>
#include <filter/filter.h>
namespace trans {
template <typename T>
class ITrans {
public:
  virtual ~ITrans() {}
  virtual void calc(T&)const=0;
};
} //namespace trans
#endif // TRANSFORM_H_
