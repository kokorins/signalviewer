#include "transformproperties.h"
#include <filter/filter.h>
#include <filter/filter_helpers.h>
#include "decimatetrans.h"
#include "filtertrans.h"
#include "logtrans.h"
#include "chaintrans.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace trans {

template <typename T>
IStrictTypedTrans<T> *TransformProperties::Create(const std::string &json)
{
  return 0;
}

template <>
IStrictTypedTrans<std::vector<double> > *TransformProperties::Create(const std::string &json)
{
  std::auto_ptr<IStrictTypedTrans<std::vector<double> > > res;
  boost::property_tree::ptree pt;
  std::stringstream ss(json);
  boost::property_tree::json_parser::read_json(ss, pt);
  switch(TransformProperties::ReadType(pt)) {
  break; case TransformProperties::Chain: {
    res.reset(new ChainTrans<std::vector<double> >());
  }
  break; case TransformProperties::Decimate: {
    res.reset(new DecimateTrans());
  }
  break; case TransformProperties::Filter: {
    res.reset(new FilterTrans());
  }
  break; default:;
  }
  if(res.get())
    res->fromString(json);
  return res.release();
}

template<>
IStrictTypedTrans<std::vector<TPoint> > *TransformProperties::Create(const std::string &json)
{
  std::auto_ptr<IStrictTypedTrans<std::vector<TPoint> > > res;
  boost::property_tree::ptree pt;
  std::stringstream ss(json);
  boost::property_tree::json_parser::read_json(ss, pt);
  switch(TransformProperties::ReadType(pt)) {
  break; case TransformProperties::Chain: {
    res.reset(new ChainTrans<std::vector<TPoint> >());
  }
  break; case TransformProperties::Decimate: {
    res.reset(new DecimateTrans());
  }
  break; case TransformProperties::Log: {
    res.reset(new LogTrans());
  }
  break; default:;
  }
  if(res.get())
    res->fromString(json);
  return res.release();
}

template<typename T>
IStrictTypedTrans<T> *TransformProperties::Create(const std::vector<std::string> &jsons)
{
  if(jsons.empty())
    return 0;
  std::auto_ptr<ChainTrans<T> > res(new ChainTrans<T>());
  for(size_t i=0; i<jsons.size(); ++i)
    res->transes().push_back(boost::shared_ptr<IStrictTypedTrans<T> >(Create<T>(jsons[i])));
  return res.release();
}
} //namespace trans
