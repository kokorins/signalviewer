#include "transformproperties.h"
#include <filter/filter.h>
#include <filter/filter_helpers.h>
#include "decimatetrans.h"
#include "filtertrans.h"
#include "chaintrans.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace trans {
const std::string TransformProperties::kType = "type_id";
void TransformProperties::Name(size_t idx, std::string &name)
{
  switch(idx) {
  break; case Filter:
    name = "Filter trans";
  break; case Chain:
    name = "Chain trans";
  break; case Decimate:
    name = "Decimate trans";
  break; case Log:
    name = "Log trans";
  break; default:
    name = "";
  }
}

TransformProperties::ETransType TransformProperties::Idx(std::string &name)
{
  std::string str;
  for(size_t i=0; i<ETransTypeNum; ++i) {
    Name(i, str);
    if(name==str)
      return (ETransType)i;
  }
  return ETransTypeNum;
}

TransformProperties::ETransType TransformProperties::ReadType(const std::string &json)
{
  boost::property_tree::ptree pt;
  std::stringstream ss(json);
  boost::property_tree::json_parser::read_json(ss, pt);
  return ReadType(pt);
}

TransformProperties::ETransType TransformProperties::ReadType(const boost::property_tree::ptree &pt)
{
  using boost::property_tree::ptree;
  ptree::const_assoc_iterator it = pt.find(kType);
  if(it==pt.not_found())
    return ETransTypeNum;
  std::string type_str = pt.get<std::string>(kType);
  return Idx(type_str);
}
} //namespace trans
