#ifndef UTILS_H_
#define UTILS_H_
#include <vector>

typedef std::pair<double,double> TPoint;
class CUtils {
public:
	static double EuclidDist(const std::vector<double>& lhs, const std::vector<double> & rhs);
  static double SqMahalanobisDist(const std::vector<double>&lhs, const std::vector<double>& rhs, const std::vector<std::vector<double> >& cov);
  static double MahalDensity(const std::vector<double>&lhs, const std::vector<double>& rhs, const std::vector<std::vector<double> >& cov, double det);
  static std::vector<TPoint> Periodogram(const std::vector<double> &signal, double epoch_len);
  static std::vector<double> CalcRealFFT(const std::vector<double> &signal, bool isInverse);
};
#endif //UTILS_H_
