#ifndef FILTERTRANS_H
#define FILTERTRANS_H
#include <boost/shared_ptr.hpp>
#include <vector>
#include "transform.h"
#include "typedtrans.h"

namespace filter {
  class IFilter;
  class ITypedFilter;
}

namespace trans {
class FilterTrans : public IStrictTypedTrans<std::vector<double> > {
public:
  virtual void calc(std::vector<double>&)const;
  virtual TransformProperties::ETransType type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
public:
  void setFilter(boost::shared_ptr<filter::ITypedFilter> filter);
  const filter::ITypedFilter& filter()const;
private:
  boost::shared_ptr<filter::ITypedFilter> _filter;
};
} //namespace trans
#endif // FILTERTRANS_H
