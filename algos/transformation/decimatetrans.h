#ifndef DECIMATETRANS_H
#define DECIMATETRANS_H
#include "typedtrans.h"
#include "utils.h"
#include <cstddef>
namespace trans {
class DecimateTrans : public IStrictTypedTrans<std::vector<double> >, public IStrictTypedTrans<std::vector<TPoint> > {
public:
  virtual void calc(std::vector<double>&)const;
  virtual void calc(std::vector<TPoint> &) const;
  virtual TransformProperties::ETransType type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
public:
  void setDec(size_t dec);
  size_t dec()const;
private:
  size_t _dec;
};
} //namespace trans
#endif // DECIMATETRANS_H
