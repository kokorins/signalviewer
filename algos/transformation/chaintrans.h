#ifndef CHAINTRANS_H
#define CHAINTRANS_H
#include <vector>
#include "transform.h"
#include "typedtrans.h"
namespace trans {
template<typename T>
class ChainTrans : public IStrictTypedTrans<T> {
public:
  typedef boost::shared_ptr<IStrictTypedTrans<T> > TransPtr;
public:
  virtual void calc(T&)const;
  virtual TransformProperties::ETransType type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
public:
  std::vector<TransPtr>& transes();
  const std::vector<TransPtr>& transes()const;
private:
  std::vector<TransPtr> _transs;
};
} //namespace trans
#endif // CHAINTRANS_H
