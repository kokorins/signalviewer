#ifndef TRANSFORMPROPERTIES_H
#define TRANSFORMPROPERTIES_H
#include "utils.h"
#include <vector>
#include <string>
#include <boost/property_tree/ptree.hpp>


namespace trans {
template <typename T> class ITrans;
template <typename T> class IStrictTypedTrans;
class FilterTrans;

class TransformProperties {
public:
  enum ETransType {
    Filter,
    Chain,
    Decimate,
    Log,
    ETransTypeNum
  };
public:
  const static std::string kType;
  static void Name(size_t idx, std::string& name);
  static ETransType Idx(std::string& name);
  static ETransType ReadType(const std::string& json);
  static ETransType ReadType(const boost::property_tree::ptree& pt);
  ///use specialisations for this creation
  template <typename T>
  static IStrictTypedTrans<T>* Create(const std::string& json);
  template <typename T>
  static IStrictTypedTrans<T>* Create(const std::vector<std::string>& jsons);
};
} //namespace trans
#endif // TRANSFORMPROPERTIES_H
