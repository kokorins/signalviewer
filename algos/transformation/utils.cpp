#include "utils.h"
#include <fft/realfft.h>
#include <numeric>
#include <algorithm>
#include <cmath>

double CUtils::EuclidDist( const std::vector<double>& lhs, const std::vector<double> & rhs )
{
	double rez = 0;
	for (size_t i=0; i<lhs.size(); ++i) {
    double elem = lhs[i]-rhs[i];
		rez += elem*elem;
  }
	return sqrt(rez);
}

std::vector<TPoint> CUtils::Periodogram( const std::vector<double> &signal, double epoch_len)
{
  std::vector<double> fft = CalcRealFFT(signal, false);
  int length = (int)fft.size() / 2;
  double delta = 0.5 / length;
  double x = delta;
  std::vector<TPoint> periodogram(length+1);
  double herz_coef = 2*signal.size()/epoch_len;
  for (int i = 0; i < length; ++i) {
    if (i == 0) {
      periodogram[0]=std::make_pair(herz_coef*0.0,abs(fft[0]));
      periodogram[length]=std::make_pair(herz_coef*0.5,abs(fft[1]));
    }
    else {
      periodogram[i]=std::make_pair(herz_coef*x,sqrt(fft[2 * i] * fft[2 * i] + fft[2 * i + 1] * fft[2 * i + 1]));
      x += delta;
    }
  }
  return periodogram;
}

std::vector<double> CUtils::CalcRealFFT(const std::vector<double> &signal, bool isInverse)
{
  std::vector<double> arr;
//  ap::real_1d_array arr;
  int len = 0;
  int padded = (int)signal.size()-1;
  while (padded) {
    padded>>=1;
    ++len;
  }
  int length=(1<<len);
//  arr.setbounds(0,length-1);
  arr.resize(length);

  for(int i=0; i<length; ++i) {
    if(i<(int)signal.size())
      arr[i]=signal[i];
    else
      arr[i]=0;
  }
  rfft(arr, isInverse);
//  realfastfouriertransform(arr,(1<<len), isInverse);

//  return std::vector<double>(arr.getcontent(),arr.getcontent()+arr.gethighbound()-arr.getlowbound()+1);
  return arr;
}

double CUtils::SqMahalanobisDist(const std::vector<double>&lhs, 
                                 const std::vector<double>& rhs, 
                                 const std::vector<std::vector<double> >& cov )
{
  double norm = 0;
  size_t s = lhs.size();
  for(size_t l = 0; l < s; l++)
    for(size_t m = 0; m < l; m++)
      norm += cov[l][m] * (lhs[l] - rhs[l]) * (lhs[m] - rhs[m]);
  norm *= 2;
  for(size_t l = 0; l < s; l++) {
    double elem = lhs[l] - rhs[l];
    norm += cov[l][l] * elem * elem;
  }
  return norm;
}

double CUtils::MahalDensity( const std::vector<double>&lhs, const std::vector<double>& rhs, const std::vector<std::vector<double> >& cov, double det )
{
  double power = SqMahalanobisDist(lhs, rhs, cov);
  double c = 1/std::sqrt(det);
  double value = std::sqrt(2 * M_PI);
  c*=std::pow(value, (double)lhs.size());
  return std::exp(-power/2)/c;
}
