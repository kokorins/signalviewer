#ifndef CHAINTRANS_HPP
#define CHAINTRANS_HPP
#include "chaintrans.h"
#include "decimatetrans.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
namespace trans {
template<typename T>
void ChainTrans<T>::calc(T& s) const
{
  for(size_t i=0; i<_transs.size(); ++i)
    _transs[i]->calc(s);
}

template<typename T>
std::vector<boost::shared_ptr<IStrictTypedTrans<T> > > &ChainTrans<T>::transes()
{
  return _transs;
}

template<typename T>
const std::vector<boost::shared_ptr<IStrictTypedTrans<T> > > &ChainTrans<T>::transes()const
{
  return _transs;
}

template<typename T>
TransformProperties::ETransType ChainTrans<T>::type() const
{
  return TransformProperties::Chain;
}

template<typename T>
std::string ChainTrans<T>::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  std::string name;
  TransformProperties::Name(TransformProperties::Chain, name);
  pt.put(TransformProperties::kType, name);
  for(size_t i=0; i<_transs.size(); ++i) {
    ptree inner_pt;
    std::stringstream inner_ss(_transs[i]->toString());
    boost::property_tree::json_parser::read_json(inner_ss, inner_pt);
    const static std::string trans_str("trans");
    ptree::assoc_iterator it = pt.find(trans_str);
    pt.insert(pt.to_iterator(it), std::make_pair(trans_str, inner_pt));
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

template<typename T>
bool ChainTrans<T>::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  if(TransformProperties::ReadType(pt)==TransformProperties::Chain) {
    ptree::const_assoc_iterator begin,end;
    boost::tie(begin, end) = pt.equal_range("trans");
    for(ptree::const_assoc_iterator it = begin; it!=end; ++it) {
      const ptree& inner_pt = it->second;
      std::stringstream ss;
      boost::property_tree::json_parser::write_json(ss, inner_pt);
      std::string inner_json = ss.str();
      TransPtr inner(TransformProperties::Create<T>(inner_json));
      if(inner) {
        inner->fromString(inner_json);
        _transs.push_back(inner);
      }
    }
    return true;
  }
  return false;
}
} //namespace trans
#endif // CHAINTRANS_HPP
