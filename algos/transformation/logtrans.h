#ifndef LOGTRANS_H
#define LOGTRANS_H
#include "typedtrans.h"
#include "utils.h"
#include <vector>
namespace trans {
class LogTrans : public IStrictTypedTrans<std::vector<double> >,
                 public IStrictTypedTrans<std::vector<TPoint> > {
public:
  enum ELogBase {
    Exp,
    Dec,
    Bin,
    ELogBaseNum
  };

public:
  virtual void calc(std::vector<double>&)const;
  virtual void calc(std::vector<TPoint> &) const;
  virtual TransformProperties::ETransType type()const;
  virtual std::string toString()const;
  virtual bool fromString(const std::string& json);
public:
  void setBase(ELogBase base);
  ELogBase base()const;
  void setFactor(double factor);
  double factor()const;
public:
  static void Name(size_t idx, std::string& name);
  static ELogBase Idx(std::string& name);
private:
  ELogBase _base;
  double _factor;
};
} // namespace trans
#endif // LOGTRANS_H
