#ifndef FFT_H
#define FFT_H
#include <vector>

void rfft(std::vector<double>& a, bool inversefft);

#endif //FFT_H
