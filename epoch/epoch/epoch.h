#ifndef EPOCH_H_
#define EPOCH_H_
#include <vector>
#include <memory>
#include <string>

namespace boost {
  namespace posix_time {
    class ptime;
  }
}

namespace epoch {
/**
 * @brief The Epoch class represents the internal data structure
 */
class Epoch {
  const static std::string kTypeId;
public:
  Epoch();
  Epoch(const Epoch&);
  Epoch& operator=(const Epoch&);
  ~Epoch();
  /**
   * checks if current type of data data exists for epoch
   */
  bool has(const std::string& tp)const;
  void set(const std::string& tp, const std::vector<double>& val);
  void swap(const std::string& tp, std::vector<double>& val);
  /**
   * Always check if current type exists
   */
  std::vector<double> const* get(const std::string& tp)const;
  /**
   * Getter and setter for epoch start timestamp
   */
  void setStartTime(const boost::posix_time::ptime& startTime);
  const boost::posix_time::ptime& startTime()const;
  boost::posix_time::ptime endTime()const;
  /**
   * \return epoch length in seconds
   */
  double epochLength()const;
  void setEpochLength(double epoch_len);
  std::string toString()const;
  bool fromString(const std::string& json);
private:
  struct Impl;
  std::unique_ptr<Impl> _impl;
};

} //namespace epoch
#endif //EPOCH_H_
