#ifndef VECTORLIST_H
#define VECTORLIST_H
#include "epoch.h"
#include <interfaces/fileformats/epochlistimpl.h>
namespace epoch {
class VectorList : public IEpochListImpl {
public:
  explicit VectorList(size_t epoch_length): _epoch_len(epoch_length) {}
  void swapEpochs(std::vector<Epoch>&epochs);
  void swapActiveTypes(std::set<std::string>& active_types);
public:
  bool open(const std::map<std::string, std::vector<std::pair<std::string, size_t> > >& files_paths);
  size_t size()const;
  bool read(size_t idx, Epoch& epoch);
  const std::set<std::string>& activeTypes()const;
private:
  size_t _epoch_len;
  std::set<std::string> _active_types;
  std::vector<Epoch> _epochs;
};
} //namespace epoch
#endif // VECTORLIST_H
