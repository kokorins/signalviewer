#ifndef EPOCHFOLDER_H_
#define EPOCHFOLDER_H_
/**
 * \filename epochfolder.h
 * \brief Extracts the information about data in dsi folder to be
 * passed to epoch list.
 */
#include <vector>
#include <cstddef>
#include <boost/shared_ptr.hpp>
namespace epoch {
class IEpochListProvider;
struct EpochFileInfo;
/**
 * Structure that uniqually identifies the channel
 */
struct ChannelId {
  std::string file;
  size_t idx;
  ChannelId(const std::string& file_, size_t idx_) : file(file_), idx(idx_) {}
};

/**
 * \brief scan folder to separate subjects, their channels and matching files.
 * Assumptions:
 * - Each file could contain number of channels of different or same rates
 * - There could be number of files belongs to same animal with different channels
 * - There couble be more then one file with same channel for one animal with different data stamps
 */
class EpochFolder {
public:
  EpochFolder();
  /**
   * Scan directory and collect information about files of a given type
   */
  void scan(const std::string &filder, const epoch::IEpochListProvider &elp);
  /**
   * \brief get all subject in a directory
   */
  void subjects(std::vector<std::string>& subjs);
  /**
   * \brief get all channels for given subject in the directory
   */
  void channels(const std::string& subj, std::vector<std::string>& channs);
  /**
   * \brief list all files for given subject, channel
   * sort according to start date.
   */
  void files(const std::string& subj, const std::string& chan,
             bool only_name, std::vector<ChannelId>& files);
  /**
   * \brief Extract all available information about the file as a string
   */
  std::string fileInfo(const std::string& file_name, int idx);
  /**
   * @brief properties specify additional properties for different type of files
   * @param prev_prop previous value
   * @param parent QWidget parent
   * @return new propererties json string
   */
  void properties(const EpochFileInfo &cur_info, void* parent, std::vector<EpochFileInfo> &fis)const;
  std::string defaultProperties(const EpochFileInfo& cur_info)const;
private:
  EpochFolder(const EpochFolder&){}
  EpochFolder& operator=(const EpochFolder&) {return *this;}
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace epoch
#endif //EPOCHFOLDER_H_
