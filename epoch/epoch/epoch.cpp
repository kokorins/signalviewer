#include "epoch.h"
#include <loglite/logging.hpp>
#include <boost/date_time.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <map>
namespace epoch {
const std::string Epoch::kTypeId = "signalviewer.epoch.Epoch.1";
struct Epoch::Impl {
  typedef std::map<std::string, std::vector<double> > TEpochData;
  double _epoch_length; // epoch length in seconds
  TEpochData _data;
  boost::posix_time::ptime _start_time;
};

Epoch::Epoch() :_impl(new Impl()) {}

Epoch::Epoch(const Epoch&rhs):_impl(new Impl(*rhs._impl)) {}

Epoch &Epoch::operator =(const Epoch & rhs)
{
  if(&rhs!=this)
    _impl.reset(new Impl(*rhs._impl));
  return *this;
}

Epoch::~Epoch() {}

bool Epoch::has(const std::string& tp) const
{
  return _impl->_data.find(tp)!=_impl->_data.end();
}

void Epoch::set(const std::string& tp, const std::vector<double>& val)
{
  Impl::TEpochData::iterator it = _impl->_data.find(tp);
  if(it==_impl->_data.end())
    _impl->_data.insert(it, std::make_pair(tp, val));
  else
    it->second = val;
}

void Epoch::swap(const std::string& tp, std::vector<double>& val)
{
  Impl::TEpochData::iterator it = _impl->_data.find(tp);
  if(it==_impl->_data.end())
    _impl->_data.insert(it, std::make_pair(tp, val));
  else
    std::swap(it->second, val);
}

std::vector<double> const* Epoch::get(const std::string& tp) const
{
  Impl::TEpochData::const_iterator it = _impl->_data.find(tp);
  if(it==_impl->_data.end()) {
    LOGLITE_LOG_L1("Try to read unconsistent type of data of a type "<<tp);
    return 0;
  }
  else
    return &(it->second);
}

const boost::posix_time::ptime& Epoch::startTime() const
{
  return _impl->_start_time;
}

void Epoch::setStartTime(const boost::posix_time::ptime& timestamp)
{
  _impl->_start_time = timestamp;
}

double Epoch::epochLength() const
{
  return _impl->_epoch_length;
}

void Epoch::setEpochLength(double epoch_len)
{
  _impl->_epoch_length = epoch_len;
}

std::string Epoch::toString() const
{
  using namespace boost::property_tree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("start_date",_impl->_start_time.date());
  pt.put("start_time",_impl->_start_time.time_of_day());
  pt.put("epoch_length", _impl->_epoch_length);
  for(auto it = _impl->_data.begin(); it!=_impl->_data.end(); ++it) {
    ptree chan_pt;
    chan_pt.put("name", (*it).first);
    for(size_t i=0; i<(*it).second.size(); ++i)
      chan_pt.add("val",(*it).second[i]);
    pt.add_child("channel", chan_pt);
  }
  std::stringstream ss;
  json_parser::write_json(ss, pt);
  return ss.str();
}

bool Epoch::fromString(const std::string &json)
{
  using namespace boost::property_tree;
  std::stringstream ss(json);
  ptree pt;
  try {
    json_parser::read_json(ss, pt);
  }
  catch(json_parser::json_parser_error& error) {
    LOGLITE_LOG_L1(error.what());
    return false;
  }
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId) {
    LOGLITE_LOG_L1("Incorrect type_id: "<<type_str);
    return false;
  }
  try {
    boost::gregorian::date dt = pt.get<boost::gregorian::date>("start_date");
    boost::posix_time::time_duration tm = pt.get<boost::posix_time::time_duration>("start_time");
    double epoch_len = pt.get<double>("epoch_length");
    Impl::TEpochData data;
    BOOST_FOREACH(const ptree::value_type &chan_it, pt.get_child("channel")) {
      const ptree& chan_pt = chan_it.second;
      std::string chan_name = chan_pt.get<std::string>("name");
      std::vector<double> chan;
      BOOST_FOREACH(const ptree::value_type &val_it, chan_pt.get_child("val")) {
        chan.push_back(val_it.second.get<double>("val"));
      }
      data.insert(std::make_pair(chan_name,chan));
    }
    _impl->_start_time = boost::posix_time::ptime(dt, tm);
    _impl->_epoch_length = epoch_len;
    _impl->_data = data;
  }
  catch(ptree_bad_data& ex) {
    LOGLITE_LOG_L1(ex.what());
    return false;
  }
  return true;
}

boost::posix_time::ptime Epoch::endTime() const
{
  using namespace boost::posix_time;
  ptime res = _impl->_start_time;
  res+=microseconds(_impl->_epoch_length*1e6);
  return res;
}
} //namespace epoch
