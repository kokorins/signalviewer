#include "epochlist.h"
#include "epoch.h"
#include "typedlist.h"
#include <interfaces/fileformats/epochlistimpl.h>
#include <interfaces/fileformats/epochlistprovider.h>
#include <loglite/logging.hpp>
#include <map>
#include <set>
#include <cstddef>

namespace epoch {

EpochList::EpochList() {}

EpochList::~EpochList() {}

size_t EpochList::epochLength() const
{
  if(!_impl)
    return 0;
  return _impl->epochLen();
}

void EpochList::setImpl(IEpochListImpl *impl)
{
  _impl=std::unique_ptr<IEpochListImpl>(impl);
}

bool EpochList::isOpen() const
{
  if(_impl)
    return true;
  return false;
}

bool EpochList::open(const IEpochListProvider &ielp, size_t epoch_len, const TFileNames& files_paths)
{
  _impl = std::unique_ptr<IEpochListImpl>(new TypedList(epoch_len, ielp));
  if(!_impl)
    return false;
  return _impl->open(files_paths);
}

size_t EpochList::size() const
{
  if(!_impl)
    return 0;
  return _impl->size();
}

bool EpochList::read(size_t idx, Epoch& epoch)
{
  if(!_impl)
    return false;
  return _impl->read(idx, epoch);
}

const std::set<std::string>& EpochList::activeTypes() const
{
  return _impl->activeTypes();
}

size_t EpochList::idxFromTimestamp(const boost::posix_time::ptime &dt) const
{
  return _impl->idxFromTimestamp(dt);
}

boost::posix_time::ptime EpochList::timeFromIdx(size_t idx) const
{
  return _impl->timeFromIdx(idx);
}
} //namespace epoch
