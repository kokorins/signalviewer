#ifndef TYPEDLIST_H
#define TYPEDLIST_H
#include <interfaces/fileformats/epochlistimpl.h>
#include <interfaces/fileformats/epochfiles.h>
#include <interfaces/fileformats/epochlistprovider.h>
#include <boost/shared_ptr.hpp>
#include <set>
#include <map>
#include <cstddef>
#include <vector>

namespace epoch {
class TypedList : public IEpochListImpl {
  typedef boost::shared_ptr<epoch::IEpochFiles> TFilesPtr;
  typedef std::map<std::string, TFilesPtr> TFileTypes;
  typedef std::set<std::string> TActiveTypes;
public:
  explicit TypedList(size_t epoch_length, const IEpochListProvider& elp): _elp(elp), _epoch_length(epoch_length) {}
  bool open(const std::map<std::string, std::vector<EpochFileInfo> >& files_paths);
  size_t size()const;
  bool read(size_t idx, Epoch& epoch);
  const std::set<std::string>& activeTypes()const;
  void setEpochLen(size_t epoch_len);
  size_t epochLen()const;
  boost::posix_time::ptime timeFromIdx(size_t idx)const;
  size_t idxFromTimestamp(const boost::posix_time::ptime &dt) const;
private:
  TypedList(const TypedList& rhs) : _elp(rhs._elp){}
  TypedList&operator=(const TypedList&) {return *this;}
private:
  const IEpochListProvider& _elp;
  size_t _epoch_length;
  TFileTypes _file_types;
  TActiveTypes _active_types;
  size_t _size_sec;
};
}
#endif // TYPEDLIST_H
