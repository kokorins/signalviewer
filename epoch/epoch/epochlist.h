#ifndef EPOCHLIST_H_
#define EPOCHLIST_H_
#include "epochtype.h"
#include <interfaces/fileformats/epochfileinfo.h>
#include <map>
#include <vector>
#include <set>
#include <memory>
#include <boost/date_time.hpp>
/// \filename epochlist.h
namespace epoch {
  class Epoch;
  class IEpochListImpl;
  class IEpochListProvider;

/**
 * Interface for list of epochs
 */
class EpochList {
public:
  typedef std::map<std::string, std::vector<EpochFileInfo> > TFileNames;
public:
  EpochList();
  ~EpochList();
  void setImpl(IEpochListImpl* impl);
  bool isOpen()const;
  /**
   * Open files for each type of signal.
   * \param files_paths contains a lists of filepaths for each of signal types
   * \return true if everything is ok
   */
  bool open(const IEpochListProvider& aelip, size_t epoch_len, const TFileNames& files_paths);
  /**
   * Reads the data to the epoch class
   * \param idx index of the epoch in list
   * \param epoch output epoch class value
   * \return if everything is ok
   */
  bool read(size_t idx, Epoch& epoch);
  /**
   * \return Number of epoch in list
   */
  size_t size()const; 
  /**
   * \return Epoch length in seconds
   */
  size_t epochLength()const;

  /**
   * \return active opened types of files included in epoch
   */
  const std::set<std::string>& activeTypes()const;
  /**
   * Search for idx by date time
   */
  size_t idxFromTimestamp(const boost::posix_time::ptime& dt)const;
  boost::posix_time::ptime timeFromIdx(size_t idx)const;
public:
  class Impl;
private:
  std::unique_ptr<IEpochListImpl> _impl;
};
} //namespace epoch
#endif //EPOCHLIST_H_
