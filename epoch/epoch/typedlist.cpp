#include "typedlist.h"
#include "epoch.h"
#include <loglite/logging.hpp>
#include <future>
#include <thread>
namespace epoch {
bool TypedList::open(const std::map<std::string, std::vector<EpochFileInfo> >& files_paths)
{
  typedef std::map<std::string, std::vector<EpochFileInfo> > TFileNames;
  _size_sec = 0;
  _file_types.clear();
  _active_types.clear();
  TFileNames::const_iterator it = files_paths.begin();
  for(;it != files_paths.end(); ++it) {
    TFilesPtr fp = _elp.createFiles();
    fp->open(it->second);
    _size_sec = std::max(fp->size(), _size_sec);
    _file_types.insert(std::make_pair(it->first, fp));
    _active_types.insert(it->first);
  }
  return true;
}

size_t TypedList::size() const
{
  return _size_sec/epochLen();
}

bool TypedList::read(size_t idx, Epoch& epoch)
{
  std::vector<std::string> names;
  std::vector<std::future<std::vector<double> > > futures;
  size_t epoch_len = epochLen();
  for(auto it = _file_types.begin(); it!=_file_types.end(); ++it) {
    TFilesPtr file = it->second;
    boost::posix_time::ptime t;
    it->second->getTimestamp(epoch_len*idx, t);
    epoch.setStartTime(t);
    names.push_back(it->first);
    std::packaged_task<std::vector<double>()> pt(
          boost::bind(&epoch::IEpochFiles::read, file.get(), epoch_len*idx, epoch_len));
    futures.push_back(pt.get_future());
    std::thread(std::move(pt));
  }
  try {
    for(size_t i=0; i<futures.size(); ++i) {
      std::vector<double> data = futures[i].get();
      if(!data.empty()) {
        epoch.swap(names[i], data);
        epoch.setEpochLength(epoch_len);
      }
    }
  }
  catch(std::exception& ex) {
    LOGLITE_LOG_L1("Thread exception: "<<ex.what());
  }
  return true;
}

const std::set<std::string>& TypedList::activeTypes() const
{
  return _active_types;
}

void TypedList::setEpochLen(size_t epoch_len)
{
  _epoch_length = epoch_len;
}

size_t TypedList::epochLen() const
{
  return _epoch_length;
}

boost::posix_time::ptime TypedList::timeFromIdx(size_t idx) const
{
  if(_file_types.empty())
    return boost::posix_time::ptime();
  TFileTypes::const_iterator it = _file_types.begin();
  TFilesPtr f = it->second;
  boost::posix_time::ptime t;
  f->getTimestamp(idx/epochLen(),t);
  return t;
}

size_t TypedList::idxFromTimestamp(const boost::posix_time::ptime &dt) const
{
  if(_file_types.empty())
    return size();
  TFileTypes::const_iterator it = _file_types.begin();
  TFilesPtr f = it->second;
  boost::posix_time::ptime t, last_t;
  f->getTimestamp(0, t);
  if(t>dt)
    return size();
  f->getTimestamp(_size_sec-1, last_t);
  last_t+=boost::posix_time::seconds(1);
  if(last_t<=dt)
    return size();
  size_t a = 0;
  size_t b = _size_sec;
  while(b-a>1) {
    if((a+b)/2>=_size_sec)
      t = last_t;
    else
      f->getTimestamp((a+b)/2, t);
    if(dt<t)
      b = (a+b)/2;
    else
      a = (a+b)/2;
  }
  return a/epochLen();
}
} //namespace epoch
