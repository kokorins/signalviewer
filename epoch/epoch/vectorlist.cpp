#include "vectorlist.h"
namespace epoch {

bool VectorList::open(const std::map<std::string, std::vector<std::pair<std::string, size_t> > >&) {return true;}

size_t VectorList::size() const
{
  return _epochs.size();
}

bool VectorList::read(size_t idx, Epoch &epoch)
{
  if(idx>=_epochs.size())
    return false;
  epoch = _epochs[idx];
  return true;
}

const std::set<std::string> &VectorList::activeTypes() const
{
  return _active_types;
}

void VectorList::swapEpochs(std::vector<Epoch>&epochs)
{
  std::swap(_epochs, epochs);
}

void VectorList::swapActiveTypes(std::set<std::string>& active_types)
{
  std::swap(_active_types, active_types);
}

} //namespace epoch
