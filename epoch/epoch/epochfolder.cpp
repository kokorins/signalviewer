#include "epochfolder.h"
#include <interfaces/fileformats/datafilescanner.h>
#include <interfaces/fileformats/epochlistimpl.h>
#include <interfaces/fileformats/epochlistprovider.h>
#include <interfaces/fileformats/epochfileinfo.h>
#include <loglite/logging.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <future>

namespace epoch {
namespace fs = boost::filesystem;
class EpochFolder::Impl {
public:
  typedef boost::posix_time::ptime TDateTime;

  typedef std::map<std::string, size_t> TRates;
  typedef std::map<TDateTime, ChannelId> TDateFiles;
  typedef std::map<std::string,  TDateFiles> TSignalFiles;
  typedef std::map<std::string, TSignalFiles> TSubjectsSignals;
public:
  void setScanner(const IEpochListProvider &elp);
  void setFolder(const std::string &folder);
  void clear();
  void handle(const std::string &file_path, const std::string &subj, const std::vector<std::string> &chans, const std::vector<TDateTime> &start_times);
  void subjects(std::vector<std::string>& subjs);
  void channels(const std::string& subj, std::vector<std::string>& channs);
  void files(const std::string& subj, const std::string& chan, bool only_name,
             std::vector<ChannelId>& files);
  std::string fileInfo(const std::string &file_name, int idx);
  void properties(const EpochFileInfo& prev_prop, void*, std::vector<EpochFileInfo>& fis)const;
  std::string defaultProperties(const EpochFileInfo &cur_info) const;
private:
  static bool Subject(const fs::path& file, std::string& subj);
  static bool Channel(const fs::path& file, std::string& channel,
                      TDateTime& start_time);
private:
  friend class EpochFolder;
private:
  fs::path _folder;
  TSubjectsSignals _subjects; // subject.channel.timestamp
  boost::shared_ptr<IDataFileScanner> _scanner;
};

void EpochFolder::Impl::setScanner(const IEpochListProvider &elp)
{
  _scanner = elp.createScanner();
}

void EpochFolder::Impl::setFolder(const std::string& folder)
{
  _folder = fs::path(folder);
}

void EpochFolder::Impl::clear()
{
  _subjects.clear();
}

void EpochFolder::Impl::handle(const std::string& file_path, const std::string&subj,
                               const std::vector<std::string>& chans, const std::vector<TDateTime>& start_times)
{
  TSubjectsSignals::iterator sit = _subjects.find(subj);
  if(sit==_subjects.end()) { // new subject
    TSignalFiles sig_file;
    for(size_t i=0; i<chans.size(); ++i) {
      TDateFiles date_file;
      date_file.insert(std::make_pair(start_times[i], ChannelId(file_path, i)));
      sig_file.insert(std::make_pair(chans[i], date_file));
    }
    _subjects.insert(sit, std::make_pair(subj, sig_file));
  }
  else {
    for(size_t i=0; i<chans.size(); ++i) {
      TSignalFiles::iterator sig_it = (*sit).second.find(chans[i]);
      if(sig_it == (*sit).second.end()) { // new channel
        TDateFiles date_file;
        date_file.insert(std::make_pair(start_times[i], ChannelId(file_path,i)));
        (*sit).second.insert(sig_it, std::make_pair(chans[i], date_file));
      }
      else {
        TDateFiles::iterator dit = (*sig_it).second.find(start_times[i]);
        if(dit==(*sig_it).second.end()) // new timestamp
          (*sig_it).second.insert(std::make_pair(start_times[i], ChannelId(file_path,i)));
        else {// Duplicated files with same start time
          LOGLITE_LOG_L2(file_path<<" duplicates the "<<(*dit).second.file);
          return;
        }
      }
    }
  }
}

void EpochFolder::Impl::subjects( std::vector<std::string>& subjs )
{
  subjs.clear();
  subjs.reserve(_subjects.size());
  std::transform(_subjects.begin(), _subjects.end(), std::back_inserter(subjs),
                 boost::bind(&TSubjectsSignals::value_type::first, _1));
}

void EpochFolder::Impl::channels( const std::string& subj, std::vector<std::string>& channs)
{
  TSubjectsSignals::iterator it = _subjects.find(subj);
  if(it==_subjects.end())
    return;

  channs.clear();
  channs.reserve(it->second.size());
  std::transform(it->second.begin(), it->second.end(), std::back_inserter(channs),
                 boost::bind(&TSignalFiles::value_type::first, _1));
}

void EpochFolder::Impl::files(const std::string& subj, const std::string& chan, bool only_name,
                           std::vector<ChannelId> &files)
{
  TSubjectsSignals::iterator sit = _subjects.find(subj);
  if(sit==_subjects.end())
    return;

  TSignalFiles::iterator sig_it = sit->second.find(chan);
  if(sig_it==sit->second.end())
    return;

  files.clear();
  files.reserve(sig_it->second.size());
  if(only_name) {
    for(TDateFiles::iterator it = sig_it->second.begin(); it!=sig_it->second.end(); ++it) {
      fs::path f(it->second.file);
      files.push_back(ChannelId(f.filename().generic_string(),it->second.idx));
    }
  }
  else {
    std::transform(sig_it->second.begin(), sig_it->second.end(), std::back_inserter(files),
                   boost::bind(&TDateFiles::value_type::second, _1));
  }
}

std::string EpochFolder::Impl::fileInfo(const std::string &file_name, int idx)
{
  fs::path file(file_name);
  fs::path pt = _folder/file;
  return _scanner->info(pt, idx);
}

void EpochFolder::Impl::properties(const EpochFileInfo &prev_prop, void * parent, std::vector<EpochFileInfo>& fis) const
{
  _scanner->properties(prev_prop, parent, fis);
}

std::string EpochFolder::Impl::defaultProperties(const EpochFileInfo &cur_info) const
{
  return _scanner->defaultProperties(cur_info);
}


EpochFolder::EpochFolder() : _impl(new Impl()) { }

void EpochFolder::scan(const std::string& folder_str, const IEpochListProvider& elp)
{
  _impl->clear();
  _impl->setScanner(elp);
  _impl->setFolder(folder_str);
  fs::path folder(folder_str);
  std::vector<std::future<epoch::DataFileInfo> > futures;
  if(!fs::is_directory(folder)) {
    LOGLITE_LOG_L1(folder<<" is not a directory");
    return;
  }
  for(fs::directory_iterator it = fs::directory_iterator(folder);
      it!=fs::directory_iterator(); ++it) {
    if(fs::is_regular_file(*it)) {
      const fs::path& p = (*it);
      if(!_impl->_scanner)
        continue;
      std::packaged_task<epoch::DataFileInfo()> pt(boost::bind(&IDataFileScanner::handle, _impl->_scanner.get(), p));
      futures.push_back(pt.get_future());
      std::thread(std::move(pt));
    }
  }
  try {
    for(size_t i=0; i<futures.size(); ++i) {
      const epoch::DataFileInfo& efi = futures[i].get();
      if(!efi.is_ok) {
        LOGLITE_LOG_L2("Omitting file: "<<efi.file_name<<";");
        continue;
      }
      _impl->handle(efi.file_name, efi.subj, efi.channels, efi.start_times);
    }
  }  catch(std::exception& ex) {
    LOGLITE_LOG_L1("Thread exception: "<<ex.what());
  }
}

void EpochFolder::subjects(std::vector<std::string>& subjs)
{
  _impl->subjects(subjs);
}

void EpochFolder::channels( const std::string& subj, std::vector<std::string>& channs )
{
  _impl->channels(subj, channs);
}

void EpochFolder::files(const std::string& subj, const std::string& chan,
                      bool only_name, std::vector<ChannelId> &files)
{
  _impl->files(subj, chan, only_name, files);
}

std::string EpochFolder::fileInfo(const std::string &file_name, int idx)
{
  return _impl->fileInfo(file_name, idx);
}

void EpochFolder::properties(const EpochFileInfo& cur_info, void *parent, std::vector<EpochFileInfo>& fis) const
{
  _impl->properties(cur_info, parent, fis);
}

std::string EpochFolder::defaultProperties(const EpochFileInfo &cur_info) const
{
  return _impl->defaultProperties(cur_info);
}
} //namespace epoch
