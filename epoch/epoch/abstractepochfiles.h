#ifndef ABSTRACTEPOCHFILES_H
#define ABSTRACTEPOCHFILES_H
#include <vector>
#include <deque>
#include <cstddef>
#include <memory>
namespace boost {
  namespace posix_time {
    class ptime;
  }
}
namespace epoch {
struct EpochFileInfo;
/**
 *  TFile properties:
 *    - TFile()
 *    - bool open(char const*const)
 *    - void setIdx(size_t)
 *    - void close()
 *    - bool isOpen()
 */
template <typename TFile>
class AbstractEpochFiles {
typedef std::shared_ptr<TFile> TFilePtr;
public:
  virtual ~AbstractEpochFiles() {}
  virtual bool open(const std::vector<epoch::EpochFileInfo>& file_names);
  virtual bool isOpen()const;
  virtual std::vector<double> read(const boost::posix_time::ptime& start, size_t dur_sec);
  virtual std::vector<double> read(size_t st_sec, size_t dur_sec);
  virtual void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  virtual size_t numSec() const;
  virtual void close();
private:
  bool open(size_t i);
  void idxToFileIdx(size_t idx, size_t &file_idx, size_t &inner_idx)const;
private:
  const static size_t kMaxOpened = 24;
private:
  std::vector<TFilePtr> _files;
  std::vector<std::string> _file_names;
  std::deque<size_t> _opened;
  std::vector<size_t> _num_secs;
  std::vector<size_t> _partial_sums;
};
} //namespace epoch
#endif // ABSTRACTEPOCHFILES_H
