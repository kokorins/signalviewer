#ifndef EPOCHTYPE_H_
#define EPOCHTYPE_H_
#include <string>
namespace epoch {
  enum EDefaultChannelType {
    EEG,
    EEGA,
    EMG,
    EOG,
    ECG,
    Activity,
    ActivityA,
    SignalStrenght,
    HeartRate,
    Temperature,
    Temperature2,
    Voltage,
    X,
    Y,
    Z,
    EFileTypeNum
  };
  std::string ToString(EDefaultChannelType);
} //namespace epoch
#endif //EPOCHTYPE_H_
