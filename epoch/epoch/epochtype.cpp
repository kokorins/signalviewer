#include "epochtype.h"
namespace epoch {
std::string ToString(EDefaultChannelType tp)
{
  switch(tp) {
  break; case EEG:
    return std::string("EEG");
  break; case EEGA:
    return std::string("EEGA");
  break; case EMG:
    return std::string("EMG");
  break; case EOG:
    return std::string("EOG");
  break; case ECG:
    return std::string("ECG");
  break; case Activity:
    return std::string("Activity");
  break; case ActivityA:
    return std::string("ActivityA");
  break; case SignalStrenght:
    return std::string("Signal strenght");
  break; case HeartRate:
    return std::string("Heart rate");
  break; case Temperature:
    return std::string("Temperature");
  break; case Temperature2:
    return std::string("Temperature2");
  break; case Voltage:
    return std::string("Voltage");
  break; case X:
    return std::string("X");
  break; case Y:
    return std::string("Y");
  break; case Z:
    return std::string("Z");
  break; default:
    return "";
  }
}
}//namepace epoch
