#ifndef ABSTRACTEPOCHFILES_HPP
#define ABSTRACTEPOCHFILES_HPP
#include "abstractepochfiles.h"
#include <interfaces/fileformats/epochfileinfo.h>
namespace epoch {
template <typename TFile>
bool AbstractEpochFiles<TFile>::open(const std::vector<epoch::EpochFileInfo>& file_names)
{
  _file_names.reserve(file_names.size());
  _files.reserve(file_names.size());
  _num_secs.reserve(file_names.size());
  for(size_t i=0; i<file_names.size(); ++i) {
    std::unique_ptr<TFile> file(new TFile());
    if(file->open(file_names[i].file_path.c_str())) {
      file->setIdx(file_names[i].channel_idx);
      _file_names.push_back(file_names[i].file_path);
      _num_secs.push_back(file->sizeSec());
      file->close();
      _files.push_back(TFilePtr(file.release()));
    }
  }
  if(_files.empty())
    return false;
  _partial_sums.resize(_num_secs.size());
  std::partial_sum(_num_secs.begin(), _num_secs.end(), _partial_sums.begin());
  return true;
}

template <typename TFile>
bool AbstractEpochFiles<TFile>::open(size_t idx)
{
  auto it = std::find(_opened.begin(), _opened.end(), idx);
  if(it==_opened.end()) {
    if(_opened.size()>=kMaxOpened) {
      size_t old_idx = _opened.front();
      _files[old_idx]->close();
      _opened.pop_front();
    }
    if(_files[idx]->open(_file_names[idx].c_str()))
      _opened.push_back(idx);
    else
      return false;
  }
  return _files[idx]->isOpen();
}

template <typename TFile>
void AbstractEpochFiles<TFile>::close()
{
  for(size_t i=0; i<_files.size(); ++i)
    if(_files[i].get() && _files[i]->isOpen())
      _files[i]->close();
}

template <typename TFile>
std::vector<double> AbstractEpochFiles<TFile>::read(size_t st_sec, size_t dur_sec)
{
  if(!isOpen() || st_sec+dur_sec>=_partial_sums.back())
    return std::vector<double>();
  size_t file_idx = 0;
  size_t idx = 0;
  idxToFileIdx(st_sec, file_idx, idx);
  open(file_idx);
  return _files[file_idx]->read(idx, dur_sec);
}

template <typename TFile>
size_t AbstractEpochFiles<TFile>::numSec() const
{
  if(_partial_sums.empty())
    return 0;
  return _partial_sums.back();
}

template <typename TFile>
void AbstractEpochFiles<TFile>::idxToFileIdx(size_t idx, size_t &file_idx, size_t &inner_idx)const
{
  file_idx = 0;
  while(idx>=_partial_sums[file_idx])
    ++file_idx;
  inner_idx = idx;
  if(file_idx>=1)
    inner_idx -= _partial_sums[file_idx-1];
}

template <typename TFile>
bool AbstractEpochFiles<TFile>::isOpen() const
{
  return true;
}

template <typename TFile>
std::vector<std::pair<boost::posix_time::ptime, boost::posix_time::ptime> >
AbstractEpochFiles<TFile>::lapses() const
{

}

std::vector<double> AbstractEpochFiles::read(const boost::posix_time::ptime &start, size_t dur_sec)
{
  if(!isOpen() || st_sec+dur_sec>=_partial_sums.back())
    return std::vector<double>();
  size_t file_idx = 0;
  size_t idx = 0;
  idxToFileIdx(st_sec, file_idx, idx);
  open(file_idx);
  return _files[file_idx]->read(idx, dur_sec);
}

template <typename TFile>
void AbstractEpochFiles<TFile>::getTimestamp(size_t sec, boost::posix_time::ptime& t)
{
  if(!isOpen())
    return;
  size_t file_idx = 0;
  size_t idx = 0;
  if(sec>=_partial_sums.back()) {
    file_idx = _files.size()-1;
    idx = sec-_partial_sums.back();
  }
  else {
    idxToFileIdx(sec, file_idx, idx);
  }
  open(file_idx);
  t = _files[file_idx]->getTime(idx);
}
} //namespace epoch
#endif // ABSTRACTEPOCHFILES_HPP
