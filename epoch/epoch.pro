QT -= qt core gui
TARGET = epoch
TEMPLATE = lib
CONFIG += staticlib
DESTDIR = $$OUT_PWD/../
include(../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../ \
  $$PWD/../algos/ \
  $$PWD/../algos/treap

DEPENDPATH += \
  $$PWD/../algos\
  $$PWD/../algos/treap
LIBS += -L$$OUT_PWD/.. -lalgos

win32:PRE_TARGETDEPS += $$OUT_PWD/../algos.lib
else:PRE_TARGETDEPS += $$OUT_PWD/../libalgos.a

HEADERS += \
    epoch/vectorlist.h \
    epoch/epochtype.h \
    epoch/epochlist.h \
    epoch/epochfolder.h \
    epoch/epoch.h \
    epoch/typedlist.h \
    event/eventlistinfo.h \
    event/eventinfo.h \
    event/eventannotation.h \
    event/eventinfosummary.h \
    event/eventinfosummary.hpp \
    epoch/abstractepochfiles.h \
    epoch/abstractepochfiles.hpp

SOURCES += \
    epoch/vectorlist.cpp \
    epoch/epochtype.cpp \
    epoch/epochlist.cpp \
    epoch/epochfolder.cpp \
    epoch/epoch.cpp \
    epoch/typedlist.cpp \
    event/eventlistinfo.cpp \
    event/eventinfo.cpp \
    event/eventannotation.cpp \
    event/eventinfosummary.cpp
