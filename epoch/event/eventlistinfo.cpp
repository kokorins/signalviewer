#include "eventlistinfo.h"
#include "eventinfo.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace event {
const std::string EventListInfo::kTypeId = "signalviewer.event.EventListInfo.1";
void EventListInfo::setInfo(size_t idx, const EventInfo &info)
{
  _annos[idx] = info;
}

bool EventListInfo::info(size_t idx, EventInfo &info) const
{
  std::map<size_t, EventInfo>::const_iterator it = _annos.find(idx);
  if(it==_annos.end())
    return false;
  info = it->second;
  return true;
}

const std::map<size_t, EventInfo> &EventListInfo::annos() const
{
  return _annos;
}

void EventListInfo::saveInfo(const char *const file_name) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  ptree annos;
  std::map<size_t, EventInfo>::const_iterator it = _annos.begin();
  for(; it!=_annos.end(); ++it) {
    std::stringstream ss(it->second.toString());
    ptree anno;
    boost::property_tree::json_parser::read_json(ss, anno);
    anno.put("idx", it->first);
    annos.push_back(std::make_pair("anno", anno));
  }
  pt.put_child("annos", annos);
  boost::property_tree::json_parser::write_json(file_name, pt);
}

bool EventListInfo::loadInfo(const char *const file_name)
{
  using boost::property_tree::ptree;
  ptree pt;
  boost::property_tree::json_parser::read_json(file_name, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId)
    return false;
  BOOST_FOREACH(const ptree::value_type &annos_it, pt.get_child("annos")) {
    const ptree& anno = annos_it.second;
    std::stringstream ss;
    boost::property_tree::json_parser::write_json(ss, anno);
    std::string json = ss.str();
    EventInfo ei;
    ei.fromString(json);
    size_t idx = anno.get<size_t>("idx");
    _annos[idx] = ei;
  }
  return true;
}
} //namespace event
