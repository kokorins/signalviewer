#ifndef EVENTANNOTATION_H
#define EVENTANNOTATION_H
#include "eventinfo.h"
#include <treap/treap.h>
#include <event/event.h>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
#include <mutex>
//#include <boost/thread.hpp>

#include <map>
#include <set>
#include <cstddef>

namespace event {
typedef EventNode<EventInfo> Tag;
class EventAnnotation {
  const static std::string kTypeId;

public:
  EventAnnotation();
  /**
   * Annotation
   * \param idx index of an epoch in a list
   * \param info epoch tags and annotation information
   * \return if there is info for the given epoch
   */
  std::vector<Tag> anno(const Tag::TTime& start, const Tag::TTime& end) const;
  std::vector<Tag> tags(const Tag::TTime& start, const Tag::TTime& end) const;

  std::set<std::string> listTags()const;
  void updateAnno(const Tag::TTime &start, const Tag::TTime &end,
                  const std::vector<EventInfo> &info);
  void updateTags(const Tag::TTime &start, const Tag::TTime &end,
                  const std::vector<EventInfo> &info);
  std::string toString()const;
  bool fromString(const std::string& json);
  /**
   * Save annotation info into file
   */
  void saveInfo(char const*const file_name)const;
  /**
   * Load annotation information from file
   */
  bool loadInfo(char const*const file_name);
  std::vector<Tag> toVector()const;
public:
  static bool IsAnno(const EventInfo&);
  static std::string ToString(const Tag&);
  /**
   * @brief FromString
   * @param json
   * @return
   * @throw ptree_error
   */
  static Tag FromString(const std::string& json);
private:
  std::map<std::string, Tag::Treap::TTreapPtr> _tags;
  Tag::Treap::TTreapPtr _annos;
  mutable std::mutex _access;
//  mutable std::shared_mutex _access;
};

} //namespace event
#endif // EVENTANNOTATION_H
