#ifndef EVENTINFO_H
#define EVENTINFO_H
#include <set>
#include <string>
#include <map>
#include <vector>
namespace event {
/**
 * Additional info about epochs, it's attached to epochs and could be indexed
 * Tags could be in any case and its differes them.
 * Annotation is simple text, which could be attached to any epoch
 * (more than one word)
 */
class EventInfo {
  const static std::string kTypeId;
public:

  EventInfo() {}
  explicit EventInfo(const std::string& tag) : _tag(tag) {}
  const std::string& tag()const;
  void setTag(const std::string&tag);

  std::string toString()const;
  bool fromString(const std::string& json);
public:
  bool friend operator==(const EventInfo& lhs, const EventInfo& rhs);
  bool friend operator<(const EventInfo& lhs, const EventInfo& rhs);
private:
  std::string _tag;
};
bool operator==(const EventInfo& lhs, const EventInfo& rhs);
bool operator<(const EventInfo& lhs, const EventInfo& rhs);
} // namespace event
#endif // EVENTINFO_H
