#include "eventinfosummary.h"
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace event {
const std::string EventInfoSummary::kTagsId = "signalviewer.event.Tags.1";

void EventInfoSummary::TagsToString(const std::set<std::string> &tags,
                                    const std::map<std::string, size_t>& c,
                                    std::string &json)
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTagsId);
  std::set<std::string>::const_iterator it = tags.begin();
  for(; it!=tags.end(); ++it)
    pt.add(std::string("tag"), *it);
  ptree pt_colors;
  std::map<std::string,size_t>::const_iterator mit = c.begin();
  for(;mit!=c.end(); ++mit)
    pt_colors.put((*mit).first, (*mit).second);
  pt.put_child("colors", pt_colors);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

bool EventInfoSummary::StringToTags(const std::string &json, std::map<std::string, size_t> &c, std::set<std::string> &tags)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  tags.clear();
  c.clear();
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTagsId)
    return false;
  BOOST_FOREACH(ptree::value_type &v, pt) {
    if(v.first=="tag")
      tags.insert(v.second.data());
  }
  BOOST_FOREACH(ptree::value_type& v, pt.get_child("colors")) {
    c.insert(std::make_pair(v.first,v.second.get<size_t>("")));
  }
  return true;
}
} //namespace event
