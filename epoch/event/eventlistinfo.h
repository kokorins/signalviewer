#ifndef EVENTLISTINFO_H
#define EVENTLISTINFO_H
#include <map>
#include <cstddef>
#include <string>
namespace event {
class EventInfo;
class EventListInfo {
static const std::string kTypeId;
public:
  void setInfo(size_t idx, const EventInfo& info);
  /**
   * Gets the annotation and tags for a given epoch
   * \return true if there is info for current epoch
   */
  bool info(size_t idx, EventInfo& info) const;
  const std::map<size_t, EventInfo> & annos()const;
  void saveInfo(char const*const file_name)const;
  bool loadInfo(char const*const file_name);
private:
  std::map<size_t, EventInfo> _annos;
};

} //namespace event
#endif // EVENTLISTINFO_H
