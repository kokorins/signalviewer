#ifndef EVENTINFOSUMMARY_H
#define EVENTINFOSUMMARY_H
#include <set>
#include <map>
#include <string>

namespace event {
class EventInfoSummary {
public:
//  template <typename iterator>
//  static std::set<std::string> ListTags(iterator begin, iterator end);
public:
  const static std::string kTagsId;
  static void TagsToString(const std::set<std::string>& tags,
                           const std::map<std::string, size_t>& c,
                           std::string& json);
  static bool StringToTags(const std::string& json,
                           std::map<std::string, size_t>& c,
                           std::set<std::string>& tags);
};
} //namespace event
#endif // EVENTINFOSUMMARY_H
