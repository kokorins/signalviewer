#include "eventannotation.h"
#include "eventinfosummary.hpp"
#include <treap/treap.hpp>
#include <loglite/logging.hpp>
#include <event/event.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/date_time.hpp>
#include <boost/algorithm/string.hpp>

namespace event {
const std::string EventAnnotation::kTypeId = "signalviewer.event.EventAnnotation.2";

EventAnnotation::EventAnnotation() : _annos(Tag::Treap::Create()) {}

std::vector<Tag> EventAnnotation::anno(const Tag::TTime& start, const Tag::TTime& end) const
{
  std::unique_lock<std::mutex> lock(_access);
//  std::shared_lock<std::shared_mutex> lock(_access);
  return Tag::Tags(_annos, start, end);
}

std::vector<Tag> EventAnnotation::tags(const Tag::TTime &start, const Tag::TTime &end) const
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::shared_lock<boost::shared_mutex> lock(_access);
  std::vector<Tag> res;
  for(auto it=_tags.begin(); it!=_tags.end(); ++it) {
    const std::vector<Tag>& tag = Tag::Tags((*it).second, start, end);
    res.insert(res.end(), tag.begin(), tag.end());
  }
  return res;
}

std::set<std::string> EventAnnotation::listTags() const
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::shared_lock<boost::shared_mutex> lock(_access);
  std::set<std::string> tags;
  for(auto it=_tags.begin(); it!=_tags.end(); ++it)
    tags.insert((*it).first);
  return tags;
}

Tag::Treap::TTreapPtr UpdateTreap(Tag::Treap::TTreapPtr t, Tag new_node)
{
  auto nodes = Tag::Tags(t, new_node.start, new_node.end);
  if(!nodes.empty()) {
    for(size_t i=0; i<nodes.size(); ++i)
      t = Tag::Treap::Remove(t, Tag::Key(nodes[i]));
    nodes = Tag::MergeNodes(nodes);
    for(size_t i=0; i<nodes.size(); ++i)
      t = Tag::Treap::Add(t, nodes[i]);
  }
  else {
    return Tag::Treap::Add(t, new_node);
  }
  return t;
}

void EventAnnotation::updateAnno(const Tag::TTime &start,
                                 const Tag::TTime &end,
                                 const std::vector<EventInfo> &infos)
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::unique_lock<boost::shared_mutex> lock(_access);
  BOOST_FOREACH(const EventInfo&info, infos) {
    Tag new_node(start, end, info);
    _annos = Tag::Treap::Remove(_annos, Tag::Key(new_node));
    _annos = Tag::Treap::Add(_annos, new_node);
  }
}

void EventAnnotation::updateTags(const Tag::TTime &start,
                                 const Tag::TTime &end,
                                 const std::vector<EventInfo> &infos)
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::unique_lock<boost::shared_mutex> lock(_access);
  for(auto it=_tags.begin(); it!=_tags.end(); ++it) {
    auto tags = Tag::Tags((*it).second, start, end);
    BOOST_FOREACH(Tag t, tags) {
      (*it).second = Tag::Treap::Remove((*it).second, Tag::Key(t));
      if(t.start<start) {
        Tag st_t(t);
        t.end = start;
        (*it).second = Tag::Treap::Add((*it).second,st_t);
      }
      if(end<t.end) {
        Tag ed_t(t);
        t.start = end;
        (*it).second = Tag::Treap::Add((*it).second, ed_t);
      }
    }
  }
  BOOST_FOREACH(const EventInfo&info, infos) {
    Tag new_node(start, end, info);
    auto it = _tags.find(info.tag());
    if(it==_tags.end()) {
      _tags.insert(std::make_pair(info.tag(), Tag::Treap::Add(Tag::Treap::Create(), new_node)));
    }
    else {
      (*it).second = UpdateTreap((*it).second, new_node);
    }
  }
}

std::string EventAnnotation::toString()const
{
  std::unique_lock<std::mutex> lock(_access);
  //  boost::shared_lock<boost::shared_mutex> lock(_access);
  using namespace boost::property_tree;
  ptree pt;
  pt.put("type_id", kTypeId);
  ptree annos;
  for(auto it = Tag::Treap::Begin(_annos);
      it!=Tag::Treap::End(); ++it) {
    std::string json = ToString(*it);
    std::stringstream ss(json);
    ptree anno;
    json_parser::read_json(ss, anno);
    annos.push_back(std::make_pair("anno", anno));
  }
  pt.put_child("annos", annos);
  for(auto tit = _tags.begin(); tit!= _tags.end(); ++tit) {
    ptree tags;
    tags.put("name", (*tit).first);
    for(auto it = Tag::Treap::Begin((*tit).second);
        it!=Tag::Treap::End(); ++it) {
      std::string json = ToString(*it);
      std::stringstream ss(json);
      ptree tag;
      json_parser::read_json(ss, tag);
      tags.push_back(std::make_pair("tag", tag));
    }
    pt.add_child("tags", tags);
  }
  std::stringstream ss;
  json_parser::write_json(ss, pt);
  return ss.str();
}

bool EventAnnotation::fromString(const std::string &json)
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::unique_lock<boost::shared_mutex> lock(_access);
  using namespace boost::property_tree;
  ptree pt;
  std::stringstream ss(json);
  json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str != kTypeId)
    return false;
  _annos = Tag::Treap::Create();
  BOOST_FOREACH(const ptree::value_type &annos_it, pt.get_child("annos")) {
    const ptree& anno = annos_it.second;
    std::stringstream ss;
    json_parser::write_json(ss, anno);
    std::string json = ss.str();
    try {
      Tag en = FromString(json);
      _annos = Tag::Treap::Add(_annos, en);
    }
    catch(ptree_error& ex) {
      LOGLITE_LOG_L1(ex.what());
      return false;
    }
  }
  _tags.clear();
  BOOST_FOREACH(const ptree::value_type &tags_it, pt.equal_range("tags")) {
    const ptree& tg = tags_it.second;
    auto tag_name = tg.get<std::string>("name");
    auto tag = Tag::Treap::Create();
    BOOST_FOREACH(const ptree::value_type& tag_it, tg.equal_range("tag")) {
      std::stringstream ss;
      json_parser::write_json(ss, tag_it.second);
      std::string json = ss.str();
      try {
        Tag en = FromString(json);
        tag = Tag::Treap::Add(tag, en);
      }
      catch(ptree_error&ex) {
        LOGLITE_LOG_L1(ex.what());
        return false;
      }
    }
    _tags.insert(std::make_pair(tag_name,tag));
  }

  return true;
}

void EventAnnotation::saveInfo(const char *const file_name) const
{
  std::ofstream fout;
  fout.open(file_name);
  if(!fout.is_open())
    return;
  fout<<toString(); // lock inside toString();
}

bool EventAnnotation::loadInfo(const char *const file_name)
{
  std::ifstream fin;
  fin.open(file_name);
  if(!fin.is_open())
    return false;
  fin.unsetf(std::ios::skipws);
  std::stringstream ss;
  std::copy(std::istream_iterator<char>(fin), std::istream_iterator<char>(), std::ostream_iterator<char>(ss));
  return fromString(ss.str());//lock inside fromString()
}

std::vector<Tag> EventAnnotation::toVector() const
{
  std::unique_lock<std::mutex> lock(_access);
//  boost::shared_lock<boost::shared_mutex> lock(_access);
  std::vector<Tag> res;
  for(Tag::Treap::iterator it = Tag::Treap::Begin(_annos); it!=Tag::Treap::End(); ++it)
    res.push_back((*it));
  for(auto tit = _tags.begin(); tit!=_tags.end(); ++tit) {
    for(auto it = Tag::Treap::Begin((*tit).second); it!= Tag::Treap::End(); ++it) {
      res.push_back(*it);
    }
  }
  return res;
}

bool EventAnnotation::IsAnno(const EventInfo &info)
{
  std::vector<std::string> res;
  boost::algorithm::split(res, info.tag(),boost::algorithm::is_any_of(" ,\t\n"));
  return res.size()>1;
}

std::string EventAnnotation::ToString(const Tag & node)
{
  using namespace boost::property_tree;
  ptree pt;
  pt.put("start_date", node.start.date());
  pt.put("start_time", node.start.time_of_day());
  pt.put("end_date", node.end.date());
  pt.put("end_time", node.end.time_of_day());
  ptree tag_pt;
  std::stringstream tag_ss(node.tag.toString());
  json_parser::read_json(tag_ss, tag_pt);
  pt.put_child("tag",tag_pt);
  std::stringstream ss;
  json_parser::write_json(ss, pt);
  return ss.str();
}

Tag EventAnnotation::FromString(const std::string &json)
{
  using namespace boost::property_tree;
  ptree pt;
  try {
    std::stringstream ss(json);
    json_parser::read_json(ss,pt);
    auto sdt = pt.get<boost::gregorian::date>("start_date");
    auto stm = pt.get<boost::posix_time::time_duration>("start_time");
    auto edt = pt.get<boost::gregorian::date>("end_date");
    auto etm = pt.get<boost::posix_time::time_duration>("end_time");
    auto start = boost::posix_time::ptime(sdt, stm);
    auto end = boost::posix_time::ptime(edt, etm);
    auto tag_pt = pt.get_child("tag");
    std::stringstream tag_ss;
    json_parser::write_json(tag_ss, tag_pt);
    EventInfo ei;
    if(!ei.fromString(tag_ss.str())) {
      LOGLITE_LOG_L1("Wrong tag field");
      throw ptree_bad_data("Wrong tag field", tag_ss.str());
    }
    return Tag(start, end, ei);
  }
  catch(ptree_error& ex) {
    LOGLITE_LOG_L1(ex.what());
    throw;
  }
}

} //namespace event
