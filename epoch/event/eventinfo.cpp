#include "eventinfo.h"
#include <loglite/logging.hpp>
#include <iterator>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace event {
const std::string EventInfo::kTypeId = "signalviewer.epoch.EventInfo.2";

void EventInfo::setTag(const std::string &tag)
{
  _tag = tag;
}

const std::string &EventInfo::tag() const
{
  return _tag;
}

std::string EventInfo::toString() const
{
  using namespace boost::property_tree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("tag", _tag);
  std::stringstream ss;
  json_parser::write_json(ss, pt);
  return ss.str();
}

bool EventInfo::fromString(const std::string &json)
{
  using namespace boost::property_tree;
  std::stringstream ss(json);
  ptree pt;
  try {
    json_parser::read_json(ss, pt);
  }
  catch(json_parser_error&ex) {
    LOGLITE_LOG_L1(ex.what());
  }

  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  try {
    std::string type_str = pt.get<std::string>("type_id");
    if(type_str!=kTypeId)
      return false;
    _tag = pt.get<std::string>("tag");
  }
  catch(ptree_bad_data& ex) {
    LOGLITE_LOG_L1(ex.what());
  }

  return true;
}

bool operator==(const EventInfo& lhs, const EventInfo& rhs)
{
  return lhs.tag()==rhs.tag();
}

bool operator<(const EventInfo& lhs, const EventInfo& rhs)
{
  return lhs.tag()<rhs.tag();
}
} //namespace event
