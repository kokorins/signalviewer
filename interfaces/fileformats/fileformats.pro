TEMPLATE = subdirs
include(../../signalviewer.pri)

HEADERS += \
    datafilescanner.h \
    epochlistimpl.h \
    epochlistprovider.h \
    epochfiles.h \
    epochfileinfo.h \
    epochlistprovider_qt.h
