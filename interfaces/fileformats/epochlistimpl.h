#ifndef EPOCHLISTIMPL_H
#define EPOCHLISTIMPL_H
//#include <epoch/epochtype.h>
#include "epochfileinfo.h"
#include <cstddef>
#include <map>
#include <set>
#include <utility>
#include <vector>
#include <boost/date_time.hpp>
namespace epoch {
class Epoch;
/**
 * Abstract interface for epoch list creation from files. There could be a list of files linked to different types of signals.
 * We suppose that there could be a number of signals in each files, for each file, there should be a possibility to link
 * the signal by it's 0-based index.
 */
class IEpochListImpl {
public:
  virtual ~IEpochListImpl() {}
  /**
   * Links the list of files to it's types of signals.
   * \returns if everythin is ok
   * \param files_paths is a complicated structure. External map links the file type and a vector of files linked to it.
   * Each file is a pair: name+idx, where name is a full path to file and idx is a index of a signal iside the file.
   * e.g. the .dat files contains 3 coordinates: x,y,z, we supose that they should be accessed by 0,1,2 accordingly.
   */
  virtual bool open(const std::map<std::string, std::vector<EpochFileInfo> >& files_paths) = 0;
  /**
   * Number of epochs over all types of signals
   */
  virtual size_t size()const = 0;
  /**
   * \param idx index of epoch in zerobased style
   * \param epoch the output epoch information.
   * \returns true if epoch has been read, or false if epoch contains garbage
   */
  virtual bool read(size_t idx, Epoch& epoch) = 0;
  /**
   * Types of signal represented in the epoch list
   */
  virtual const std::set<std::string>& activeTypes()const = 0;
  /**
   * Getter and setter for epoch length in seconds
   */
  virtual void setEpochLen(size_t epoch_len) = 0;
  virtual size_t epochLen()const = 0;
  virtual size_t idxFromTimestamp(const boost::posix_time::ptime &dt) const = 0;
  virtual boost::posix_time::ptime timeFromIdx(size_t idx)const = 0;
};
} // namespace epoch
#endif // EPOCHLISTIMPL_H
