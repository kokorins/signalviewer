#ifndef EPOCHFILES_H
#define EPOCHFILES_H
#include "epochfileinfo.h"
#include <boost/date_time.hpp>
#include <cstddef>
#include <vector>
#include <string>
namespace epoch {
class IEpochFiles {
public:
  virtual ~IEpochFiles() {}
  virtual void setProperties(const std::string&) = 0;
  virtual bool open(const std::vector<EpochFileInfo>& paths) = 0;
  virtual std::vector<double> read(size_t st_sec, size_t dur_sec) = 0;
  virtual void getTimestamp(size_t sec, boost::posix_time::ptime& t) = 0;
  virtual size_t size()const = 0;
  virtual void close() = 0;
};
} //namespace epoch
#endif // EPOCHFILES_H
