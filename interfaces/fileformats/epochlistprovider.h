#ifndef EPOCHLISTPROVIDER_H
#define EPOCHLISTPROVIDER_H
#include <boost/shared_ptr.hpp>
namespace epoch {
class IDataFileScanner;
class IEpochFiles;

/**
 * The interface for file formats providers. Uses the Qt Plugin approach for deploying
 * the dynamic libraries.
 */
class IEpochListProvider {
public:
  /**
   * Creates the Epoch creation provider
   */
  virtual boost::shared_ptr<IEpochFiles> createFiles()const=0;
  /**
   * Creates the directory scanner provider, for searching the appropriate files
   * and getting the information about the file
   */
  virtual boost::shared_ptr<IDataFileScanner> createScanner()const=0;
  /**
   * Name of file format
   */
  virtual std::wstring name()const=0;
  /**
   * Searching files extention
   */
  virtual std::wstring extFilter()const=0;
  /**
   * Author name
   */
  virtual std::wstring author()const=0;
  /**
   * Unique version
   */
  virtual std::wstring version()const=0;
  virtual ~IEpochListProvider() {}
};
} //namespace epoch
#endif // EPOCHLISTPROVIDER_H
