#ifndef DATAFILESCANNER_H
#define DATAFILESCANNER_H
#include <string>
#include <boost/date_time.hpp>
#include <boost/filesystem/path.hpp>
namespace epoch {

struct EpochFileInfo;

struct DataFileInfo {
  bool is_ok;
  std::string file_name;
  std::string subj;
  std::vector<std::string> channels;
  std::vector<boost::posix_time::ptime> start_times;
};

/**
 * \brief Interface for folder scanner procedures
 */
class IDataFileScanner {
public:
  typedef boost::posix_time::ptime TDateTime;
  virtual ~IDataFileScanner() {}
/**
 * \brief checks that the given file should be considered as a file of current type
 * and that it should be handled
 * \param file path to file
 * \param subj returns the name of a subject
 * \param channels names of a channel coded in a file
 * \param start_time a startup moment of data in a file
 * \return true if should be handled
 */
  virtual DataFileInfo handle(const boost::filesystem::path&file) const = 0;
  virtual std::string info(const boost::filesystem::path& file, int idx)const = 0;
  virtual void properties(const EpochFileInfo &prev_prop, void *, std::vector<EpochFileInfo>& fis) const = 0;
  /**
   * @brief defaultProperties is a default file properties related to a file,
   *actually should call static function, check multithread availability
   * @param cur_info information about file in a view
   * @return the defautl json parameters
   */
  virtual std::string defaultProperties(const EpochFileInfo &cur_info)const = 0;
};

} //namespace epoch
#endif // DATAFILESCANNER_H
