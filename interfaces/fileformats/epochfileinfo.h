#ifndef EPOCHFILEINFO_H
#define EPOCHFILEINFO_H
#include <string>
#include <cstddef>

namespace epoch {
struct EpochFileInfo {
  std::string file_name;
  std::string file_path;
  size_t channel_idx;
  std::string prop_json;
};
} //namespace epoch
#endif // EPOCHFILEINFO_H
