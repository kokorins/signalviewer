#ifndef STATISTICSPROVIDER_H
#define STATISTICSPROVIDER_H
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>

namespace stats {
class ITypedStatCalculator;
typedef boost::shared_ptr<ITypedStatCalculator> Calculator;

class IStatisticsProvider {
public:
  virtual std::wstring name()const=0;
  virtual std::wstring author()const=0;
  virtual std::wstring version()const=0;
  virtual Calculator createCalculator()const=0;
  virtual std::string type()const=0;
  virtual std::vector<std::string> defaultParameters(size_t epoch_len)const=0;
  virtual std::string updateParameters(const std::string& new_json, void* parent)=0;
  virtual ~IStatisticsProvider() {}
};
} //namespace stats
#endif // STATISTICSPROVIDER_H
