QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets concurrent

TARGET = signalviewer_v06
TEMPLATE = app

DESTDIR = $$OUT_PWD/../

unix:QMAKE_RPATHDIR += /home/kokorins/Qt5.0.2/5.0.2/gcc_64/lib/

INCLUDEPATH += \
  $$PWD/../ \
  $$PWD/ \
  $$PWD/../algos/ \
  $$PWD/../algos/treap \
  $$PWD/../epoch/

include(../signalviewer.pri)

win32 {
  QWT_ROOT = "F:/sources/qwt"
  INCLUDEPATH += $$QWT_ROOT/src
  LIBS += -L$$QWT_ROOT/lib
}
else {
  QWT_ROOT = "/home/kokorins/opt/qwt-6.1"
  INCLUDEPATH += $$QWT_ROOT/src
  DEPENDPATH += $$QWT_ROOT/src
  LIBS += -L$$QWT_ROOT/lib
}


DEPENDPATH += \
  $$PWD/../algos/ \
  $$PWD/../epoch/

HEADERS += \
    src/transformationsdialog.h \
    src/tagsdialog.h \
    src/statsdialog.h \
    src/ssamodel.h \
    src/signalnavigatorwidget.h \
    src/preferencesdialog.h \
    src/openfolderdialog.h \
    src/edittransformationdialog.h \
    src/editstatsdialog.h \
    src/editffttransformdialog.h \
    src/defaultstatsdialog.h \
    src/animationlabel.h \
    src/aboutdialog.h \
    src/findepochdialog.h \
    src/findtimestampdialog.h \
    src/aboutplugindialog.h \
    src/eventswidget.h \
    src/utils.h \
    src/editeventdialog.h \
    src/chartwidget.h \
    src/signalviewer.h

SOURCES += \
    src/transformationsdialog.cpp \
    src/tagsdialog.cpp \
    src/statsdialog.cpp \
    src/ssamodel.cpp \
    src/signalnavigatorwidget.cpp \
    src/preferencesdialog.cpp \
    src/openfolderdialog.cpp \
    src/main.cpp \
    src/edittransformationdialog.cpp \
    src/editstatsdialog.cpp \
    src/editffttransformdialog.cpp \
    src/defaultstatsdialog.cpp \
    src/animationlabel.cpp \
    src/aboutdialog.cpp \
    src/findepochdialog.cpp \
    src/findtimestampdialog.cpp \
    src/aboutplugindialog.cpp \
    src/eventswidget.cpp \
    src/utils.cpp \
    src/editeventdialog.cpp \
    src/chartwidget.cpp \
    src/signalviewer.cpp

FORMS += \
    src/transformationsdialog.ui \
    src/tagsdialog.ui \
    src/statsdialog.ui \
    src/signalnavigatorwidget.ui \
    src/preferencesdialog.ui \
    src/openfolderdialog.ui \
    src/edittransformationdialog.ui \
    src/editstatsdialog.ui \
    src/editffttransformdialog.ui \
    src/defaultstatsdialog.ui \
    src/aboutdialog.ui \
    src/findepochdialog.ui \
    src/findtimestampdialog.ui \
    src/aboutplugindialog.ui \
    src/eventswidget.ui \
    src/editeventdialog.ui \
    src/signalviewer.ui

RESOURCES += \
    src/signalviewer.qrc

win32 {
  CONFIG(release, debug|release) {
    LIBS += -lqwt
  }
  CONFIG(debug, debug|release) {
    LIBS += -lqwtd
  }
}
else {
  LIBS += -lqwt
}

LIBS += -L$$OUT_PWD/.. -lalgos -lepoch

win32:PRE_TARGETDEPS += $$OUT_PWD/../algos.lib \
                        $$OUT_PWD/../epoch.lib \
                        $$OUT_PWD/../plugins/fileformats/abfplugin.lib \
                        $$OUT_PWD/../plugins/fileformats/atxtplugin.lib \
                        $$OUT_PWD/../plugins/fileformats/csvplugin.lib \
                        $$OUT_PWD/../plugins/fileformats/edfplugin.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libalgos.a $$OUT_PWD/../libepoch.a
