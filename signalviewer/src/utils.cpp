#include "utils.h"
#include <strconvert/strconvert.h>
#include <QDebug>
QDateTime Utils::FromBoost(const boost::posix_time::ptime &dt)
{
  QDate qd(dt.date().year(), dt.date().month(), dt.date().day());
  QTime qt(dt.time_of_day().hours(), dt.time_of_day().minutes(), dt.time_of_day().seconds(),
           1000*dt.time_of_day().fractional_seconds()/std::pow(10.0,(long)dt.time_of_day().num_fractional_digits()));
  return QDateTime(qd, qt);
}

boost::posix_time::ptime Utils::FromQt(const QDateTime &dt)
{
  boost::gregorian::date bd(dt.date().year(), dt.date().month(), dt.date().day());
  boost::posix_time::time_duration bt(dt.time().hour(), dt.time().minute(), dt.time().second(), dt.time().msec());
  return boost::posix_time::ptime(bd, bt);
}

std::wstring Utils::FromQt(const QString &str)
{
  return vr::str::any_string(str.toStdString()).wstr();
}

QString Utils::FromStd(const std::wstring &str)
{
  return QString::fromStdString(vr::str::any_string(str).str());
}
