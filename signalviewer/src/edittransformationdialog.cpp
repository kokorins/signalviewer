#include "edittransformationdialog.h"
#include "ui_edittransformationdialog.h"
#include <filter/filter_helpers.h>
#include <filter/butterfilter.h>
#include <filter/robustfilter.h>
#include <transformation/transformproperties.h>
#include <transformation/filtertrans.h>
#include <transformation/decimatetrans.h>

EditTransformationDialog::EditTransformationDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::EditTransformationDialog)
{
  ui->setupUi(this);
  connect(ui->ok_button, SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancel_button, SIGNAL(clicked()), this, SLOT(reject()));
  QStringList names;
  names<<"Step filter"<<"Quantile filter"<<"Butterworth filter"<<"Decimate";
  ui->type_combo->addItems(names);
  connect(ui->type_combo, SIGNAL(currentIndexChanged(int)), ui->stackedWidget, SLOT(setCurrentIndex(int)));
  ui->stackedWidget->setCurrentIndex(ui->type_combo->currentIndex());
}

EditTransformationDialog::~EditTransformationDialog()
{
  delete ui;
}

void EditTransformationDialog::setTransform(QString str)
{
  std::string json = str.toStdString();
  switch(trans::TransformProperties::ReadType(json)) {
  break; case trans::TransformProperties::Chain:
    return;
  break; case trans::TransformProperties::Decimate: {
    trans::DecimateTrans dt;
    dt.fromString(json);
    ui->type_combo->setCurrentIndex(Decimate);
  }
  break; case trans::TransformProperties::Filter: {
    trans::FilterTrans ft;
    ft.fromString(json);
    const filter::ITypedFilter& tf = ft.filter();
    std::string inner_json;
    tf.toString(inner_json);
    switch(filter::FilterProperties::ReadType(inner_json)) {
      break; case filter::FilterProperties::Butter: {
        filter::ButterFilter bf;
        bf.fromString(inner_json);
        ui->type_combo->setCurrentIndex(Butter);
        ui->low_spin->setValue(bf.params().f1);
        ui->high_spin->setValue(bf.params().f2);
      }
      break; case filter::FilterProperties::StepRobust: {
        filter::StepRobustFilterAlgo srfa;
        srfa.fromString(inner_json);
        ui->type_combo->setCurrentIndex(Step);
        ui->step_window_spin->setValue(srfa.window());
        ui->step_spin->setValue(srfa.stepFactor());
      }
      break; case filter::FilterProperties::QuanRobust: {
        filter::QuantileWinsorizedFilterAlgo qwfa;
        qwfa.fromString(inner_json);
        ui->type_combo->setCurrentIndex(Quan);
        ui->quan_window_spin->setValue(qwfa.window());
        ui->quan_spin->setValue(qwfa.quantile());
        ui->coef_spin->setValue(qwfa.coef());
      }
      break; default:
      return;
      }
    }
  break; default:
    return;
  }
}

QString EditTransformationDialog::transform() const
{
  std::string json;
  using filter::FilterProperties;
  switch(ui->type_combo->currentIndex()) {
  break; case Butter: {
    boost::shared_ptr<filter::ButterFilter> bf(new filter::ButterFilter());
    filter::ButterParams params;
    params.f1 = ui->low_spin->value();
    params.f2 = ui->high_spin->value();
    bf->init(params);
    trans::FilterTrans ft;
    ft.setFilter(bf);
    json = ft.toString();
  }
  break; case Step: {
    boost::shared_ptr<filter::StepRobustFilterAlgo> srfa(
          new filter::StepRobustFilterAlgo(ui->step_window_spin->value(), ui->step_spin->value()));
    trans::FilterTrans ft;
    ft.setFilter(srfa);
    json = ft.toString();
  }
  break; case Quan: {
    boost::shared_ptr<filter::QuantileWinsorizedFilterAlgo> qwfa(
          new filter::QuantileWinsorizedFilterAlgo(ui->quan_window_spin->value(),
                                                   ui->quan_spin->value(),
                                                   ui->coef_spin->value()));
    trans::FilterTrans ft;
    ft.setFilter(qwfa);
    json = ft.toString();
  }
  break; case Decimate: {
    trans::DecimateTrans dt;
    dt.setDec(ui->decimate_spin->value());
    json = dt.toString();
  }
  break; default:;
  }
  return QString::fromStdString(json);
}
