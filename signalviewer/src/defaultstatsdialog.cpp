#include "defaultstatsdialog.h"
#include "ui_defaultstatsdialog.h"
#include "ssamodel.h"
#include <stats/statcalculator.h>

DefaultStatsDialog::DefaultStatsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::DefaultStatsDialog)
{
  ui->setupUi(this);
}

DefaultStatsDialog::~DefaultStatsDialog()
{
  delete ui;
}

void DefaultStatsDialog::setModel(SSAModel *model)
{
  _model = model;
  const std::vector<boost::shared_ptr<stats::ITypedStatCalculator> >& stats_list = _model->defaultStats();
  for(size_t i = 0; i<stats_list.size(); ++i) {
    _names.push_back(QString::fromStdString(stats_list[i]->name()));
    _jsons.push_back(QString::fromStdString(stats_list[i]->toString()));
  }
  ui->defaults_table->setRowCount(_names.size());
  ui->defaults_table->setColumnCount(2);
  QStringList headers;
  headers<<"Name"<<"Json";
  ui->defaults_table->setHorizontalHeaderLabels(headers);
  for(int i=0; i<_names.size(); ++i) {
    QTableWidgetItem* name_item = new QTableWidgetItem(_names[i]);
    ui->defaults_table->setItem(i, 0, name_item);
    QTableWidgetItem* json_item = new QTableWidgetItem(_jsons[i]);
    ui->defaults_table->setItem(i, 1, json_item);
  }
  connect(ui->add_button, SIGNAL(clicked()), this, SLOT(slotAdd()));
}

void DefaultStatsDialog::slotAdd()
{
  QModelIndexList qmil = ui->defaults_table->selectionModel()->selectedRows();
  QSet<int> qmis;
  foreach(const QModelIndex& qm, qmil)
    qmis.insert(qm.row());
  QVector<QString> selected_stats;
  selected_stats.reserve(qmis.size());
  foreach(int i, qmis)
    selected_stats.push_back(_jsons[i]);
  emit sigAddStats(selected_stats);
}
