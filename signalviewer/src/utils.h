#ifndef UTILS_H
#define UTILS_H
#include <QDateTime>
#include <boost/date_time.hpp>

class Utils {
public:
  static QDateTime FromBoost(const boost::posix_time::ptime& dt);
  static boost::posix_time::ptime FromQt(const QDateTime& dt);
  static std::wstring FromQt(const QString& str);
  static QString FromStd(const std::wstring& str);
};

#endif // UTILS_H
