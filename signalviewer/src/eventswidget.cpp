#include "eventswidget.h"
#include "ssamodel.h"
#include "editeventdialog.h"
#include "utils.h"
#include "ui_eventswidget.h"
#include <epoch/epochlist.h>
#include <event/eventinfo.h>
#include <event/eventannotation.h>
#include <epoch/epoch.h>
#include <QDebug>

struct EventsTableModel::Impl {
  SSAModel * model;
  std::vector<event::Tag> nodes;
  Impl() : model(0) {}
};

EventsTableModel::EventsTableModel(QObject *parent) : QAbstractTableModel(parent), _data(new Impl()) {}

EventsTableModel::~EventsTableModel() {}

void EventsTableModel::setModel(SSAModel *model)
{
  if(_data->model) {
    disconnect(_data->model, SIGNAL(sigInfoUpdate()), this, SLOT(slotInfoUpdate()));
    disconnect(_data->model, SIGNAL(sigDataOpen()), this, SLOT(slotInfoUpdate()));
  }
  _data->model = model;
  connect(_data->model, SIGNAL(sigInfoUpdate()), this, SLOT(slotInfoUpdate()));
  connect(_data->model, SIGNAL(sigDataOpen()), this, SLOT(slotInfoUpdate()));
  _data->nodes = _data->model->info().toVector();
}

int EventsTableModel::rowCount(const QModelIndex &) const
{
  return _data->nodes.size();
}

int EventsTableModel::columnCount(const QModelIndex &) const
{
  return 3;
}

QVariant EventsTableModel::data(const QModelIndex &index, int role) const
{
  if(!index.isValid())
    return QVariant();
  if(role == Qt::DisplayRole) {
    switch(index.column()) {
    break; case 0: {
      const QDateTime& t = Utils::FromBoost(_data->nodes[index.row()].start);
      return t.toString("yyyy.MM.dd hh:mm:ss.zzz");
    }
    break; case 1: {
      const QDateTime& t =Utils::FromBoost(_data->nodes[index.row()].end);
      return t.toString("yyyy.MM.dd hh:mm:ss.zzz");
    }
    break; case 2: {
      return QString::fromStdString(_data->nodes[index.row()].tag.tag());
    }
    break; default:
      return QVariant();
    }
  }
  return QVariant();
}

QVariant EventsTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role == Qt::DisplayRole) {
    if(orientation == Qt::Vertical) {
      return QVariant();
    }
    else {
      switch(section) {
      break; case 0:
        return tr("Start time");
      break; case 1:
        return tr("End time");
      break; case 2:
        return tr("Tags");
      break; default:
        return QVariant();
      }
    }
  }
  return QVariant();
}

Qt::ItemFlags EventsTableModel::flags(const QModelIndex &/*index*/) const
{
  return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

const std::vector<event::Tag> &EventsTableModel::nodes() const
{
  return _data->nodes;
}

void EventsTableModel::slotInfoUpdate()
{
  beginResetModel();
  _data->nodes = _data->model->info().toVector();
  endResetModel();
}

EventsWidget::EventsWidget(QWidget *parent) :
  QWidget(parent), ui(new Ui::EventsWidget)
{
  ui->setupUi(this);
  connect(ui->epoch_button, SIGNAL(clicked()), this, SLOT(slotGoToEpoch()));
  connect(ui->add_event_button, SIGNAL(clicked()), this, SLOT(slotAddEvent()));
  connect(ui->edit_event_button, SIGNAL(clicked()), this, SLOT(slotEditEvent()));
  connect(ui->remove_event_button, SIGNAL(clicked()), this, SLOT(slotRemoveEvents()));
}

EventsWidget::~EventsWidget()
{
  delete ui;
}

void EventsWidget::setModel(SSAModel *model)
{
  _model = model;
  _events_model = new EventsTableModel(ui->events_table);
  _events_model->setModel(model);
  ui->events_table->setModel(_events_model);
}

void EventsWidget::slotGoToEpoch()
{
  if(!_model->isOpen())
    return;
  if(!ui->events_table->selectionModel())
    return;
  QModelIndexList mil = ui->events_table->selectionModel()->selectedRows();
  if(mil.size()!=1)
    return;
  if(!mil.front().isValid())
    return;
  int row_idx = mil.front().row();
  boost::posix_time::ptime st_time = _events_model->nodes()[row_idx].start;
  size_t idx = _model->list().idxFromTimestamp(st_time);
  if(_model->list().size()<=idx)
    return;
  _model->setEpochIdx(idx);
}

void EventsWidget::slotAddEvent()
{
  EditEventDialog * eed = new EditEventDialog(this);
  if(eed->exec() != QDialog::Accepted)
    return;
  const QVector<event::EventInfo>& infos = eed->eventInfo();
  std::vector<event::EventInfo> ins(infos.begin(), infos.end());
  _model->updateAnno(Utils::FromQt(eed->start()), Utils::FromQt(eed->end()), ins);
}

void EventsWidget::slotEditEvent()
{
  if(!ui->events_table->selectionModel())
    return;
  QModelIndexList mil = ui->events_table->selectionModel()->selectedRows();
  if(mil.size()!=1)
    return;
  if(!mil.front().isValid())
    return;
  int row_idx = mil.front().row();
  const event::Tag& n = _events_model->nodes()[row_idx];
  EditEventDialog * eed = new EditEventDialog(this);
  eed->setStart(Utils::FromBoost(n.start));
  eed->setEnd(Utils::FromBoost(n.end));
  QVector<event::EventInfo> infos;
  infos.append(n.tag);
  eed->setEventInfo(infos);
  if(eed->exec() != QDialog::Accepted)
    return;
  std::vector<event::EventInfo> r(1, event::EventInfo());
  const QVector<event::EventInfo>& k = eed->eventInfo();

  _model->updateAnno(n.start, n.end, r);//Remove previous
  _model->updateAnno(Utils::FromQt(eed->start()),
                     Utils::FromQt(eed->end()),
                     std::vector<event::EventInfo>(k.begin(), k.end()));
}

void EventsWidget::slotRemoveEvents()
{
  if(!ui->events_table->selectionModel())
    return;
  QModelIndexList mil = ui->events_table->selectionModel()->selectedRows();
  if(mil.size()<1)
    return;

  QList<int> rows;
  foreach( const QModelIndex & index, mil)
    rows.append( index.row() );
  qSort(rows);
  event::EventInfo ei; // Update with empty for deletion
  foreach(int i, rows) {
    const event::Tag& n = _events_model->nodes()[i];
    _model->updateAnno(n.start, n.end, std::vector<event::EventInfo>(1,ei));
  }
}
