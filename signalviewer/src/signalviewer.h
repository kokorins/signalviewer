#ifndef SIGNALVIEWER_H
#define SIGNALVIEWER_H

#include <QtWidgets/QMainWindow>

namespace Ui {
  class SignalViewerClass;
}

class SSAModel;

class SignalViewer : public QMainWindow {
  Q_OBJECT
public:
  explicit SignalViewer(QWidget *parent = 0);
  ~SignalViewer();
  void setupToolBar();
  void setupActions();
protected slots:
  void selectFolder();
  void slotAbout();
  void slotAboutQt();
  void slotAboutPlugins();
  void preferences();
  void slotOpenAnnos();
  void slotSaveAnnos();
  void slotOpenSession();
  void slotSaveSession();
  void slotActivateEpochTab();
private:
  bool loadPlugin();
private:
  Ui::SignalViewerClass* ui;
  SSAModel * _model;
};

#endif // SIGNALVIEWER_H
