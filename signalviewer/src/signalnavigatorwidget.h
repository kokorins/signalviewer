#ifndef SIGNALNAVIGATORWIDGET_H
#define SIGNALNAVIGATORWIDGET_H

#include <QWidget>
#include <QVector>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QBrush>
#include <QDateTime>

namespace Ui {
  class SignalNavigatorWidget;
}

class QSplitter;
class SSAModel;
class QAction;
class QwtPlot;
class QwtPlotCurve;
class QwtPlotZoomer;
class QwtPlotScaleItem;
class QwtScaleWidget;
class ChartWidget;

class ChartsTableModel : public QAbstractTableModel {
  Q_OBJECT
public:
  static const int kShowIndex = 0;
  static const int kNameIndex = 1;
  static const int kFFTIndex = 2;
  static const int kNumIndex = 3;
public:
  ChartsTableModel(QObject* parent) : QAbstractTableModel(parent) {}
  void setFileInfos(const QStringList& file_names, const QVector<bool>& is_shown);
  virtual int rowCount(const QModelIndex& model=QModelIndex()) const;
  virtual int columnCount(const QModelIndex& model=QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
  virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
  virtual Qt::ItemFlags flags( const QModelIndex & index ) const;
signals:
  void sigShowChart(int idx, bool is_visible);
  void sigDrawChart(int idx, bool is_visible);
private:
  QStringList _charts_names;
  QVector<bool> _is_shown;
  QVector<bool> _is_fft;
};

class ChartsTableDelegate : public QItemDelegate {
  Q_OBJECT
public:
  ChartsTableDelegate(QObject *parent = 0) : QItemDelegate(parent)  {}
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;
private slots:
    void emitCommitData();
};

class SignalNavigatorWidget : public QWidget {
  Q_OBJECT
  
public:
  explicit SignalNavigatorWidget(QWidget *parent = 0);
  ~SignalNavigatorWidget();
  void setModel(SSAModel* model);
private:
  void clearCentral();
  QColor findTagColor(const QStringList &tags) const;
public:
  static QSplitter* CreateSplitter(QWidget* parent = 0);
  static QwtPlotCurve* CreateCurve();
public slots:
  void slotShowChart(int, bool);
  void slotDrawChart(int, bool);
  void slotEpochIndexChanged(int);
  void slotEpochSizeChanged(int);
  void slotEpochChanged(int,int);
protected slots:
  void slotDataOpen();
  void slotDataUpdate();
  void slotTrans();
  void slotFftTrans();
  void slotStats();
  void slotAnno();
  void slotSetTags();
  void slotTags();
  void slotFastTag();
  void slotFindEpoch();
  void slotFindTimestamp();
  void slotPrevEpoch();
  void slotNextEpoch();
  void slotIncEpoch();
  void slotDecEpoch();
  void slotSetEpochIdx(int);
  void addStats(const QVector<QPair<QString, QString> >&, const QString& chan_name);
  void slotEditAnno(QWidget*,QWidget*);
private:
  Ui::SignalNavigatorWidget *ui;

  SSAModel * _ssa_model;
  std::vector<std::string> _act_tps;

  QStringList _charts_names;
  QVector<bool> _is_fft;
  QSplitter* chart_splitter;
  QVector<ChartWidget*> _charts;

  QwtPlotScaleItem* _scale;
  QwtPlot *_scale_plot;
  QwtPlotCurve *_fake_curve;
};

#endif // SIGNALNAVIGATORWIDGET_H
