#include "preferencesdialog.h"
#include "ssamodel.h"
#include "ui_preferencesdialog.h"
#include <QAbstractButton>

PreferencesDialog::PreferencesDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::PreferencesDialog)
{
  ui->setupUi(this);
  connect(ui->button_box, SIGNAL(clicked(QAbstractButton*)), this, SLOT(slotSelectButtons(QAbstractButton*)));
  connect(this, SIGNAL(accepted()), this, SLOT(slotApply()));
}

PreferencesDialog::~PreferencesDialog()
{
  delete ui;
}

void PreferencesDialog::setProps(const SSAProperties &props)
{
  ui->epoch_len->setValue(props.epoch_len);
}

void PreferencesDialog::slotApply()
{
  SSAProperties ssp;
  ssp.epoch_len = ui->epoch_len->value();
  emit apply(ssp);
}

void PreferencesDialog::slotSelectButtons(QAbstractButton * button)
{
  switch(ui->button_box->buttonRole(button)) {
  break; case QDialogButtonBox::ApplyRole:
    slotApply();
  break; case QDialogButtonBox::AcceptRole:
    accept();
  break; case QDialogButtonBox::RejectRole:
    reject();
  break; default:
    return;
  }
}
