#ifndef ANIMATIONLABEL_H
#define ANIMATIONLABEL_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QMovie>

class AnimationLabel : public QWidget {
  Q_OBJECT
public:
  AnimationLabel(QWidget* parent);
//  AnimationLabel(QString animationPath,
//                  QSize size,
//                  QWidget* parent);
  virtual ~AnimationLabel();
  void setPath(QString animationPath);
public slots:
  void start();
  void stop();

private:
  QPointer<QLabel> _container;
  QPointer<QMovie> _animation;

  void init(const QString& animationPath,
            const QSize& size);
};

#endif // ANIMATIONLABEL_H
