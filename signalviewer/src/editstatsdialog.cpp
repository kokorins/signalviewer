#include "editstatsdialog.h"
#include "ssamodel.h"
#include <stats/statcalculator.h>
#include "ui_editstatsdialog.h"

EditStatsDialog::EditStatsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::EditStatsDialog)
{
  ui->setupUi(this);
  connect(ui->dialog_buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->dialog_buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

EditStatsDialog::~EditStatsDialog()
{
  delete ui;
}

void EditStatsDialog::setStats(const QString &str)
{
  const std::string& json = str.toStdString();
  const std::string& type = stats::StatProperties::ReadType(json);
  _model->statsPropertiesFromType(type, str, this);
  QString name = _model->statsProviderNameFromType(type);
  ui->type_combo->setCurrentIndex(ui->type_combo->findText(name));
}

QString EditStatsDialog::stats() const
{
  return _model->statsProperties(ui->type_combo->currentText(), "", ui->stackedWidget);
}

void EditStatsDialog::setModel(SSAModel *model)
{
  _model = model;
  _names = _model->initStatsWidgets(ui->stackedWidget);
  ui->type_combo->addItems(_names);
  connect(ui->type_combo, SIGNAL(currentIndexChanged(int)), ui->stackedWidget, SLOT(setCurrentIndex(int)));
  ui->stackedWidget->setCurrentIndex(ui->type_combo->currentIndex());
}
