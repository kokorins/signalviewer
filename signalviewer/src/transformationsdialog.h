#ifndef TRANSFORMATIONSDIALOG_H
#define TRANSFORMATIONSDIALOG_H

#include <QDialog>
#include <QVector>
#include <QMap>

namespace Ui {
  class TransformationsDialog;
}

enum ETransType {
  StepRobust,
  QuanRobust,
  Butter,
  ETransTypeNum
};

class TransformationsDialog : public QDialog {
  Q_OBJECT
public:
  explicit TransformationsDialog(QWidget *parent = 0);
  ~TransformationsDialog();
  void setFFT(bool is_fft);
  const QVector<QVector<QString> >& trans()const;
  void setChannels(const QStringList &channs, const QVector<QVector<QString> >& trans);
signals:
  void sigApplyTrans(const QVector<QVector<QString> >&trans);
protected slots:
  void slotAddTransform();
  void slotEditTransform();
  void slotRemTransform();
  void slotUpTransform();
  void slotDownTransform();
  void updateList(int idx);
  void slotChangeChan(int);
  void slotApply();
private:
  QVector<QVector<QString> > transs;
  bool _is_fft;
  Ui::TransformationsDialog *ui;
};

#endif // TRANSFORMATIONSDIALOG_H
