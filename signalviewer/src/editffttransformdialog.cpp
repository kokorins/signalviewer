#include "editffttransformdialog.h"
#include "ui_editffttransformdialog.h"
#include <transformation/chaintrans.hpp>
#include <transformation/decimatetrans.h>
#include <transformation/logtrans.h>

EditFftTransformDialog::EditFftTransformDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::EditFftTransformDialog)
{
  ui->setupUi(this);
  connect(ui->ok_button, SIGNAL(clicked()), this, SLOT(accept()));
  connect(ui->cancel_button, SIGNAL(clicked()), this, SLOT(reject()));
  QStringList names;
  names<<tr("Decimate")<<tr("Log");
  ui->type_combo->addItems(names);
  connect(ui->type_combo, SIGNAL(currentIndexChanged(int)), ui->stackedWidget, SLOT(setCurrentIndex(int)));
  ui->stackedWidget->setCurrentIndex(ui->type_combo->currentIndex());
  QStringList log_items;
  for(int i=0; i<trans::LogTrans::ELogBaseNum; ++i) {
    std::string log_name;
    trans::LogTrans::Name(i, log_name);
    log_items.push_back(QString::fromStdString(log_name));
  }
  ui->base_combo->addItems(log_items);
}

EditFftTransformDialog::~EditFftTransformDialog()
{
  delete ui;
}

void EditFftTransformDialog::setTransform(QString str)
{
  std::string json = str.toStdString();
  switch(trans::TransformProperties::ReadType(json)) {
  break; case trans::TransformProperties::Chain:
    return;
  break; case trans::TransformProperties::Decimate: {
    trans::DecimateTrans dt;
    dt.fromString(json);
    ui->decimate_spin->setValue(dt.dec());
    ui->type_combo->setCurrentIndex(Decimate);
  }
  break; case trans::TransformProperties::Log: {
    trans::LogTrans log;
    log.fromString(json);
    ui->factor_spin->setValue(log.factor());
    ui->base_combo->setCurrentIndex(log.base());
    ui->type_combo->setCurrentIndex(Log);
  }
  break; default:
    return;
  }
}

QString EditFftTransformDialog::transform() const
{
  std::string json;
  switch(ui->type_combo->currentIndex()) {
  break; case Decimate: {
    trans::DecimateTrans dt;
    dt.setDec(ui->decimate_spin->value());
    json = dt.toString();
  }
  break; case Log: {
    trans::LogTrans log;
    log.setFactor(ui->factor_spin->value());
    log.setBase((trans::LogTrans::ELogBase)ui->base_combo->currentIndex());
    json = log.toString();
  }
  break; default:
    ;
  }
  return QString::fromStdString(json);
}
