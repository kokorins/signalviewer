#ifndef CHARTWIDGET_H
#define CHARTWIDGET_H

#include <QWidget>
#include <QColor>
#include <QDateTime>

class QwtPlot;
class QwtTextLabel;
class QwtPlotZoomer;
class QwtPlotCurve;
class QwtPlotIntervalCurve;


struct ColorEvent {
  QDateTime start;
  QDateTime end;
  QColor color;
};

class ChartWidget : public QWidget {
  Q_OBJECT
public:
  explicit ChartWidget(QWidget *parent = 0);
  void setTitle(const QString& title);
  void setEpoch(const QVector<QPointF>& points);
  void setEvents(const QVector<ColorEvent>& events, const QDateTime &start, const QDateTime &end);
private:
  QwtTextLabel *_title_label;
  QVector<QwtPlotIntervalCurve*> _events;
//  QwtPlotZoomer* _zoomer;
  QwtPlotCurve * _curve;
  QwtPlot *_plot;
  QDateTime _start, _end;
};

#endif // CHARTWIDGET_H
