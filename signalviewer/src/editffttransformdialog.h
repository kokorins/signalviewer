#ifndef EDITFFTTRANSFORMDIALOG_H
#define EDITFFTTRANSFORMDIALOG_H

#include <QDialog>

namespace Ui {
class EditFftTransformDialog;
}

class EditFftTransformDialog : public QDialog
{
  Q_OBJECT
  enum EEditType {
    Decimate,
    Log,
    EEditTypeNum
  };
public:
  explicit EditFftTransformDialog(QWidget *parent = 0);
  ~EditFftTransformDialog();
  void setTransform(QString str);
  QString transform()const;
private:
  Ui::EditFftTransformDialog *ui;
};

#endif // EDITFFTTRANSFORMDIALOG_H
