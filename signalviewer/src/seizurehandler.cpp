#include "seizurehandler.h"
#include <QFile>

SeizureHandler::SeizureHandler() : _seizure_algo(new seizure::HmmSeizureAlgorithm()) {}

SeizureHandler::~SeizureHandler() {}

bool SeizureHandler::loadSeizureParams(const QString &path)
{
  seizure::HmmSeizureAlgorithmParams hsap;
  QFile fin(path);
  fin.open(QIODevice::ReadOnly | QIODevice::Text);
  QString json(fin.readAll());
  fin.close();
  if(!hsap.fromString(json.toStdString()))
    return false;
  _seizure_algo->setParams(hsap);
  return true;
}

void SeizureHandler::saveSeizureParams(const QString &path) const
{
  std::string json;
  _seizure_algo->params().toString(json);
  QFile fout(path);
  fout.open(QIODevice::WriteOnly);
  fout.write(QString::fromStdString(json).toUtf8());
  fout.flush();
  fout.close();
}

bool SeizureHandler::trainSeizure(epoch::EpochList &list, size_t from, size_t to)
{
  return _seizure_algo->train(list, from, to);
}

bool SeizureHandler::loadSeizureProps(const QString &path)
{
  seizure::HmmSeizureProperties hsp;
  QFile fin(path);
  fin.open(QIODevice::ReadOnly | QIODevice::Text);
  QString json(fin.readAll());
  fin.close();
  if(!hsp.fromString(json.toStdString()))
    return false;
  _seizure_algo->setProps(hsp);
  return true;
}

void SeizureHandler::saveSeizureProps(const QString &path) const
{
  std::string json;
  _seizure_algo->props().toString(json);
  QFile fout(path);
  fout.open(QIODevice::WriteOnly);
  fout.write(QString::fromStdString(json).toUtf8());
  fout.flush();
  fout.close();
}

void SeizureHandler::loadMsm(const QString &path, epoch::EpochList &list)
{
  seizure::MsmFile::Read(path.toStdString().c_str(), list);
}

void SeizureHandler::saveMsm(const QString &path, epoch::EpochList &list) const
{
  seizure::MsmFile::Write(path.toStdString().c_str(), list);
}
