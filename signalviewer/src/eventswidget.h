#ifndef EVENTSWIDGET_H
#define EVENTSWIDGET_H

#include <QAbstractTableModel>
#include <QWidget>
#include <QSharedPointer>
#include <map>

namespace Ui {
class EventsWidget;
}

class SSAModel;

namespace event {
  class EventInfo;
  template <typename TagInfo> struct EventNode;
  typedef EventNode<EventInfo> Tag;
}

class EventsTableModel : public QAbstractTableModel {
  Q_OBJECT
public:
  EventsTableModel(QObject* parent);
  ~EventsTableModel();
  void setModel(SSAModel* model);
  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
  Qt::ItemFlags flags(const QModelIndex &index) const;
  const std::vector<event::Tag>& nodes()const;
protected slots:
  void slotInfoUpdate();
private:
  struct Impl;
  QSharedPointer<Impl> _data;
};


class EventsWidget : public QWidget {
  Q_OBJECT
public:
  explicit EventsWidget(QWidget *parent = 0);
  ~EventsWidget();
  void setModel(SSAModel* model);
signals:
  void sigGoToEpoch(int idx);
protected slots:
  void slotGoToEpoch();
  void slotAddEvent();
  void slotEditEvent();
  void slotRemoveEvents();
private:
  Ui::EventsWidget *ui;
  EventsTableModel * _events_model;
  SSAModel* _model;
};

#endif // EVENTSWIDGET_H
