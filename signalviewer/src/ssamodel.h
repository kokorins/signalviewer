#ifndef SSAMODEL_H
#define SSAMODEL_H

#include <transformation/utils.h>
#include <QObject>
#include <QSharedPointer>
#include <QString>
#include <QList>
#include <QPair>
#include <QColor>
#include <QMap>
#include <QVector>
#include <vector>
#include <set>
#include <boost/shared_ptr.hpp>

namespace boost {
  namespace posix_time {
    class ptime;
  }
}

namespace epoch {
  class EpochList;
  class Epoch;
  class IEpochListProvider;
}

namespace event {
  class EventInfo;
  class EventInfoSummary;
  class EventAnnotation;
  template <typename TagInfo> struct EventNode;
  typedef EventNode<EventInfo> Tag;
}

namespace stats {
  class IStatisticsProvider;
  class ITypedStatCalculator;
}

struct FileInfo;
class QStringList;

struct SSAProperties {
  const static std::string kTypeId;
  int epoch_len;
  SSAProperties() : epoch_len(1) {}
  std::string toString()const;
  bool fromString(const std::string& json);
};

struct TagsInfo {
  QMap<QString, QColor> colors;
};

class SSAModel : public QObject {
  Q_OBJECT
public:
  const static std::string kTypeId;
  SSAModel(QObject *parent);
  virtual ~SSAModel();
public: //Epoch
  bool isOpen()const;
  void open(const QString& folder, const QList<FileInfo>& list, int data_type, int epoch_len);
  void open(const QString& folder, const QList<FileInfo>& list, int data_type);
  void chartsNames(QStringList& charts_names)const;
  bool read(int idx, epoch::Epoch& epoch);
  std::vector<std::string> getActiveTypes()const;
  int numEpochs() const;
  const SSAProperties& props()const;
  const QVector<QVector<QString> >& transs()const;
  const QVector<QVector<QString> >& fftTranss()const;
  const QVector<QVector<QString> >& statss()const;
  void transform(int idx, std::vector<double>& signal)const;
  void fftTrans(int idx, std::vector<TPoint>& fft)const;
  QVector<QPair<QString, QString> > stats(int idx, const std::vector<double>& signal);
  const epoch::EpochList& list()const;
  void addFileFormat(epoch::IEpochListProvider * elp);
  void addStatsProvider(stats::IStatisticsProvider *stats_provider);
  QVector<epoch::IEpochListProvider*> fileFormats();
  void setEpochIdx(size_t epoch_idx);
public: //Event
  std::vector<event::Tag> readAnno(int start, int sz) const;
  std::vector<event::Tag> readTags(int start, int sz) const;
  bool updateAnno(int idx, const std::vector<event::EventInfo> &anno);
  bool updateTags(int idx, const std::vector<event::EventInfo> &anno);
  bool updateAnno(const boost::posix_time::ptime& start, const boost::posix_time::ptime& end, const std::vector<event::EventInfo> &annos);
  bool updateTags(const boost::posix_time::ptime& start, const boost::posix_time::ptime& end, const std::vector<event::EventInfo> &annos);
  bool loadAnnos(char const*const file_name);
  void saveAnnos(char const*const file_name)const;
  bool fromFile(char const*const file_name);
  void toFile(char const*const file_name)const;
  std::set<std::string> tags() const;
  const event::EventAnnotation& info()const;
  const TagsInfo& tagsInfo()const;
  std::string toString()const;
  bool fromString(const std::string& json);
public: //Stats
  std::vector<boost::shared_ptr<stats::ITypedStatCalculator> > defaultStats();
  QStringList initStatsWidgets(QWidget*)const;
  QString statsPropertiesFromType(const std::string& type, const QString& new_json, QWidget *parent);
  QString statsProviderNameFromType(const std::string& type)const;
  QString statsProperties(const QString& name, const QString& new_json, const QWidget *parent);
public slots:
  void setTrans(const QVector<QVector<QString> >& transs);
  void setStats(const QVector<QVector<QString> >& stats);
  void setFftTrans(const QVector<QVector<QString> >& transs);
  void setProps(const SSAProperties& props);
  void setTagsInfo(const TagsInfo& tags);
signals:
  void sigDataOpen();
  void sigDataUpdate();
  void sigInfoUpdate();
  void sigSetEpoch(int);
private:
  struct SSAModelData;
  QSharedPointer<SSAModelData> _data;
  QVector<epoch::IEpochListProvider*> _file_formats;
  QSharedPointer<epoch::EpochList> _list;
  QSharedPointer<event::EventAnnotation> _info;
  SSAProperties _props;
  TagsInfo _tags;
};

#endif // SSAMODEL_H
