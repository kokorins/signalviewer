#include "openfolderdialog.h"
#include "ssamodel.h"
#include "ui_openfolderdialog.h"
#include <interfaces/fileformats/epochlistprovider.h>
#include <epoch/epochtype.h>
#include <epoch/epochlist.h>
#include <epoch/epochfolder.h>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <QFileDialog>
#include <QStringList>
#include <QDebug>
#include <QFileInfo>
#include <QLineEdit>
#include <QtConcurrent/QtConcurrentRun>
#include <QtConcurrent/QtConcurrentMap>
#include <QFuture>
#include <QFutureWatcher>

OpenFolderDialog::OpenFolderDialog(SSAModel *model, QWidget *parent) :
    QDialog(parent), ui(new Ui::OpenFolderDialog()), _scanner(new epoch::EpochFolder()), _model(model)
{
  ui->setupUi(this);
  connect(ui->folder_button, SIGNAL(clicked()), this, SLOT(chooseFolder()));
  connect(ui->dialog_button_box, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->dialog_button_box, SIGNAL(rejected()), this, SLOT(reject()));
  connect(ui->add_button, SIGNAL(clicked()), this, SLOT(addRow()));
  connect(ui->remove_button, SIGNAL(clicked()), this, SLOT(removeRows()));
  for(int i=0; i<_model->fileFormats().size(); ++i) {
#ifdef _MSC_VER
    ui->data_type_combo->addItem(QString::fromUtf16((const ushort*)_model->fileFormats()[i]->name().c_str()));
#else
    ui->data_type_combo->addItem(QString::fromStdWString(_model->fileFormats()[i]->name()));
#endif
  }
  for(int i=0; i<epoch::EFileTypeNum; ++i)
    ui->type_combo->addItem(QString::fromStdString(epoch::ToString((epoch::EDefaultChannelType)i)));
  connect(ui->channel_type_button, SIGNAL(clicked()), this, SLOT(slotSetChannelType()));
  connect(ui->subject_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(subjectChanged(int)));
  connect(ui->update_info_button, SIGNAL(clicked()), this, SLOT(updateFileInfo()));
  connect(ui->file_properties_button, SIGNAL(clicked()), this, SLOT(slotFilePropertiesButton()));
  connect(&_file_info_watcher, SIGNAL(finished()), this, SLOT(slotFileInfoFinished()), Qt::QueuedConnection);
  connect(&_scan_watcher, SIGNAL(finished()), this, SLOT(slotScanFinished()),Qt::QueuedConnection);
  connect(&_default_props_watcher, SIGNAL(finished()), this, SLOT(slotDefaultPropsFinished()),Qt::QueuedConnection);
  ui->progress->setMinimum(0);
  ui->progress->setMaximum(0);
  ui->progress->setValue(0);
  ui->progress->hide();
  ui->progress->setTextVisible(false);
}

OpenFolderDialog::~OpenFolderDialog()
{
  delete ui;
}

QString OpenFolderDialog::folder() const
{
  return ui->path_line->text();
}

int OpenFolderDialog::curDataType() const
{
  return ui->data_type_combo->currentIndex();
}

const QList<FileInfo> &OpenFolderDialog::list() const
{
  if(_default_props_watcher.isRunning())
    _default_props_watcher.waitForFinished();
  return _file_infos;
}

void OpenFolderDialog::chooseFolder()
{
  QString caption = tr("Choose folder with scoring data");
  QStringList filters;
  for(int i=0; i<_model->fileFormats().size(); ++i) {
#ifdef _MSC_VER
    QString filter = QString::fromUtf16((const ushort*)_model->fileFormats()[i]->name().c_str());
    filter += tr(" (")+QString::fromUtf16((const ushort*)_model->fileFormats()[i]->extFilter().c_str())+tr(")");
#else
    QString filter = QString::fromStdWString(_model->fileFormats()[i]->name());
    filter += tr(" (")+QString::fromStdWString(_model->fileFormats()[i]->extFilter())+tr(")");
#endif
    filters.append(filter);
  }
  int data_type = ui->data_type_combo->currentIndex();
  _current_filter = filters [data_type];
  QString file_name = QFileDialog::getOpenFileName(this, caption, QString(),
                                                   _current_filter, 0,
                                                   QFileDialog::ShowDirsOnly);
  if(file_name.isEmpty())
    return;
  QFileInfo qfi(file_name);
  ui->path_line->setText(qfi.absolutePath());
  ui->progress->setVisible(true);
  QFuture<void> future = QtConcurrent::run(_scanner.data(), &epoch::EpochFolder::scan,
                                                  qfi.absolutePath().toStdString(),
                                                  boost::ref(*_model->fileFormats()[data_type]));
  _scan_watcher.setFuture(future);
}

void OpenFolderDialog::slotScanFinished()
{
  if(!_scan_watcher.isFinished())
    return;
  ui->progress->setVisible(false);
  std::vector<std::string> subjs;
  _scanner->subjects(subjs);
  ui->subject_combo->blockSignals(true);
  ui->subject_combo->clear();
  for(size_t i=0; i<subjs.size(); ++i)
    ui->subject_combo->addItem(QString::fromStdString(subjs[i]));
  ui->subject_combo->update();
  ui->subject_combo->blockSignals(false);
  if(subjs.empty())
    return;
  ui->subject_combo->setCurrentIndex(0);
  subjectChanged(0);
}

void OpenFolderDialog::slotDefaultPropsFinished()
{
  if(!_default_props_watcher.isFinished())
    return;
  ui->progress->setVisible(false);
  QFuture<std::string> f = _default_props_watcher.future();
  for(int i=0; i<f.resultCount(); ++i)
    _file_infos[i].file_info.prop_json = f.resultAt(i);
}

std::string OpenFolderDialog::getDefaultProperties(const FileInfo& fi)
{
  return _scanner->defaultProperties(fi.file_info);
}

void OpenFolderDialog::subjectChanged(int subj_idx)
{
  if(_default_props_watcher.isRunning())
    _default_props_watcher.cancel();
  _file_infos.clear();
  std::vector<std::string> subjs;
  _scanner->subjects(subjs);
  std::vector<std::string> channs;
  _scanner->channels(subjs[subj_idx], channs);
  int idx=0;
  QDir path(folder());
  for(size_t i=0; i<channs.size(); ++i) {
    std::string tp = channs[i];
    std::vector<epoch::ChannelId> files;
    _scanner->files(subjs[subj_idx], channs[i], true, files);

    for(size_t j=0; j<files.size(); ++j) {
      FileInfo fi;
      fi.id = idx++;
      fi.name = QString::fromStdString(files[j].file);
      fi.file_type = tp;
      fi.file_info.file_name = files[j].file;
      fi.file_info.file_path = path.absoluteFilePath(QString::fromStdString(files[j].file)).toStdString();
      fi.chan_name = QString::fromStdString(channs[i]);
      fi.file_info.channel_idx = files[j].idx;
      _file_infos.push_back(fi);
    }
  }
  QFuture<std::string> mf = QtConcurrent::mapped(_file_infos, boost::bind(&OpenFolderDialog::getDefaultProperties, this,_1));
  _default_props_watcher.setFuture(mf);
  ui->progress->setVisible(true);
  FilesTableModel * model = new FilesTableModel(_file_infos, this);
  ui->files_table->setModel(model);
  ui->files_table->setItemDelegate(new FilesTableDelegate(_current_filter, this));
  for(int i=0; i<model->rowCount(); ++i)
    ui->files_table->openPersistentEditor(model->index(i, 2));
//  for(int i=0; i<model->columnCount(); ++i)
//    ui->files_table->horizontalHeader()->setResizeMode(i, QHeaderView::ResizeToContents);
}

void OpenFolderDialog::addRow()
{
  if(ui->path_line->text().isEmpty())
    return;
  int row_count = ui->files_table->model()->rowCount();
  ui->files_table->model()->insertRows(row_count, 1);
  ui->files_table->openPersistentEditor(ui->files_table->model()->index(row_count, 2));
}

void OpenFolderDialog::removeRows()
{
  if(!ui->files_table->selectionModel())
    return;
  QModelIndexList mil = ui->files_table->selectionModel()->selectedRows();
  if(mil.size()<1)
    return;

  QList<int> rows;
  foreach( const QModelIndex & index, mil)
    rows.append(index.row());
  qSort(rows);

  int prev = -1;
  for( int i = rows.count() - 1; i >= 0; --i) {
    int current = rows[i];
    if( current != prev ) {
      ui->files_table->model()->removeRows(current,1);
      prev = current;
    }
  }
}

void OpenFolderDialog::slotSetChannelType()
{
  if(!ui->files_table->selectionModel())
    return;
  QModelIndexList mil = ui->files_table->selectionModel()->selectedRows();
  if(mil.size()<1)
    return;
  int dt = ui->type_combo->currentIndex();
  QList<int> rows;
  foreach( const QModelIndex & index, mil)
    rows.append( index.row() );
  QAbstractItemModel * m = ui->files_table->model();
  for(int i=0; i<rows.size(); ++i)
    m->setData(m->index(rows[i], 1), QString::fromStdString(epoch::ToString((epoch::EDefaultChannelType)dt)));
}

struct IdEqual : public std::unary_function<FileInfo, bool> {
public:
  IdEqual(int id) : _id(id) {}
  bool operator()(const FileInfo& lhs)const {
    return lhs.id == _id;
  }
private:
  int _id;
};

void OpenFolderDialog::updateFileInfo()
{
  if(!ui->files_table->selectionModel())
    return;
  QModelIndexList mil = ui->files_table->selectionModel()->selectedRows();
  if(mil.size()!=1)
    return;
  if(!mil.front().isValid())
    return;
  QAbstractItemModel * m = ui->files_table->model();
  QString file_name = m->data(m->index(mil.front().row(), 3)).toString();
  int fi_idx = m->data(m->index(mil.front().row(),0)).toInt();
  QList<FileInfo>::iterator it = std::find_if(_file_infos.begin(), _file_infos.end(),
                                              IdEqual(fi_idx));
  if(it==_file_infos.end())
    return;
  QFuture<std::string> future = QtConcurrent::run(_scanner.data(), &epoch::EpochFolder::fileInfo,
                                                  file_name.toStdString(),
                                                  (*it).file_info.channel_idx);
  _file_info_watcher.setFuture(future);
}

void OpenFolderDialog::slotFilePropertiesButton()
{
  if(!ui->files_table->selectionModel())
    return;
  QModelIndexList mil = ui->files_table->selectionModel()->selectedRows();
  if(mil.size()!=1)
    return;
  if(!mil.front().isValid())
    return;
  if(_default_props_watcher.isRunning())
    return;
  QAbstractItemModel * m = ui->files_table->model();
  int fi_idx = m->data(m->index(mil.front().row(),0)).toInt();
  std::vector<epoch::EpochFileInfo> fis;
  fis.reserve(_file_infos.size());
  for(int i=0; i<_file_infos.size(); ++i)
    fis.push_back(_file_infos[i].file_info);
  for(int i=0; i<_file_infos.size(); ++i) {
    FileInfo& fi = _file_infos[i];
    if(fi.id == fi_idx) {
      _scanner->properties(fi.file_info, (void*)this, fis);
      for(int i=0; i<_file_infos.size(); ++i)
        _file_infos[i].file_info = fis[i];
      return;
    }
  }
}

void OpenFolderDialog::slotFileInfoFinished()
{
  if(!_file_info_watcher.isFinished())
    return;
  ui->file_info->setText(QString::fromStdString(_file_info_watcher.result()));
}

int FilesTableModel::rowCount(const QModelIndex&) const
{
  return _file_info.size();
}

int FilesTableModel::columnCount(const QModelIndex&) const
{
  return 5;
}

QVariant FilesTableModel::data(const QModelIndex &index, int role /*= Qt::DisplayRole*/ ) const
{
  int row = index.row();
  int col = index.column();
  if(role == Qt::DisplayRole) {
    switch(col) {
    break; case 0:
      return QString::number(_file_info[row].id);
    break; case 1:
      return QString::fromStdString(_file_info[row].file_type);
    break; case 2:
      return QVariant();
    break; case 3:
      return _file_info[row].name;
    break; case 4:
      return _file_info[row].chan_name;
    break; default:
      return QVariant();
    }
  }
  if(role==Qt::EditRole) {
    switch(col) {
    break; case 1:
      return QString::fromStdString(_file_info[row].file_type);
    break; default:
      return QVariant();
    }
  }
  if(role==Qt::SizeHintRole) {
    switch(col) {
    break; case 2:
      return QSize(30,30);
    break; default:
      return QVariant();
    }
  }
  return QVariant();
}

QVariant FilesTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role==Qt::DisplayRole) {
    if(orientation == Qt::Horizontal) {
      switch(section) {
      break; case 0:
        return tr("Id");
      break; case 1:
        return tr("Channel name");
      break; case 2:
        return tr("Choose file");
      break; case 3:
        return tr("File name");
      break; case 4:
        return tr("File channel name");
      break; default:
        return QVariant();
      }
    }
  }
  return QVariant();
}

bool FilesTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if(role == Qt::EditRole) {
    if(index.column()==1) {
      _file_info[index.row()].file_type = value.toString().toStdString();
      emit dataChanged(index,index);
      return true;
    }
    if(index.column()==2) {
      _file_info[index.row()].name = value.toString();
      emit dataChanged(index,this->index(index.row(), index.column()+1));
      return true;
    }
  }
  return true;
}

bool FilesTableModel::insertRows(int row, int count, const QModelIndex &parent)
{
  if(count<1)
    return false;
  beginInsertRows(parent, row, row+count-1);
  FileInfo fi;
  fi.id = row;
  fi.file_type = epoch::ToString(epoch::EEG);
  fi.name = tr("");
  fi.chan_name = tr("");
  fi.file_info.channel_idx = 0;
  _file_info.append(fi);
  endInsertRows();
  return true;
}

bool FilesTableModel::removeRows(int row, int count, const QModelIndex & parent)
{
  if(count<1)
    return false;
  beginRemoveRows(parent, row, row + count - 1);
  for(int i=qMin(row+count-1, _file_info.size()); i>=row; i--)
    _file_info.removeAt(i);
  endRemoveRows();
  return true;
}

Qt::ItemFlags FilesTableModel::flags(const QModelIndex &index) const
{
  if(index.column()==1)
    return Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if(index.column()==2)
    return Qt::ItemIsEditable | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

FilesTableDelegate::FilesTableDelegate(QString filter, QObject *parent) :
      QItemDelegate(parent), _filter(filter) {}

QWidget *FilesTableDelegate::createEditor(QWidget *parent,
                                          const QStyleOptionViewItem &,
                                          const QModelIndex &index) const
{
  if (index.column() == 1) {
    QLineEdit * edit = new QLineEdit(parent);
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
    connect(edit, SIGNAL(textChanged(QString)), this, SLOT(emitCommitData()));
    return edit;
  }
  if (index.column() == 2) {
    QPushButton *but = new QPushButton(tr("..."), parent);
    connect(but, SIGNAL(clicked()), this, SLOT(selectFile()));
    return but;
  }
  return 0;
}

void FilesTableDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  if(index.column() == 1) {
    QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
    if (!edit)
        return;
    edit->setText(index.model()->data(index, Qt::EditRole).toString());
  }
}

void FilesTableDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  if(index.column()==1) {
    QLineEdit *edit = qobject_cast<QLineEdit *>(editor);
    if (!edit)
        return;
    model->setData(index, edit->text());
  }
  if(index.column()==2) {
    QPushButton *button = qobject_cast<QPushButton *>(editor);
    if(!button)
      return;
    QString name = button->toolTip();
    if(!name.isEmpty()) {
      QFileInfo fi(name);
      model->setData(index, fi.fileName());
    }
  }
}

void FilesTableDelegate::emitCommitData()
{
  emit commitData(qobject_cast<QWidget *>(sender()));
}

void FilesTableDelegate::selectFile()
{
  QPushButton *button = qobject_cast<QPushButton *>(sender());
  QString name = QFileDialog::getOpenFileName(qobject_cast<QWidget *>(sender()), tr("Select file"), QString(), _filter);
  button->setToolTip(name);
  emit commitData(qobject_cast<QWidget *>(sender()));
}
