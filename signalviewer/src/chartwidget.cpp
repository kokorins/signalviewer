#include "chartwidget.h"
#include "utils.h"
#include <loglite/logging.hpp>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_item.h>
#include <qwt_text_label.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_plot_intervalcurve.h>
#include <qwt_scale_draw.h>
#include <QPainter>
#include <QHBoxLayout>
#include <QDateTime>
#include <QDateTimeEdit>
#include <QDebug>
#include <boost/date_time.hpp>

class Zoomer: public QwtPlotZoomer {
public:
//#ifdef WIN32
  Zoomer( int xAxis, int yAxis, QWidget *canvas_widget): QwtPlotZoomer( xAxis, yAxis, canvas_widget)
//#else
//  Zoomer( int xAxis, int yAxis, QwtPlotCanvas *canvas ): QwtPlotZoomer( xAxis, yAxis, canvas )
//#endif
  {
    setRubberBandPen( QColor( Qt::blue ) );
    setTrackerMode( QwtPicker::ActiveOnly );
    setTrackerPen( QColor( Qt::white ) );
    //Patterns forbidden for now
    QVector<MousePattern> mp;
    setMousePattern(mp);
    QVector<KeyPattern> kp;
    setKeyPattern(kp);
    // RightButton: zoom out by 1
    // Ctrl+RightButton: zoom out to full size
//    setMousePattern(QwtEventPattern::MouseSelect2, Qt::RightButton, Qt::ControlModifier);
//    setMousePattern(QwtEventPattern::MouseSelect3, Qt::RightButton);
  }
};

QwtPlotScaleItem * CreateAxis(const QFont& font, QwtScaleDraw::Alignment align)
{
  QwtPlotScaleItem *scale_item = new QwtPlotScaleItem(align, 0.0);
  scale_item->setFont(font);
  scale_item->setBorderDistance(0);
  return scale_item;
}

QwtPlotCurve *CreateCurve()
{
  QwtPlotCurve *curve = new QwtPlotCurve();
  QColor col = QColor(Qt::red);
  col.setAlphaF(0.2);
  QPen pen(col);
  curve->setStyle(QwtPlotCurve::Lines);
  curve->setPen(pen);
  return curve;
}

ChartWidget::ChartWidget(QWidget *parent) : QWidget(parent)
{
  _plot = new QwtPlot(this);
  QLayout * layout = new QHBoxLayout(this);
  layout->setMargin(0);
  layout->addWidget(_plot);
  setLayout(layout);
  QFont font = _plot->font();
  font.setPointSizeF(font.pointSizeF()/1.4);
  _plot->setFont(font);
  _plot->setMinimumSize(0,70);
  _plot->setBaseSize(0,0);
  _plot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  _plot->enableAxis(QwtPlot::xBottom, false);
  _plot->enableAxis(QwtPlot::yLeft, false);
  _plot->enableAxis(QwtPlot::yRight, false);
  QwtPlotScaleItem *scale_item = CreateAxis(_plot->font(), QwtScaleDraw::RightScale);
  scale_item->attach(_plot);

//  _zoomer = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, _plot->canvas());
//  _zoomer->setZoomBase();

  _curve = CreateCurve();
  _curve->attach(_plot);
  _plot->replot();
}

void ChartWidget::setTitle(const QString &title)
{
  _title_label = new QwtTextLabel(title, _plot);
  _title_label->setGeometry(70,10, _title_label->width(), _title_label->height());
}

void ChartWidget::setEpoch(const QVector<QPointF> &points)
{
  _curve->setSamples(points);
  _plot->replot();
//  _zoomer->setZoomBase();
}

void ChartWidget::setEvents(const QVector<ColorEvent> &events, const QDateTime& start, const QDateTime& end)
{
  foreach(QwtPlotIntervalCurve* event, _events)
    event->detach();
  _events.clear();
  _start = start;
  _end = end;
  foreach(const ColorEvent& ev, events) {
    QwtPlotIntervalCurve * qpic = new QwtPlotIntervalCurve();
    qpic->setAxes(QwtPlot::xBottom, QwtPlot::yRight);
    QBrush b(ev.color);
    QPen p(b, 1);
    qpic->setPen(p);
    qpic->setBrush(b);

    boost::posix_time::ptime st = Utils::FromQt(start);
    boost::posix_time::ptime ed = Utils::FromQt(end);
    boost::posix_time::ptime ev_st = Utils::FromQt(ev.start);
    boost::posix_time::ptime ev_ed = Utils::FromQt(ev.end);
    const boost::posix_time::time_duration& st_dur = (ev_st-st);
    const boost::posix_time::time_duration& ed_dur = (ev_ed-st);
    const boost::posix_time::time_duration& tot_dur = (ed-st);
    size_t num_pts = _curve->dataSize();
    size_t st_idx = (size_t)std::floor(1.*num_pts*st_dur.total_milliseconds()/tot_dur.total_milliseconds());
    size_t ed_idx = (size_t)std::floor(1.*num_pts*ed_dur.total_milliseconds()/tot_dur.total_milliseconds());
    QVector<QwtIntervalSample> ints;
    ints.push_back(QwtIntervalSample(st_idx, QwtInterval(0, 1)));
    ints.push_back(QwtIntervalSample(ed_idx, QwtInterval(0, 1)));

    qpic->setSamples(ints);
    qpic->attach(_plot);
    _events.push_back(qpic);
  }
//  _zoomer->setZoomBase();
  _plot->replot();
}

