#ifndef ABOUTPLUGINDIALOG_H
#define ABOUTPLUGINDIALOG_H

#include <QDialog>

class QLabel;
class QPushButton;
class QStringList;
class QTreeWidget;
class QTreeWidgetItem;


namespace Ui {
class AboutPluginDialog;
}

class AboutPluginDialog : public QDialog {
  Q_OBJECT  
public:
  explicit AboutPluginDialog(const QString &path, const QStringList &file_names, QWidget *parent = 0);
  ~AboutPluginDialog();
private:
  void findPlugins(const QString &path, const QStringList &file_names);
  void addItem(QTreeWidgetItem *pluginItem,
               const QString & interface_name, const QString &feature,
               const QString& plugin_version, const QString& plugin_author);
private:
  Ui::AboutPluginDialog *ui;
};

#endif // ABOUTPLUGINDIALOG_H
