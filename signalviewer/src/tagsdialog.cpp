#include "tagsdialog.h"
#include "ui_tagsdialog.h"
#include <event/eventinfo.h>
#include <event/eventinfosummary.hpp>
#include <QFileDialog>
#include <QColorDialog>
#include <QFile>
#include <QDebug>
#include <QMap>

TagsDialog::TagsDialog(QWidget *parent): QDialog(parent), ui(new Ui::TagsDialog)
{
  ui->setupUi(this);
  connect(ui->dialog_buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->dialog_buttons, SIGNAL(rejected()), this, SLOT(reject()));
  connect(ui->save_button, SIGNAL(clicked()), this, SLOT(slotSaveTags()));
  connect(ui->load_button, SIGNAL(clicked()), this, SLOT(slotLoadTags()));
  connect(ui->add_fast_button, SIGNAL(clicked()), this, SLOT(slotAddFastTags()));
  connect(ui->remove_fast_button, SIGNAL(clicked()), this, SLOT(slotRemoveFastTags()));
  connect(ui->add_tag_button, SIGNAL(clicked()), this, SLOT(slotAddTags()));
  connect(ui->remove_tag_button, SIGNAL(clicked()), this, SLOT(slotRemoveTags()));
  connect(ui->color_button,SIGNAL(clicked()), this, SLOT(slotColor()));
  connect(ui->remove_color_button,SIGNAL(clicked()), this, SLOT(slotRemColor()));
}

TagsDialog::~TagsDialog()
{
  delete ui;
}

void TagsDialog::setTags(const QStringList& tags, const QMap<QString, QColor>& colors)
{
  QStringList tags_list = tags;
  for(int i=0; i<ui->tags_list->count(); ++i)
    tags_list<<ui->tags_list->item(i)->text();
  tags_list.removeDuplicates();
  ui->tags_list->clear();
  ui->tags_list->addItems(tags_list);
  ui->fast_list->clear();
  foreach(const QString& k, colors.keys()) {
    QListWidgetItem*item = new QListWidgetItem(k, ui->fast_list);
    item->setBackgroundColor(colors[k]);
  }
  _colors = colors;
}

QStringList TagsDialog::fastTags() const
{
  QStringList res;
  for(int i=0; i<ui->fast_list->count(); ++i)
    res.append(ui->fast_list->item(i)->text());
  return res;
}

const QMap<QString, QColor> &TagsDialog::colors() const
{
  return _colors;
}

void TagsDialog::slotUpdateTagsList()
{
  ui->fast_list->clear();
}

void TagsDialog::slotSaveTags()
{
  std::string json;
  std::set<std::string> tags;
  for(int i=0; i<ui->tags_list->count(); ++i)
    tags.insert(ui->tags_list->item(i)->text().toStdString());
  std::map<std::string, size_t> colors;
  foreach(const QString& c, _colors.keys())
    colors.insert(std::make_pair(c.toStdString(),_colors[c].rgb()));
  event::EventInfoSummary::TagsToString(tags, colors, json);
  QString file_path = QFileDialog::getSaveFileName(this, tr("Save tags to file"), QString(), tr("tags(*.json)"));
  QFileInfo qfi(file_path);
  if(qfi.suffix().isEmpty())
    file_path+=tr(".json");
  if(file_path.isEmpty())
    return;
  QFile f(file_path);
  if(!f.open(QIODevice::WriteOnly | QIODevice::Text))
    return;
  f.write(json.c_str());
  f.close();
}

void TagsDialog::slotLoadTags()
{
  QString file_path = QFileDialog::getOpenFileName(this, tr("Load tags from file"), QString(), tr("tags(*.json)"));
  if(file_path.isEmpty())
    return;
  QFile f(file_path);
  if(!f.open(QIODevice::ReadOnly | QIODevice::Text))
    return;
  std::string json = QString(f.readAll()).toStdString();
  std::set<std::string> tags;
  std::map<std::string, size_t> colors;
  event::EventInfoSummary::StringToTags(json, colors, tags);
  QStringList tags_list;
  QMap<QString,QColor> colors_list;
  foreach(const std::string& t, tags)
    tags_list<<QString::fromStdString(t);
  std::map<std::string, size_t>::const_iterator cit = colors.begin();
  for(;cit!=colors.end(); ++cit)
    colors_list.insert(QString::fromStdString((*cit).first), QColor((*cit).second));
  tags_list.removeDuplicates();
  setTags(tags_list, colors_list);
}

void TagsDialog::slotAddFastTags()
{
  foreach(QListWidgetItem* item, ui->tags_list->selectedItems()) {
    if(ui->fast_list->findItems(item->text(), Qt::MatchExactly).isEmpty())
      ui->fast_list->addItem(item->text());
  }
}

void TagsDialog::slotRemoveFastTags()
{
  ui->fast_list->setUpdatesEnabled(false);
  QModelIndexList indexes = ui->fast_list->selectionModel()->selectedIndexes();
  qSort(indexes);
  for (int i = indexes.count() - 1; i > -1; --i)
    ui->fast_list->model()->removeRow(indexes.at(i).row());
  ui->fast_list->setUpdatesEnabled(true);
}

void TagsDialog::slotAddTags()
{
  QStringList tags = ui->tag_line->text().split(QRegExp("[\\s,\\.]+"), QString::SkipEmptyParts);
  tags.removeDuplicates();
  setTags(tags, _colors);
}

void TagsDialog::slotRemoveTags()
{
  ui->tags_list->setUpdatesEnabled(false);
  QModelIndexList indexes = ui->tags_list->selectionModel()->selectedIndexes();
  foreach(const QModelIndex& fidx, indexes) {
    ui->fast_list->setUpdatesEnabled(false);
    QList<QListWidgetItem*> fast_tags = ui->fast_list->findItems(ui->tags_list->item(fidx.row())->text(), Qt::MatchExactly);
    foreach(QListWidgetItem* fitem, fast_tags)
      delete fitem;
    ui->fast_list->setUpdatesEnabled(true);
  }
  qSort(indexes);
  for (int i = indexes.count() - 1; i > -1; --i)
    ui->tags_list->model()->removeRow(indexes.at(i).row());
  ui->tags_list->setUpdatesEnabled(true);
}

void TagsDialog::slotColor()
{
  if(ui->fast_list->currentIndex().row()<0)
    return;
  QColor col = QColorDialog::getColor(Qt::transparent, this, tr("Color for ")+ ui->fast_list->currentItem()->text());
  if(col.isValid())
    _colors[ui->fast_list->currentItem()->text()]=col;
  ui->fast_list->currentItem()->setBackgroundColor(col);
}

void TagsDialog::slotRemColor()
{
  if(ui->fast_list->currentIndex().row()<0)
    return;
  ui->fast_list->currentItem()->setBackgroundColor(Qt::white);
  _colors.remove(ui->fast_list->currentItem()->text());
}
