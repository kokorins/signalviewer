#ifndef EDITSTATSDIALOG_H
#define EDITSTATSDIALOG_H

#include <QDialog>

namespace Ui {
class EditStatsDialog;
}

class SSAModel;

class EditStatsDialog : public QDialog {
  Q_OBJECT
public:
  explicit EditStatsDialog(QWidget *parent = 0);
  ~EditStatsDialog();
  void setStats(const QString& str);
  QString stats()const;
  void setModel(SSAModel* model);
private:
  Ui::EditStatsDialog *ui;
  SSAModel* _model;
  QStringList _names;
};

#endif // EDITSTATSDIALOG_H
