#include "editeventdialog.h"
#include "ui_editeventdialog.h"
#include <QRegExp>
#include <QRegularExpression>

EditEventDialog::EditEventDialog(QWidget *parent) : QDialog(parent), ui(new Ui::EditEventDialog)
{
  ui->setupUi(this);
}

EditEventDialog::~EditEventDialog()
{
  delete ui;
}

void EditEventDialog::setStart(const QDateTime &start)
{
  ui->start_datetime->setDateTime(start);
}

void EditEventDialog::setEnd(const QDateTime &end)
{
  ui->end_datetime->setDateTime(end);
}

void EditEventDialog::setEventInfo(const QVector<event::EventInfo> &infos)
{
  QStringList annos;
  QStringList tags;
  foreach(const event::EventInfo& tag, infos) {
    QString info = QString::fromStdString(tag.tag());
    if(info.contains(QRegularExpression("\\s")))
      annos<<info;
    else
      tags<<info;
  }
  ui->annotation_edit->setPlainText(annos.join(tr("\n")));
  ui->tags_edit->setText(tags.join(tr(", ")));
}

QDateTime EditEventDialog::start() const
{
  return ui->start_datetime->dateTime();
}

QDateTime EditEventDialog::end() const
{
  return ui->end_datetime->dateTime();
}

QVector<event::EventInfo> EditEventDialog::eventInfo() const
{
  QVector<event::EventInfo> eis;
  QString anno = ui->annotation_edit->toPlainText();
  if(!anno.isEmpty()) {
    event::EventInfo an;
    an.setTag(anno.toStdString());
    eis.append(an);
  }
  QStringList tags = ui->tags_edit->text().split(QRegExp("[\\s,\\.]+"), QString::SkipEmptyParts);
  tags.removeDuplicates();
  foreach(const QString& t, tags) {
    event::EventInfo ei;
    ei.setTag(t.toStdString());
    eis.append(ei);
  }
  return eis;
}
