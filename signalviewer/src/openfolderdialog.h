#ifndef OPENFOLDERDIALOG_H
#define OPENFOLDERDIALOG_H

#include <epoch/epochtype.h>
#include <interfaces/fileformats/epochfileinfo.h>
#include <QDialog>
#include <QAbstractTableModel>
#include <QItemDelegate>
#include <QObject>
#include <QList>
#include <QFutureWatcher>

class SSAModel;
namespace Ui {
  class OpenFolderDialog;
}

namespace epoch {
  class EpochFolder;
}

struct FileInfo {
public:
  std::string file_type;
  epoch::EpochFileInfo file_info;
public:
  int id;
  QString name;
  QString chan_name;
};

/**
 * \brief model for data files representation
 * File view which allows change file path and setup the type of signal.
 */
class FilesTableModel : public QAbstractTableModel {
public:
  FilesTableModel(QList<FileInfo>& file_info, QObject* parent=0):QAbstractTableModel(parent), _file_info(file_info) { }
  virtual int rowCount(const QModelIndex& model=QModelIndex()) const;
  virtual int columnCount(const QModelIndex& model=QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
  virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
  virtual bool insertRows(int row, int count, const QModelIndex &parent);
  virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
  virtual Qt::ItemFlags flags( const QModelIndex & index ) const;
private:
  QList<FileInfo>& _file_info;
};

class FilesTableDelegate : public QItemDelegate {
  Q_OBJECT

public:
  FilesTableDelegate(QString filter, QObject *parent = 0);
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                        const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model,
                    const QModelIndex &index) const;
private slots:
  void emitCommitData();
  void selectFile();
private:
  QString _filter;
};

class OpenFolderDialog : public QDialog {
  Q_OBJECT
public:
  OpenFolderDialog(SSAModel* _model, QWidget *parent = 0);
  ~OpenFolderDialog();
  QString folder()const;
  int curDataType()const;
  const QList<FileInfo>& list()const;
private:
  std::string getDefaultProperties(const FileInfo &fi);
protected slots:
  void chooseFolder();
  void subjectChanged(int);
  void addRow();
  void removeRows();
  void slotSetChannelType();
  void updateFileInfo();
  void slotFilePropertiesButton();
  void slotFileInfoFinished();
  void slotScanFinished();
  void slotDefaultPropsFinished();
private:
  Ui::OpenFolderDialog * ui;
  QSharedPointer<epoch::EpochFolder> _scanner;
  QString _current_filter;
  QList<FileInfo> _file_infos;
  QFutureWatcher<std::string> _file_info_watcher;
  mutable QFutureWatcher<std::string> _default_props_watcher;
  QFutureWatcher<void> _scan_watcher;
  SSAModel* _model;
};

#endif // OPENFOLDERDIALOG_H
