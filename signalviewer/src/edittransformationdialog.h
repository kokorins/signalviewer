#ifndef EDITTRANSFORMATIONDIALOG_H
#define EDITTRANSFORMATIONDIALOG_H

#include <QDialog>

namespace Ui {
class EditTransformationDialog;
}

class EditTransformationDialog : public QDialog
{
  Q_OBJECT
  enum EEditType {
    Step,
    Quan,
    Butter,
    Decimate,
    EEditTypeNum
  };

public:
  explicit EditTransformationDialog(QWidget *parent = 0);
  ~EditTransformationDialog();
  void setTransform(QString str);
  QString transform()const;
private:
  Ui::EditTransformationDialog *ui;
};

#endif // EDITTRANSFORMATIONDIALOG_H
