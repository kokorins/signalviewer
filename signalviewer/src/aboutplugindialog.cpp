#include "aboutplugindialog.h"
#include <interfaces/fileformats/epochlistprovider_qt.h>
#include "ui_aboutplugindialog.h"
#include <QLabel>
#include <QTreeWidget>
#include <QGridLayout>
#include <QDir>
#include <QPluginLoader>

AboutPluginDialog::AboutPluginDialog(const QString &path,
                                     const QStringList &file_names,
                                     QWidget *parent) : QDialog(parent), ui(new Ui::AboutPluginDialog)
{
  ui->setupUi(this);
  ui->label->setText(tr("Plugin path: %1").arg(path));
  findPlugins(path, file_names);
}

void AboutPluginDialog::findPlugins(const QString &path, const QStringList &file_names)
{
  const QDir dir(path);

  QTreeWidgetItem * epoch_list_item = new QTreeWidgetItem(ui->tree_widget);
  epoch_list_item->setText(0, tr("Epoch list provider"));
  ui->tree_widget->setItemExpanded(epoch_list_item, true);


  foreach (QObject *plugin, QPluginLoader::staticInstances()) {
    if(plugin) {
      epoch::IEpochListProvider *elp = qobject_cast<epoch::IEpochListProvider*>(plugin);
      if(elp) {
        QString author, version;
#ifdef _MSC_VER
        version = QString::fromUtf16((const ushort *)elp->version().c_str());
        author = QString::fromUtf16((const ushort *)elp->author().c_str());
#else
        version = QString::fromStdWString(elp->version());
        author = QString::fromStdWString(elp->author());
#endif
        addItem(epoch_list_item, tr("%1 (Static Plugin)").arg(plugin->metaObject()->className()),
                plugin->metaObject()->className(), version, author);
      }
    }
  }

  foreach (QString file_name, file_names) {
    QPluginLoader loader(dir.absoluteFilePath(file_name));
    QObject *plugin = loader.instance();
    if (plugin) {
      epoch::IEpochListProvider *elp = qobject_cast<epoch::IEpochListProvider*>(plugin);
      if(elp) {
        QString name, author, version;
#ifdef _MSC_VER
        name = QString::fromUtf16((const ushort *)elp->name().c_str());
        version = QString::fromUtf16((const ushort *)elp->version().c_str());
        author = QString::fromUtf16((const ushort *)elp->author().c_str());
#else
        name = QString::fromStdWString(elp->name());
        version = QString::fromStdWString(elp->version());
        author = QString::fromStdWString(elp->author());
#endif
        addItem(epoch_list_item, name, file_name, version, author);
      }
    }
  }
}

void AboutPluginDialog::addItem(QTreeWidgetItem *plugin_item,
                                const QString& plugin_name, const QString& plugin_file,
                                const QString& plugin_version, const QString& plugin_author)
{
  QTreeWidgetItem *interface_item = new QTreeWidgetItem(plugin_item);
  interface_item->setText(0, plugin_name);
  interface_item->setText(1, plugin_file);
  interface_item->setText(2, plugin_version);
  interface_item->setText(3, plugin_author);
}

AboutPluginDialog::~AboutPluginDialog()
{
  delete ui;
}
