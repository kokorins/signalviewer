#ifndef FINDTIMESTAMPDIALOG_H
#define FINDTIMESTAMPDIALOG_H

#include <QDialog>
#include <QDateTime>

namespace Ui {
class FindTimestampDialog;
}

class FindTimestampDialog : public QDialog {
  Q_OBJECT
public:
  explicit FindTimestampDialog(QWidget *parent = 0);
  ~FindTimestampDialog();
  void setTimestamp(const QDateTime&cur_timestamp, const QDateTime &from, const QDateTime &to);
  QDateTime timestamp()const;
  
private:
  Ui::FindTimestampDialog *ui;
};

#endif // FINDTIMESTAMPDIALOG_H
