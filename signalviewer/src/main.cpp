#include "signalviewer.h"
#include <loglite/logging.hpp>

#include <iostream>

#include <boost/filesystem.hpp>

#include <QtWidgets/QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
  //https://svn.boost.org/trac/boost/ticket/6320
  //Boost filesystem multithread bug
  boost::filesystem::path p("./");
  p.codecvt();
  int level = 3;
  int mask_level = 0;
  for(int i=0; i<level; ++i) {
    mask_level<<=1;
    mask_level+=1;
  }
  LOGLITE_INIT(("" >> loglite::trace>> loglite::eol));
  std::string log_path("./log.txt");
  loglite::sink sink_file(new std::ofstream(log_path.c_str()),mask_level);
  sink_file.attach_qualifier(loglite::log);
  LOGLITE_ADD_OUTPUT_STREAM(sink_file);

  loglite::sink s1(&std::cout, mask_level);
  s1.attach_qualifier(loglite::log);
  LOGLITE_ADD_OUTPUT_STREAM(s1);

  //Bug with default libraries search
  QStringList paths = QCoreApplication::libraryPaths();
  paths.append(".");
  paths.append("platforms");
  QCoreApplication::setLibraryPaths(paths);

  QApplication a(argc, argv);
  a.setApplicationName(QObject::tr("Signal viewer"));
  a.setApplicationVersion(QObject::tr("0.6"));
  SignalViewer w;
  w.show();
  return a.exec();
}
