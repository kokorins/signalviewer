#include "transformationsdialog.h"
#include "edittransformationdialog.h"
#include "editffttransformdialog.h"
#include <filter/filter_helpers.h>
#include <filter/robustfilter.h>
#include <transformation/decimatetrans.h>
#include "ui_transformationsdialog.h"
#include <QDialogButtonBox>
#include <QPushButton>

TransformationsDialog::TransformationsDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::TransformationsDialog)
{
  ui->setupUi(this);
  QPushButton* apply_button = ui->dialog_buttons->button(QDialogButtonBox::Apply);
  connect(ui->dialog_buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->dialog_buttons, SIGNAL(rejected()), this, SLOT(reject()));
  connect(apply_button, SIGNAL(clicked()), this, SLOT(slotApply()));
  connect(ui->add_button, SIGNAL(clicked()), this, SLOT(slotAddTransform()));
  connect(ui->edit_button, SIGNAL(clicked()), this, SLOT(slotEditTransform()));
  connect(ui->remove_button, SIGNAL(clicked()), this, SLOT(slotRemTransform()));
  connect(ui->up_button, SIGNAL(clicked()), this, SLOT(slotUpTransform()));
  connect(ui->down_button, SIGNAL(clicked()), this, SLOT(slotDownTransform()));
}

TransformationsDialog::~TransformationsDialog()
{
  delete ui;
}

void TransformationsDialog::setFFT(bool is_fft)
{
  _is_fft = is_fft;
}

const QVector<QVector<QString> > &TransformationsDialog::trans() const
{
  return transs;
}

void TransformationsDialog::slotAddTransform()
{
  //For both fft and usual
  trans::DecimateTrans dt;
  std::string str = dt.toString();
  int idx = ui->channel_combo->currentIndex();
  if(idx<0)
    return;
  transs[idx].append(QString::fromStdString(str));
  updateList(transs[idx].size()-1);
}

void TransformationsDialog::slotEditTransform()
{
  if(_is_fft) {
    EditFftTransformDialog *edit = new EditFftTransformDialog(this);
    int chan_idx = ui->channel_combo->currentIndex();
    int idx = ui->transforms_edit->currentRow();
    if(idx<0)
      return;
    edit->setTransform(transs[chan_idx][idx]);
    if(edit->exec()==QDialog::Accepted) {
      transs[chan_idx][idx] = edit->transform();
      updateList(idx);
    }
  }
  else {
    EditTransformationDialog *edit = new EditTransformationDialog(this);
    int chan_idx = ui->channel_combo->currentIndex();
    int idx = ui->transforms_edit->currentRow();
    if(idx<0)
      return;
    edit->setTransform(transs[chan_idx][idx]);
    if(edit->exec()==QDialog::Accepted) {
      transs[chan_idx][idx] = edit->transform();
      updateList(idx);
    }
  }
}

void TransformationsDialog::slotRemTransform()
{
  int idx = ui->transforms_edit->currentRow();
  if(idx<0)
    return;
  int chan_idx = ui->channel_combo->currentIndex();
  transs[chan_idx].remove(idx);
  updateList(-1);
}

void TransformationsDialog::slotUpTransform()
{
  int idx = ui->transforms_edit->currentRow();
  int chan_idx = ui->channel_combo->currentIndex();
  if(idx<1)
    return;
  qSwap(transs[chan_idx][idx], transs[chan_idx][idx-1]);
  updateList(idx-1);
}

void TransformationsDialog::slotDownTransform()
{
  int chan_idx = ui->channel_combo->currentIndex();
  int idx = ui->transforms_edit->currentRow();
  if(idx<0 || transs[chan_idx].size()-1<=idx)
    return;
  qSwap(transs[chan_idx][idx], transs[chan_idx][idx+1]);
  updateList(idx+1);
}

void TransformationsDialog::updateList(int idx)
{
  ui->transforms_edit->clear();
  int chan_idx = ui->channel_combo->currentIndex();
  if(chan_idx<0)
    return;
  foreach(const QString& ti, transs[chan_idx])
    ui->transforms_edit->addItem(ti);
  if(idx<0 || idx>= ui->transforms_edit->count())
    return;
  ui->transforms_edit->setCurrentRow(idx);
}

void TransformationsDialog::slotChangeChan(int)
{
  updateList(-1);
}

void TransformationsDialog::slotApply()
{
  emit sigApplyTrans(transs);
}

void TransformationsDialog::setChannels(const QStringList &channs, const QVector<QVector<QString> >& trans)
{
  transs = trans;
  ui->channel_combo->clear();
  ui->channel_combo->addItems(channs);
  connect(ui->channel_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChangeChan(int)));
  slotChangeChan(0);
}
