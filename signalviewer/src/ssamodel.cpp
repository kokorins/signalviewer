#include "ssamodel.h"
#include "openfolderdialog.h"
#include "utils.h"
#include <transformation/utils.h>
#include <epoch/epochlist.h>
#include <event/eventinfo.h>
#include <event/eventannotation.h>
#include <event/eventinfosummary.hpp>
#include <strconvert/strconvert.h>
#include <interfaces/fileformats/epochlistprovider.h>
#include <interfaces/stats/statisticsprovider.h>
#include <transformation/typedtrans.h>
#include <transformation/transformproperties.hpp>
#include <stats/statcollector.h>
#include <stats/statcalculator.h>
#include <loglite/logging.hpp>
#include <boost/date_time.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <QStringList>
#include <QDebug>
#include <QMessageBox>
#include <QDir>

const std::string SSAModel::kTypeId = "signalviewer.SSAModel.1";
const std::string SSAProperties::kTypeId = "signalviewer.SSAProperties.1";

struct SSAModel::SSAModelData {
  SSAModelData() : is_open(false) {}
  QString folder;
  QList<FileInfo> list;
  QVector<QVector<QString> > transs;
  QVector<QSharedPointer<trans::IStrictTypedTrans<std::vector<double> > > > transformations;
  QVector<QVector<QString> > fft_transs;
  QVector<QSharedPointer<trans::IStrictTypedTrans<std::vector<TPoint> > > > fft_transformations;
  QVector<QVector<QString> > stats;
  QVector<QSharedPointer<stats::StatCollector> > stat_coll;
  stats::StatProperties stats_providers;
  int data_type;
  bool is_open;
};

SSAModel::SSAModel(QObject *parent) : QObject(parent)
                                     ,_data(new SSAModelData())
                                     ,_list(new epoch::EpochList())
                                     ,_info(new event::EventAnnotation())
                                     {}

SSAModel::~SSAModel() {}

bool SSAModel::isOpen() const
{
  return _list->isOpen();
}

int SSAModel::numEpochs() const
{
  return _list->size();
}

const SSAProperties &SSAModel::props() const
{
  return _props;
}

const QVector<QVector<QString> > &SSAModel::transs() const
{
  return _data->transs;
}

const QVector<QVector<QString> > &SSAModel::fftTranss() const
{
  return _data->fft_transs;
}

const QVector<QVector<QString> > &SSAModel::statss() const
{
  return _data->stats;
}

void SSAModel::transform(int idx, std::vector<double> &signal) const
{
  if(idx>=_data->transformations.size() || idx<0)
    return;
  if(!_data->transformations[idx])
    return;
  _data->transformations[idx]->calc(signal);
}

void SSAModel::fftTrans(int idx, std::vector<TPoint> &fft) const
{
  if(idx>=_data->fft_transformations.size() || idx<0)
    return;
  if(!_data->fft_transformations[idx])
    return;
  _data->fft_transformations[idx]->calc(fft);
}

QVector<QPair<QString, QString> > SSAModel::stats(int idx, const std::vector<double> &signal)
{
  QVector<QPair<QString, QString> > stats;
  if(idx>=_data->stat_coll.size() || idx<0)
    return stats;
  if(!_data->stat_coll[idx])
    return stats;
  std::vector<double> sts;
  _data->stat_coll[idx]->calc(signal, sts);
  for(size_t i=0; i<sts.size(); ++i)
    stats.push_back(QPair<QString, QString>(QString::fromStdString(_data->stat_coll[idx]->calculator()[i]->name()),
                                            QString::number(sts[i])));
  return stats;
}

const epoch::EpochList &SSAModel::list()const
{
  return *_list;
}

const event::EventAnnotation &SSAModel::info() const
{
  return *_info;
}

const TagsInfo &SSAModel::tagsInfo() const
{
  return _tags;
}

std::string SSAModel::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  if(_data->is_open) {
    pt.put("folder", _data->folder.toStdString());
    pt.put("file_format", vr::str::any_string(_file_formats[_data->data_type]->name()).str());
    std::string ssa_props_json = _props.toString();
    ptree ssa_props_ptree;
    std::stringstream ssa_props_ss(ssa_props_json);
    boost::property_tree::json_parser::read_json(ssa_props_ss, ssa_props_ptree);
    pt.put_child("ssa_props", ssa_props_ptree);
    foreach(const FileInfo& fi, _data->list) {
      ptree fi_pt;
      fi_pt.put("file_name", fi.file_info.file_name);
      fi_pt.put("file_path", fi.file_info.file_path);
      fi_pt.put("channel_idx", fi.file_info.channel_idx);
      fi_pt.put("json_props", fi.file_info.prop_json);
      fi_pt.put("chan_name", fi.chan_name.toStdString());
      fi_pt.put("file_type", fi.file_type);
      pt.add_child("file_info", fi_pt);
    }
  }
  std::string info_json = _info->toString();
  ptree info_pt;
  std::stringstream info_ss(info_json);
  boost::property_tree::json_parser::read_json(info_ss, info_pt);
  pt.put_child("info", info_pt);
  std::string tags_json;
  std::set<std::string> tags = _info->listTags();
  foreach(const QString tag_key, _tags.colors.keys())
    tags.insert(tag_key.toStdString());
  std::map<std::string, size_t> tags_colors;
  foreach(const QString& c, _tags.colors.keys())
    tags_colors.insert(std::make_pair(c.toStdString(), _tags.colors[c].rgb()));
  event::EventInfoSummary::TagsToString(tags, tags_colors, tags_json);
  std::stringstream tags_ss(tags_json);
  ptree tags_pt;
  boost::property_tree::json_parser::read_json(tags_ss, tags_pt);
  pt.put_child("tags", tags_pt);
  for(int i=0; i<_data->stat_coll.size(); ++i) {
    if(!_data->stat_coll[i])
      continue;
    ptree stats_pt;
    foreach(const stats::StatCollector::Calculator& calc,_data->stat_coll[i]->calculator()) {
      std::stringstream stat_ss(calc->toString());
      ptree stat_pt;
      boost::property_tree::json_parser::read_json(stat_ss, stat_pt);
      stats_pt.add_child("stat", stat_pt);
    }
    stats_pt.put("idx", i);
    pt.add_child("stats", stats_pt);
  }
  for(int i=0; i<_data->transformations.size(); ++i) {
    if(!_data->transformations[i])
      continue;
    ptree trans_pt;
    std::stringstream trans_ss(_data->transformations[i]->toString());
    boost::property_tree::json_parser::read_json(trans_ss, trans_pt);
    ptree transs_pt;
    transs_pt.put_child("trans", trans_pt);
    transs_pt.put("idx", i);
    pt.add_child("transes", transs_pt);
  }
  for(int i=0; i<_data->fft_transformations.size(); ++i) {
    if(!_data->fft_transformations[i])
      continue;
    ptree trans_pt;
    std::stringstream trans_ss(_data->fft_transformations[i]->toString());
    boost::property_tree::json_parser::read_json(trans_ss, trans_pt);
    ptree transs_pt;
    transs_pt.put_child("trans", trans_pt);
    transs_pt.put("idx", i);
    pt.add_child("fft_transes", transs_pt);
  }
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool SSAModel::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId)
    return false;
  _data = QSharedPointer<SSAModelData>::create();
  _list = QSharedPointer<epoch::EpochList>::create();
  _info = QSharedPointer<event::EventAnnotation>::create();
  _tags.colors.clear();

  QString folder = QString::fromStdString(pt.get<std::string>("folder"));
  std::wstring file_format = vr::str::any_string(pt.get<std::string>("file_format")).wstr();
  int data_type = -1;
  for(int i=0; i<_file_formats.size(); ++i)
    if(_file_formats[i]->name() == file_format)
      data_type = i;
  if(data_type<0)
    return false;
  ptree ssa_props_pt = pt.get_child("ssa_props");
  std::stringstream ssa_props_ss;
  boost::property_tree::json_parser::write_json(ssa_props_ss, ssa_props_pt);
  if(!_props.fromString(ssa_props_ss.str()))
     return false;

  QList<FileInfo> list;
  int idx = 0;
  BOOST_FOREACH(const ptree::value_type &file_info_it, pt.equal_range("file_info")) {
    const ptree& fi_pt = file_info_it.second;
    FileInfo fi;
    fi.file_info.file_name = fi_pt.get<std::string>("file_name");
    fi.file_info.file_path = fi_pt.get<std::string>("file_path");
    fi.file_info.channel_idx = fi_pt.get<size_t>("channel_idx");
    fi.file_info.prop_json = fi_pt.get<std::string>("json_props");
    fi.chan_name = QString::fromStdString(fi_pt.get<std::string>("chan_name"));
    fi.file_type = fi_pt.get<std::string>("file_type");
    fi.id = idx++;
    fi.name = QString::fromStdString(fi.file_info.file_name);
    list.push_back(fi);
  }

  open(folder, list, data_type);// the ssa_props should be already filled
  ptree info_pt = pt.get_child("info");
  std::stringstream info_ss;
  boost::property_tree::json_parser::write_json(info_ss, info_pt);
  if(!_info->fromString(info_ss.str()))
    return false;
  ptree tags_pt = pt.get_child("tags");
  std::stringstream tags_ss;
  boost::property_tree::json_parser::write_json(tags_ss, tags_pt);
  std::set<std::string> tags;
  std::map<std::string,size_t> tags_colors;
  if(!event::EventInfoSummary::StringToTags(tags_ss.str(), tags_colors, tags))
    return false;
  for(std::map<std::string, size_t>::const_iterator it = tags_colors.begin(); it != tags_colors.end(); ++it)
    _tags.colors.insert(QString::fromStdString((*it).first), QColor((*it).second));
  QVector<QVector<QString> > stats(_list->activeTypes().size());
  BOOST_FOREACH(const ptree::value_type &stats_it, pt.equal_range("stats")) {
    QVector<QString> sts;
    const ptree& stats_pt = stats_it.second;
    BOOST_FOREACH(const ptree::value_type &stat_it, stats_pt.equal_range("stat")) {
      const ptree& stat_pt = stat_it.second;
      std::stringstream stat_ss;
      boost::property_tree::json_parser::write_json(stat_ss, stat_pt);
      sts.push_back(QString::fromStdString(stat_ss.str()));
    }
    int idx = stats_pt.get<int>("idx");
    stats[idx] = sts;
  }
  setStats(stats);
  QVector<QVector<QString> > trans(_list->activeTypes().size());
  BOOST_FOREACH(const ptree::value_type &stats_it, pt.equal_range("transes")) {
    QVector<QString> trs;
    const ptree& transes_pt = stats_it.second;
    const ptree& trans_pt = transes_pt.get_child("trans");
    std::stringstream trans_ss;
    boost::property_tree::json_parser::write_json(trans_ss, trans_pt);
    const std::string& trans_js = trans_ss.str();
    if(trans_js.empty())
      continue;
    if(trans::TransformProperties::ReadType(trans_js) == trans::TransformProperties::Chain) {
      trans::ChainTrans<std::vector<double> > ct;
      if(!ct.fromString(trans_js))
        return false;
      foreach(trans::ChainTrans<std::vector<double> >::TransPtr t, ct.transes())
        trs.append(QString::fromStdString(t->toString()));
    }
    else
      trs.append(QString::fromStdString(trans_js));
    int idx = transes_pt.get<int>("idx");
    trans[idx] = trs;
  }
  setTrans(trans);

  QVector<QVector<QString> > fft_trans(_list->activeTypes().size());
  BOOST_FOREACH(const ptree::value_type &stats_it, pt.equal_range("fft_transes")) {
    QVector<QString> trs;
    const ptree& transes_pt = stats_it.second;
    const ptree& trans_pt = transes_pt.get_child("trans");
    std::stringstream trans_ss;
    boost::property_tree::json_parser::write_json(trans_ss, trans_pt);
    const std::string& trans_js = trans_ss.str();
    if(trans_js.empty())
      continue;
    if(trans::TransformProperties::ReadType(trans_js) == trans::TransformProperties::Chain) {
      trans::ChainTrans<std::vector<TPoint> > ct;
      if(!ct.fromString(trans_js))
        return false;
      foreach(trans::ChainTrans<std::vector<TPoint> >::TransPtr t, ct.transes())
        trs.append(QString::fromStdString(t->toString()));
    }
    else
      trs.append(QString::fromStdString(trans_js));
    int idx = transes_pt.get<int>("idx");
    fft_trans[idx] = trs;
  }
  setFftTrans(fft_trans);
  return true;
}

std::vector<boost::shared_ptr<stats::ITypedStatCalculator> > SSAModel::defaultStats()
{
  return _data->stats_providers.defaultStats(_props.epoch_len);
}

QStringList SSAModel::initStatsWidgets(QWidget *parent) const
{
  const std::vector<std::wstring>& names = _data->stats_providers.initProviders(parent);
  QStringList stats_names;
  foreach(const std::wstring& name, names)
    stats_names<<Utils::FromStd(name);
  return stats_names;
}

QString SSAModel::statsPropertiesFromType(const std::string &type, const QString &new_json, QWidget* parent)
{
  const std::wstring& name = _data->stats_providers.nameByType(type);
  return QString::fromStdString(_data->stats_providers.getParameter(name, new_json.toStdString(), parent));
}

QString SSAModel::statsProviderNameFromType(const std::string &type) const
{
  return Utils::FromStd(_data->stats_providers.nameByType(type));
}

QString SSAModel::statsProperties(const QString &name, const QString &new_json, const QWidget* parent)
{
  return QString::fromStdString(_data->stats_providers.getParameter(Utils::FromQt(name), new_json.toStdString(), (void*)parent));
}

void SSAModel::addFileFormat(epoch::IEpochListProvider *elp)
{
  _file_formats.append(elp);
}

void SSAModel::addStatsProvider(stats::IStatisticsProvider* stats_provider)
{
  _data->stats_providers.addProvider(boost::shared_ptr<stats::IStatisticsProvider>(stats_provider));
}

QVector<epoch::IEpochListProvider *> SSAModel::fileFormats()
{
  return _file_formats;
}

void SSAModel::setEpochIdx(size_t epoch_idx)
{
  emit sigSetEpoch(epoch_idx);
}

void SSAModel::setTrans(const QVector<QVector<QString> > &transs)
{
  _data->transs = transs;
  _data->transformations.clear();
  for(int i=0; i<transs.size(); ++i) {
    std::vector<std::string> jsons(transs[i].size());
    for(int j=0; j<transs[i].size(); ++j)
      jsons[j] = transs[i][j].toStdString();
    _data->transformations.append(QSharedPointer<trans::IStrictTypedTrans<std::vector<double> > >(trans::TransformProperties::Create<std::vector<double> >(jsons)));
  }
  emit sigDataUpdate();
}

void SSAModel::setStats(const QVector<QVector<QString> > &sts)
{
  _data->stats = sts;
  _data->stat_coll.clear();
  for(int i=0; i<sts.size(); ++i) {
    std::vector<std::string> jsons(sts[i].size());
    for(int j=0; j<sts[i].size(); ++j)
      jsons[j] = sts[i][j].toStdString();
    QSharedPointer<stats::StatCollector> psc(new stats::StatCollector());
    _data->stats_providers.updateCollector(jsons, *psc);
    _data->stat_coll.append(psc);
  }
  emit sigDataUpdate();
}

void SSAModel::setFftTrans(const QVector<QVector<QString> > &transs)
{
  _data->fft_transs = transs;
  _data->fft_transformations.clear();
  for(int i=0; i<transs.size(); ++i) {
    std::vector<std::string> jsons(transs[i].size());
    for(int j=0; j<transs[i].size(); ++j)
      jsons[j] = transs[i][j].toStdString();
    _data->fft_transformations.append(QSharedPointer<trans::IStrictTypedTrans<std::vector<TPoint> > >(trans::TransformProperties::Create<std::vector<TPoint> >(jsons)));
  }
  emit sigDataUpdate();
}

void SSAModel::setProps(const SSAProperties &props)
{
  if(_props.epoch_len!=props.epoch_len)
    if(_data->is_open)
      open(_data->folder, _data->list, _data->data_type, props.epoch_len);
  _props = props;
}

void SSAModel::setTagsInfo(const TagsInfo &tags)
{
  _tags = tags;
  emit sigInfoUpdate();
}

void SSAModel::open(const QString& folder, const QList<FileInfo>& list, int data_type, int epoch_len)
{
  _data->folder = folder;
  _data->list = list;
  _data->data_type = data_type;
  _data->is_open = true;
  epoch::EpochList::TFileNames files_paths;

  for (int i = 0; i < list.size(); ++i)
    files_paths[list[i].file_type].push_back(list[i].file_info);
  if(!_list->open(*_file_formats[data_type], (size_t)epoch_len, files_paths))
    return;
  _data->transs = QVector<QVector<QString> >(_list->activeTypes().size());
  _data->fft_transs = QVector<QVector<QString> >(_list->activeTypes().size());
  _data->stats = QVector<QVector<QString> >(_list->activeTypes().size());
  emit sigDataOpen();
}

void SSAModel::open(const QString &folder, const QList<FileInfo> &list, int data_type)
{
  open(folder, list, data_type, _props.epoch_len);
}

void SSAModel::chartsNames(QStringList &charts_names)const
{
  charts_names.clear();
  const std::set<std::string>& act_types = _list->activeTypes();
  foreach(const std::string& tp, act_types)
    charts_names.append(QString::fromStdString(tp));
}

bool SSAModel::read(int idx, epoch::Epoch &epoch)
{
  return _list->read(idx, epoch);
}

std::vector<event::Tag> SSAModel::readAnno(int st, int sz) const
{
  boost::posix_time::ptime start = _list->timeFromIdx(st);
  boost::posix_time::ptime end = _list->timeFromIdx(st+sz);
  return _info->anno(start, end);
}

std::vector<event::Tag> SSAModel::readTags(int st, int sz) const
{
  boost::posix_time::ptime start = _list->timeFromIdx(st);
  boost::posix_time::ptime end = _list->timeFromIdx(st+sz);
  return _info->tags(start, end);
}

bool SSAModel::updateAnno(int idx, const std::vector<event::EventInfo> &anno)
{
  boost::posix_time::ptime start = _list->timeFromIdx(idx);
  boost::posix_time::ptime end =_list->timeFromIdx(idx+1);
  return updateAnno(start, end, anno);
}

bool SSAModel::updateTags(int idx, const std::vector<event::EventInfo> &tags)
{
  boost::posix_time::ptime start = _list->timeFromIdx(idx);
  boost::posix_time::ptime end =_list->timeFromIdx(idx+1);
  return updateTags(start, end, tags);
}

bool SSAModel::updateAnno(const boost::posix_time::ptime &start,
                          const boost::posix_time::ptime &end,
                          const std::vector<event::EventInfo> &annos)
{
  _info->updateAnno(start, end, annos);
  emit sigInfoUpdate();
  return true;
}

bool SSAModel::updateTags(const boost::posix_time::ptime &start, const boost::posix_time::ptime &end,
                          const std::vector<event::EventInfo> &tags)
{
  _info->updateTags(start, end, tags);
  emit sigInfoUpdate();
  return true;
}

bool SSAModel::loadAnnos(const char *const file_name)
{
  bool res = _info->loadInfo(file_name);
  emit sigInfoUpdate();
  return res;
}

void SSAModel::saveAnnos(const char *const file_name) const
{
  _info->saveInfo(file_name);
}

bool SSAModel::fromFile(const char *const file_name)
{
  std::ifstream fin;
  fin.unsetf(std::ios::skipws);
  fin.open(file_name);
  if(!fin.is_open()) {
    LOGLITE_LOG_L1("Cant open file:"<<file_name);
    return false;
  }
  std::stringstream ss;
  std::copy(std::istream_iterator<char>(fin), std::istream_iterator<char>(), std::ostream_iterator<char>(ss));
  return fromString(ss.str());
}

void SSAModel::toFile(const char *const file_name) const
{
  std::ofstream fout;
  fout.open(file_name);
  if(!fout.is_open()) {
    LOGLITE_LOG_L1("Cant open file to write:"<<file_name);
    return;
  }
  fout<<toString();
}


std::set<std::string> SSAModel::tags() const
{
  return _info->listTags();
}

std::vector<std::string> SSAModel::getActiveTypes()const
{
  const std::set<std::string>& act_tps=_list->activeTypes();
  return std::vector<std::string> (act_tps.begin(), act_tps.end());;
}

std::string SSAProperties::toString() const
{
  using namespace boost::property_tree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("epoch_len", epoch_len);
  std::stringstream ss;
  json_parser::write_json(ss, pt);
  return ss.str();
}

bool SSAProperties::fromString(const std::string &json)
{
  using namespace boost::property_tree;
  std::stringstream ss(json);
  ptree pt;
  try {
    json_parser::read_json(ss, pt);
  }
  catch(json_parser_error& ex) {
    LOGLITE_LOG_L1(ex.what());
    return false;
  }
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found()) {
    LOGLITE_LOG_L1("cant read the type_id from "<<json);
    return false; 
  }
  try {
    std::string type_str = pt.get<std::string>("type_id");
    if(type_str!=kTypeId) {
      LOGLITE_LOG_L1("Incorrect type_id "<<type_str);
      return false;
    }
    epoch_len = pt.get<int>("epoch_len");
  }
  catch(ptree_bad_data&ex) {
    LOGLITE_LOG_L1(ex.what());
  }
  return true;
}
