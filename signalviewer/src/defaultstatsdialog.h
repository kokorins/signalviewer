#ifndef DEFAULTSTATSDIALOG_H
#define DEFAULTSTATSDIALOG_H

#include <QDialog>

namespace Ui {
  class DefaultStatsDialog;
}

class SSAModel;

class DefaultStatsDialog : public QDialog {
  Q_OBJECT
public:
  explicit DefaultStatsDialog(QWidget *parent = 0);
  ~DefaultStatsDialog();
  void setModel(SSAModel* model);
protected slots:
  void slotAdd();
signals:
  void sigAddStats(const QVector<QString>&);
private:
  SSAModel * _model;
  QVector<QString> _names;
  QVector<QString> _jsons;
  Ui::DefaultStatsDialog *ui;
};

#endif // DEFAULTSTATSDIALOG_H
