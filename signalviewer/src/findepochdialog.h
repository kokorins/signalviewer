#ifndef FINDEPOCHDIALOG_H
#define FINDEPOCHDIALOG_H

#include <QDialog>

namespace Ui {
class FindEpochDialog;
}

class FindEpochDialog : public QDialog
{
  Q_OBJECT  
public:
  explicit FindEpochDialog(QWidget *parent = 0);
  ~FindEpochDialog();
  int epoch()const;
  void setEpoch(int idx, int max);
private:
  Ui::FindEpochDialog *ui;
};

#endif // FINDEPOCHDIALOG_H
