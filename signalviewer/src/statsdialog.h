#ifndef STATSDIALOG_H
#define STATSDIALOG_H

#include <QDialog>

namespace Ui {
class StatsDialog;
}

class SSAModel;
class StatsDialog : public QDialog {
  Q_OBJECT
public:
  explicit StatsDialog(QWidget *parent = 0);
  ~StatsDialog();
  const QVector<QVector<QString> >& stats()const;
  void setModel(SSAModel*model);
protected slots:
  void slotAdd();
  void slotEdit();
  void slotRem();
  void updateList(int idx);
  void slotChangeChan(int);
  void slotDefault();
  void slotAddDefault(const QVector<QString>&);
private:
  QVector<QVector<QString> > statss;
  SSAModel* _model;
  Ui::StatsDialog *ui;
};

#endif // STATSDIALOG_H
