#include "statsdialog.h"
#include "editstatsdialog.h"
#include "ssamodel.h"
#include "defaultstatsdialog.h"
#include "ui_statsdialog.h"

#include <stats/statcalculator.h>

StatsDialog::StatsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::StatsDialog)
{
  ui->setupUi(this);
  connect(ui->dialog_buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->dialog_buttons, SIGNAL(rejected()), this, SLOT(reject()));
  connect(ui->add_button, SIGNAL(clicked()), this, SLOT(slotAdd()));
  connect(ui->edit_button, SIGNAL(clicked()), this, SLOT(slotEdit()));
  connect(ui->remove_button, SIGNAL(clicked()), this, SLOT(slotRem()));
  connect(ui->default_button, SIGNAL(clicked()), this, SLOT(slotDefault()));
}

StatsDialog::~StatsDialog()
{
  delete ui;
}

const QVector<QVector<QString> > &StatsDialog::stats() const
{
  return statss;
}

void StatsDialog::slotAdd()
{
  //TODO check for both fft and usual
  const std::vector<boost::shared_ptr<stats::ITypedStatCalculator> >& default_stats = _model->defaultStats();
  if(default_stats.empty())
    return;
  int idx = ui->channel_combo->currentIndex();
  if(idx<0)
    return;
  statss[idx].append(QString::fromStdString(default_stats.front()->toString()));
  updateList(statss[idx].size()-1);
}

void StatsDialog::slotEdit()
{
  int idx = ui->stats_edit->currentRow();
  if(idx<0)
    return;
  EditStatsDialog *edit = new EditStatsDialog(this);
  edit->setModel(_model);
  int chan_idx = ui->channel_combo->currentIndex();
  edit->setStats(statss[chan_idx][idx]);
  if(edit->exec()!=QDialog::Accepted)
    return;
  statss[chan_idx][idx] = edit->stats();
  updateList(idx);
}

void StatsDialog::slotRem()
{
  int idx = ui->stats_edit->currentRow();
  if(idx<0)
    return;
  int chan_idx = ui->channel_combo->currentIndex();
  statss[chan_idx].remove(idx);
  updateList(-1);
}

void StatsDialog::updateList(int idx)
{
  ui->stats_edit->clear();
  int chan_idx = ui->channel_combo->currentIndex();
  if(chan_idx<0)
    return;
  foreach(const QString& ti, statss[chan_idx])
    ui->stats_edit->addItem(ti);
  if(idx<0 || idx>= ui->stats_edit->count())
    return;
  ui->stats_edit->setCurrentRow(idx);
}

void StatsDialog::slotChangeChan(int)
{
  updateList(-1);
}

void StatsDialog::slotDefault()
{
  DefaultStatsDialog* dsd = new DefaultStatsDialog(this);
  dsd->setModel(_model);
  connect(dsd, SIGNAL(sigAddStats(QVector<QString>)),
          this, SLOT(slotAddDefault(QVector<QString>)));
  dsd->exec();
}

void StatsDialog::slotAddDefault(const QVector<QString> &jsons)
{
  int idx = ui->channel_combo->currentIndex();
  if(idx<0)
    return;
  for(int i=0; i<jsons.size(); ++i)
    statss[idx].append(jsons[i]);
  updateList(statss[idx].size()-1);
}

void StatsDialog::setModel(SSAModel* model)
{
  _model = model;
  QStringList charts_names;
  if(_model->isOpen())
    _model->chartsNames(charts_names);
  statss = _model->statss();
  ui->channel_combo->clear();
  ui->channel_combo->addItems(charts_names);
  connect(ui->channel_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(slotChangeChan(int)));
  slotChangeChan(0);
}
