#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent), ui(new Ui::AboutDialog)
{
  ui->setupUi(this);
  connect(ui->ok_button, SIGNAL(clicked()), this, SLOT(accept()));
  ui->name_label->setText(qApp->applicationName());
  ui->version_label->setText(qApp->applicationVersion());
}

AboutDialog::~AboutDialog()
{
  delete ui;
}
