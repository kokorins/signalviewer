#ifndef TAGSDIALOG_H
#define TAGSDIALOG_H

#include <QDialog>
#include <QSet>
#include <QString>
#include <QMap>

namespace Ui {
class TagsDialog;
}

class TagsDialog : public QDialog {
  Q_OBJECT
  
public:
  explicit TagsDialog(QWidget *parent = 0);
  ~TagsDialog();
public:
  void setTags(const QStringList&, const QMap<QString, QColor> &colors);
  QStringList fastTags()const;
  const QMap<QString, QColor>& colors()const;
private slots:
  void slotUpdateTagsList();
  void slotSaveTags();
  void slotLoadTags();
  void slotAddFastTags();
  void slotRemoveFastTags();
  void slotAddTags();
  void slotRemoveTags();
  void slotColor();
  void slotRemColor();
private:
  QMap<QString,QColor> _colors;
  Ui::TagsDialog *ui;
};

#endif // TAGSDIALOG_H
