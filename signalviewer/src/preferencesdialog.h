#ifndef PREFERENCESDIALOG_H
#define PREFERENCESDIALOG_H

#include <QDialog>

namespace Ui {
class PreferencesDialog;
}

class QAbstractButton;
struct SSAProperties;

class PreferencesDialog : public QDialog {
  Q_OBJECT
public:
  explicit PreferencesDialog(QWidget *parent = 0);
  ~PreferencesDialog();
  void setProps(const SSAProperties& props);
protected slots:
  void slotApply();
  void slotSelectButtons(QAbstractButton*);
signals:
  void apply(const SSAProperties& props);
private:
  Ui::PreferencesDialog *ui;
};

#endif // PREFERENCESDIALOG_H
