#include "findtimestampdialog.h"
#include "ui_findtimestampdialog.h"
#include <QDebug>

FindTimestampDialog::FindTimestampDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::FindTimestampDialog)
{
  ui->setupUi(this);
}

FindTimestampDialog::~FindTimestampDialog()
{
  delete ui;
}

void FindTimestampDialog::setTimestamp(const QDateTime & cur_timestamp,
                                       const QDateTime& from, const QDateTime& to)
{
  ui->epoch_date->setDateTimeRange(from, to);
  ui->epoch_date->setDateTime(cur_timestamp);
}


QDateTime FindTimestampDialog::timestamp() const
{
  return ui->epoch_date->dateTime();
}
