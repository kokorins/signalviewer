#ifndef EDITEVENTDIALOG_H
#define EDITEVENTDIALOG_H

#include <event/eventinfo.h>
#include <QDialog>
#include <QDateTime>

namespace Ui {
  class EditEventDialog;
}

class EditEventDialog : public QDialog {
  Q_OBJECT
public:
  explicit EditEventDialog(QWidget *parent = 0);
  ~EditEventDialog();
  void setStart(const QDateTime& start);
  void setEnd(const QDateTime& end);
  void setEventInfo(const QVector<event::EventInfo> &info);
  QDateTime start()const;
  QDateTime end()const;
  QVector<event::EventInfo> eventInfo()const;
private:
  Ui::EditEventDialog *ui;
};

#endif // EDITEVENTDIALOG_H
