#include "signalnavigatorwidget.h"
#include "ui_signalnavigatorwidget.h"
#include "ssamodel.h"
#include "transformationsdialog.h"
#include "statsdialog.h"
#include "tagsdialog.h"
#include "findepochdialog.h"
#include "findtimestampdialog.h"
#include "chartwidget.h"
#include "utils.h"
#include <epoch/epoch.h>
#include <event/eventinfo.h>
#include <event/eventannotation.h>
#include <epoch/epochlist.h>
#include <transformation/utils.h>
#include <boost/date_time.hpp>
#include <loglite/logging.hpp>
#include <algorithm>
#include <QSplitter>
#include <QCheckBox>
#include <QBrush>
#include <QRegExp>
#include <QAction>
#include <QDateTime>
#include <QIcon>
#include <QPainter>
#include <QSet>
#include <QRegularExpression>
#include <qwt_plot.h>
#include <qwt_plot_layout.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_zoomer.h>
#include <qwt_legend.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_draw.h>
#include <qwt_plot_scaleitem.h>
#include <qwt_text_label.h>
#include <qwt_plot_zoomer.h>
#include <qwt_scale_widget.h>
#include <qwt_scale_engine.h>

class TimeScaleDraw: public QwtScaleDraw {
public:
  TimeScaleDraw(const QDateTime &start, const QDateTime &end, int num_pt): _start(start), _end(end), _num_pt(num_pt) {}
  virtual QwtText label(double v) const
  {
    boost::posix_time::ptime end = Utils::FromQt(_end);
    boost::posix_time::ptime start = Utils::FromQt(_start);
    boost::posix_time::time_duration dur = (end-start);
    double step = 1.* dur.total_milliseconds()/_num_pt;
    boost::posix_time::ptime val = start + boost::posix_time::milliseconds(v*step);
    return Utils::FromBoost(val).time().toString(QString("hh:mm:ss.zzz"));
  }
private:
  QDateTime _start, _end;
  int _num_pt;
};

SignalNavigatorWidget::SignalNavigatorWidget(QWidget *parent) :
  QWidget(parent), ui(new Ui::SignalNavigatorWidget)
{
  ui->setupUi(this);
  if(!ui->central_widget->layout())
    ui->central_widget->setLayout(new QVBoxLayout(ui->central_widget));
  chart_splitter = new QSplitter(ui->central_widget);
  chart_splitter->setOrientation(Qt::Vertical);
  ui->central_widget->layout()->addWidget(chart_splitter);
  ui->charts_list->verticalHeader()->setVisible(false);
  connect(ui->epoch_index, SIGNAL(valueChanged(int)), this, SLOT(slotEpochIndexChanged(int)));
  connect(ui->epoch_size, SIGNAL(valueChanged(int)), this, SLOT(slotEpochSizeChanged(int)));
  connect(ui->trans_button, SIGNAL(clicked()), this, SLOT(slotTrans()));
  connect(ui->trans_fft_button, SIGNAL(clicked()), this, SLOT(slotFftTrans()));
  connect(ui->stats_button, SIGNAL(clicked()), this, SLOT(slotStats()));
  connect(ui->tags_button, SIGNAL(clicked()), this, SLOT(slotTags()));

  QIcon qi = QIcon::fromTheme("system-monitor--arrow.png", QIcon(":/icons/system-monitor--arrow.png"));
  QAction * act = new QAction(qi, tr("Find epoch"), this);
  act->setShortcut(QKeySequence(tr("Ctrl+g")));
  connect(act, SIGNAL(triggered()), this, SLOT(slotFindEpoch()));
  this->addAction(act);

  qi = QIcon::fromTheme("clock--arrow.png", QIcon(":/icons/clock--arrow.png"));
  act = new QAction(qi, tr("Find timestamp"), this);
  act->setShortcut(QKeySequence(tr("Ctrl+f")));
  connect(act, SIGNAL(triggered()), this, SLOT(slotFindTimestamp()));
  this->addAction(act);

  qi = QIcon::fromTheme("arrow.png", QIcon(":/icons/arrow.png"));
  act = new QAction(qi, tr("Next epoch"), this);
  act->setShortcut(Qt::CTRL+Qt::Key_Right);
  connect(act, SIGNAL(triggered()), this, SLOT(slotNextEpoch()));
  this->addAction(act);

  qi = QIcon::fromTheme("arrow-180.png", QIcon(":/icons/arrow-180.png"));
  act = new QAction(qi, tr("Previous epoch"), this);
  act->setShortcut(Qt::CTRL + Qt::Key_Left);
  connect(act, SIGNAL(triggered()), this, SLOT(slotPrevEpoch()));
  this->addAction(act);

  qi = QIcon::fromTheme("arrow-090.png", QIcon(":/icons/arrow-090.png"));
  act = new QAction(qi, tr("Increase number of epochs"), this);
  act->setShortcut(Qt::CTRL+Qt::Key_Up);
  connect(act, SIGNAL(triggered()), this, SLOT(slotIncEpoch()));
  this->addAction(act);

  qi = QIcon::fromTheme("arrow-270.png", QIcon(":/icons/arrow-270.png"));
  act = new QAction(qi, tr("Decrease number of epochs"), this);
  act->setShortcut(Qt::CTRL + Qt::Key_Down);
  connect(act, SIGNAL(triggered()), this, SLOT(slotDecEpoch()));
  this->addAction(act);
}

SignalNavigatorWidget::~SignalNavigatorWidget()
{
  delete ui;
}

void SignalNavigatorWidget::setModel(SSAModel *model)
{
  _ssa_model = model;
  connect(_ssa_model, SIGNAL(sigDataOpen()), this, SLOT(slotDataOpen()));
  connect(_ssa_model, SIGNAL(sigDataUpdate()), this, SLOT(slotDataUpdate()));
  connect(_ssa_model, SIGNAL(sigSetEpoch(int)), this, SLOT(slotSetEpochIdx(int)));
  //TODO only colors and tags could be changed in this case
  connect(_ssa_model, SIGNAL(sigInfoUpdate()), this, SLOT(slotDataUpdate()));
}

void SignalNavigatorWidget::clearCentral()
{
  _charts_names.clear();
  delete chart_splitter;
  chart_splitter = 0;
  QLayoutItem *child;
  while ((child = ui->central_widget->layout()->takeAt(0)) != 0)
    delete child;
}

QColor SignalNavigatorWidget::findTagColor(const QStringList & tags) const
{
  foreach(const QString& tag, tags) {
    if(_ssa_model->tagsInfo().colors.find(tag)!=_ssa_model->tagsInfo().colors.end()) {
      QColor c = _ssa_model->tagsInfo().colors[tag];
      c.setAlphaF(0.3);
      return c;
    }
  }
  return QColor(QColor::Invalid);
}

QSplitter *SignalNavigatorWidget::CreateSplitter(QWidget *parent)
{
  QSplitter*chart_splitter = new QSplitter(parent);
  chart_splitter->setOrientation(Qt::Vertical);
  chart_splitter->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  chart_splitter->setHandleWidth(2);
  return chart_splitter;
}

void SignalNavigatorWidget::slotDataOpen()
{
  if(!_ssa_model)
    return;
  clearCentral();
  _charts.clear();
  chart_splitter = CreateSplitter(ui->central_widget);
  ui->central_widget->layout()->setMargin(0);
  ui->central_widget->layout()->addWidget(chart_splitter);
  _scale_plot = new QwtPlot(chart_splitter);
  _fake_curve = new QwtPlotCurve();
  _fake_curve->setVisible(false);
  _fake_curve->attach(_scale_plot);
  _scale_plot->enableAxis(QwtPlot::yLeft, false);
  _scale_plot->enableAxis(QwtPlot::xBottom, false);
  _scale = new QwtPlotScaleItem();
  _scale->setBorderDistance(0);
  QFont font = _scale->font();
  font.setPointSizeF(font.pointSizeF()/1.4);
  _scale->setFont(font);
  _scale->attach(_scale_plot);
  if(!_scale_plot->layout()) {
    _scale_plot->setLayout(new QHBoxLayout(_scale_plot));
    _scale_plot->layout()->setMargin(0);
    _scale_plot->setMinimumHeight(24);
    _scale_plot->setMaximumHeight(34);
  }
  chart_splitter->addWidget(_scale_plot);
  int idx = 0;
  int sz = 1;
  ui->epoch_index->setMaximum(_ssa_model->numEpochs()-1);
  ui->epoch_size->setMinimum(1);
  ui->epoch_size->setMaximum(_ssa_model->numEpochs());
  ChartsTableModel * model = new ChartsTableModel(this);
  connect(model, SIGNAL(sigShowChart(int,bool)), this, SLOT(slotShowChart(int, bool)));
  connect(model, SIGNAL(sigDrawChart(int,bool)), this, SLOT(slotDrawChart(int, bool)));
  _ssa_model->chartsNames(_charts_names);
  model->setFileInfos(_charts_names, QVector<bool>(_charts_names.size(), true));
  ui->charts_list->setModel(model);
  ui->charts_list->setItemDelegate(new ChartsTableDelegate(this));
  for(int i=0; i<model->rowCount(); ++i) {
    ui->charts_list->openPersistentEditor(model->index(i, ChartsTableModel::kShowIndex));
    ui->charts_list->openPersistentEditor(model->index(i, ChartsTableModel::kFFTIndex));
  }
//  for(int i=0; i<model->columnCount(); ++i)
//    ui->charts_list->horizontalHeader()->setResizeMode(i, QHeaderView::ResizeToContents);
  _act_tps = _ssa_model->getActiveTypes();
  _is_fft = QVector<bool>(_act_tps.size());
  for(size_t i=0; i<_act_tps.size(); ++i) {
    ChartWidget * plot = new ChartWidget(this);
    if(!plot->layout()) {
      plot->setLayout(new QHBoxLayout(plot));
      plot->layout()->setMargin(0);
    }
    plot->setTitle(_charts_names[i]);
    _charts.push_back(plot);
    chart_splitter->addWidget(plot);
  }
  chart_splitter->updateGeometry();
  ui->epoch_index->setValue(idx);
  ui->epoch_size->setValue(sz);
  connect(qApp, SIGNAL(focusChanged(QWidget*,QWidget*)), this, SLOT(slotEditAnno(QWidget*,QWidget*)));
  connect(ui->tags_edit, SIGNAL(editingFinished()), this, SLOT(slotSetTags()));
  slotEpochChanged(idx, sz);
}

void SignalNavigatorWidget::slotDataUpdate()
{
  slotEpochChanged(ui->epoch_index->value(), ui->epoch_size->value());
}

void SignalNavigatorWidget::slotShowChart(int idx, bool is_visible)
{
  chart_splitter->widget(idx+1)->setVisible(is_visible);
}

void SignalNavigatorWidget::slotDrawChart(int idx, bool is_fft)
{
  _is_fft[idx] = is_fft;
  slotEpochChanged(ui->epoch_index->value(), ui->epoch_size->value());
}

void SignalNavigatorWidget::slotEpochIndexChanged(int idx)
{
  slotEpochChanged(idx, ui->epoch_size->value());
}

void SignalNavigatorWidget::slotEpochSizeChanged(int sz)
{
  slotEpochChanged(ui->epoch_index->value(), sz);
}

void SignalNavigatorWidget::slotEpochChanged(int idx, int size)
{
  QVector<epoch::Epoch> epochs;
  epoch::Epoch epoch;

  for(int i = idx; i<idx+size; ++i)
    if(_ssa_model->read(i, epoch))
      epochs.push_back(epoch);
  if(epochs.empty())
    return;
  QDateTime start = Utils::FromBoost(_ssa_model->list().timeFromIdx(idx));
  QDateTime end = Utils::FromBoost(_ssa_model->list().timeFromIdx(idx+size));
  const static int kScaleGrain = 10;
  _scale->setScaleDraw(new TimeScaleDraw(start, end, kScaleGrain));
  QVector<QPointF> pts(kScaleGrain);
  for(int i=0; i<pts.size(); ++i)
    pts[i] = QPointF(i, 0);
  _fake_curve->setSamples(pts);
  _fake_curve->plot()->replot();
  _fake_curve->plot()->setAxisScale(QwtPlot::xBottom, 0, kScaleGrain);
  ui->stats_table->setRowCount(0);
  ui->tags_edit->blockSignals(true);
  ui->anno_edit->blockSignals(true);
  ui->anno_edit->setText("");
  ui->tags_edit->setText("");
  QStringList tags;
  QStringList annotations;
  const std::vector<event::Tag>& annos_events = _ssa_model->readAnno(idx, epochs.size());
  foreach(const event::Tag& n, annos_events)
    annotations<<QString::fromStdString(n.tag.tag());
  const std::vector<event::Tag>& tags_events = _ssa_model->readTags(idx, epochs.size());
  foreach(const event::Tag& n, tags_events)
    tags<<QString::fromStdString(n.tag.tag());
  for(size_t i=0; i<_act_tps.size(); ++i) {
    ChartWidget* chart = _charts[i];
    QVector<ColorEvent> evts;
    foreach(const event::Tag& n, tags_events) {
      QStringList epoch_tag;
      QString inf = QString::fromStdString(n.tag.tag());
      epoch_tag<<inf;
      ColorEvent ce;
      ce.color = findTagColor(epoch_tag);
      ce.start = Utils::FromBoost(n.start);
      ce.end = Utils::FromBoost(n.end);
      if(ce.color.isValid())
        evts.append(ce);
    }
    if(!_is_fft[i])
      chart->setEvents(evts, start, end);
  }
  annotations.removeDuplicates();
  ui->anno_edit->setText(annotations.join(tr(", ")));
  tags.removeDuplicates();
  ui->tags_edit->setText(tags.join(", "));
  for(size_t i=0; i<_act_tps.size(); ++i) {
    std::vector<double> y_val;
    for(int j=0; j<epochs.size(); ++j) {
      std::vector<double>const*const y = epochs[j].get(_act_tps[i]);
      if(y)
        y_val.insert(y_val.end(), y->begin(), y->end());
    }
    addStats(_ssa_model->stats(i, y_val), _charts_names[i]);
    _ssa_model->transform(i, y_val);
    if(_is_fft[i]) {
      std::vector<TPoint> pts = CUtils::Periodogram(y_val, _ssa_model->props().epoch_len);
      _ssa_model->fftTrans(i, pts);
      QVector<QPointF> points(pts.size());
      for(size_t pt_idx=0; pt_idx<pts.size(); ++pt_idx) {
        points[pt_idx].setX(pts[pt_idx].first);
        points[pt_idx].setY(pts[pt_idx].second);
      }
      _charts[i]->setEpoch(points);
    }
    else {
      QVector<QPointF> points(y_val.size());
      for(size_t pt_idx=0; pt_idx<y_val.size(); ++pt_idx)
        points[pt_idx] = QPointF(pt_idx, y_val[pt_idx]);
      if(y_val.size()==1)
        points.push_back(QPointF(y_val.size(), y_val.front()));
      _charts[i]->setEpoch(points);
    }
  }
  ui->tags_edit->blockSignals(false);
  ui->anno_edit->blockSignals(false);
}

void SignalNavigatorWidget::slotTrans()
{
  TransformationsDialog* trans = new TransformationsDialog(this);
  trans->setFFT(false);
  QStringList charts_names;
  if(_ssa_model->isOpen())
    _ssa_model->chartsNames(charts_names);
  trans->setChannels(charts_names, _ssa_model->transs());
  connect(trans, SIGNAL(sigApplyTrans(QVector<QVector<QString> >)), _ssa_model, SLOT(setTrans(QVector<QVector<QString> >)));
  if(trans->exec()==QDialog::Accepted)
    _ssa_model->setTrans(trans->trans());
}

void SignalNavigatorWidget::slotFftTrans()
{
  TransformationsDialog* trans = new TransformationsDialog(this);
  trans->setFFT(true);
  QStringList charts_names;
  if(_ssa_model->isOpen())
    _ssa_model->chartsNames(charts_names);
  trans->setChannels(charts_names, _ssa_model->fftTranss());
  connect(trans, SIGNAL(sigApplyTrans(QVector<QVector<QString> >)), _ssa_model, SLOT(setFftTrans(QVector<QVector<QString> >)));
  if(trans->exec()==QDialog::Accepted)
    _ssa_model->setFftTrans(trans->trans());
}

void SignalNavigatorWidget::slotStats()
{
  StatsDialog* stats = new StatsDialog(this);
  stats->setModel(_ssa_model);
  if(stats->exec()==QDialog::Accepted)
    _ssa_model->setStats(stats->stats());
}

void SignalNavigatorWidget::slotAnno()
{
  std::vector<event::EventInfo> eis;
  QString text = ui->anno_edit->document()->toPlainText();
  eis.emplace_back(text.toStdString());
  _ssa_model->updateAnno(ui->epoch_index->value(), eis);
}

void SignalNavigatorWidget::slotSetTags()
{
  std::vector<event::EventInfo> eis;
  QStringList tags = ui->tags_edit->text().split(QRegExp("[\\s,\\.]+"), QString::SkipEmptyParts);
  foreach(const QString& tag, tags)
    eis.emplace_back(tag.toStdString());
  _ssa_model->updateTags(ui->epoch_index->value(), eis);
}

void SignalNavigatorWidget::slotTags()
{
  TagsDialog * tags = new TagsDialog(this);
  QStringList tags_list;
  foreach(const std::string& t, _ssa_model->tags())
    tags_list.append(QString::fromStdString(t));
  tags_list.removeDuplicates();
  tags->setTags(tags_list, _ssa_model->tagsInfo().colors);
  if(tags->exec() !=  QDialog::Accepted)
    return;
  const QStringList& fast_tags = tags->fastTags();
  ui->fast_tags->clear();
  TagsInfo tags_info;
  tags_info.colors = tags->colors();
  _ssa_model->setTagsInfo(tags_info);
  for(int i=0; i<fast_tags.size(); ++i) {
    QToolButton* fast_button = new QToolButton(this);
    if(tags->colors().find(fast_tags[i])!=tags->colors().end()) {
      QPixmap pixmap(12, 12);
      pixmap.fill(tags->colors()[fast_tags[i]]);
      QIcon icon(pixmap);
      fast_button->setIcon(icon);
      fast_button->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    }
    fast_button->setText(fast_tags[i]);
    fast_button->setShortcut(QKeySequence(Qt::ALT+(Qt::Key_1+i)));
    connect(fast_button, SIGNAL(clicked()), this, SLOT(slotFastTag()));
    ui->fast_tags->addButton(fast_button, QDialogButtonBox::ActionRole);
  }
  slotDataUpdate();
}

void SignalNavigatorWidget::slotFastTag()
{
  QString tag = ((QToolButton*) sender())->text();
  std::vector<event::Tag> ens = _ssa_model->readAnno(ui->epoch_index->value(), 1);
  auto eis = std::vector<event::EventInfo>(ens.size());
  for(size_t i=0; i<ens.size(); ++i)
    eis[i] = ens[i].tag;
  event::EventInfo ei;
  ei.setTag(tag.toStdString());
  eis.push_back(ei);
  _ssa_model->updateTags(ui->epoch_index->value(), eis);
  slotDataUpdate();
}

void SignalNavigatorWidget::slotFindEpoch()
{
  if(!_ssa_model || !_ssa_model->isOpen())
    return;
  FindEpochDialog * fed = new FindEpochDialog(this);
  fed->setEpoch(ui->epoch_index->value(), ui->epoch_index->maximum());
  if(fed->exec()!=QDialog::Accepted)
    return;
  ui->epoch_index->setValue(fed->epoch());
}

void SignalNavigatorWidget::slotFindTimestamp()
{
  if(!_ssa_model || !_ssa_model->isOpen())
    return;
  FindTimestampDialog * fed = new FindTimestampDialog(this);
  epoch::Epoch ep;
  QDateTime cur, from, to;
  {
    _ssa_model->read(ui->epoch_index->value(), ep);
    const boost::posix_time::ptime& ts = ep.startTime();
    QDate dt(ts.date().year(), ts.date().month(), ts.date().day());
    QTime t(ts.time_of_day().hours(), ts.time_of_day().minutes(),
            ts.time_of_day().seconds());
    cur  = QDateTime(dt, t);
  }
  {
    _ssa_model->read(0, ep);
    const boost::posix_time::ptime& ts = ep.startTime();
    QDate dt(ts.date().year(), ts.date().month(), ts.date().day());
    QTime t(ts.time_of_day().hours(), ts.time_of_day().minutes(),
            ts.time_of_day().seconds());
    from  = QDateTime(dt, t);
  }
  {
    _ssa_model->read(_ssa_model->list().size(), ep);
    const boost::posix_time::ptime& ts = ep.startTime();
    QDate dt(ts.date().year(), ts.date().month(), ts.date().day());
    QTime t(ts.time_of_day().hours(), ts.time_of_day().minutes(),
            ts.time_of_day().seconds());
    to  = QDateTime(dt, t);
  }
  fed->setTimestamp(cur, from, to);
  if(fed->exec()!=QDialog::Accepted)
    return;
  const QDateTime& t = fed->timestamp();
  boost::gregorian::date dt(t.date().year(), t.date().month(), t.date().day());
  boost::posix_time::time_duration td(t.time().hour(), t.time().minute(), t.time().second());
  boost::posix_time::ptime new_t(dt, td);
  size_t idx = _ssa_model->list().idxFromTimestamp(new_t);
  if(idx>=_ssa_model->list().size())
    return;
  ui->epoch_index->setValue(idx);
}

void SignalNavigatorWidget::slotPrevEpoch()
{
  if(ui->epoch_index->value()>0)
    ui->epoch_index->setValue(ui->epoch_index->value()-1);
}

void SignalNavigatorWidget::slotNextEpoch()
{
  if(ui->epoch_index->value()<ui->epoch_index->maximum())
    ui->epoch_index->setValue(ui->epoch_index->value()+1);
}

void SignalNavigatorWidget::slotIncEpoch()
{
  if(ui->epoch_size->value()<ui->epoch_size->maximum())
    ui->epoch_size->setValue(ui->epoch_size->value()+1);
}

void SignalNavigatorWidget::slotDecEpoch()
{
  if(ui->epoch_size->value()>1)
    ui->epoch_size->setValue(ui->epoch_size->value()-1);
}

void SignalNavigatorWidget::slotSetEpochIdx(int epoch_idx)
{
  ui->epoch_index->setValue(epoch_idx);
}

void SignalNavigatorWidget::addStats(const QVector<QPair<QString, QString> > & stats, const QString& chan_name)
{
  int first_idx = ui->stats_table->rowCount();
  ui->stats_table->setRowCount(ui->stats_table->rowCount()+stats.size());
  for(int i=0; i<stats.size(); ++i) {
    QTableWidgetItem* name_item = new QTableWidgetItem(chan_name+QString(": ")+stats[i].first);
    ui->stats_table->setItem(first_idx+i, 0, name_item);
    QTableWidgetItem* value_item = new QTableWidgetItem(stats[i].second);
    ui->stats_table->setItem(first_idx+i, 1, value_item);
  }
}

void SignalNavigatorWidget::slotEditAnno(QWidget *, QWidget * to)
{
  if(to && QString(to->objectName()) == QString("anno_edit"))
    slotAnno();
}

void ChartsTableModel::setFileInfos(const QStringList &file_names, const QVector<bool> &is_shown)
{
  _charts_names = file_names;
  _is_shown = is_shown;
  _is_fft = QVector<bool>(_is_shown.size(), false);
}

int ChartsTableModel::rowCount(const QModelIndex &) const
{
  return _charts_names.size();
}

int ChartsTableModel::columnCount(const QModelIndex &) const
{
  return kNumIndex;
}

QVariant ChartsTableModel::data(const QModelIndex &index, int role) const
{
  int row = index.row();
  int col = index.column();
  if(role == Qt::DisplayRole) {
    switch(col) {
    break; case kNameIndex:
      return _charts_names[row];
    break; default:
      return QVariant();
    }
  }
  if(role==Qt::EditRole) {
    switch(col) {
    break; case kShowIndex:
      return _is_shown[row];
    break; case kFFTIndex:
      return _is_fft[row];
    break; default:
      return QVariant();
    }
  }
  return QVariant();
}

QVariant ChartsTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role==Qt::DisplayRole) {
    if(orientation == Qt::Horizontal) {
      switch(section) {
      break; case kShowIndex:
        return tr("Show");
      break; case kNameIndex:
        return tr("Chart name");
      break; case kFFTIndex:
        return tr("Show fft");
      break; default:
        return QVariant();
      }
    }
  }
  return QVariant();
}

bool ChartsTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if(role == Qt::EditRole) {
    if(index.column()==kShowIndex) {
      _is_shown[index.row()] = value.toBool();
      emit dataChanged(index,index);
      emit sigShowChart(index.row(), value.toBool());
      return true;
    }
    if(index.column()==kFFTIndex) {
      _is_fft[index.row()] = value.toBool();
      emit dataChanged(index,index);
      emit sigDrawChart(index.row(), value.toBool());
      return true;
    }
  }
  return true;
}

Qt::ItemFlags ChartsTableModel::flags(const QModelIndex &index) const
{
  if(index.column()==kShowIndex)
    return Qt::ItemIsEditable |Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if(index.column()==kFFTIndex)
    return Qt::ItemIsEditable |Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

QWidget *ChartsTableDelegate::createEditor(QWidget *parent,
                                           const QStyleOptionViewItem &,
                                           const QModelIndex &index) const
{
  if (index.column() == ChartsTableModel::kShowIndex) {
    QCheckBox *show_box = new QCheckBox(parent);
    show_box->setAutoFillBackground(true);
    show_box->setChecked(index.model()->data(index).toBool());
    connect(show_box, SIGNAL(clicked()), this, SLOT(emitCommitData()));
    return show_box;
  }
  if (index.column() == ChartsTableModel::kFFTIndex) {
    QCheckBox *show_box = new QCheckBox(parent);
    show_box->setAutoFillBackground(true);
    show_box->setChecked(index.model()->data(index).toBool());
    connect(show_box, SIGNAL(clicked()), this, SLOT(emitCommitData()));
    return show_box;
  }
  return 0;
}

void ChartsTableDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  if(index.column() == ChartsTableModel::kShowIndex) {
    QCheckBox *check_box = qobject_cast<QCheckBox*>(editor);
    if (!check_box)
        return;
    check_box->setChecked(index.model()->data(index, Qt::EditRole).toBool());
  }
  if(index.column() == ChartsTableModel::kFFTIndex) {
    QCheckBox *check_box = qobject_cast<QCheckBox*>(editor);
    if (!check_box)
        return;
    check_box->setChecked(index.model()->data(index, Qt::EditRole).toBool());
  }
}

void ChartsTableDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  if(index.column()==ChartsTableModel::kShowIndex) {
    QCheckBox *check_box = qobject_cast<QCheckBox *>(editor);
    if (!check_box)
        return;
    model->setData(index, check_box->isChecked());
  }
  if(index.column()==ChartsTableModel::kFFTIndex) {
    QCheckBox *check_box = qobject_cast<QCheckBox *>(editor);
    if (!check_box)
        return;
    model->setData(index, check_box->isChecked());
  }
}

void ChartsTableDelegate::emitCommitData()
{
  emit commitData(qobject_cast<QWidget *>(sender()));
}
