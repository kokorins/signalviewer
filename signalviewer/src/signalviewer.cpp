#include "signalviewer.h"
#include "ui_signalviewer.h"
#include "openfolderdialog.h"
#include "ssamodel.h"
#include "aboutdialog.h"
#include "preferencesdialog.h"
#include "loglite/logging.hpp"
#include "aboutplugindialog.h"
#include <interfaces/fileformats/epochlistprovider_qt.h>
#include <interfaces/stats/statisticsprovider_qt.h>
#include "qwt_plot.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPluginLoader>
#include <QDialog>

SignalViewer::SignalViewer(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::SignalViewerClass()), _model(new SSAModel(this))
{
  ui->setupUi(this);
  setupToolBar();
  setupActions();
  ui->signal_tab->setModel(_model);
  ui->events_tab->setModel(_model);
  connect(_model, SIGNAL(sigSetEpoch(int)), this, SLOT(slotActivateEpochTab()));
  if(!loadPlugin())
    LOGLITE_LOG_L1("No pluggins loaded");
}

SignalViewer::~SignalViewer()
{
  delete ui;
}

void SignalViewer::setupToolBar()
{
  ui->tool_bar->setWindowTitle(tr("File toolbar"));
  ui->tool_bar->addAction(ui->action_open);
  ui->tool_bar->addSeparator();
  ui->tool_bar->addAction(ui->action_open_session);
  ui->tool_bar->addAction(ui->action_save_session);
  ui->tool_bar->addAction(ui->action_open_annotations);
  ui->tool_bar->addAction(ui->action_save_annotations);
  ui->tool_bar->addSeparator();
}

void SignalViewer::setupActions()
{
  connect(ui->action_open, SIGNAL(triggered()), this, SLOT(selectFolder()));
  connect(ui->action_quit, SIGNAL(triggered()), this, SLOT(close()));
  connect(ui->action_about, SIGNAL(triggered()), this, SLOT(slotAbout()));
  connect(ui->action_preferences, SIGNAL(triggered()), this, SLOT(preferences()));
  connect(ui->action_plugins, SIGNAL(triggered()), this, SLOT(slotAboutPlugins()));
  connect(ui->action_qt, SIGNAL(triggered()), this, SLOT(slotAboutQt()));
  connect(ui->action_open_annotations, SIGNAL(triggered()), this, SLOT(slotOpenAnnos()));
  connect(ui->action_save_annotations, SIGNAL(triggered()), this, SLOT(slotSaveAnnos()));
  connect(ui->action_open_session, SIGNAL(triggered()), this, SLOT(slotOpenSession()));
  connect(ui->action_save_session, SIGNAL(triggered()), this, SLOT(slotSaveSession()));
}


void SignalViewer::selectFolder()
{
  OpenFolderDialog * ofd = new OpenFolderDialog(_model, this);
  ofd->show();
  if(ofd->exec()!=QDialog::Accepted)
    return;
  _model->open(ofd->folder(), ofd->list(), ofd->curDataType());
}

void SignalViewer::slotAbout()
{
  AboutDialog* ad = new AboutDialog(this);
  ad->show();
  ad->exec();
}

void SignalViewer::slotAboutQt()
{
  QMessageBox::aboutQt(this);
}

void SignalViewer::preferences()
{
  PreferencesDialog * pd = new PreferencesDialog(this);
  pd->setProps(_model->props());
  connect(pd, SIGNAL(apply(SSAProperties)), _model, SLOT(setProps(SSAProperties)));
  pd->show();
  if(pd->exec()!=QDialog::Accepted)
    return;
}

void SignalViewer::slotOpenAnnos()
{
  QString filename = QFileDialog::getOpenFileName(this, tr("Open annotations"), QString(), "Annotations (*.ann)");
  if(filename.isEmpty())
    return;
  if(!_model->loadAnnos(filename.toLatin1()))
    QMessageBox::information(this, tr("couldn't load annotations"), tr("The error while the annotation file loading"), QMessageBox::Ok);
}

void SignalViewer::slotSaveAnnos()
{
  QString filename = QFileDialog::getSaveFileName(this, tr("Save annotations"), QString(), "Annotations (*.ann)");
  if(filename.isEmpty())
    return;
  QFileInfo fi(filename);
  if(fi.suffix().isEmpty())
    filename.append(tr(".ann"));
  _model->saveAnnos(filename.toLatin1());
}

void SignalViewer::slotOpenSession()
{
  QString filename = QFileDialog::getOpenFileName(this, tr("Open session"), QString(), tr("Sessions (*.ses)"));
  if(filename.isEmpty())
    return;
  if(!_model->fromFile(filename.toLatin1()))
    QMessageBox::information(this, tr("couldn't load session"), tr("The error while the annotation file loading"), QMessageBox::Ok);
}

void SignalViewer::slotSaveSession()
{
  QString filename = QFileDialog::getSaveFileName(this, tr("Save session"), QString(), tr("Sessions (*.ses)"));
  if(filename.isEmpty())
    return;
  QFileInfo fi(filename);
  if(fi.suffix().isEmpty())
    filename.append(tr(".ses"));
  _model->toFile(filename.toLatin1());
}

void SignalViewer::slotActivateEpochTab()
{
  if(ui->tabWidget->currentWidget()!=ui->signal_tab)
    ui->tabWidget->setCurrentWidget(ui->signal_tab);
}

bool SignalViewer::loadPlugin()
{
  bool any_plugin = false;
  QDir pluginsDir(qApp->applicationDirPath());
  pluginsDir.cd("plugins");
  pluginsDir.cd("fileformats");
  QStringList nfs;
  nfs<<"*.dll"<<"*.so"<<"*.a"<<"*.lib";
  pluginsDir.setNameFilters(nfs);
  foreach (const QString& file_name, pluginsDir.entryList(QDir::Files)) {
    LOGLITE_LOG_L2("Plugin from "<<file_name.toStdString());
    QPluginLoader *pluginLoader = new QPluginLoader(pluginsDir.absoluteFilePath(file_name), this);
    QObject *plugin = pluginLoader->instance();
    if (plugin) {
      if(plugin->inherits("epoch::IEpochListProvider")) {
        epoch::IEpochListProvider * epoch_files = qobject_cast<epoch::IEpochListProvider*>(plugin);
        _model->addFileFormat(epoch_files);
      }
      else {
        plugin->deleteLater();
      }
      any_plugin = true;
    }
  }
  pluginsDir.cdUp();
  pluginsDir.cd("stats");
  foreach(const QString& file_name, pluginsDir.entryList(QDir::Files)) {
    LOGLITE_LOG_L2("Plugin from "<<file_name.toStdString());
    QPluginLoader *pluginLoader = new QPluginLoader(pluginsDir.absoluteFilePath(file_name), this);
    QObject *plugin = pluginLoader->instance();
    if (plugin) {
      if(plugin->inherits("stats::IStatisticsProvider")) {
        stats::IStatisticsProvider * stats_provider = qobject_cast<stats::IStatisticsProvider*>(plugin);
        _model->addStatsProvider(stats_provider);
      }
      else {
        plugin->deleteLater();
      }
      any_plugin = true;
    }
  }
  return any_plugin;
}

//TODO update plugin about
void SignalViewer::slotAboutPlugins()
{
  QDir pluginsDir(qApp->applicationDirPath());
  pluginsDir.cd("plugins");
  pluginsDir.cd("fileformats");
  AboutPluginDialog * apd = new AboutPluginDialog(pluginsDir.path(), pluginsDir.entryList(QDir::Files), this);
  apd->exec();
}
