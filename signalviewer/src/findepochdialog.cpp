#include "findepochdialog.h"
#include "ui_findepochdialog.h"

FindEpochDialog::FindEpochDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::FindEpochDialog)
{
  ui->setupUi(this);
}

FindEpochDialog::~FindEpochDialog()
{
  delete ui;
}

int FindEpochDialog::epoch() const
{
  return ui->epoch_spin->value();
}

void FindEpochDialog::setEpoch(int idx, int max)
{
  ui->epoch_spin->setMaximum(max);
  ui->epoch_spin->setValue(idx);
}
