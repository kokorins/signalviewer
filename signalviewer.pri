!win32:QMAKE_CXXFLAGS += -std=c++0x

win32 {
  BOOST_ROOT = "G:/opt/boost_1_54_0"
}
else {
  BOOST_ROOT = "/home/kokorins/boost_1_54_0"
}

INCLUDEPATH += $$BOOST_ROOT
LIBS += -L$$BOOST_ROOT/stage/lib
!win32:QMAKE_CXXFLAGS += -std=c++0x

win32 {
  CONFIG(release, debug|release) {
    LIBS += -llibboost_system-vc110-mt-1_54 -llibboost_filesystem-vc110-mt-1_54 -llibboost_date_time-vc110-mt-1_54 -llibboost_thread-vc110-mt-1_54
  }
  CONFIG(debug, debug|release) {
    LIBS += -llibboost_system-vc110-mt-gd-1_54 -llibboost_filesystem-vc110-mt-gd-1_54 -llibboost_date_time-vc110-mt-gd-1_54 -llibboost_thread-vc110-mt-gd-1_54
  }
}
else {
  LIBS += -lboost_system -lboost_filesystem -lboost_date_time
}


