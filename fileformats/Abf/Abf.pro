TARGET = abfplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/fileformats
include(../../signalviewer.pri)

DEFINES += ABF_EXPORT

win32 {
  DEFINES += _CRT_SECURE_NO_WARNINGS
}


INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/libaxon-code/src/

HEADERS += \
    abf/abffilescanner.h \
    abf/abffiles.h \
    abf/abfepochlistprovider.h \
    libaxon-code/src/axon_structs.h \
    libaxon-code/src/axon_defs.h \
    libaxon-code/src/axon.h \
    abf/abfreader.h \
    abf/defines.h

SOURCES += \
    abf/abffilescanner.cpp \
    abf/abffiles.cpp \
    abf/abfepochlistprovider.cpp \
    libaxon-code/src/axon.cpp \
    abf/abfreader.cpp
