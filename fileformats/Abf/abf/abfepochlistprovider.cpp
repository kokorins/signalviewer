#include "abfepochlistprovider.h"
#include "abffiles.h"
#include "abffilescanner.h"
#include <QtPlugin>

namespace abf {
AbfEpochListProvider::AbfEpochListProvider(QObject *parent) : QObject(parent) {}

boost::shared_ptr<epoch::IEpochFiles> AbfEpochListProvider::createFiles() const
{
  return boost::shared_ptr<epoch::IEpochFiles>(new AbfFiles());
}

boost::shared_ptr<epoch::IDataFileScanner> AbfEpochListProvider::createScanner() const
{
  return boost::shared_ptr<epoch::IDataFileScanner>(new AbfFileScanner());
}

std::wstring AbfEpochListProvider::name() const
{
  return L"Abf";
}

std::wstring AbfEpochListProvider::extFilter() const
{
  return L"*.abf";
}
std::wstring AbfEpochListProvider::author() const
{
  return L"Sergey Kokorin";
}

std::wstring AbfEpochListProvider::version() const
{
  return L"0.5";
}

} //namespace abf
