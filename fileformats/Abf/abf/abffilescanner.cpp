#include "abffilescanner.h"
#include "abfreader.h"
#include <axon.h>
#include <interfaces/fileformats/epochfileinfo.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace abf {
namespace fs = boost::filesystem;
epoch::DataFileInfo AbfFileScanner::handle(const fs::path &file) const
{
  epoch::DataFileInfo res;
  res.is_ok = false;
  if(!fs::is_regular_file(file))
    return res;
  if(fs::extension(file)!=std::string(".abf"))
    return res;
  res.file_name = file.filename().generic_string();
  AbfReader abf;
  if(!abf.open(file.generic_string().c_str()))
    return res;
  res.subj = file.branch_path().leaf().generic_string();
  for(size_t i=0; i<abf.numChannels(); ++i) {
    std::stringstream ss;
    ss<<i;
    res.channels.push_back(ss.str());
    res.start_times.push_back(abf.getTime(0));
  }
  res.is_ok = true;
  return res;
}

std::string AbfFileScanner::info(const fs::path &file, int /*idx*/) const
{
  //TODO channels
  std::stringstream ss;
  if(!fs::is_regular_file(file))
    return "Non regular file";
  AbfReader abf;
  if(!abf.open(file.generic_string().c_str()))
    return "Cant open it";
  ss<<"file: "<<file.generic_string()<<std::endl;
  ss<<"File info part\n";
  ss<<abf.getTime(0);
  ss<<"Data format: "<<abf::ToString(abf.dataFormat())<<std::endl;
  ss<<"File type: "<<abf.fileInfo().nFileType<<std::endl;
  ss<<"Protocol info part:\n";
  ss<<"Operation mode: "<<abf::ToString(abf.operationMode())<<std::endl;
  ss<<"Seconds per run: "<<abf.protocol().fSecondsPerRun<<std::endl;
  ss<<"Episodes per run: "<<abf.protocol().lEpisodesPerRun<<std::endl;
  ss<<"Runs per trial: "<<abf.protocol().lRunsPerTrial<<std::endl;
  ss<<"Trials per file: "<<abf.protocol().lNumberOfTrials<<std::endl;
  return ss.str();
}

void AbfFileScanner::properties(const epoch::EpochFileInfo &, void *, std::vector<epoch::EpochFileInfo> &) const
{
}

std::string AbfFileScanner::defaultProperties(const epoch::EpochFileInfo &) const
{
  return "";
}
} // namespace abf
