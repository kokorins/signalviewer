#ifndef ABFREADER_H
#define ABFREADER_H
#include "defines.h"
#include <memory>
#include <vector>
#include <boost/date_time.hpp>

class ABF;
struct ABF_FileInfo;
struct ABF_ProtocolInfo;
struct ABF_ADCInfo;
struct ABF_DACInfo;
struct ABF_EpochInfo;
/// \filename abfreader.h
namespace abf {
/**
 * This is main file for edf format reader according to interface for
 */
class ABF_CLASS_EXPORT AbfReader {
public:
  enum EOperationMode {
    VarLenEventDriven = 1,
    FixLenEventDriven,
    GapFree,
    HighSpeedOsc,
    EpisodicStimMode,
    EOperationTypeNum
  };
  enum EDataFormat {
    Integer = 0,
    Float,
    EDataFormatNum
  };

public:
  AbfReader();
  ~AbfReader();
  /**
   * \param file_name  : Filename, including path.
   */
  bool open(char const*const file_name);
  /**
   * \param st_idx  : Starting index of samples to be read (first sample in file is 0)
   * \param sample_count : Number of samples to be read. If 'ALL' it reads from start to the end of the file.
   * \param is_scaled      : If true the data is scaled according to unit gain in the file (default is 1)
   *                     Scaling should only be used if the unit of the data file is V.
   * \param seg_idx: The index of the data segment within the file.  The first index has an index of zero
   *                     which if also the default if this parameter is omitted.
   * \return size of data
   */
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  /**
   * \return if file is open
   */
  bool isOpen()const;
  /**
   * Number of values over whole file
   */
  size_t size()const;
  /**
   * Size of file in seconds
   */
  size_t sizeSec()const;
  boost::posix_time::ptime getTime(size_t sec)const;
  void close();
public:
  const ABF_FileInfo& fileInfo()const;
  const ABF_ProtocolInfo& protocol()const;
  const ABF_ADCInfo& channel(size_t idx)const;
  const ABF_EpochInfo& epoch(size_t idx)const;
  const ABF_DACInfo& DACChannel(size_t idx)const;
  size_t numDACChannels()const;
  size_t numEpochs()const;
  EDataFormat dataFormat()const;
  EOperationMode operationMode()const;
public:
  /**
   * Abf files could contains number of channels, each of them could be obtained by index here
   */
  void setIdx(size_t idx);
  size_t idx()const;
  /**
   * Number of channels in file
   */
  size_t numChannels()const;
private:
  std::unique_ptr<ABF> _impl;
  size_t _idx;
private:
  bool _is_open;
};

std::string ABF_CLASS_EXPORT ToString(AbfReader::EOperationMode ot);
std::string ABF_CLASS_EXPORT ToString(AbfReader::EDataFormat ot);
} // namespace abf
#endif // ABFREADER_H
