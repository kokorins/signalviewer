#include "abfreader.h"
#include <loglite/logging.hpp>
#include <boost/shared_array.hpp>
#include <boost/date_time.hpp>
#include <axon.h>

namespace abf {
AbfReader::AbfReader() : _impl(new ABF()), _idx(0) {}

AbfReader::~AbfReader()
{
  _is_open = false;
}

bool AbfReader::open(const char *const file_name)
{
  _is_open = _impl->Open((char*)file_name)>=0;
  _is_open = _is_open && _impl->ReadAllSections()>=0;
  return _is_open;
}

std::vector<double> AbfReader::read(size_t st_sec, size_t dur_sec)
{
  std::vector<double> data;
  size_t start_idx = (size_t)_impl->ProtocolInfo.lEpisodesPerRun*_impl->ProtocolInfo.lNumSamplesPerEpisode*st_sec/_impl->ProtocolInfo.fSecondsPerRun;
  size_t end_idx = (size_t)_impl->ProtocolInfo.lEpisodesPerRun*_impl->ProtocolInfo.lNumSamplesPerEpisode*(st_sec+dur_sec)/_impl->ProtocolInfo.fSecondsPerRun;
  if(end_idx>_impl->FileInfo.DataSection.llNumEntries)
    return data;
  switch(_impl->FileInfo.nDataFormat) {
  break; case Integer: {
    boost::shared_array<short> res(_impl->ReadIntData());
    if(!res)
      return data;
    return std::vector<double> (res.get()+start_idx, res.get() + end_idx);
  }
  break; case Float: {
    boost::shared_array<float> res(_impl->ReadFloatData());
    if(!res)
      return data;
    return std::vector<double> (res.get()+start_idx, res.get() + end_idx);
  }
  break; default:
    return data;
  }
  return data;
}

bool AbfReader::isOpen() const
{
  return _is_open;
}

size_t AbfReader::size() const
{
  return (size_t)(_impl->ProtocolInfo.lNumberOfTrials*_impl->ProtocolInfo.lRunsPerTrial*_impl->ProtocolInfo.lEpisodesPerRun*_impl->ProtocolInfo.lNumSamplesPerEpisode);
}

size_t AbfReader::sizeSec() const
{
  return (size_t)(_impl->ProtocolInfo.lNumberOfTrials*_impl->ProtocolInfo.lRunsPerTrial*_impl->ProtocolInfo.fSecondsPerRun);
}

boost::posix_time::ptime AbfReader::getTime(size_t sec) const
{
  unsigned int date = _impl->FileInfo.uFileStartDate;
  unsigned int time = _impl->FileInfo.uFileStartTimeMS;
  std::stringstream ss;
  ss<<date;
  boost::gregorian::date dt = boost::gregorian::from_undelimited_string(ss.str());
  boost::posix_time::time_duration td = boost::posix_time::milliseconds(time);
  boost::posix_time::ptime t(dt, td);
  t += boost::posix_time::seconds(sec);
  return t;
}

void AbfReader::close()
{
  _impl->Close();
  _is_open = false;
}

const ABF_FileInfo &AbfReader::fileInfo() const
{
  return _impl->FileInfo;
}

const ABF_ProtocolInfo &AbfReader::protocol() const
{
  return _impl->ProtocolInfo;
}

const ABF_ADCInfo &AbfReader::channel(size_t idx) const
{
  return _impl->ADCInfo[idx];
}

const ABF_EpochInfo &AbfReader::epoch(size_t idx) const
{
  return _impl->EpochInfo[idx];
}

const ABF_DACInfo &AbfReader::DACChannel(size_t idx) const
{
  return _impl->DACInfo[idx];
}

size_t AbfReader::numDACChannels() const
{
  size_t count = 0;
  for(size_t i=0; i<ABF_DACCOUNT; ++i)
    if(_impl->DACInfo[i].nDACNum>=0)
      ++count;
  return count;
}

void AbfReader::setIdx(size_t idx)
{
  _idx = idx;
}

size_t AbfReader::idx() const
{
  return _idx;
}

size_t AbfReader::numChannels() const
{
  size_t count = 0;
  for(size_t i=0; i<ABF_ADCCOUNT; ++i)
    if(_impl->ADCInfo[i].nADCNum>=0)
      ++count;
  return count;
}

size_t AbfReader::numEpochs() const
{
  size_t count = 0;
  for(size_t i=0; i<ABF_EPOCHCOUNT; ++i) {
    if(_impl->EpochInfo[i].nEpochNum>=0)
      ++count;
  }
  return count;
}

AbfReader::EDataFormat AbfReader::dataFormat() const
{
  return (EDataFormat)_impl->FileInfo.nDataFormat;
}

AbfReader::EOperationMode AbfReader::operationMode() const
{
  return (EOperationMode)_impl->ProtocolInfo.nOperationMode;
}


std::string ToString(AbfReader::EOperationMode ot)
{
   switch(ot) {
   break; case AbfReader::VarLenEventDriven:
     return "Variable-Length Event-Driven";
   break; case AbfReader::FixLenEventDriven:
     return "Fixed-Length Event-Driven";
   break; case AbfReader::GapFree:
     return "Gap Free";
   break; case AbfReader::HighSpeedOsc:
     return "High-Speed Oscilloscope Mode";
   break; case AbfReader::EpisodicStimMode:
     return "Episodic Stimulation Mode";
   break; default:
     return "";
   }
}

std::string ToString(AbfReader::EDataFormat ot)
{
  switch(ot) {
  break; case AbfReader::Integer:
    return "Integer values";
  break; case AbfReader::Float:
    return "Float values";
  break; default:
    return "";
  }
}

} //namespace abf
