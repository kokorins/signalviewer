#ifndef ABFEPOCHLISTPROVIDER_H
#define ABFEPOCHLISTPROVIDER_H
#include <interfaces/fileformats/epochlistprovider_qt.h>
#include <QObject>

namespace abf {
class AbfEpochListProvider : public QObject, public epoch::IEpochListProvider {
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "signalviewer.AbfEpochListProvider")
  Q_INTERFACES(epoch::IEpochListProvider)
public:
  explicit AbfEpochListProvider(QObject *parent = 0);
  virtual boost::shared_ptr<epoch::IEpochFiles> createFiles()const;
  virtual boost::shared_ptr<epoch::IDataFileScanner> createScanner()const;
  virtual std::wstring name()const;
  virtual std::wstring extFilter()const;
  virtual std::wstring author()const;
  virtual std::wstring version()const;
};
}//namespace abf

#endif // ABFEPOCHLISTPROVIDER_H
