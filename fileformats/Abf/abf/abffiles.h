#ifndef ABFFILES_H
#define ABFFILES_H
#include <interfaces/fileformats/epochfiles.h>
#include <vector>
#include <memory>
#include <boost/date_time.hpp>

/**
 * \filename edffiles.h
 * abf includes the reader for abf (axon binary format) data format
 */
namespace abf {
class AbfFiles : public epoch::IEpochFiles {
public:
  AbfFiles();
  ~AbfFiles();
  void setProperties(const std::string&) {}
  bool open(const std::vector<epoch::EpochFileInfo>& paths);
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  size_t size() const;
  void close();
private:
  class Impl;
  std::unique_ptr<Impl> _impl;
};
} //namespace abf
#endif // ABFFILES_H
