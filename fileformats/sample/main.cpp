#include <iostream>
#include <string>
#include <abf/abfreader.h>
#include <libaxon-code/src/axon.h>
#include <iterator>
#include <fstream>
#include <string>
#include <boost/date_time.hpp>

void ReadFileInfo(const abf::AbfReader& r)
{
  const ABF_FileInfo& fi = r.fileInfo();
  std::cout<<"\nFile info:\n";
  std::cout<<"CRC Enable: "<<fi.nCRCEnable<<std::endl;
  std::cout<<"Data Format: "<<abf::ToString((abf::AbfReader::EDataFormat)fi.nDataFormat)<<std::endl;
  std::cout<<"File Type: "<<fi.nFileType<<std::endl;
  std::cout<<"Simultaneous Scan: "<<fi.nSimultaneousScan<<std::endl;
  std::cout<<"Unused: "<<fi.sUnused<<std::endl;
  std::cout<<"Actual Episodes: "<<fi.uActualEpisodes<<std::endl;
  std::cout<<"Creator Name Index: "<<fi.uCreatorNameIndex<<std::endl;
  std::cout<<"Creator Version: "<<fi.uCreatorVersion<<std::endl;
  std::cout<<"File CRC: "<<fi.uFileCRC<<std::endl;
  std::cout<<"File info size: "<<fi.uFileInfoSize<<std::endl;
  std::cout<<"File signature: "<<fi.uFileSignature<<std::endl;
  std::cout<<"File start Date: "<<fi.uFileStartDate<<std::endl;
  std::cout<<"File start Time (ms): "<<fi.uFileStartTimeMS<<std::endl;
  std::cout<<"File start time: "<<boost::posix_time::to_iso_string(r.getTime(0))<<std::endl;
  std::cout<<"File version Number: "<<fi.uFileVersionNumber<<std::endl;
  std::cout<<"Modifier name Index: "<<fi.uModifierNameIndex<<std::endl;
  std::cout<<"Modifier version: "<<fi.uModifierVersion<<std::endl;
  std::cout<<"Protocol path index: "<<fi.uProtocolPathIndex<<std::endl;
  std::cout<<"Stopwatch time: "<<fi.uStopwatchTime<<std::endl;
  std::cout<<"\nProtocol info:\n";
  auto prot = r.protocol();
  std::cout<<"Enable file compression: "<<prot.bEnableFileCompression<<std::endl;
  std::cout<<"ADC range: "<<prot.fADCRange<<std::endl;
  std::cout<<"ADC sequence interval: "<<prot.fADCSequenceInterval<<std::endl;
  std::cout<<"Average weighting: "<<prot.fAverageWeighting<<std::endl;
  std::cout<<"Cell ID: "<<prot.fCellID<<std::endl;
  std::cout<<"DAC range: "<<prot.fDACRange<<std::endl;
  std::cout<<"Episode start to start: "<<prot.fEpisodeStartToStart<<std::endl;
  std::cout<<"First run delay (s): "<<prot.fFirstRunDelayS<<std::endl;
  std::cout<<"Scope output interval: "<<prot.fScopeOutputInterval<<std::endl;
  std::cout<<"Seconds per run: "<<prot.fSecondsPerRun<<std::endl;
  std::cout<<"Statistics period: "<<prot.fStatisticsPeriod<<std::endl;
  std::cout<<"Synch time unit: "<<prot.fSynchTimeUnit<<std::endl;
  std::cout<<"Trial start to start: "<<prot.fTrialStartToStart<<std::endl;
  std::cout<<"Trigger threshold: "<<prot.fTriggerThreshold<<std::endl;
  std::cout<<"ADC resolution: "<<prot.lADCResolution<<std::endl;
  std::cout<<"Average count: "<<prot.lAverageCount<<std::endl;
  std::cout<<"DAC resolution: "<<prot.lDACResolution<<std::endl;
  std::cout<<"Episodes per run: "<<prot.lEpisodesPerRun<<std::endl;
  std::cout<<"File comment index: "<<prot.lFileCommentIndex<<std::endl;
  std::cout<<"FinishDisplayNum: "<<prot.lFinishDisplayNum<<std::endl;
  std::cout<<"NumberOfTrials: "<<prot.lNumberOfTrials<<std::endl;
  std::cout<<"NumSamplesPerEpisode: "<<prot.lNumSamplesPerEpisode<<std::endl;
  std::cout<<"PreTriggerSamples: "<<prot.lPreTriggerSamples<<std::endl;
  std::cout<<"RunsPerTrial: "<<prot.lRunsPerTrial<<std::endl;
  std::cout<<"SamplesPerTrace: "<<prot.lSamplesPerTrace<<std::endl;
  std::cout<<"StartDisplayNum: "<<prot.lStartDisplayNum<<std::endl;
  std::cout<<"StatisticsMeasurements: "<<prot.lStatisticsMeasurements<<std::endl;
  std::cout<<"TimeHysteresis: "<<prot.lTimeHysteresis<<std::endl;
  std::cout<<"ActiveDACChannel: "<<prot.nActiveDACChannel<<std::endl;
  std::cout<<"AllowExternalTags: "<<prot.nAllowExternalTags<<std::endl;
  std::cout<<"AlternateDACOutputState: "<<prot.nAlternateDACOutputState<<std::endl;
  std::cout<<"AlternateDigitalOutputState: "<<prot.nAlternateDigitalOutputState<<std::endl;
  std::cout<<"Auto analyse enable: "<<prot.nAutoAnalyseEnable<<std::endl;
  std::cout<<"Auto trigger strategy: "<<prot.nAutoTriggerStrategy<<std::endl;
  std::cout<<"Average algorithm: "<<prot.nAverageAlgorithm<<std::endl;
  std::cout<<"Average mode: "<<prot.nAveragingMode<<std::endl;
  std::cout<<"Channel stats strategy: "<<prot.nChannelStatsStrategy<<std::endl;
  std::cout<<"Comments enable: "<<prot.nCommentsEnable<<std::endl;
  std::cout<<"Digital DAC channel: "<<prot.nDigitalDACChannel<<std::endl;
  std::cout<<"Digital enable: "<<prot.nDigitalEnable<<std::endl;
  std::cout<<"Digital holding: "<<prot.nDigitalHolding<<std::endl;
  std::cout<<"Digital... "<<std::endl;
  std::cout<<"Experiment type: "<<prot.nExperimentType<<std::endl;
  std::cout<<"External tag type: "<<prot.nExternalTagType<<std::endl;
  std::cout<<"First episode in run: "<<prot.nFirstEpisodeInRun<<std::endl;
  std::cout<<"Level hysteresis: "<<prot.nLevelHysteresis<<std::endl;
  std::cout<<"LTP type: "<<prot.nLTPType<<std::endl;
  std::cout<<"Manual info strategy: "<<prot.nManualInfoStrategy<<std::endl;
  std::cout<<"Operation mode: "<<abf::ToString((abf::AbfReader::EOperationMode)prot.nOperationMode)<<std::endl;
  std::cout<<"Scope trigger out: "<<prot.nScopeTriggerOut<<std::endl;
  std::cout<<"Show PN raw data: "<<prot.nShowPNRawData<<std::endl;
  std::cout<<"Signal type: "<<prot.nSignalType<<std::endl;
  std::cout<<"Statistics clear strategy: "<<prot.nStatisticsClearStrategy<<std::endl;
  std::cout<<"Statistics ... "<<std::endl;
  std::cout<<"Stats enable: "<<prot.nStatsEnable<<std::endl;
  std::cout<<"Trial trigger source: "<<prot.nTrialTriggerSource<<std::endl;
  std::cout<<"Trigger ... "<<std::endl;
  std::cout<<"Undo prompt strategy: "<<prot.nUndoPromptStrategy<<std::endl;
  std::cout<<"Undo run count: "<<prot.nUndoRunCount<<std::endl;
  std::cout<<"Unused: "<<prot.sUnused<<std::endl;
  std::cout<<"Unused: "<<prot.sUnused1<<std::endl;
  std::cout<<"File compression ratio: "<<prot.uFileCompressionRatio<<std::endl;
  std::cout<<std::endl;
  for(size_t i=0; i<r.numChannels(); ++i) {
    std::cout<<"Channel: "<<i<<std::endl;
    auto ch = r.channel(i);
    std::cout<<"EnabledDuringPN: "<<ch.bEnabledDuringPN<<std::endl;
    std::cout<<"ADCDisplayAmplification: "<<ch.fADCDisplayAmplification<<std::endl;
    std::cout<<"ADCDisplayOffset: "<<ch.fADCDisplayOffset<<std::endl;
    std::cout<<"ADCProgrammableGain: "<<ch.fADCProgrammableGain<<std::endl;
    std::cout<<"InstrumentOffset: "<<ch.fInstrumentOffset<<std::endl;
    std::cout<<"InstrumentScaleFactor: "<<ch.fInstrumentScaleFactor<<std::endl;
    std::cout<<"PostProcessLowpassFilter: "<<ch.fPostProcessLowpassFilter<<std::endl;
    std::cout<<"SignalGain: "<<ch.fSignalGain<<std::endl;
    std::cout<<"SignalHighpassFilter: "<<ch.fSignalHighpassFilter<<std::endl;
    std::cout<<"SignalLowpassFilter: "<<ch.fSignalLowpassFilter<<std::endl;
    std::cout<<"SignalOffset: "<<ch.fSignalOffset<<std::endl;
    std::cout<<"TelegraphAccessResistance: "<<ch.fTelegraphAccessResistance<<std::endl;
    std::cout<<"TelegraphAdditGain: "<<ch.fTelegraphAdditGain<<std::endl;
    std::cout<<"TelegraphFilter: "<<ch.fTelegraphFilter<<std::endl;
    std::cout<<"TelegraphMembraneCap: "<<ch.fTelegraphMembraneCap<<std::endl;
    std::cout<<"ADCChannelNameIndex: "<<ch.lADCChannelNameIndex<<std::endl;
    std::cout<<"ADCUnitsIndex: "<<ch.lADCUnitsIndex<<std::endl;
    std::cout<<"ADCNum: "<<ch.nADCNum<<std::endl;
    std::cout<<"ADCPtoLChannelMap: "<<ch.nADCPtoLChannelMap<<std::endl;
    std::cout<<"ADCSamplingSeq: "<<ch.nADCSamplingSeq<<std::endl;
    std::cout<<"HighpassFilterType: "<<ch.nHighpassFilterType<<std::endl;
    std::cout<<"LowpassFilterType: "<<ch.nLowpassFilterType<<std::endl;
    std::cout<<"PostProcessLowpassFilterType: "<<ch.nPostProcessLowpassFilterType<<std::endl;
    std::cout<<"StatsChannelPolarity: "<<ch.nStatsChannelPolarity<<std::endl;
    std::cout<<"TelegraphEnable: "<<ch.nTelegraphEnable<<std::endl;
    std::cout<<"Telegraph...\n";
    std::cout<<"Unused: "<<ch.sUnused<<std::endl;
  }
  if(r.numChannels()==0)
    std::cout<<"No ADC channels\n";
  std::cout<<std::endl;
  for(size_t i=0; i<r.numDACChannels(); ++i) {
    std::cout<<"DAC channel: "<<i<<std::endl;
    auto ch = r.DACChannel(i);
    std::cout<<"BaselineDuration: "<<ch.fBaselineDuration
             <<"\nBaselineLevel: "<<ch.fBaselineLevel
             <<"\nDACCalibrationFactor: "<<ch.fDACCalibrationFactor
             <<"\nDACCalibrationOffset: "<<ch.fDACCalibrationOffset
             <<"\nDACFileOffset: "<<ch.fDACFileOffset
             <<"\nDACFileScale: "<<ch.fDACFileScale
             <<"\nDACHoldingLevel: "<<ch.fDACHoldingLevel
             <<"\nDACScaleFactor: "<<ch.fDACScaleFactor
             <<"\nInstrumentHoldingLevel: "<<ch.fInstrumentHoldingLevel
             <<"\nMembTestPostSettlingTimeMS: "<<ch.fMembTestPostSettlingTimeMS
             <<"\nMembTestPreSettlingTimeMS: "<<ch.fMembTestPreSettlingTimeMS
             <<"\nPNHoldingLevel: "<<ch.fPNHoldingLevel
             <<"\nPNInterpulse: "<<ch.fPNInterpulse
             <<"\nPNSettlingTime: "<<ch.fPNSettlingTime
             <<"\nPostTrainLevel: "<<ch.fPostTrainLevel
             <<"\nPostTrainPeriod: "<<ch.fPostTrainPeriod
             <<"\nStepDuration: "<<ch.fStepDuration
             <<"\nStepLevel: "<<ch.fStepLevel
             <<"\nConditNumPulses: "<<ch.lConditNumPulses
             <<"\nDACChannelNameIndex: "<<ch.lDACChannelNameIndex
             <<"\nDACChannelUnitsIndex: "<<ch.lDACChannelUnitsIndex
             <<"\nDACFileEpisodeNum: "<<ch.lDACFileEpisodeNum
             <<"\nDACFileNumEpisodes: "<<ch.lDACFileNumEpisodes
             <<"\nDACFilePathIndex: "<<ch.lDACFilePathIndex
             <<"\nDACFilePtr: "<<ch.lDACFilePtr
             <<"\nConditEnable: "<<ch.nConditEnable
             <<"\nDACFileADCNum: "<<ch.nDACFileADCNum
             <<"\nInterEpisodeLevel: "<<ch.nInterEpisodeLevel
             <<"\nLeakSubtractADCIndex: "<<ch.nLeakSubtractADCIndex
             <<"\nLeakSubtractType: "<<ch.nLeakSubtractType
             <<"\nLTPPresynapticPulses: "<<ch.nLTPPresynapticPulses
             <<"\nLTPUsageOfDAC: "<<ch.nLTPUsageOfDAC
             <<"\nMembTestEnable: "<<ch.nMembTestEnable
             <<"\nPNNumADCChannels: "<<ch.nPNNumADCChannels
             <<"\nPNNumPulses: "<<ch.nPNNumPulses
             <<"\nPNPolarity: "<<ch.nPNPolarity
             <<"\nPNPosition: "<<ch.nPNPosition
             <<"\nTelegraphDACScaleFactorEnable: "<<ch.nTelegraphDACScaleFactorEnable
             <<"\nWaveformEnable: "<<ch.nWaveformEnable
             <<"\nWaveformSource: "<<ch.nWaveformSource<<std::endl;
  }
  if(r.numDACChannels()==0)
    std::cout<<"No DAC channels\n";
  std::cout<<std::endl;
  for(size_t i=0; i<r.numEpochs(); ++i) {
    auto e = r.epoch(i);
    std::cout<<"Epoch: "<<i<<std::endl
             <<"Epoch compression: "<<e.bEpochCompression
             <<"\nAlternateDigitalTrainValue: "<<e.nAlternateDigitalTrainValue
             <<"\nAlternateDigitalValue: "<<e.nAlternateDigitalValue
             <<"\nDigitalTrainValue: "<<e.nDigitalTrainValue
             <<"\nDigitalValue:"<<e.nDigitalValue
             <<"\nUnused:"<<e.sUnused<<std::endl;
  }
  if(r.numEpochs()==0)
    std::cout<<"No epochs\n";
}

int main()
{
  abf::AbfReader r;
  r.open("F:/ucb/Stib/source/Abf/data/test.abf");
  const std::vector<double> vals = r.read(0,1);
  std::cout<<"Sample second size: "<<vals.size()<<std::endl;
  std::cout<<"File size: "<<r.size()<<std::endl;
  std::cout<<"File size (sec): "<<r.sizeSec()<<std::endl;
  return 0;
}

