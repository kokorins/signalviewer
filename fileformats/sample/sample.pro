TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
include(../../signalviewer.pri)
win32 {
  CONFIG(release, debug|release) {
    LIBS += -llibboost_system-vc100-mt-1_53 -llibboost_filesystem-vc100-mt-1_53 -llibboost_date_time-vc100-mt-1_53 -llibboost_thread-vc100-mt-1_53
  }
  CONFIG(debug, debug|release) {
    LIBS += -llibboost_system-vc100-mt-gd-1_53 -llibboost_filesystem-vc100-mt-gd-1_53 -llibboost_date_time-vc100-mt-gd-1_53 -llibboost_thread-vc100-mt-gd-1_53
  }
}
else {
  LIBS += -lpthread
  LIBS += -lboost_thread -lboost_system -lboost_filesystem -lboost_date_time
}

DESTDIR = $$OUT_PWD/../../plugins/fileformats

SOURCES += \
  main.cpp

!win32:QMAKE_CXXFLAGS += -std=c++0x
unix:QMAKE_RPATHDIR += /home/kokorins/Qt5.0.2/5.0.2/gcc_64/lib/

INCLUDEPATH += \
  $$PWD/../Abf \
  $$PWD/../Abf/axon/libaxon-code/src

DEPENDPATH += \
  $$PWD/../Abf

LIBS += -L$$DESTDIR/ -labfplugin

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$DESTDIR/abfplugin.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$DESTDIR/abfplugin.lib
else:unix:!macx: PRE_TARGETDEPS += $$DESTDIR/libabfplugin.so
