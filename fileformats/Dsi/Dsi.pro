TARGET = Dsi
TEMPLATE = lib
CONFIG += staticlib plugin

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../epoch/

win32 {
  BOOST_ROOT = "G:/opt/boost_1_53_0"
  INCLUDEPATH += $$BOOST_ROOT
  LIBS += -L$$BOOST_ROOT/stage/lib
}


HEADERS += \
    dsi/dsirowreader.h \
    dsi/dsireader.h \
    dsi/dsifiles.h \
    dsi/dsifilescanner.h

SOURCES += \
    dsi/dsirowreader.cpp \
    dsi/dsireader.cpp \
    dsi/dsifiles.cpp \
    dsi/dsifilescanner.cpp
