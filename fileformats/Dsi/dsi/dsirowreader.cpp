#include "dsirowreader.h"
#include <numeric>
#include <loglite/logging.hpp>
#include <boost/shared_array.hpp>
namespace dsi {
class DsiRowReader::Impl {
public:
  bool open(char const*const file_name);
  bool read(size_t idx, DsiRow& row);
  void close();
  bool isOpen() const;
  size_t size()const;
  size_t sizeSec()const;
  int findSec(size_t sec) const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  size_t rate()const;
  const DsiHeader& header();
public:
  static std::streamoff FindSynch(std::ifstream& fin, unsigned long cur_pos);
private:
  bool readHeader();
  std::streamoff passRow(std::streamoff offset, std::streamoff &start_of, boost::posix_time::ptime& start_time, int& duration);
private:
  DsiHeader _header;
  std::streamoff _header_offset;
  std::ifstream _in;
  std::string _file_name;
  std::vector<std::streamoff> _offsets; // starts of each row offsets
  std::vector<size_t> _durations; // length in seconds
  std::vector<size_t> _part_sums; // sums in seconds
  std::vector<boost::posix_time::ptime> _row_time; // length in seconds
  size_t _rate;
};

bool DsiRowReader::Impl::open( char const*const file_name )
{
  _file_name = file_name;
  _in.open(file_name, std::ios_base::binary);
  if(!_in.is_open()) {
    LOGLITE_LOG_L1("Can't open file "<<file_name);
    return false;
  }
  if(!readHeader()) {
    _in.close();
    return false;
  }
  std::streamoff offset = _header_offset;
  while(!_in.eof()) {
    std::streamoff start_of;
    boost::posix_time::ptime start;
    int duration;
    offset = passRow(offset, start_of, start, duration);
    _offsets.push_back(start_of);
    _durations.push_back(duration);
    _row_time.push_back(start);
  }
  _part_sums.resize(_durations.size());
  std::partial_sum(_durations.begin(), _durations.end(), _part_sums.begin());
  _in.clear();
  _in.seekg(_header_offset, std::ios::beg);
  DsiRow row;
  read(0, row);
  _rate = row.rate;
  return true;
}

bool DsiRowReader::Impl::readHeader()
{
  _in.clear();
  _in.seekg(0,std::ios::beg);
  unsigned int ui;
  unsigned short us;
  unsigned char uch;
  _in.seekg(FindSynch(_in, 0), std::ios::beg);
  _in.read((char*)&uch, sizeof(uch));
  if(uch!=30) {
    LOGLITE_LOG_L1("Incorrect format");
    return false;
  }
  _header.rec_def = uch;
  _in.read((char*)&ui, sizeof(ui));
  _header.rec_size = ui;
  _in.read((char*)&us, sizeof(us));
  _header.checksum = us;
  _in.read((char*)&ui, sizeof(ui));
  _header.start = ui;
  _in.read((char*)&ui, sizeof(ui));
  _header.link_off = ui;
  _in.read((char*)&uch, sizeof(uch));
  _header.idlen = uch;
  _header.id.resize(_header.idlen);
  for(size_t i=0; i<_header.idlen; ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.id[i]=uch;
  }
  _in.read((char*)&uch, sizeof(uch));
  _header.loclen = uch;
  _header.loc.resize(_header.loclen);
  for(size_t i=0; i<_header.loclen; ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.loc[i]=uch;
  }
  _in.read((char*)&uch, sizeof(uch));
  _header.num_bins = uch;
  _header.chnl_mask.resize(4);
  for(size_t i=0; i<_header.chnl_mask.size(); ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.chnl_mask[i] = uch;
  }
  _in.read((char*)&uch, sizeof(uch));
  _header.whatlen = uch;
  _header.datatype.resize(_header.whatlen);
  for(size_t i=0; i<_header.whatlen; ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.datatype[i] = uch;
  }
  _in.read((char*)&uch, sizeof(uch));
  _header.unitlen = uch;
  _header.units.resize(_header.unitlen);
  for (size_t i=0; i<_header.unitlen; ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.units[i] = uch;
  }
  _header.sync.resize(8);
  for (size_t i=0; i<_header.sync.size(); ++i) {
    _in.read((char*)&uch, sizeof(uch));
    _header.sync[i] = uch;
    if(uch!=0xFF) {
      LOGLITE_LOG_L1("Incorrect synch");
      return false;
    }
  }
  _header_offset = _in.tellg();
  boost::gregorian::date d(1970, 1, 1);
  boost::posix_time::time_duration t(0,0,0);
  _header.start_time = boost::posix_time::ptime(d, t);
  _header.start_time += boost::posix_time::seconds(_header.start);
  return true;
}

void DsiRowReader::Impl::close()
{
  if(_in.is_open())
    _in.close();
}

bool DsiRowReader::Impl::isOpen() const
{
  return _in.is_open();
}

bool DsiRowReader::Impl::read( size_t idx, DsiRow& row )
{
  unsigned int ui;
  unsigned short us;
  unsigned char uch;
  short s;
  float f;
  _in.seekg(_offsets[idx], std::ios::beg);
  _in.read((char*)&uch, sizeof(uch));
  if(uch!=31) {
    LOGLITE_LOG_L1("Incorrect format");
    return false;
  }
  _in.read((char*)&ui, sizeof(ui));
  row.rec_size = ui;
  _in.read((char*)&us, sizeof(us));
  row.chksum = us;
  _in.read((char*)&ui, sizeof(ui));
  row.start = ui;
  _in.read((char*)&uch, sizeof(uch));
  row.state = uch;
  _in.read((char*)&us, sizeof(us));
  row.rate = us;
  _in.read((char*)&ui, sizeof(ui));
  row.burst_len = ui;
  _in.read((char*)&ui, sizeof(ui));
  row.samples = ui;
  _in.read((char*)&uch, sizeof(uch));
  row.compress = uch;
  _in.read((char*)&f, sizeof(f));
  row.full_scale = f;
  row.data.resize(row.samples);
  for(size_t i=0; i<row.samples; ++i) {
    _in.read((char*)&s, sizeof(s));
    row.data[i] = s;
  }
  _header.sync.resize(8);
  for (size_t i=0; i<row.sync.size(); ++i) {
    _in.read((char*)&uch, sizeof(uch));
    row.sync[i] = uch;
    if(uch!=0xFF) {
      LOGLITE_LOG_L1("Incorrect synch");
      return false;
    }
  }
  boost::gregorian::date d(1970, 1, 1);
  boost::posix_time::time_duration t(0,0,0);
  row.start_time = boost::posix_time::ptime(d, t);
  row.start_time += boost::posix_time::seconds(row.start);
  row.duration = row.samples/row.rate;
  return true;
}

std::streamoff DsiRowReader::Impl::passRow(std::streamoff offset, std::streamoff& start_of,
                                           boost::posix_time::ptime& start_time,int& duration)
{
  unsigned int ui;
  unsigned short us;
  unsigned char uch;
  _in.seekg(offset, std::ios::beg);
  start_of = offset;
  _in.seekg(sizeof(uch) + sizeof(ui) + sizeof(us), std::ios::cur);
  _in.read((char*)&ui, sizeof(ui));
  unsigned int st = ui;
  _in.seekg(sizeof(uch), std::ios::cur);
  _in.read((char*)&us, sizeof(us));
  unsigned short rate = us;
  _in.seekg(sizeof(ui), std::ios::cur);
  _in.read((char*)&ui, sizeof(ui));
  unsigned int samples = ui;
  _in.seekg(sizeof(uch) + sizeof(float), std::ios::cur);
  _in.seekg(sizeof(short)*samples, std::ios::cur);
  for (size_t i=0; i<8; ++i)
    _in.read((char*)&uch, sizeof(uch));
  boost::gregorian::date d(1970, 1, 1);
  boost::posix_time::time_duration t(0,0,0);
  start_time = boost::posix_time::ptime(d, t);
  start_time += boost::posix_time::seconds(st);
  duration = samples/rate;
  return _in.tellg();
}

size_t DsiRowReader::Impl::size() const
{
  return _offsets.size();
}

size_t DsiRowReader::Impl::sizeSec() const
{
  return std::accumulate(_durations.begin(), _durations.end(), 0u);
}

int DsiRowReader::Impl::findSec( size_t sec ) const
{
  for(size_t i=0; i<_part_sums.size(); ++i) {
    if(sec<_part_sums[i])
      return i;
  }
  return -1;
}

size_t DsiRowReader::Impl::rate() const
{
  return _rate;
}

void DsiRowReader::Impl::getTime( size_t sec, boost::posix_time::ptime& t ) const
{
  int idx = findSec(sec);
  if(idx<0)
    return;
  t = _row_time[idx];
  size_t row_start_sec = 0;
  if(idx>0)
    row_start_sec = _part_sums[idx-1];
  t += boost::posix_time::seconds(sec-row_start_sec);
}

std::streamoff DsiRowReader::Impl::FindSynch(std::ifstream& fin, unsigned long cur_pos)
{
  fin.seekg(cur_pos, std::ios::beg);
  unsigned char uch;
  int count=0;
  while(!fin.eof()) {
    fin.read((char*)&uch, sizeof(uch));
    if(uch==0xff) {
      ++count;
      if(count==8)
        break;
    }
    else
      count = 0;
  }
  std::streamoff res = 0;
  if(count==8)
     res = fin.tellg();
  fin.seekg(cur_pos, std::ios::beg);
  return res;
}

const DsiHeader& DsiRowReader::Impl::header()
{
  return _header;
}

DsiRowReader::DsiRowReader() : _impl(new Impl()) {}

bool DsiRowReader::open( char const*const file_name )
{
  return _impl->open(file_name);
}

size_t DsiRowReader::read( size_t idx, DsiRow& row )
{
  return _impl->read(idx, row);
}

bool DsiRowReader::isOpen() const
{
  return _impl->isOpen();
}

size_t DsiRowReader::size() const
{
  return _impl->size();
}

size_t DsiRowReader::sizeSec() const
{
  return _impl->sizeSec();
}

int DsiRowReader::findSec(size_t sec) const
{
  return _impl->findSec(sec);
}

size_t DsiRowReader::rate() const
{
  return _impl->rate();
}

void DsiRowReader::close()
{
  return _impl->close();
}

void DsiRowReader::getTime(size_t sec, boost::posix_time::ptime& t) const
{
  _impl->getTime(sec, t);
}

const DsiHeader& DsiRowReader::header() const
{
  return _impl->header();
}

} //namespace dsi
