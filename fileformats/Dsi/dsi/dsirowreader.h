#ifndef DSIROWREADER_H_
#define DSIROWREADER_H_
#include <vector>
#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
/// \filename dsirowreader.h
namespace dsi {
/**
 * \brief Dsi file header data
 */
struct DsiHeader {
  unsigned char rec_def;
  unsigned int rec_size;
  unsigned short checksum;
  unsigned int start;
  unsigned int link_off;
  unsigned char idlen;
  std::string id;
  unsigned char loclen;
  std::string loc;
  unsigned char num_bins;
  std::vector<unsigned char> chnl_mask;
  unsigned char whatlen;
  ///string that defines the channel, the first letter of file name defines the channel too.
  std::string datatype; // label
  unsigned char unitlen;
  std::string units;
  std::vector<unsigned char> sync;
  boost::posix_time::ptime start_time;
};

struct DsiRow {
  unsigned int rec_size;
  unsigned short chksum;
  unsigned int start;
  unsigned char state;
  unsigned short rate;
  unsigned int burst_len;
  unsigned int samples;
  unsigned char compress;
  float full_scale;
  std::vector<short> data;
  std::vector<unsigned char> sync;
  int duration; // duration in seconds;
  boost::posix_time::ptime start_time;
};

class DsiRowReader {
public:
  DsiRowReader();
  bool open(char const*const file_name);
  size_t read(size_t idx, DsiRow& row);
  bool isOpen()const;
  size_t size()const;
  size_t sizeSec()const;
  int findSec(size_t sec)const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  void close();
  size_t rate()const;
  const DsiHeader& header()const;
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace dsi
#endif //DSIROWREADER_H_
