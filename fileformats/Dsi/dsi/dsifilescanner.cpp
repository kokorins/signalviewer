#include "dsifilescanner.h"
#include "dsirowreader.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace dsi {
namespace fs = boost::filesystem;
bool DsiFileScanner::handle(const fs::path &file, std::string &subj,
                            std::vector<std::string>& channels, std::vector<TDateTime>& start_times) const
{
  if(!fs::is_regular_file(file))
    return false;
  if(fs::extension(file)==std::string(".mat"))
    return false;
  if(file.stem().generic_string()==std::string("trigger"))
    return false;
  subj = file.stem().generic_string();
  dsi::DsiRowReader drr;
  if(!drr.open(file.generic_string().c_str()))
    return false;
  const dsi::DsiHeader& header = drr.header();
  std::string channel = header.datatype;
  boost::algorithm::to_upper(channel);
  channels.push_back(channel);
  TDateTime start_time = header.start_time;
  drr.close();
  start_times.push_back(start_time);
  return true;
}

std::string DsiFileScanner::info(const fs::path &file) const
{
  if(!fs::is_regular_file(file))
    return "Non regular file";
  dsi::DsiRowReader drr;
  if(!drr.open(file.generic_string().c_str()))
    return "Cant open it";
  std::stringstream ss;
  const DsiHeader& h = drr.header();
  ss<<"file: "<<file.generic_string()<<std::endl;
  ss<<"checksum: "<<h.checksum<<std::endl;
  ss<<"datatype: "<<h.datatype<<std::endl;
  ss<<"id: "<<h.id<<std::endl;
  ss<<"link_off: "<<h.link_off<<std::endl;
  ss<<"loc: "<<h.loc<<std::endl;
  ss<<"num_bins: "<<h.num_bins<<std::endl;
  ss<<"rec_def: "<<h.rec_def<<std::endl;
  ss<<"units: "<<h.units<<std::endl;
  return ss.str();
}
} // namespace dsi
