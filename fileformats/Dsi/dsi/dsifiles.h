#ifndef DSIFILES_H_
#define DSIFILES_H_
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>

/**
 * \filename dsifiles.h
 * dsi includes the reader for dsi data format
 */
namespace dsi {
class DsiFiles {
public:
  DsiFiles();
  bool open(const std::vector<std::pair<std::string, size_t> >& paths);
  void read(size_t st_sec, size_t dur_sec, std::vector<double>& data);
  void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  size_t size() const;
  void close();
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace dsi
#endif //DSIFILES_H_
