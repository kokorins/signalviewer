#ifndef DSIFILESCANNER_H
#define DSIFILESCANNER_H
#include <interfaces/fileformats/datafilescanner.h>
#include <boost/date_time.hpp>
#include <string>
namespace dsi {
class DsiFileScanner : public epoch::IDataFileScanner {
public:
  virtual bool handle(const boost::filesystem::path&file, std::string& subj,
                      std::vector<std::string>& channels, std::vector<TDateTime>& start_times) const;
  virtual std::string info(const boost::filesystem::path& file)const;
};
} //namespace dsi
#endif // DSIFILESCANNER_H
