#include "dsireader.h"
#include "dsirowreader.h"
#include <loglite/logging.hpp>
namespace dsi {
  DsiReader::DsiReader():_row_reader(new DsiRowReader()) {}

  bool DsiReader::open( char const*const file_name )
  {
    return _row_reader->open(file_name);
  }

  void DsiReader::close()
  {
    _row_reader->close();
  }

  bool DsiReader::isOpen() const
  {
    return _row_reader->isOpen();
  }

  size_t DsiReader::read(size_t st_sec, size_t dur_sec, std::vector<double>& data)
  {
    data.clear();
    int st_idx = _row_reader->findSec(st_sec);
    if(st_idx<0)
      return 0;
    int ed_idx = _row_reader->findSec(st_sec + dur_sec);
    if(ed_idx<0)
      ed_idx = _row_reader->size();
    for(int i=st_idx; i<=ed_idx; ++i) {
      DsiRow row;
      _row_reader->read(i, row);
      data.insert(data.end(), row.data.begin(), row.data.end());
    }
    return data.size();
  }

  double DsiReader::sampleRate() const 
  {
    return _row_reader->rate();
  }

  size_t DsiReader::sizeSec() const 
  {
    return _row_reader->sizeSec();
  }

  size_t DsiReader::size() const
  {
    return _row_reader->size();
  }

  void DsiReader::getTime( size_t sec, boost::posix_time::ptime& t ) const
  {
    _row_reader->getTime(sec, t);
  }
} //namespace dsi