#ifndef CSVREADER_H
#define CSVREADER_H
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
#include <vector>
namespace csvfile {
  struct CsvDoubleRow;
  struct CsvOneLineHeader;
  template <typename CsvDoubleRow, typename CsvOneLineHeader> class CsvChunkReader;
}
/// \filename csvreader.h
namespace csv {
struct CsvReaderProperties {
  size_t rate;
  boost::posix_time::ptime start;
public:
  const static std::string kTypeId;
public:
  CsvReaderProperties() : rate(1) {}
  const std::string& type()const;
  void toString(std::string& json)const;
  bool fromString(const std::string& json);
};

/**
 * This is main file for csv format reader according to interface
 * The name of file should be subject_dateinisoformat_rate.csv
 */
class CsvReader {
public:
  typedef CsvReaderProperties Properties;
public:
  CsvReader();
  void setProperties(const Properties&);
  /**
   * \param file_name  : Filename, including path.
   */
  bool open(char const*const file_name);
  /**
   * \param st_idx  : Starting index of samples to be read (first sample in file is 0)
   * \param sample_count : Number of samples to be read. If 'ALL' it reads from start to the end of the file.
   * \param is_scaled      : If true the data is scaled according to unit gain in the file (default is 1)
   *                     Scaling should only be used if the unit of the data file is V.
   * \param seg_idx: The index of the data segment within the file.  The first index has an index of zero
   *                     which if also the default if this parameter is omitted.
   * \return size of data
   */
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  /**
   * \return if file is open
   */
  bool isOpen()const;
  /**
   * Number of values over whole file
   */
  size_t size()const;
  /**
   * Size of file in seconds
   */
  size_t sizeSec()const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  void close();
public:
  /**
   * Csv files could contain number of channels, each of them could be obtained by index here
   */
  void setIdx(size_t idx);
  size_t idx()const;
  /**
   * Number of channels in file
   */
  size_t numChannels()const;
private:
  boost::shared_ptr<csvfile::CsvChunkReader<csvfile::CsvDoubleRow, csvfile::CsvOneLineHeader> > _impl;
  boost::shared_ptr<Properties> _props;
  std::string _subj;
  size_t _idx;
};
} // namespace csv
#endif // CSVREADER_H
