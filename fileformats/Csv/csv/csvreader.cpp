#include "csvreader.h"
#include "csvfilescanner.h"
#include <csvfile/csvchunk.hpp>
#include <csvfile/csvrow.h>
#include <csvfile/csvheader.h>
#include <boost/algorithm/string.hpp>
#include <boost/date_time.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <functional>

namespace csv {
struct getIdx : std::unary_function<csvfile::CsvDoubleRow, double> {
public:
  getIdx(size_t idx): _idx(idx) {}
  double operator()(const csvfile::CsvDoubleRow& row)const {
    return row.values()[_idx];
  }
private:
  size_t _idx;
};

const std::string CsvReaderProperties::kTypeId = "signalviewer.csv.CsvReader.Properties.1";
const std::string &CsvReaderProperties::type() const
{
  return kTypeId;
}

void CsvReaderProperties::toString(std::string &json) const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("rate", rate);
  pt.put("start", start);
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  json = ss.str();
}

bool CsvReaderProperties::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str!=kTypeId)
    return false;
  rate = pt.get<size_t>("rate");
  start = pt.get<boost::posix_time::ptime>("start");
  return true;
}


CsvReader::CsvReader() : _impl(new csvfile::CsvChunkReader<csvfile::CsvDoubleRow,
                                                           csvfile::CsvOneLineHeader>("\t, ", true)),
                         _props(new Properties()), _idx(0) {}

void CsvReader::setProperties(const CsvReader::Properties & props)
{
  _props.reset(new Properties(props));
}

bool CsvReader::open(const char *const file_name)
{
  return _impl->open(file_name);
}

std::vector<double> CsvReader::read(size_t st_sec, size_t dur_sec)
{
  std::vector<double> data;
  if((st_sec+dur_sec)*_props->rate>_impl->knownSize())
    return data;
  const std::vector<csvfile::CsvDoubleRow>& s = _impl->readChunk(st_sec*_props->rate, dur_sec*_props->rate);
  std::transform(s.begin(), s.end(), std::back_inserter(data), getIdx(_idx));
  return data;
}

bool CsvReader::isOpen() const
{
  return _impl->isOpen();
}

size_t CsvReader::size() const
{
  return _impl->knownSize();
}

size_t CsvReader::sizeSec() const
{
  return _impl->knownSize()/_props->rate;
}

void CsvReader::getTime(size_t sec, boost::posix_time::ptime &t) const
{
  t = _props->start;
  t += boost::posix_time::seconds(sec);
}

void CsvReader::close()
{
  _impl->close();
}

void CsvReader::setIdx(size_t idx)
{
  _idx = idx;
}

size_t CsvReader::idx() const
{
  return _idx;
}

size_t CsvReader::numChannels() const
{
  return _impl->headers().values().size();
}
} //namespace csv
