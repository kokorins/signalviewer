#include "csvfilescanner.h"
#include "csvfiledialog.h"
#include "csvreader.h"
#include <interfaces/fileformats/epochfileinfo.h>
#include <csvfile/csvchunk.hpp>
#include <csvfile/csvrow.h>
#include <csvfile/csvheader.h>
#include <loglite/logging.hpp>
#include <boost/date_time.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

namespace csv {
namespace fs = boost::filesystem;
epoch::DataFileInfo CsvFileScanner::handle(const fs::path &file) const
{
  epoch::DataFileInfo res;
  res.is_ok = false;
  if(!fs::is_regular_file(file)) {
    LOGLITE_LOG_L1(file<<" is not a file");
    return res;
  }
  if(fs::extension(file)!=std::string(".csv")) {
    LOGLITE_LOG_L1(file<<" has no csv extention");
    return res;
  }
  res.file_name = file.filename().generic_string();
  csvfile::CsvChunkReader<csvfile::CsvDoubleRow, csvfile::CsvOneLineHeader> ccr(",\t ", 100, false);
  std::string file_name = file.generic_string();
  if(!ccr.open(file_name)) {
    LOGLITE_LOG_L1(file_name<<" cant be opened");
    return res;
  }
  boost::posix_time::ptime t;
  size_t rate;
  if(!NameToInfo(file.filename().generic_string(), res.subj, t, rate))
    res.subj = file.filename().generic_string();
  for(size_t i=0; i<ccr.headers().values().size(); ++i) {
    std::string channel = ccr.headers().values()[i];
    boost::algorithm::to_upper(channel);
    res.channels.push_back(channel);
    res.start_times.push_back(t);
  }
  ccr.close();
  res.is_ok = true;
  return res;
}

std::string CsvFileScanner::info(const fs::path &file, int idx) const
{
  if(!fs::is_regular_file(file))
    return "Non regular file";
  csvfile::CsvChunkReader<csvfile::CsvDoubleRow, csvfile::CsvOneLineHeader> ccr("\t, ", true);
  if(!ccr.open(file.generic_string()))
    return "Cant open it";
  std::stringstream ss;
  std::string subj;
  boost::posix_time::ptime start_time;
  size_t rate;
  bool name_info = NameToInfo(file.filename().generic_string(), subj, start_time, rate);
  size_t sz = ccr.knownSize();
  ss<<"file: "<<file.generic_string()<<std::endl;
  if(name_info) {
    ss<<"rate: "<<rate<<std::endl;
    ss<<"num points: "<<sz*rate<<std::endl;
    ss<<"start time: "<<start_time<<std::endl;
    subj = file.filename().generic_string();
  }
  ss<<"num channels: "<<ccr.headers().values().size()<<std::endl;
  ss<<"patient: "<<subj<<std::endl;
  ss<<"size: "<<sz<<std::endl;
  if(idx<0 || idx>=(int)ccr.headers().values().size()) {
    ss<<"There is no such channel id:"<<idx;
    return ss.str();
  }
  ss<<"Channel "<<idx<<":"<<ccr.headers().values()[idx]<<std::endl;
  return ss.str();
}

void CsvFileScanner::properties(const epoch::EpochFileInfo &prev_prop, void *parent, std::vector<epoch::EpochFileInfo> &fis) const
{
  CsvFileDialog* cfd = new CsvFileDialog((QWidget*)parent);
  CsvReader::Properties props;
  std::string subj;
  if(!prev_prop.prop_json.empty())
    props.fromString(prev_prop.prop_json);
  else {
    NameToInfo(prev_prop.file_name, subj, props.start, props.rate);
  }
  const boost::posix_time::ptime& bdt = props.start;
  QDate d(bdt.date().year(), bdt.date().month(), bdt.date().day());
  QTime t(bdt.time_of_day().hours(), bdt.time_of_day().minutes(), bdt.time_of_day().seconds());
  QDateTime dt(d, t);
  dt.setDate(d);
  dt.setTime(t);
  cfd->setStart(dt);
  cfd->setRate(props.rate);
  if(cfd->exec() != QDialog::Accepted)
    return;
  props.rate = cfd->rate();
  dt = cfd->start();
  boost::gregorian::date bd(dt.date().year(), dt.date().month(), dt.date().day());
  boost::posix_time::time_duration bt(dt.time().hour(), dt.time().minute(), dt.time().second());
  props.start = boost::posix_time::ptime(bd, bt);
  std::string json;
  props.toString(json);
  for(size_t i=0; i<fis.size(); ++i)
    if(fis[i].file_path == prev_prop.file_path)
      fis[i].prop_json = json;
}

std::string CsvFileScanner::defaultProperties(const epoch::EpochFileInfo &cur_info) const
{
  return DefaultProps(cur_info.file_name);
}

bool CsvFileScanner::NameToInfo(const std::string &naked_file_name, std::string &subject,
                                epoch::IDataFileScanner::TDateTime &start_time, size_t &rate)
{
  std::vector<std::string> res;
  boost::algorithm::split(res, naked_file_name, boost::is_any_of("_"));
  subject = res[0];
  try {
    start_time = boost::posix_time::from_iso_string(res[1]);
    std::vector<std::string> ress;
    boost::algorithm::split(ress, res[2], boost::is_any_of("."));
    if(ress.size()<2)
      return false;
    rate = boost::lexical_cast<size_t>(ress.front());
  }
  catch(boost::bad_lexical_cast& ex) {
    LOGLITE_LOG_L1(ex.what());
    return false;
  }
  LOGLITE_LOG_L3(subject<<start_time<<rate);
  return true;
}

std::string CsvFileScanner::DefaultProps(const std::string& file_name)
{
  CsvReader::Properties props;
  std::string subj;
  NameToInfo(file_name, subj, props.start, props.rate);
  std::string json;
  props.toString(json);
  return json;
}
} // namespace csv
