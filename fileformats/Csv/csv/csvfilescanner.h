#ifndef CSVFILESCANNER_H
#define CSVFILESCANNER_H
#include <interfaces/fileformats/datafilescanner.h>
#include <boost/date_time.hpp>
#include <string>
namespace csv {
class CsvFileScanner : public epoch::IDataFileScanner {
public:
  epoch::DataFileInfo handle(const boost::filesystem::path&file) const;
  std::string info(const boost::filesystem::path& file, int idx)const;
  void properties(const epoch::EpochFileInfo &prev_prop, void *, std::vector<epoch::EpochFileInfo>& fis) const;
  std::string defaultProperties(const epoch::EpochFileInfo &cur_info)const;
public:
  static bool NameToInfo(const std::string& naked_file_name, std::string& subject,
                         TDateTime& start_time, size_t& rate);
  static std::string DefaultProps(const std::string &file_name);
};
} //namespace csv

#endif // CSVFILESCANNER_H
