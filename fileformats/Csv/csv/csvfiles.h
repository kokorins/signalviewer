#ifndef CSVFILES_H
#define CSVFILES_H
#include <interfaces/fileformats/epochfiles.h>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>

/**
 * \filename csvfiles.h
 * edf includes the reader for edf data format
 */
namespace csv {
class CsvFiles : public epoch::IEpochFiles {
public:
  CsvFiles();
  void setProperties(const std::string&);
  bool open(const std::vector<epoch::EpochFileInfo>& paths);
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  size_t size() const;
  void close();
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace csv
#endif // CSVFILES_H
