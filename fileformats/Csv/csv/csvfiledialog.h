#ifndef CSVFILEDIALOG_H
#define CSVFILEDIALOG_H

#include <QtWidgets/QDialog>
#include <QDateTime>

namespace Ui {
class CsvFileDialog;
}

class CsvFileDialog : public QDialog {
  Q_OBJECT
public:
  explicit CsvFileDialog(QWidget *parent = 0);
  ~CsvFileDialog();
  void setStart(const QDateTime&);
  void setRate(int);
  int rate()const;
  QDateTime start()const;
private:
  Ui::CsvFileDialog *ui;
};

#endif // CSVFILEDIALOG_H
