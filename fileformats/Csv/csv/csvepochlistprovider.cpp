#include "csvepochlistprovider.h"
#include "csvfiles.h"
#include "csvfilescanner.h"
#include "csvfiledialog.h"
#include "csvreader.h"
#include <interfaces/fileformats/epochfileinfo.h>
#include <QtPlugin>
#include <QDateTime>
#include <QWidget>

namespace csv {
CsvEpochListProvider::CsvEpochListProvider(QObject *parent) : QObject(parent) {}

boost::shared_ptr<epoch::IEpochFiles> CsvEpochListProvider::createFiles() const
{
  return boost::shared_ptr<epoch::IEpochFiles>(new CsvFiles());
}

boost::shared_ptr<epoch::IDataFileScanner> CsvEpochListProvider::createScanner() const
{
  return boost::shared_ptr<epoch::IDataFileScanner>(new CsvFileScanner());
}

std::wstring CsvEpochListProvider::name() const
{
  return L"Csv";
}

std::wstring CsvEpochListProvider::extFilter() const
{
  return L"*.csv";
}

std::wstring CsvEpochListProvider::author() const
{
  return L"Sergey Kokorin";
}

std::wstring CsvEpochListProvider::version() const
{
  return L"0.4";
}
} //namespace csv
