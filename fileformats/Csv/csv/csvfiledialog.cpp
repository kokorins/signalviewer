#include "csvfiledialog.h"
#include "ui_csvfiledialog.h"

CsvFileDialog::CsvFileDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::CsvFileDialog)
{
  ui->setupUi(this);
  connect(ui->buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

CsvFileDialog::~CsvFileDialog()
{
  delete ui;
}

void CsvFileDialog::setStart(const QDateTime & dt)
{
  ui->start_datetime->setDateTime(dt);
}

void CsvFileDialog::setRate(int rt)
{
  ui->rate_edit->setValue(rt);
}

int CsvFileDialog::rate() const
{
  return ui->rate_edit->value();
}

QDateTime CsvFileDialog::start() const
{
  return ui->start_datetime->dateTime();
}
