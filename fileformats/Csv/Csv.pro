QT += core gui widgets
TARGET = csvplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/fileformats
include(../../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../algos/

win32 {
  DEFINES += _SCL_SECURE_NO_WARNINGS
}

DEPENDPATH += $$PWD/../../algos

HEADERS += \
    csv/csvepochlistprovider.h \
    csv/csvfilescanner.h \
    csv/csvfiles.h \
    csv/csvreader.h \
    csv/csvfiledialog.h

SOURCES += \
    csv/csvepochlistprovider.cpp \
    csv/csvfilescanner.cpp \
    csv/csvfiles.cpp \
    csv/csvreader.cpp \
    csv/csvfiledialog.cpp

LIBS += -L$$OUT_PWD/../.. -lalgos

win32:PRE_TARGETDEPS += $$OUT_PWD/../../algos.lib
else:PRE_TARGETDEPS += $$OUT_PWD/../../libalgos.a

FORMS += \
    csv/csvfiledialog.ui
