#include "atxtfilescanner.h"
#include "atxtheader.h"
#include "atxtreader.h"
#include "atxtfiledialog.h"
#include <csvfile/csvchunk.hpp>
#include <csvfile/csvrow.h>
#include <interfaces/fileformats/epochfileinfo.h>
#include <loglite/logging.hpp>
#include <boost/date_time.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

namespace atxt {
namespace fs = boost::filesystem;
epoch::DataFileInfo AtxtFileScanner::handle(const fs::path &file) const
{
  epoch::DataFileInfo res;
  res.is_ok = false;
  if(!fs::is_regular_file(file)) {
    LOGLITE_LOG_L1(file<<" is not a file");
    return res;
  }
  if(fs::extension(file)!=std::string(".txt")) {
    LOGLITE_LOG_L1(file<<" has no csv extention");
    return res;
  }
  res.file_name = file.filename().generic_string();
  csvfile::CsvChunkReader<csvfile::CsvDoubleRow, AtxtHeader> ccr("\t ,", 100, false);
  std::string file_name = file.generic_string();
  if(!ccr.open(file_name)) {
    LOGLITE_LOG_L1(file_name<<" cant be opened");
    return res;
  }
  const atxt::AtxtHeader& h = ccr.headers();
  const std::vector<atxt::AtxtChannel>& chan = h.chans();
  if(chan.empty()) {
    LOGLITE_LOG_L1(file_name<<" has no chnannels");
    return res;
  }
  res.subj = chan.front().subject;
  for(size_t i=0; i<chan.size(); ++i) {
    std::string channel = chan[i].chan_name;
    boost::algorithm::to_upper(channel);
    res.channels.push_back(channel);
    res.start_times.push_back(h.start());
    LOGLITE_LOG_L2("Channel added: "<<file_name<<" "<<channel<<h.start());
  }
  ccr.close();
  res.is_ok = true;
  return res;
}

std::string AtxtFileScanner::info(const fs::path &file, int idx) const
{
  if(!fs::is_regular_file(file))
    return "Non regular file";
  csvfile::CsvChunkReader<csvfile::CsvDoubleRow, AtxtHeader> ccr("\t, ");
  if(!ccr.open(file.generic_string()))
    return "Cant open it";
  std::stringstream ss;
  const atxt::AtxtHeader& h = ccr.headers();
  std::string subj;
  size_t sz = ccr.knownSize();
  ss<<"file: "<<file.generic_string()<<std::endl;
  ss<<"rate: "<<sz/3600<<std::endl;
  ss<<"num points: "<<sz<<std::endl;
  ss<<"start time: "<<h.start()<<std::endl;
  if(!h.chans().empty())
    subj = h.chans().front().subject;
  ss<<"num channels: "<<h.chans().size()<<std::endl;
  ss<<"patient: "<<subj<<std::endl;
  if(idx<0 || idx>=(int)h.chans().size()) {
    ss<<"There is no such channel id:"<<idx;
    return ss.str();
  }
  ss<<"Channel "<<idx<<":"<<h.chans()[idx].chan_name<<std::endl;
  return ss.str();
}

void AtxtFileScanner::properties(const epoch::EpochFileInfo & prev_prop, void * parent,
                                 std::vector<epoch::EpochFileInfo> & fis) const
{
  AtxtFileDialog* cfd = new AtxtFileDialog((QWidget*)parent);
  AtxtReader::Properties props;
  if(!prev_prop.prop_json.empty())
    props.fromString(prev_prop.prop_json);
  else
    props.fromString(defaultProperties(prev_prop));
  const boost::posix_time::ptime& bdt = props.start;
  QDate d(bdt.date().year(), bdt.date().month(), bdt.date().day());
  QTime t(bdt.time_of_day().hours(), bdt.time_of_day().minutes(), bdt.time_of_day().seconds());
  QDateTime dt(d, t);
  dt.setDate(d);
  dt.setTime(t);
  cfd->setStart(dt);
  cfd->setRate(props.rate);
  if(cfd->exec() != QDialog::Accepted)
    return;
  props.rate = cfd->rate();
  dt = cfd->start();
  boost::gregorian::date bd(dt.date().year(), dt.date().month(), dt.date().day());
  boost::posix_time::time_duration bt(dt.time().hour(), dt.time().minute(), dt.time().second());
  props.start = boost::posix_time::ptime(bd, bt);
  std::string json = props.toString();
  for(size_t i=0; i<fis.size(); ++i)
    if(fis[i].file_path == prev_prop.file_path)
      fis[i].prop_json = json;
}

std::string AtxtFileScanner::defaultProperties(const epoch::EpochFileInfo &cur_info) const
{
  return DefaultProps(cur_info.file_path);
}

std::string AtxtFileScanner::DefaultProps(const std::string& file_name)
{
  AtxtReader reader;
  reader.open(file_name.c_str());
  return reader.props().toString();
}
} // namespace atxt
