#include "atxtfiledialog.h"
#include "ui_atxtfiledialog.h"

AtxtFileDialog::AtxtFileDialog(QWidget *parent) :
  QDialog(parent), ui(new Ui::AtxtFileDialog)
{
  ui->setupUi(this);
  connect(ui->buttons, SIGNAL(accepted()), this, SLOT(accept()));
  connect(ui->buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

AtxtFileDialog::~AtxtFileDialog()
{
  delete ui;
}

void AtxtFileDialog::setStart(const QDateTime & dt)
{
  ui->start_datetime->setDateTime(dt);
}

void AtxtFileDialog::setRate(int rt)
{
  ui->rate_edit->setValue(rt);
}

int AtxtFileDialog::rate() const
{
  return ui->rate_edit->value();
}

QDateTime AtxtFileDialog::start() const
{
  return ui->start_datetime->dateTime();
}
