#ifndef ATXTFILES_H
#define ATXTFILES_H
#include <interfaces/fileformats/epochfiles.h>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>

/**
 * \filename atxtfiles.h
 * edf includes the reader for edf data format
 */
namespace atxt {
class AtxtFiles : public epoch::IEpochFiles {
public:
  AtxtFiles();
  void setProperties(const std::string&);
  bool open(const std::vector<epoch::EpochFileInfo>& paths);
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  size_t size() const;
  void close();
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace atxt
#endif // ATXTFILES_H
