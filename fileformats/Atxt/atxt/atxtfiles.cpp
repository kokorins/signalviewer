#include "atxtfiles.h"
#include "atxtreader.h"
#include <numeric>
#include <deque>
namespace atxt {
typedef boost::shared_ptr<AtxtReader> AtxtReaderPtr;
class AtxtFiles::Impl {
public:
  bool open(const std::vector<epoch::EpochFileInfo>& file_names);
  void setProperties(const std::string&);
  bool isOpen()const;
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  void getTimestamp( size_t sec, boost::posix_time::ptime& t );
  size_t numSec() const;
  void close();
private:
  bool open(size_t i);
  void idxToFileIdx( size_t idx, size_t &file_idx, size_t &inner_idx )const;
private:
  const static size_t kMaxOpened = 24;
private:
  std::vector<std::string> _file_names;
  std::deque<size_t> _opened;
  std::vector<AtxtReaderPtr> _files;
  std::vector<size_t> _num_secs;
  std::vector<size_t> _partial_sums;
  std::string _props;
};

bool AtxtFiles::Impl::open(const std::vector<epoch::EpochFileInfo>& file_names)
{
  _file_names.reserve(file_names.size());
  _files.reserve(file_names.size());
  _num_secs.reserve(file_names.size());
  for(size_t i=0; i<file_names.size(); ++i) {
    std::auto_ptr<AtxtReader> file(new AtxtReader());
    AtxtReader::Properties arp;
    if(arp.fromString(file_names[i].prop_json))
      file->setProperties(arp);
    if(file->open(file_names[i].file_path.c_str())) {
      file->setIdx(file_names[i].channel_idx);
      _file_names.push_back(file_names[i].file_path);
      _num_secs.push_back(file->sizeSec());
      file->close();
      _files.push_back(AtxtReaderPtr(file.release()));
    }
  }
  if(_files.empty())
    return false;
  _partial_sums.resize(_num_secs.size());
  std::partial_sum(_num_secs.begin(), _num_secs.end(), _partial_sums.begin());
  return true;
}

void AtxtFiles::Impl::setProperties(const std::string & props)
{
  _props = props;
}

bool AtxtFiles::Impl::open(size_t idx)
{
  std::deque<size_t>::const_iterator it = std::find(_opened.begin(), _opened.end(), idx);
  if(it==_opened.end()) {
    if(_opened.size()>=kMaxOpened) {
      size_t old_idx = _opened.front();
      _files[old_idx]->close();
      _opened.pop_front();
    }
    if(_files[idx]->open(_file_names[idx].c_str()))
      _opened.push_back(idx);
    else
      return false;
  }
  return _files[idx]->isOpen();
}

void AtxtFiles::Impl::close()
{
  for(size_t i=0; i<_files.size(); ++i)
    if(_files[i].get() && _files[i]->isOpen())
      _files[i]->close();
}

std::vector<double> AtxtFiles::Impl::read(size_t st_sec, size_t dur_sec)
{
  if(!isOpen() || st_sec+dur_sec>=_partial_sums.back())
    return std::vector<double>();
  size_t file_idx = 0;
  size_t idx = 0;
  idxToFileIdx(st_sec, file_idx, idx);
  open(file_idx);
  return _files[file_idx]->read(idx, dur_sec);
}

size_t AtxtFiles::Impl::numSec() const
{
  if(_partial_sums.empty())
    return 0;
  return _partial_sums.back();
}

void AtxtFiles::Impl::idxToFileIdx( size_t idx, size_t &file_idx, size_t &inner_idx )const
{
  file_idx = 0;
  while(idx>=_partial_sums[file_idx])
    ++file_idx;
  inner_idx = idx;
  if(file_idx>=1)
    inner_idx -= _partial_sums[file_idx-1];
}

bool AtxtFiles::Impl::isOpen() const
{
  return true;
}

void AtxtFiles::Impl::getTimestamp(size_t sec, boost::posix_time::ptime& t)
{
  if(!isOpen())
    return;
  size_t file_idx = 0;
  size_t idx = 0;
  if(sec>=_partial_sums.back()) {
    file_idx = _files.size()-1;
    idx = sec-_partial_sums.back();
  }
  else {
    idxToFileIdx(sec, file_idx, idx);
  }
  open(file_idx);
  _files[file_idx]->getTime(idx, t);
}

AtxtFiles::AtxtFiles() : _impl(new Impl()) {}

void AtxtFiles::setProperties(const std::string & props)
{
  _impl->setProperties(props);
}

bool AtxtFiles::open(const std::vector<epoch::EpochFileInfo> &paths )
{
  return _impl->open(paths);
}

std::vector<double> AtxtFiles::read(size_t st_sec, size_t dur_sec)
{
  return _impl->read(st_sec, dur_sec);
}

void AtxtFiles::getTimestamp( size_t sec, boost::posix_time::ptime& t )
{
  _impl->getTimestamp(sec, t);
}

size_t AtxtFiles::size() const
{
  return _impl->numSec();
}

void AtxtFiles::close()
{
  _impl->close();
}
} //namespace atxt
