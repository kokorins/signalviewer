#ifndef ATXTFILESCANNER_H
#define ATXTFILESCANNER_H
#include <interfaces/fileformats/datafilescanner.h>
#include <boost/date_time.hpp>
#include <string>
namespace atxt {
class AtxtFileScanner : public epoch::IDataFileScanner {
public:
  epoch::DataFileInfo handle(const boost::filesystem::path& file) const;
  std::string info(const boost::filesystem::path& file, int idx)const;
  void properties(const epoch::EpochFileInfo &prev_prop, void *, std::vector<epoch::EpochFileInfo>& fis) const;
  std::string defaultProperties(const epoch::EpochFileInfo &cur_info)const;
private:
  static std::string DefaultProps(const std::string &file_name);
};
} //namespace atxt

#endif // ATXTFILESCANNER_H
