#include "atxtheader.h"
#include <boost/algorithm/string.hpp>
namespace atxt {
bool AtxtHeader::read(std::istream &fin, const std::string &)
{
  bool is_header = false;
  while(!fin.eof()) {
    std::string line;
    std::streamoff pos = fin.tellg();
    std::getline(fin, line);
    if(line.empty()) {
      return is_header;
    }
    if(!boost::algorithm::starts_with(line, "#")) {
      fin.seekg(pos);
      return is_header;
    }
    is_header = true;
    if(boost::algorithm::starts_with(line, "# UTC: ")) {
      boost::algorithm::erase_first(line, "# UTC: ");
      boost::algorithm::replace_all(line, "\r", "");
      std::vector<std::string> dt;
      boost::algorithm::split(dt, line, boost::algorithm::is_any_of(" "));
      if(dt.size() == 2) {
        boost::gregorian::date gd = boost::gregorian::from_string(dt[0]);
        boost::posix_time::time_duration td = boost::posix_time::duration_from_string(dt[1]);
        _start = boost::posix_time::ptime(gd, td);
      }
    }
    if(boost::algorithm::starts_with(line, "# Col: ")) {
      boost::algorithm::erase_first(line, "# Col: ");
      boost::algorithm::replace_all(line, "\t", "");
      std::vector<std::string> props;
      boost::algorithm::split(props, line, boost::algorithm::is_any_of(","));
      if(props.size()==5) {
        AtxtChannel chan;
        chan.col_name = props[0];
        chan.subject = props[1];
        chan.id = props[2];
        chan.chan_name = props[3];
        chan.units = props[4];
        _chans.push_back(chan);
      }
    }
  }
  return false;
}

const boost::posix_time::ptime &AtxtHeader::start() const
{
  return _start;
}

const std::vector<AtxtChannel>& AtxtHeader::chans() const
{
  return _chans;
}
} //namespace atxt
