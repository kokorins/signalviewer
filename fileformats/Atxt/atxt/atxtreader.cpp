#include "atxtreader.h"
#include "atxtheader.h"
#include <csvfile/csvchunk.hpp>
#include <csvfile/csvrow.h>
#include <loglite/logging.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/date_time.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <functional>

namespace atxt {
struct getIdx : std::unary_function<csvfile::CsvDoubleRow, double> {
public:
  getIdx(size_t idx): _idx(idx) {}
  double operator()(const csvfile::CsvDoubleRow& row)const {
    return row.values()[_idx];
  }
private:
  size_t _idx;
};

const std::string AtxtReaderProperties::kTypeId = "signalviewer.atxt.AtxtReader.Properties.1";
const std::string &AtxtReaderProperties::type() const
{
  return kTypeId;
}

std::string AtxtReaderProperties::toString() const
{
  using boost::property_tree::ptree;
  ptree pt;
  pt.put("type_id", kTypeId);
  pt.put("rate", rate);
  pt.put("start_date", start.date());
  pt.put("start_time", start.time_of_day());
  std::stringstream ss;
  boost::property_tree::json_parser::write_json(ss, pt);
  return ss.str();
}

bool AtxtReaderProperties::fromString(const std::string &json)
{
  using boost::property_tree::ptree;
  std::stringstream ss(json);
  ptree pt;
  boost::property_tree::json_parser::read_json(ss, pt);
  ptree::assoc_iterator it = pt.find("type_id");
  if(it==pt.not_found())
    return false;
  std::string type_str = pt.get<std::string>("type_id");
  if(type_str==kTypeId) {
    rate = pt.get<size_t>("rate");
    boost::gregorian::date dt = pt.get<boost::gregorian::date>("start_date");
    boost::posix_time::time_duration tm = pt.get<boost::posix_time::time_duration>("start_time");
    start = boost::posix_time::ptime(dt, tm);
    return true;
  }
  return false;
}


AtxtReader::AtxtReader() : _impl(new csvfile::CsvChunkReader<csvfile::CsvDoubleRow, AtxtHeader>(",\t ")),
                           _props(new Properties()),
                           _idx(0) {}

void AtxtReader::setProperties(const AtxtReader::Properties & props)
{
  _props.reset(new Properties(props));
}

const AtxtReader::Properties &AtxtReader::props() const
{
  return *_props;
}

bool AtxtReader::open(const char *const file_name)
{
  bool is_open = _impl->open(file_name);
  if(_props->start.is_special()) {
    const AtxtHeader& h = _impl->headers();
    _props->start = h.start();
    if(_impl->knownSize()%3600) {
      LOGLITE_LOG_L1("Partial file: "<<file_name);
    }
    _props->rate = _impl->knownSize()/3600;
    if(_props->rate<1)
      _props->rate = 1;
  }
  return is_open;
}

std::vector<double> AtxtReader::read(size_t st_sec, size_t dur_sec)
{
  std::vector<double> data;
  if((st_sec+dur_sec)*_props->rate>_impl->knownSize())
    return data;
  const std::vector<csvfile::CsvDoubleRow>& s = _impl->readChunk(st_sec*_props->rate, dur_sec*_props->rate);
  std::transform(s.begin(), s.end(), std::back_inserter(data), getIdx(_idx));
  return data;
}

bool AtxtReader::isOpen() const
{
  return _impl->isOpen();
}

size_t AtxtReader::size() const
{
  return _impl->knownSize();
}

size_t AtxtReader::sizeSec() const
{
  return _impl->knownSize()/_props->rate;
}

void AtxtReader::getTime(size_t sec, boost::posix_time::ptime &t) const
{
  t = _props->start;
  t += boost::posix_time::seconds(sec);
}

void AtxtReader::close()
{
  _impl->close();
}

void AtxtReader::setIdx(size_t idx)
{
  _idx = idx;
}

size_t AtxtReader::idx() const
{
  return _idx;
}

size_t AtxtReader::numChannels() const
{
  return _impl->headers().chans().size();
}
} //namespace edf
