#ifndef ATXTFILEDIALOG_H
#define ATXTFILEDIALOG_H

#include <QDialog>
#include <QDateTime>

namespace Ui {
class AtxtFileDialog;
}

class AtxtFileDialog : public QDialog {
  Q_OBJECT
public:
  explicit AtxtFileDialog(QWidget *parent = 0);
  ~AtxtFileDialog();
  void setStart(const QDateTime&);
  void setRate(int);
  int rate()const;
  QDateTime start()const;
private:
  Ui::AtxtFileDialog *ui;
};

#endif // ATXTFILEDIALOG_H
