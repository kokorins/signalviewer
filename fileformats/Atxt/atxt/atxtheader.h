#ifndef ATXTHEADER_H
#define ATXTHEADER_H
#include <istream>
#include <ostream>
#include <boost/date_time.hpp>

namespace atxt {
struct AtxtChannel {
  std::string col_name;
  std::string subject;
  std::string id;
  std::string chan_name;
  std::string units;
};

class AtxtHeader {
public:
  bool read(std::istream& in_str, const std::string& splitter);
  void write(std::ostream& out, const std::string& splitter) const;
  const boost::posix_time::ptime& start()const;
  const std::vector<AtxtChannel>& chans()const;
private:
  boost::posix_time::ptime _start;
  std::vector<AtxtChannel> _chans;
};
} //namespace atxt
#endif // ATXTHEADER_H
