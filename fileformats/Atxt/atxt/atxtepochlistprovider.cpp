#include "atxtepochlistprovider.h"
#include "atxtfilescanner.h"
#include "atxtfiles.h"
#include <interfaces/fileformats/epochfileinfo.h>
#include <QtPlugin>
#include <QDateTime>
#include <QtWidgets/QWidget>

namespace atxt {
AtxtEpochListProvider::AtxtEpochListProvider(QObject *parent) : QObject(parent) {}

boost::shared_ptr<epoch::IEpochFiles> AtxtEpochListProvider::createFiles() const
{
  return boost::shared_ptr<epoch::IEpochFiles>(new AtxtFiles());
}

boost::shared_ptr<epoch::IDataFileScanner> AtxtEpochListProvider::createScanner() const
{
  return boost::shared_ptr<epoch::IDataFileScanner>(new AtxtFileScanner());
}

std::wstring AtxtEpochListProvider::name() const
{
  return L"ATxt";
}

std::wstring AtxtEpochListProvider::extFilter() const
{
  return L"*.txt";
}

std::wstring AtxtEpochListProvider::author() const
{
  return L"Sergey Kokorin";
}

std::wstring AtxtEpochListProvider::version() const
{
  return L"0.4";
}
} //namespace atxt
