QT += core gui widgets
TARGET = atxtplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/fileformats
include(../../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../algos/

DEPENDPATH += $$PWD/../../algos
LIBS += -L$$OUT_PWD/../.. -lalgos

win32:PRE_TARGETDEPS += $$OUT_PWD/../../algos.lib
else:PRE_TARGETDEPS += $$OUT_PWD/../../libalgos.a

win32 {
  DEFINES += _SCL_SECURE_NO_WARNINGS
}

HEADERS += \
    atxt/atxtepochlistprovider.h \
    atxt/atxtfilescanner.h \
    atxt/atxtfiles.h \
    atxt/atxtreader.h \
    atxt/atxtheader.h \
    atxt/atxtfiledialog.h
SOURCES += \
    atxt/atxtepochlistprovider.cpp \
    atxt/atxtfilescanner.cpp \
    atxt/atxtfiles.cpp \
    atxt/atxtreader.cpp \
    atxt/atxtheader.cpp \
    atxt/atxtfiledialog.cpp

FORMS += \
    atxt/atxtfiledialog.ui
