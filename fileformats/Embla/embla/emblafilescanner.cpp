#include "emblafilescanner.h"
#include "emblareader.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace embla {
namespace fs = boost::filesystem;
bool EmblaFileScanner::handle(const fs::path &file, std::string &subj,
                              std::vector<std::string>& channels, std::vector<TDateTime>& start_times) const
{
  if(!fs::is_regular_file(file))
    return false;
  if(fs::extension(file)!=std::string(".ebm"))
    return false;
  embla::EmblaReader er;
  if(!er.open(file.generic_string().c_str()))
    return false;
  subj = er.info().subject;
  std::string channel = er.info().channel_name;
  boost::algorithm::to_upper(channel);
  channels.push_back(channel);
  TDateTime start_time;
  er.getTime(0, start_time);
  er.close();
  start_times.push_back(start_time);
  return true;
}

std::string EmblaFileScanner::info(const fs::path &file) const
{
  if(!fs::is_regular_file(file))
    return "Non regular file";
  if(fs::extension(file)!=std::string(".ebm"))
    return "Non .ebm file";
  embla::EmblaReader er;
  if(!er.open(file.generic_string().c_str()))
    return "Cant open it";
  std::stringstream ss;
  ss<<"file: "<<file.generic_string()<<std::endl;
  ss<<"subject: "<<er.info().subject<<std::endl;
  ss<<"version: "<<er.info().version<<std::endl;
  ss<<"serial: "<<er.info().serial<<std::endl;
  ss<<"sample_rate: "<<er.info().sample_rate<<std::endl;
  ss<<"gain: "<<er.info().gain<<std::endl;
  ss<<"device_type: "<<er.info().device_type<<std::endl;
  ss<<"channel_name: "<<er.info().channel_name<<std::endl;
  ss<<"calibrate_unit: "<<er.info().calibrate_unit<<std::endl;
  return ss.str();
}
} // namespace embla
