#ifndef EMBLAFILES_H_
#define EMBLAFILES_H_
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
/**
 * \filename emblafiles.h
 * embla namespace for ebm files
 */
namespace embla {
  /**
   * Contains information about a list of files as a ribbon with 
   * a continuous index access to same type embla files.
   * Note: shallow copying
   */
class EmblaFiles {
public:
  EmblaFiles();
  bool open(const std::vector<std::pair<std::string, size_t> >& paths);
  void read(size_t st_sec, size_t dur_sec, std::vector<double>& data);
  void getTimestamp(size_t sec, boost::posix_time::ptime& t);
  size_t size()const;
  void close();
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
} //namespace embla
#endif //EMBLAFILES_H_
