#ifndef EMBLAREADER_H_
#define EMBLAREADER_H_
#include <cstddef>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
#include <vector>
#include <string>
/**
 * \filename emblareader.h
 */
namespace embla {
  /*
   * Embla format file info
   */
struct EmblaFileInfo {
  double sample_rate;
  double gain;
  std::string channel_name;
  std::string version;
  std::string subject;
  std::string serial;
  std::string device_type;
  std::string calibrate_unit;
};

  /*
   *
   * Read the Header information of a EBM file.
   *
   * Header is a data structure with the following fields
   *      EBMFile, IsRawData, Endian, IsNewVersionEBM, DataOffsetPos, nDataPoint, 
   *      SampleRate, UnitGain, and
   *      many other EBM fields in the header part
   * Embla File Format - EBM Developers Kit. Embla format reader
   * This a copy of Matlab routine, that reads samples from EBM files into a Matlab matrix.
   */
  class EmblaReader {
  public:
    EmblaReader();
    /**
     * \param file_name  : Filename, including path. If [] a standard get file dialog will appear.
     */
    bool open(char const*const file_name);
    /**
     * \param st_idx  : Starting index of samples to be read (first sample in file is 0)
     * \param sample_count : Number of samples to be read. If 'ALL' it reads from start to the end of the file.
     * \param is_scaled      : If true the data is scaled according to unit gain in the file (default is 1)
     *                     Scaling should only be used if the unit of the data file is V.
     * \param seg_idx: The index of the data segment within the file.  The first index has an index of zero 
     *                     which if also the default if this parameter is omitted.
     * \return size of data
     */
    size_t read(std::vector<double>& data, size_t st_sec, size_t dur_sec);
    /**
     * \return if file is open
     */
    bool isOpen()const;
    /**
     * Number of values over whole file
     */
    size_t size()const;
    size_t sizeSec()const;
    void getTime(size_t sec, boost::posix_time::ptime& t)const;
    void close();
  public:
    const EmblaFileInfo& info()const;
  private:
    class Impl;
    boost::shared_ptr<Impl> _impl;
  };
} //namespace embla
#endif //EMBLAREADER_H_
