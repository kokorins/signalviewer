#ifndef EMBLAFILESCANNER_H
#define EMBLAFILESCANNER_H
#include <interfaces/fileformats/datafilescanner.h>
#include <boost/date_time.hpp>
#include <string>
namespace embla {
class EmblaFileScanner : public epoch::IDataFileScanner {
public:
  virtual bool handle(const boost::filesystem::path&file, std::string& subj,
                      std::vector<std::string>& channel, std::vector<TDateTime>& start_time) const;
  virtual std::string info(const boost::filesystem::path& file)const;
};
} //namespace embla
#endif // EMBLAFILESCANNER_H
