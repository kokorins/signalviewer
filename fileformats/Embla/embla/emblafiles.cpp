#include "emblafiles.h"
#include "emblareader.h"
#include <deque>
#include <memory>
#include <numeric>
namespace embla {
typedef boost::shared_ptr<EmblaReader> EmblaReaderPtr;
class EmblaFiles::Impl {
public:
  bool open( const std::vector<std::pair<std::string, size_t> >& file_names);
  bool isOpen()const;
  size_t read(size_t st_sec, size_t dur_sec, std::vector<double>& data);
  void getTimestamp( size_t sec, boost::posix_time::ptime& t );
  size_t numSec() const;
  void close();
private:
  bool open(size_t i);
  void idxToFileIdx( size_t idx, size_t &file_idx, size_t &inner_idx )const;
private:
  const static size_t kMaxOpened = 24;
private:
  std::vector<std::string> _file_names;
  std::deque<size_t> _opened;
  std::vector<EmblaReaderPtr> _files;
  std::vector<size_t> _num_secs;
  std::vector<size_t> _partial_sums;
};

bool EmblaFiles::Impl::open(const std::vector<std::pair<std::string, size_t> >& file_names)
{
  _file_names.reserve(file_names.size());
  _files.reserve(file_names.size());
  _num_secs.reserve(file_names.size());
  for(size_t i=0; i<file_names.size(); ++i) {
    std::auto_ptr<EmblaReader> file(new EmblaReader());
    if(file->open(file_names[i].first.c_str())) {
      _file_names.push_back(file_names[i].first);
      _num_secs.push_back(file->sizeSec());
      file->close();
      _files.push_back(EmblaReaderPtr(file.release()));
    }
  }
  if(_files.empty())
    return false;
  _partial_sums.resize(_num_secs.size());
  std::partial_sum(_num_secs.begin(), _num_secs.end(), _partial_sums.begin());
  return true;
}

bool EmblaFiles::Impl::open( size_t idx )
{
  std::deque<size_t>::const_iterator it = std::find(_opened.begin(), _opened.end(), idx);
  if(it==_opened.end()) {
    if(_opened.size()>=kMaxOpened) {
      size_t old_idx = _opened.front();
      _files[old_idx]->close();
      _opened.pop_front();
    }
    if(_files[idx]->open(_file_names[idx].c_str()))
      _opened.push_back(idx);
    else 
      return false;
  }
  return _files[idx]->isOpen();
}

void EmblaFiles::Impl::close()
{
  for(size_t i=0; i<_files.size(); ++i)
    if(_files[i].get() && _files[i]->isOpen())
      _files[i]->close();
}

size_t EmblaFiles::Impl::read( size_t st_sec, size_t dur_sec, std::vector<double>& data )
{
  if(!isOpen() || st_sec+dur_sec>=_partial_sums.back())
    return 0;
  size_t file_idx = 0;
  size_t idx = 0;
  idxToFileIdx(st_sec, file_idx, idx);
  open(file_idx);
  return _files[file_idx]->read(data, idx, dur_sec)/_files[file_idx]->info().sample_rate;
}

size_t EmblaFiles::Impl::numSec() const
{
  if(_partial_sums.empty())
    return 0;
  return _partial_sums.back();
}

void EmblaFiles::Impl::idxToFileIdx( size_t idx, size_t &file_idx, size_t &inner_idx )const
{
  file_idx = 0;
  while(idx>=_partial_sums[file_idx])
    ++file_idx;
  inner_idx = idx;
  if(file_idx>=1)
    inner_idx -= _partial_sums[file_idx-1];
}

bool EmblaFiles::Impl::isOpen() const
{
  return true;
}

void EmblaFiles::Impl::getTimestamp( size_t sec, boost::posix_time::ptime& t )
{
  if(!isOpen() || sec>=_partial_sums.back())
    return;
  size_t file_idx = 0;
  size_t idx = 0;
  idxToFileIdx(sec, file_idx, idx);
  open(file_idx);
  _files[file_idx]->getTime(sec, t);
}

EmblaFiles::EmblaFiles() : _impl(new Impl())
{
}

bool EmblaFiles::open( const std::vector<std::pair<std::string, size_t> >& paths )
{
  return _impl->open(paths);
}

void EmblaFiles::read( size_t st_sec, size_t dur_sec, std::vector<double>& data )
{
  _impl->read(st_sec, dur_sec, data);
}

void EmblaFiles::close()
{
  _impl->close();
}

size_t EmblaFiles::size() const
{
  return _impl->numSec();
}

void EmblaFiles::getTimestamp( size_t sec, boost::posix_time::ptime& t )
{
  _impl->getTimestamp(sec, t);
}

} //namespace embla
