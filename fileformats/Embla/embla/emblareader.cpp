#include "emblareader.h"
#include <loglite/logging.hpp>
#include <fstream>
#include <algorithm>
#include <functional>
#include <numeric>
#include <ios>
#include <boost/date_time.hpp>
#include <boost/format.hpp>
namespace embla {
class EmblaReader::Impl {
public:
  Impl():_is_signed(true){}
  bool open( char const*const file_name);
  bool isOpen() const;
  size_t read(size_t st_sec, size_t num_sec, std::vector<double>& data);
  size_t size()const;
  size_t sizeSec() const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  void close();
private:
  static bool ReadSignature(std::istream& in, std::string& signature);
  template <typename ValueType, int num_bytes>
  static void InverseBytes(ValueType& val) {
    union UType{
      char a[num_bytes];
      ValueType v;
    }vt;
    vt.v = val;
    for(int i=num_bytes/2; i-->0;)
      std::swap(*(vt.a+i),*(vt.a+num_bytes-i-1));
    std::swap(vt.v, val);
  }
  template <typename ValueType> 
  static void InverseBytes(ValueType& val) {
    InverseBytes<ValueType, sizeof(ValueType)>(val);
  }
public:
  const EmblaFileInfo& info()const {
    return _fi;
  }
private:
  EmblaFileInfo _fi;
  std::ifstream _in;
  std::streampos _first_block_offset;
  bool _wide_id;
  std::string _file_name;
  std::vector<boost::posix_time::ptime> _start_time;
  std::vector<std::streampos> _offpos;
  std::vector<size_t> _num_pts;
  std::vector<std::pair<size_t,size_t> > _seg_idx;
  std::string _version;
  unsigned short _chan_num;
  unsigned short _mask;
  bool _is_signed;
  size_t _data_in_bytes;
  bool _swap;
};

struct IncludeInterval : std::unary_function<bool, std::pair<size_t,size_t> >{
public:
  IncludeInterval(size_t st_idx, size_t end_idx):_st_idx(st_idx),_end_idx(end_idx){}
  bool operator()(const std::pair<size_t,size_t>& interval) 
  {
      return ((interval.first <= _st_idx) && (interval.second >= _st_idx)) || 
             ((interval.first <= _end_idx) && (interval.second >= _end_idx)) ||
             ((interval.first >= _st_idx) && (interval.second <= _end_idx));
  }
private:
  size_t _st_idx;
  size_t _end_idx;
};

bool EmblaReader::Impl::open( char const*const file_name)
{
  _file_name = file_name;
  static const unsigned char EBM_MAC_ENDIAN = 0xFF;
  static const unsigned char EBM_INTEL_ENDIAN = 0x00;
  static const long EBM_R_VERSION = 0x80;
  static const long EBM_R_SUBJECT_INFO = 0xD0;
  static const long EBM_R_HEADER = 0x81;
  static const long EBM_R_TIME = 0x84;
  static const long EBM_R_CHANNEL = 0x85;
  static const long EBM_R_SAMPLING_RATE = 0x86;
  static const long EBM_R_UNIT_GAIN = 0x87;
  static const long EBM_R_CHANNEL_NAME = 0x90;
  static const long EBM_R_DATA_MASK16 = 0x95;
  static const long EBM_R_SIGNED_DATA = 0x96;
  static const long EBM_R_CALIBRATE_FUNC = 0x98;
  static const long EBM_R_CALIBRATE_UNIT = 0x99;
  static const long EBM_R_EVENT = 0xA0;
  static const long EBM_R_DATA = 0x20;
  static const long EBM_R_INVALID = 0xff;
  static const long EBM_R_SERIALNUMBER = 0xC0;
  static const long EBM_R_DEVICETYPE = 0xC1;
  static const std::streampos EBM_UNKNOWN_DATASIZE = 0xFFFFFFFF;


  size_t DW_SIZE = 4;   // Size of double word
  size_t UINT16_SIZE = 2;
  size_t BYTE_SIZE = 1; // Size of byte

  _in.open(file_name, std::ios::binary);
  if(!_in.is_open())
    return false;
  std::string signature;
  if(!ReadSignature(_in, signature)) {
    _in.close();
    return false;
  }
  unsigned char endian;
  _in.read((char*)&endian, sizeof(endian));
  _swap = (endian==EBM_MAC_ENDIAN);
  _first_block_offset = _in.tellg();
  // Store the position of the start of the block structure
  // If this is not a file with 8 bit block IDs then we will change
  // this again.
  _wide_id = false;
  unsigned char uch;
  unsigned long ulg;
  _in.read((char*)&uch, sizeof(uch));
  if(uch == 0xFF) {
    _in.read((char*)&ulg, sizeof(ulg));
    if ( ulg == 0xFFFFFFFF) {
      // We have 32 bit block IDs so we skip the rest of the
      // 32 byte header and store the position of the block
      // structure which should start right after.
      // Once we decide this is the new version, we know we have 32 bit record 
      // IDs, and we know there are 26 bytes reserved after 5 byte feature chars
      // and the remaining records should start right after.
      // Note that 31 = 26 (reserved) + 5 (feature chars)
      _first_block_offset += 31;
      _wide_id = true;
    }
  }
  _in.seekg(_first_block_offset, std::ios::beg);
  //Begin to read header records one by one
  unsigned long RecordID = 0;
  std::streampos RecordSize = 0;
  std::streampos RecordPos;
  // there can be multiple start time because of multiple segments
  while(true) {
    if (_wide_id) {
      _in.read((char*)&ulg,sizeof(ulg));
      if(_swap)
        InverseBytes(ulg);
      RecordID = ulg;
    }
    else {
      _in.read((char*)&uch,sizeof(uch));
      RecordID = uch;
    }
    if (_in.eof() || _in.fail()) // reach the end of the file
      break;
    else {
      _in.read((char*)&ulg, sizeof(ulg));
      if(_swap)
        InverseBytes(ulg);
      RecordSize = ulg;
      if (_in.eof() || _in.fail())
        break;
    }
    RecordPos = _in.tellg();
    unsigned int unt;
    const size_t buf_size = 1000;
    char buf[buf_size + 1];
    buf [buf_size] = 0;
    LOGLITE_LOG_L3(std::hex<<std::showbase<<RecordID);
    switch(RecordID) {
    break; case EBM_R_VERSION: //version of the EBM format
      unsigned char minor, major;
      if (_swap) {
        _in.read((char*)&uch, sizeof(uch));
        major = uch;
        _in.read((char*)&uch, sizeof(uch));
        minor = uch;
      }
      else {
        _in.read((char*)&uch, sizeof(uch));
        minor = uch;
        _in.read((char*)&uch, sizeof(uch));
        major = uch;
      }
      _version = boost::str(boost::format("%1%.%2$02s")%(int)major%(int)minor);
    break; case EBM_R_TIME: {// recording date and time
      //CurRowIndex = size(Header.StartTime, 1);
      unsigned short int year;
      unsigned char mon, day, hour, minute, sec, msec;
      _in.read((char*)&year, sizeof(year)); // Year (ex. 1996)
      if(_swap)
        InverseBytes<unsigned short, sizeof(unsigned short)>(year);
      _in.read((char*)&mon, sizeof(mon)); // Month 1-12
      _in.read((char*)&day, sizeof(day)); // Day 1-31
      _in.read((char*)&hour, sizeof(hour)); // Hour 0-23
      _in.read((char*)&minute, sizeof(minute)); // Minute 0-59
      _in.read((char*)&sec, sizeof(sec)); // Second 0-59
      _in.read((char*)&msec, sizeof(msec)); // Hundredth of second 0-99
      boost::gregorian::date dt(year, mon, day);
      boost::posix_time::time_duration tm(hour, minute, sec, msec*10);
      boost::posix_time::ptime t(dt, tm);
      _start_time.push_back(t);
    }
    break; case EBM_R_CHANNEL: // Channel number
      _in.read((char*)&_chan_num, sizeof(_chan_num));
      if(_swap)
        InverseBytes(_chan_num);            
    break; case EBM_R_SAMPLING_RATE: //sampling rate (in Hz)
      _in.read((char*)&unt, sizeof(unt));
      if(_swap)
        InverseBytes(unt);
      _fi.sample_rate = unt/1000.0;// it is stored in mHz
    break; case EBM_R_UNIT_GAIN: // Unit gain (in uV/bit MicroVoltage/Bit)
      _in.read((char*)&unt,sizeof(unt));
      if(_swap)
        InverseBytes(unt);
      _fi.gain = unt/1000.0; // it is stored in nV/bit
    break; case EBM_R_CHANNEL_NAME:
      _in.read(buf, std::min((std::streampos)buf_size, RecordSize)*sizeof(char));
      _fi.channel_name = buf;
    break; case EBM_R_SUBJECT_INFO:
      _in.read(buf, std::min((std::streampos)buf_size, RecordSize)*sizeof(char));
      _fi.subject = buf;
    break; case EBM_R_DATA_MASK16:
      _in.read((char*)&_mask, sizeof(_mask));
      if(_swap)
        InverseBytes(_mask);            
    break; case EBM_R_SIGNED_DATA:
      _in.read((char*)&uch, sizeof(uch));
      _is_signed = (uch!=0);
    break; case EBM_R_SERIALNUMBER:
      _in.read(buf, std::min((std::streampos)buf_size, RecordSize)*sizeof(char));
      _fi.serial = buf;
    break; case EBM_R_DEVICETYPE:
      _in.read(buf, std::min((std::streampos)buf_size, RecordSize)*sizeof(char));
      _fi.device_type = buf;
    break; case EBM_R_DATA:
      _offpos.push_back(_in.tellg());
      size_t nSamples;
      if ( RecordSize == EBM_UNKNOWN_DATASIZE ) {
        _in.seekg(0, std::ios::end);
        nSamples = (_in.tellg() - _offpos.back())/2; // data is recorded as 16bit int
      }
      else {
          nSamples = RecordSize / 2;
      }
      _num_pts.push_back(nSamples);
      _data_in_bytes = UINT16_SIZE; // the number of bytes in each data point
    break; case EBM_R_CALIBRATE_UNIT:
      _in.read(buf, std::min((std::streampos)buf_size, RecordSize)*sizeof(char));
      _fi.calibrate_unit = buf;
    break; default:;
    } // end of switch-case
    _in.seekg(RecordPos + RecordSize, std::ios::beg);
  } // end of reading of the records
  for(size_t i=0; i<_start_time.size(); ++i) {
    size_t str_idx;
    size_t end_idx;
    if(i==0)
      str_idx = 0;
    else {
      str_idx += (_start_time[i]-_start_time[i-1]).total_milliseconds() * _fi.sample_rate/1000;
      // the str_idx should be at least larger than the Previous Segment's End Index          
      str_idx = std::max(_seg_idx.back().second + 1, str_idx);
    }
    end_idx = str_idx + _num_pts[i] - 1;
    _seg_idx.push_back(std::make_pair(str_idx, end_idx));
  }
  if(_in.eof() || _in.fail()) {
    _in.clear();
    _in.seekg(0, std::ios::beg);
  }
  return _in.is_open();
}

size_t EmblaReader::Impl::read(size_t st_sec, size_t num_sec, std::vector<double>& data)
{
  data.clear();
  static const unsigned long EBM_R_SAMPLING_RATE =0x86;
  static const unsigned long EBM_R_UNIT_GAIN = 0x87;
  static const long EBM_R_DATA = 0x20;
  static const long EBM_UNKNOWN_DATASIZE = 0xFFFFFFFF;
  static const size_t W_SIZE = 2;    // Size of word

  size_t st_idx = st_sec*_fi.sample_rate;
  size_t read_length = _seg_idx.back().second - st_idx +1; // the maximal number of data left in the recording
  if(num_sec>0)
    read_length = std::min(read_length, (size_t)std::floor(num_sec * _fi.sample_rate));
  size_t end_idx = st_idx + read_length-1;
  // select covered segment
  std::vector<std::pair<size_t,size_t> > sel_seg;
  std::vector<size_t> sel_idxs;
  IncludeInterval ii(st_idx, end_idx);
  for(size_t i=0; i<_seg_idx.size(); ++i) {
    if(ii(_seg_idx[i])) {
      sel_seg.push_back(_seg_idx[i]);
      sel_idxs.push_back(i);
    }
  }
  // open the file
  data.reserve(read_length);
  size_t sig_st = 0;
  _in.seekg(0, std::ios::beg);
  for(size_t i=0; i<sel_seg.size(); ++i) {
    size_t inside_seg_start = std::max(0, ((int)st_idx-(int)sel_seg[i].first));
    size_t rem_len;
    // update the read_length by removing the previously read length
    if (i > 1) {
      rem_len = read_length - sel_seg[i].first - st_idx;
      // find the gap (in data points) between the two segments. 
      size_t gap = sel_seg[i].first - sel_seg[i-1].second - 1;
      sig_st += gap;
    }
    else
      rem_len = read_length;
	  //make sure the current remainingLength does not go beyond the current segment end.
    size_t inside_seg_len = std::min(rem_len, _num_pts[sel_idxs[i]] - inside_seg_start);
    // move to the right position
    _in.seekg(_offpos[sel_idxs[i]]+(std::streampos)_data_in_bytes*inside_seg_start, std::ios::beg);
    if (_is_signed) {
      short st;
      for(size_t j=0; j<inside_seg_len; ++j) {
        _in.read((char*)&st, sizeof(st));
        if(_swap)
          InverseBytes(st);            
        data.push_back(st);
      }
    }
    else {
      unsigned short ust;
      for(size_t j=0; j<inside_seg_len; ++j) {
        _in.read((char*)&ust, sizeof(ust));
        if(_swap)
          InverseBytes(ust);            
        data.push_back(ust);
      }
    }
	  st_idx += inside_seg_len;
  }
  std::transform(data.begin(), data.end(), data.begin(),
                 std::bind2nd(std::multiplies<double>(), _fi.gain));
  return data.size();
}

void EmblaReader::Impl::close()
{
  if(_in.is_open())
    _in.close();
}

bool EmblaReader::Impl::ReadSignature(std::istream& in, std::string& signature )
{
  static const std::string EBM_RAWDATA_HEADER = "Embla data file";
  static const std::string EBM_RESULTS_HEADER = "Embla results file";

  static const long EBM_END_OF_SIGNATURE = 0x1A;
  static const long EBM_MAX_SIGNATURE_SIZE = 80;
  signature.clear();
  signature.reserve(EBM_MAX_SIGNATURE_SIZE+1);
  char s;
  in.read(&s, sizeof(s));
  size_t i = 0;
  signature.push_back(s);
  while(signature[i] != EBM_END_OF_SIGNATURE && i < EBM_MAX_SIGNATURE_SIZE) {
    ++i;
    in.read(&s, sizeof(s));
    signature.push_back(s);
  }

  if (i == EBM_MAX_SIGNATURE_SIZE) {
    return false;
  }
  std::string::size_type it = signature.find(EBM_RAWDATA_HEADER);
  if(it==std::string::npos) {
    it = signature.find(EBM_RESULTS_HEADER);
    if(it==std::string::npos)
      return false;
  }
  return true;
}

size_t EmblaReader::Impl::size() const
{
  return std::accumulate(_num_pts.begin(), _num_pts.end(),0.0);
}

size_t EmblaReader::Impl::sizeSec() const
{
  return size()/_fi.sample_rate;
}

bool EmblaReader::Impl::isOpen() const
{
  return _in.is_open();
}

void EmblaReader::Impl::getTime(size_t sec, boost::posix_time::ptime& t)const
{
  size_t idx = sec*_fi.sample_rate;
  // select covered segment
//  std::vector<std::pair<size_t,size_t> > sel_seg;
//  std::vector<size_t> sel_idxs;
  IncludeInterval ii(idx, idx);
  for(size_t i=0; i<_seg_idx.size(); ++i) {
    if(ii(_seg_idx[i])) {
      t = _start_time[i];
      t += boost::posix_time::seconds((idx-_seg_idx[i].first)/_fi.sample_rate);
      return;
    }
  }
}

bool EmblaReader::open( char const*const file_name)
{
  return _impl->open(file_name);
}

EmblaReader::EmblaReader() : _impl(new EmblaReader::Impl())
{
}

void EmblaReader::close()
{
  _impl->close();
}

const EmblaFileInfo &EmblaReader::info() const
{
  return _impl->info();
}

size_t EmblaReader::read(std::vector<double>& data, size_t st_sec, size_t dur_sec)
{
  return _impl->read(st_sec, dur_sec, data);
}

size_t EmblaReader::size() const
{
  return _impl->size();
}

size_t EmblaReader::sizeSec() const
{
  return _impl->sizeSec();
}

bool EmblaReader::isOpen() const
{
  return _impl->isOpen();
}

void EmblaReader::getTime( size_t sec, boost::posix_time::ptime& t ) const
{
  _impl->getTime(sec, t);
}

} //namespace embla
