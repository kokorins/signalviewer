TARGET = Embla
TEMPLATE = lib
CONFIG += staticlib plugin

INCLUDEPATH += \
  $$PWD/../../ \
  $$PWD/../../epoch/

win32 {
  BOOST_ROOT = "G:/opt/boost_1_53_0"
  INCLUDEPATH += $$BOOST_ROOT
  LIBS += -L$$BOOST_ROOT/stage/lib
}


HEADERS += \
    embla/emblareader.h \
    embla/emblafiles.h \
    embla/emblafilescanner.h

SOURCES += \
    embla/emblareader.cpp \
    embla/emblafiles.cpp \
    embla/emblafilescanner.cpp
