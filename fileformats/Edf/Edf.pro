TARGET = edfplugin
TEMPLATE = lib
CONFIG += plugin
DESTDIR = $$OUT_PWD/../../plugins/fileformats
include(../../signalviewer.pri)

INCLUDEPATH += \
  $$PWD/../../

HEADERS += \
    edf/edfrowreader.h \
    edf/edfrow.h \
    edf/edfreader.h \
    edf/edffiles.h \
    edf/edffilescanner.h \
    edf/edfepochlistprovider.h

SOURCES += \
    edf/edfrowreader.cpp \
    edf/edfrow.cpp \
    edf/edfreader.cpp \
    edf/edffiles.cpp \
    edf/edffilescanner.cpp \
    edf/edfepochlistprovider.cpp
