#ifndef EDFREADER_H
#define EDFREADER_H
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
#include <vector>
/// \filename edfreader.h
namespace edf {

class EdfRowReader;

/**
 * This is main file for edf format reader according to interface for
 */
class EdfReader {
public:
  EdfReader();
  /**
   * \param file_name  : Filename, including path.
   */
  bool open(char const*const file_name);
  /**
   * \param st_idx  : Starting index of samples to be read (first sample in file is 0)
   * \param sample_count : Number of samples to be read. If 'ALL' it reads from start to the end of the file.
   * \param is_scaled      : If true the data is scaled according to unit gain in the file (default is 1)
   *                     Scaling should only be used if the unit of the data file is V.
   * \param seg_idx: The index of the data segment within the file.  The first index has an index of zero
   *                     which if also the default if this parameter is omitted.
   * \return size of data
   */
  std::vector<double> read(size_t st_sec, size_t dur_sec);
  /**
   * \return if file is open
   */
  bool isOpen()const;
  /**
   * Number of values over whole file
   */
  size_t size()const;
  /**
   * Size of file in seconds
   */
  size_t sizeSec()const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  void close();
public:
  /**
   * Edf files could contains number of channels, each of them could be obtained by index here
   */
  void setIdx(size_t idx);
  size_t idx()const;
  /**
   * Number of channels in file
   */
  size_t numChannels()const;
private:
  boost::shared_ptr<EdfRowReader> _impl;
  size_t _idx;
};
} // namespace edf
#endif // EDFREADER_H
