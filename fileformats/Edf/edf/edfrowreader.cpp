#include "edfrowreader.h"
#include "edfrow.h"
#include <loglite/logging.hpp>
#include <fstream>
#include <limits>
#include <sstream>

namespace edf {

class EdfRowReader::Impl {
public:
  bool open(char const*const file_name);
  size_t read(size_t idx, EdfRow& row);
  bool isOpen()const;
  void close();
  const EdfHeader& header()const;
  const EdfRowHeader& rowHeader(size_t idx)const;
  size_t size()const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
private:
  static size_t BlockSize(std::istream& in, size_t num_blocks, size_t idx);
  static double PhisicalLow(std::istream& in, size_t num_blocks, size_t idx);
  static double PhisicalHigh(std::istream& in, size_t num_blocks, size_t idx);
private:
  std::ifstream _in;
  EdfHeader _header;
  std::vector<EdfRowHeader> _row_header;
private:
  //Version of input format supported.
  static const std::string VERSION;
};
const std::string EdfRowReader::Impl::VERSION = "0       ";

bool EdfRowReader::Impl::open(const char *const file_name)
{
  /* Open file with sampled values. */
  _in.open(file_name, std::ios::binary);
  if (!_in) {
    LOGLITE_LOG_L1("Cant open file");
    return false;
  }
  char buffer[81];
//  int i;
  /* Read and check version string. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  if (buffer != VERSION) {
    LOGLITE_LOG_L1("Incorrect version "<<buffer);
    return false;
  }
  /* Read patient information. */
  _in.read(buffer, 80);
  if (!_in)
    return false;
  buffer[80] = 0;
  for (int i = 79; i >= 0 && buffer[i] == ' '; i--)
    buffer[i] = 0;
  _header.patient = buffer;
  /* Read recording information. */
  _in.read(buffer, 80);
  if (!_in)
    return false;
  buffer[80] = 0;
  for (int i = 79; i >= 0 && buffer[i] == ' '; i--)
    buffer[i] = 0;
  _header.recording = buffer;
  /* Read start date of recording. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  for (int i = 7; i >= 0 && buffer[i] == ' '; i--)
    buffer[i] = 0;
  std::istringstream date(buffer);
  int day, month, year;
  int hour, minute, second;
  date >> day;
  date.get();
  date >> month;
  date.get();
  date >> year;
  if(year<50)
    year += 2000;
  if(year<100)
    year += 1900;
  if (!date)
    return false;
  boost::gregorian::date dt(year, month, day);
  /* Read start time of recording. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  for (int i = 7; i >= 0 && buffer[i] == ' '; i--)
    buffer[i] = 0;
  std::istringstream time(buffer);
  time >> hour;
  time.get();
  time >> minute;
  time.get();
  time >> second;
  if (!time)
    return false;
  boost::posix_time::time_duration tm(hour, minute, second);
  _header.start_time = boost::posix_time::ptime(dt, tm);
  /* Read number of bytes in input file header. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  _header.offset = std::atoi(buffer);
  /* Read first reserved block. */
  _in.read(buffer, 44);
  if (!_in)
    return false;
  /* Read number of data records. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  _header.size = std::atoi(buffer);
    /* Read data record duration. */
  _in.read(buffer, 8);
  if (!_in)
    return false;
  buffer[8] = 0;
  _header.duration = std::atoi(buffer);
  /* Read number of signals in input file. */
  _in.read(buffer, 4);
  if (!_in)
    return false;
  buffer[4] = 0;
  _header.num_channels = std::atoi(buffer);
  _row_header.clear();

  for (size_t chan = 0; chan < _header.num_channels; chan++) {
    EdfRowHeader erh;
    /* Read signal label. */
    _in.seekg(256 + chan * 16);
    _in.read(buffer, 16);
    if (!_in)
      return false;
    buffer[16] = 0;
    for (int i = 15; i >= 0 && buffer[i] == ' '; i--)
      buffer[i] = 0;
    erh.label = buffer;
    LOGLITE_LOG_L3(erh.label<<" signal is processing");
    /* Read signal transducer type. */
    _in.seekg(256 + _header.num_channels * 16 + chan * 80);
    _in.read(buffer, 80);
    if (!_in)
      return false;
    buffer[80] = 0;
    for (int i = 79; i >= 0 && buffer[i] == ' '; i--)
      buffer[i] = 0;
    erh.transducer = buffer;
    /* Read and calculate signal unit (must be in volts). */
    _in.seekg(256 + _header.num_channels * 96 + chan * 8);
    _in.read(buffer, 8);
    if (!_in)
      return false;
    buffer[8] = 0;

    int ch_s;
    for (ch_s = 7; ch_s >= 0 && buffer[ch_s] == ' '; ch_s--)
      buffer[ch_s] = 0;
    switch (ch_s) {
    break; case 0:
      if (toupper(buffer[ch_s]) != 'V') {
        LOGLITE_LOG_L3("Strange units:"<<buffer);
        erh.unit = 1;
      }
      else
        erh.unit = 1e6;
    break; case 1:
      if (toupper(buffer[ch_s]) != 'V') {
        LOGLITE_LOG_L2("Incorrect units");
        return false;
      }
      switch (buffer[ch_s - 1]) {
      break; case 'Y':
        erh.unit = 1e30;
      break; case 'Z':
        erh.unit = 1e27;
      break; case 'E':
        erh.unit = 1e24;
      break; case 'P':
        erh.unit = 1e21;
      break; case 'T':
        erh.unit = 1e18;
      break; case 'G':
        erh.unit = 1e15;
      break; case 'M':
        erh.unit = 1e12;
      break; case 'K':
        erh.unit = 1e9;
      break; case 'H':
        erh.unit = 1e8;
      break; case 'D':
        erh.unit = 1e7;
      break; case 'd':
        erh.unit = 1e5;
      break; case 'c':
        erh.unit = 1e4;
      break; case 'm':
        erh.unit = 1e3;
      break; case 'u':
        erh.unit = 1;
      break; case 'n':
        erh.unit = 1e-3;
      break; case 'p':
        erh.unit = 1e-6;
      break; case 'f':
        erh.unit = 1e-9;
      break; case 'a':
        erh.unit = 1e-12;
      break; case 'z':
        erh.unit = 1e-15;
      break; case 'y':
        erh.unit = 1e-18;
      break; default:
        LOGLITE_LOG_L2("Incorrect units");
        return false;
      }
    break; default:
      LOGLITE_LOG_L2("Incorrect units");
      return false;
    }
    erh.physical_low = PhisicalLow(_in, _header.num_channels, chan);
    erh.physical_high = PhisicalHigh(_in, _header.num_channels, chan);
    /* Read digital minimum and maximum. */
    _in.seekg(256 + _header.num_channels * 120 + chan * 8);
    _in.read(buffer, 8);
    if (!_in)
      return false;
    buffer[8] = 0;
    erh.digital_low = std::atoi(buffer);
    _in.seekg(256 + _header.num_channels * 128 + chan * 8);
    _in.read(buffer, 8);
    if (!_in)
      return false;
    buffer[8] = 0;
    erh.digital_high = std::atoi(buffer);
    /* Read prefiltering indicator. */
    _in.seekg(256 + _header.num_channels * 136 + chan * 80);
    _in.read(buffer, 80);
    if (!_in)
      return false;
    buffer[80] = 0;
    for (int i = 79; i >= 0 && buffer[i] == ' '; i--)
      buffer[i] = 0;
    erh.prefiltering = buffer;
    erh.block_count = BlockSize(_in, _header.num_channels, chan);
    erh.num_point = _header.size * erh.block_count;
    /* Read reserved bytes. */
    _in.seekg(256 + _header.num_channels * 224 + chan * 32);
    _in.read(buffer, 32);
    if (!_in)
      return false;
    erh.c1 = (erh.digital_high != erh.digital_low)
             ? (erh.physical_high - erh.physical_low) * erh.unit
                / (erh.digital_high - erh.digital_low) : 0;
    erh.c0 = erh.physical_low * erh.unit - erh.digital_low * erh.c1;
    _in.seekg(184);
    _in.read(buffer, 8);
    if (!_in)
      return false;
    buffer[8] = 0;
    erh.offset = std::atoi(buffer);
    /* Calculate offset of first signal sample in input file and
     number of bytes to skip between two successive signal data
     records. */
    erh.skip = 0;
    for (size_t ch = 0; ch < chan; ch++) {
      _in.seekg(256 + _header.num_channels * 216 + ch * 8);
      _in.read(buffer, 8);
      if (!_in)
        return false;
      buffer[8] = 0;
      erh.offset += 2 * std::atoi(buffer);
      erh.skip += 2 * std::atoi(buffer);
    }
    erh.skip += 2 * erh.block_count;
    for (size_t ch = chan; ch < _header.num_channels; ch++) {
      _in.seekg(256 + _header.num_channels * 216 + ch * 8);
      _in.read(buffer, 8);
      if (!_in)
        return false;
      buffer[8] = 0;
      erh.skip += 2 * std::atoi(buffer);
    }

    erh.rate = (_header.duration!=0) ? erh.block_count/_header.duration : 0;
    _row_header.push_back(erh);
  }
  return true;
}

size_t EdfRowReader::Impl::read(size_t idx, EdfRow &row)
{
  row.sample.clear();
  for(size_t chan=0; chan<_row_header.size(); ++chan) {
    _in.seekg(_row_header[chan].offset+_row_header[chan].block_count*idx);
    char bf[2];
    std::vector<double> signal;
    for (int i = 0; i < _row_header[chan].block_count; ++i) {
      /* Read raw sample from input file (samples are stored
       in little-endian format) and calculate corresponding
       physical value. */
      _in.read(bf, 2);
      if (!_in) {
        return 0;
      }
      signal.push_back(_row_header[chan].c0 + _row_header[chan].c1 * ((bf[1] << 8) | bf[0]));
    }
    row.sample.push_back(signal);
  }
  if(row.sample.empty())
    return 0;
  /* Read samples. */
  return row.sample.size();
}

bool EdfRowReader::Impl::isOpen() const
{
  return _in.is_open();
}

void EdfRowReader::Impl::close()
{
  _in.close();
}

const EdfHeader &EdfRowReader::Impl::header() const
{
  return _header;
}

const EdfRowHeader &EdfRowReader::Impl::rowHeader(size_t idx) const
{
  return _row_header[idx];
}

size_t EdfRowReader::Impl::size() const
{
  return _header.size;
}

void EdfRowReader::Impl::getTime(size_t idx, boost::posix_time::ptime &t) const
{
  t = _header.start_time;
  t += boost::posix_time::seconds(idx*_header.duration);
}

size_t EdfRowReader::Impl::BlockSize(std::istream& in, size_t num_blocks, size_t idx)
{
  char buffer[9];
  in.seekg(256 + num_blocks * 216 + idx * 8);
  in.read(buffer, 8);
  if (!in)
    return false;
  buffer[8] = 0;
  return std::atoi(buffer);
}

double EdfRowReader::Impl::PhisicalLow(std::istream &in, size_t num_blocks, size_t idx)
{
  char buffer[9];
  in.seekg(256 + num_blocks * 104 + idx * 8);
  in.read(buffer, 8);
  if (!in)
    return false;
  buffer[8] = 0;
  return std::atoi(buffer);
}

double EdfRowReader::Impl::PhisicalHigh(std::istream &in, size_t num_blocks, size_t idx)
{
  char buffer[9];
  in.seekg(256 + num_blocks * 112 + idx * 8);
  in.read(buffer, 8);
  if (!in)
    return false;
  buffer[8] = 0;
  return std::atoi(buffer);
}

EdfRowReader::EdfRowReader() : _impl(new Impl()) {}


bool EdfRowReader::open(const char *const file_name)
{
  return _impl->open(file_name);
}

size_t EdfRowReader::read(size_t idx, EdfRow &row)
{
  return _impl->read(idx, row);
}

bool EdfRowReader::isOpen() const
{
  return _impl->isOpen();
}

size_t EdfRowReader::size() const
{
  return _impl->size();
}

void EdfRowReader::getTime(size_t sec, boost::posix_time::ptime &t) const
{
  _impl->getTime(sec, t);
}

void EdfRowReader::close()
{
  _impl->close();
}

const EdfHeader &EdfRowReader::header() const
{
  return _impl->header();
}

const EdfRowHeader& EdfRowReader::rowHeader(size_t idx) const
{
  return _impl->rowHeader(idx);
}
} //namespace edf
