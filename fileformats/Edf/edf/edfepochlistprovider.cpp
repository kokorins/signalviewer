#include "edfepochlistprovider.h"
#include "edffiles.h"
#include "edffilescanner.h"
#include <QtPlugin>

namespace edf {
EdfEpochListProvider::EdfEpochListProvider(QObject *parent) : QObject(parent) {}

boost::shared_ptr<epoch::IEpochFiles> EdfEpochListProvider::createFiles() const
{
  return boost::shared_ptr<epoch::IEpochFiles>(new EdfFiles());
}

boost::shared_ptr<epoch::IDataFileScanner> EdfEpochListProvider::createScanner() const
{
  return boost::shared_ptr<epoch::IDataFileScanner>(new EdfFileScanner());
}

//std::string EdfEpochListProvider::properties(const std::string& prev_prop, void*) const
//{
//  return prev_prop;
//}

std::wstring EdfEpochListProvider::name() const
{
  return L"Edf";
}

std::wstring EdfEpochListProvider::extFilter() const
{
  return L"*.edf";
}
std::wstring EdfEpochListProvider::author() const
{
  return L"Sergey Kokorin";
}

std::wstring EdfEpochListProvider::version() const
{
  return L"0.4";
}

} //namespace edf
//Q_EXPORT_PLUGIN2(edfplugin, edf::EdfEpochListProvider)
