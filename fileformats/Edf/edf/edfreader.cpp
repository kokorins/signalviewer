#include "edfreader.h"
#include "edfrow.h"
#include "edfrowreader.h"

namespace edf {
EdfReader::EdfReader() : _impl(new EdfRowReader()), _idx(0) {}

bool EdfReader::open(const char *const file_name)
{
  return _impl->open(file_name);
}

std::vector<double> EdfReader::read(size_t st_sec, size_t dur_sec)
{
  std::vector<double> data;
  EdfRow row;
  size_t dur = _impl->header().duration;
  std::div_t st_qr = std::div((int)st_sec, (int)dur);
  std::div_t en_qr = std::div((int)(st_sec + dur_sec), (int)dur);
  for(int i = st_qr.quot; i<=en_qr.quot; ++i) {
    if(!_impl->read(i, row))
      return data;
    double rate = _impl->rowHeader(_idx).rate;
    const std::vector<double> &s = row.sample[_idx];
    size_t beg = ((i==st_qr.quot) ?(size_t)std::floor(st_qr.rem*rate):0);
    size_t end = ((i==en_qr.quot) ?(size_t)std::floor(en_qr.rem*rate):s.size());
    std::copy(s.begin() + beg, s.begin() + end, std::back_inserter(data));
  }
  return data;
}

bool EdfReader::isOpen() const
{
  return _impl->isOpen();
}

size_t EdfReader::size() const
{
  return _impl->size();
}

size_t EdfReader::sizeSec() const
{
  return _impl->size()*_impl->header().duration;
}

void EdfReader::getTime(size_t sec, boost::posix_time::ptime &t) const
{
  _impl->getTime(sec, t);
}

void EdfReader::close()
{
  _impl->close();
}

void EdfReader::setIdx(size_t idx)
{
  _idx = idx;
}

size_t EdfReader::idx() const
{
  return _idx;
}

size_t EdfReader::numChannels() const
{
  return _impl->header().num_channels;
}
} //namespace edf
