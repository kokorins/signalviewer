#include "edffilescanner.h"
#include "edfrowreader.h"
#include <interfaces/fileformats/epochfileinfo.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

namespace edf {
namespace fs = boost::filesystem;
epoch::DataFileInfo EdfFileScanner::handle(const fs::path &file) const
{
  epoch::DataFileInfo res;
  res.is_ok = false;
  if(!fs::is_regular_file(file))
    return res;
  if(fs::extension(file)!=std::string(".edf"))
    return res;
  res.file_name = file.filename().generic_string();
  edf::EdfRowReader err;
  if(!err.open(file.generic_string().c_str()))
    return res;
  const edf::EdfHeader& header = err.header();
  res.subj = header.patient;
  for(size_t i=0; i<header.num_channels; ++i) {
    const edf::EdfRowHeader& rh = err.rowHeader(i);
    std::string channel = rh.label;
    boost::algorithm::to_upper(channel);
    res.channels.push_back(channel);
    res.start_times.push_back(header.start_time);
  }
  err.close();
  res.is_ok = true;
  return res;
}

std::string EdfFileScanner::info(const fs::path &file, int idx) const
{
  if(!fs::is_regular_file(file))
    return "Non regular file";
  edf::EdfRowReader err;
  if(!err.open(file.generic_string().c_str()))
    return "Cant open it";
  std::stringstream ss;
  const edf::EdfHeader& h = err.header();
  ss<<"file: "<<file.generic_string()<<std::endl;
  ss<<"duration: "<<h.duration<<std::endl;
  ss<<"num channels: "<<h.num_channels<<std::endl;
  ss<<"num points: "<<h.num_points<<std::endl;
  ss<<"patient: "<<h.patient<<std::endl;
  ss<<"recording: "<<h.recording<<std::endl;
  ss<<"size: "<<h.size<<std::endl;
  ss<<"start time: "<<h.start_time<<std::endl;
  if(idx<0 || idx>=(int)h.num_channels) {
    ss<<"Now such channel id:"<<idx;
    ss.str();
  }
  ss<<"Channel "<<idx<<":"<<std::endl;
  const edf::EdfRowHeader& rh = err.rowHeader(idx);
  ss<<"block count: "<<rh.block_count<<std::endl;
  ss<<"offset: "<<rh.c0<<" factor: "<<rh.c1<<std::endl;
  ss<<"digitals: ["<<rh.digital_low<<", "<<rh.digital_high<<"]"<<std::endl;
  ss<<"physical: ["<<rh.physical_low<<", "<<rh.physical_high<<"]"<<std::endl;
  ss<<"label: "<<rh.label<<std::endl;
  ss<<"num points: "<<rh.num_point<<std::endl;
  ss<<"prefiltering: "<<rh.prefiltering<<std::endl;
  ss<<"rate: "<<rh.rate<<std::endl;
  ss<<"transducer: "<<rh.transducer<<std::endl;
  ss<<"unit: "<<rh.unit<<std::endl;
  return ss.str();
}

void EdfFileScanner::properties(const epoch::EpochFileInfo &, void *, std::vector<epoch::EpochFileInfo> &) const
{
}

std::string EdfFileScanner::defaultProperties(const epoch::EpochFileInfo &) const
{
  return "";
}
} // namespace edf
