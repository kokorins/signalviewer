#ifndef EDFROW_H_
#define EDFROW_H_
#include <string>
#include <vector>
/// \filename edfrow.h
namespace edf {
/**
 * \brief This class represents set of sampled data values.
 */
class EdfRow {
public:
  std::vector<std::vector<double> > sample;
};
}// namespace edfhandler
#endif /* EDFSIGNAL_H_ */
