#ifndef EDFROWREADER_H_
#define EDFROWREADER_H_
#include <string>
#include <cstddef>
#include <boost/shared_ptr.hpp>
#include <boost/date_time.hpp>
/**
 * \filename edfrowreader.h
 * edf readers namespace
 */
namespace edf {
class EdfRow;
/**
 * \brief class represents the main header of Edf file
 */
struct EdfHeader {
  //Patient information.
  std::string patient;
  //Recording information.
  std::string recording;
  ///start time of data acquisition
  boost::posix_time::ptime start_time;
  //Number of bytes in input file header.
  int offset;
  //Number of data records in input file.
  size_t size;
  //Duration of data records (in seconds).
  size_t duration;
  //Number of signals recorded in input file.
  size_t num_channels;
  //Last sample index.
  size_t num_points;
};

/**
 * \brief class represents the header of Edf file for special signal in it
 */
struct EdfRowHeader {
  ///Signal label.
  std::string label;
  //Signal transducer type.
  std::string transducer;
  //Signal physical dimension.
  double unit;
  //Signal physical minimum and maximum.
  double physical_low, physical_high;
  //Signal digital minimum and maximum.
  double digital_low, digital_high;
  // Prefiltering indicator.
  std::string prefiltering;
  // Number of samples in each data record.
  int block_count;
  // Coefficients from conversion from digital to physical signal value.
  double c0, c1;
  // Offset of first signal sample in input file.
  size_t offset;
  // Number of bytes to skip between two successive signal data records.
  int skip;
  double rate;
  size_t num_point;
};

/**
 * \brief Edf file format reader. It chanks it in a blocks according to
 * internal structure.
 */
class EdfRowReader {
public:
  EdfRowReader();
  bool open(char const*const file_name);
  size_t read(size_t idx, EdfRow& row);
  bool isOpen()const;
  size_t size()const;
  void getTime(size_t sec, boost::posix_time::ptime& t)const;
  void close();
  const EdfHeader& header()const;
  const EdfRowHeader& rowHeader(size_t idx)const;
private:
  class Impl;
  boost::shared_ptr<Impl> _impl;
};
}//namespace edf
#endif /* EDFROWREADER_H_ */
