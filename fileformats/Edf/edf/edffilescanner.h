#ifndef EDFFILESCANNER_H
#define EDFFILESCANNER_H
#include <interfaces/fileformats/datafilescanner.h>
#include <boost/date_time.hpp>
#include <string>
namespace edf {
class EdfFileScanner : public epoch::IDataFileScanner {
public:
  virtual epoch::DataFileInfo handle(const boost::filesystem::path&file) const;
  virtual std::string info(const boost::filesystem::path& file, int idx)const;
  virtual void properties(const epoch::EpochFileInfo &, void *, std::vector<epoch::EpochFileInfo>&) const;
  std::string defaultProperties(const epoch::EpochFileInfo &cur_info)const;
};
} //namespace edf

#endif // EDFFILESCANNER_H
